<?php

use App\Http\Controllers\MalaysiaReportController;
use App\Http\Controllers\MilashashipManagementController;
use App\Http\Controllers\MilashashipManagementDocumentController;
use App\Http\Controllers\SingaporeController;
use App\Http\Controllers\SmcseafarerRecrordController;
use App\Http\Controllers\RmesFormController;
use App\Http\Controllers\SeafarerMadicalCertificateController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SigninController;
use App\Http\Controllers\SingapurFormController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.sign_in');
});
require __DIR__.'/auth.php';

Route::middleware(['guest'])->group(function() {
    Route::get('/dashboard/search', [SigninController::class, 'guest_dashboard'])->name('guest_dashboard');
    Route::get('search/smcseafarerRecord/index', [SmcseafarerRecrordController::class, 'index'])->name('smcseafarer_recrord.index.guest');
    Route::get('search/rmes_form/all_form', [RmesFormController::class, 'allForms'])->name('rmes_form.allForm.guest');
    Route::get('search/smCertificate/index', [SeafarerMadicalCertificateController::class, 'index'])->name('smCertificate.index.guest');
    Route::get('search/mshipmanagement/index', [MilashashipManagementController::class, 'index'])->name('milashaship_management.index.guest');
    Route::get('search/pfrmeofseafarer/form/index', [\App\Http\Controllers\PfrmeofseafarersController::class, 'index'])->name('pfrmeofseafarer.index.guest');
    Route::get('search/mcfsatsea/index', [\App\Http\Controllers\McfsatseaController::class, 'index'])->name('mcfsatsea.index.search');
    Route::get('search/liberiapers/index', [\App\Http\Controllers\LiberiaperController::class, 'index'])->name('liberiapers.index.search');
    Route::get('search/belgium/index', [\App\Http\Controllers\BelgiumController::class, 'index'])->name('belgium.index.search');
    Route::get('search/malaysia/index', [\App\Http\Controllers\MalaysiaController::class, 'index'])->name('malaysia.index.search');
    Route::get('search/belize/index', [\App\Http\Controllers\BelizeController::class, 'index'])->name('belize.index.search');
    Route::get('search/marshall_islands/index', [\App\Http\Controllers\MarshallIslandController::class, 'index'])->name('marshall-islands.index.search');
    Route::get('search/halul_offshore/index', [\App\Http\Controllers\HaluloffshoreController::class, 'index'])->name('halul-offshore.index.search');
    Route::get('search/mcfps_board/index', [\App\Http\Controllers\McfpsboardController::class, 'index'])->name('mcfps-board.index.search');
});

Route::middleware(['auth'])->group(function () {
    //dashboard
    Route::get('/dashboard', [SigninController::class, 'dashboard'])->name('dashboard');
    // Forms Route Start
    Route::get('rmes_form/page1/create', [RmesFormController::class, 'create'])->name('form_page1.create');
    Route::get('rmes_form/{form}/page2/page2_create', [RmesFormController::class, 'page2_create'])->name('form_page2.create');
    Route::get('rmes_form/{form}/page3/page3_create', [RmesFormController::class, 'page3_create'])->name('form_page3.create');
    Route::get('rmes_form/{form}/page4/page4_create', [RmesFormController::class, 'page4_create'])->name('form_page4.create');
    Route::post('rmes_form/page1/store', [RmesFormController::class, 'store'])->name('form_page1.store');
    Route::post('rmes_form/{form}/page1/update', [RmesFormController::class, 'page1_update'])->name('form_page1.update');
    Route::post('rmes_form/{form}/page2/store', [RmesFormController::class, 'page2_store'])->name('form_page2.store');
    Route::post('rmes_form/{form}/page3/store', [RmesFormController::class, 'page3_store'])->name('form_page3.store');
    Route::post('rmes_form/{form}/page4/store', [RmesFormController::class, 'page4_store'])->name('form_page4.store');
    Route::get('rmes_form/all_form', [RmesFormController::class, 'allForms'])->name('rmes_form.allForm');
    //pages edit
    Route::get('rmes_form/{form}/page1/edit', [RmesFormController::class, 'editPage1'])->name('rmes_form.editPage1');
    Route::get('rmes_form/form/{form}/download', [RmesFormController::class, 'download'])->name('rmes_form.download');
    Route::delete('rmes_form/form/{form}/delete', [RmesFormController::class, 'delete'])->name('rmes_form.delete');

    //singapore form is related to rmses_form
    Route::get('singapore_form/{form}/create', [SingaporeController::class, 'create'])->name('singapore-form.create');
    Route::post('singapore_form/{form}/store', [SingaporeController::class, 'store'])->name('singapore-form.store');
    Route::get('singapore_form/{form}/edit', [SingaporeController::class, 'edit'])->name('singapore-form.edit');
    Route::post('singapore_form/{form}/update', [SingaporeController::class, 'update'])->name('singapore-form.update');



    //2nd form
    Route::get('smCertificate/index', [SeafarerMadicalCertificateController::class, 'index'])->name('smCertificate.index');
    Route::get('smCertificate/form/create', [SeafarerMadicalCertificateController::class, 'create'])->name('smCertificate.create');
    Route::post('smCertificate/store', [SeafarerMadicalCertificateController::class, 'store'])->name('smCertificate.store');
    Route::get('smCertificate/{smCertificate}/edit', [SeafarerMadicalCertificateController::class, 'edit'])->name('smCertificate.edit');
    Route::post('smCertificate/{smCertificate}/update', [SeafarerMadicalCertificateController::class, 'update'])->name('smCertificate.update');
    Route::delete('smCertificate/{smCertificate}/delete', [SeafarerMadicalCertificateController::class, 'delete'])->name('smCertificate.delete');
    Route::get('smCertificate/{smCertificate}/download', [SeafarerMadicalCertificateController::class, 'download'])->name('smCertificate.download');

    //Smc Seafarers records
    Route::get('smcseafarerRecord/index', [SmcseafarerRecrordController::class, 'index'])->name('smcseafarer_recrord.index');
    Route::get('smcseafarerRecord/page1/create', [SmcseafarerRecrordController::class, 'page1_create'])->name('smcseafarer_recrord.page1_create');
    Route::get('smcseafarerRecord/{form}/page2/create', [SmcseafarerRecrordController::class, 'page2_create'])->name('smcseafarer_recrord.page2_create');
    Route::get('smcseafarerRecord/{form}/page3/create', [SmcseafarerRecrordController::class, 'page3_create'])->name('smcseafarer_recrord.page3_create');
    Route::get('smcseafarerRecord/{form}/page4/create', [SmcseafarerRecrordController::class, 'page4_create'])->name('smcseafarer_recrord.page4_create');
    Route::post('smcseafarerRecord/page1/store', [SmcseafarerRecrordController::class, 'page1_store'])->name('smcseafarer_recrord.page1_store');
    Route::post('smcseafarerRecord/{form}/page1/update', [SmcseafarerRecrordController::class, 'page1_update'])->name('smcseafarer_recrord.page1_update');
    Route::post('smcseafarerRecord/{form}/page2/store', [SmcseafarerRecrordController::class, 'page2_store'])->name('smcseafarer_recrord.page2_store');
    Route::post('smcseafarerRecord/{form}/page3/store', [SmcseafarerRecrordController::class, 'page3_store'])->name('smcseafarer_recrord.page3_store');
    Route::post('smcseafarerRecord/{form}/page4/store', [SmcseafarerRecrordController::class, 'page4_store'])->name('smcseafarer_recrord.page4_store');

    // Smc Seafarers records pages Edit
    Route::get('smcseafarerRecord/{form}/page1/edit', [SmcseafarerRecrordController::class, 'page1_edit'])->name('smcseafarer_recrord.page1_edit');
    Route::delete('smcseafarerRecord/{form}/delete', [SmcseafarerRecrordController::class, 'delete'])->name('smcseafarer_recrord.delete');
    Route::get('smcseafarerRecord/{form}/download', [SmcseafarerRecrordController::class, 'download'])->name('smcseafarer_recrord.download');

    //Milaha ship management forms
    Route::get('mshipmanagement/index', [MilashashipManagementController::class, 'index'])->name('milashaship_management.index');
    Route::get('mshipmanagement/page1/create', [MilashashipManagementController::class, 'page1_create'])->name('milashaship_management.page1_create');
    Route::get('mshipmanagement/{form}/page2/create', [MilashashipManagementController::class, 'page2_create'])->name('milashaship_management.page2_create');
    Route::get('mshipmanagement/{form}/page3/create', [MilashashipManagementController::class, 'page3_create'])->name('milashaship_management.page3_create');
    Route::get('mshipmanagement/{form}/page4/create', [MilashashipManagementController::class, 'page4_create'])->name('milashaship_management.page4_create');
    Route::post('mshipmanagement/page1/store', [MilashashipManagementController::class, 'page1_store'])->name('milashaship_management.page1_store');
    Route::post('mshipmanagement/{form}/page2/store', [MilashashipManagementController::class, 'page2_store'])->name('milashaship_management.page2_store');
    Route::post('mshipmanagement/{form}/page3/store', [MilashashipManagementController::class, 'page3_store'])->name('milashaship_management.page3_store');
    Route::post('mshipmanagement/{form}/page4/store', [MilashashipManagementController::class, 'page4_store'])->name('milashaship_management.page4_store');


    //milaha ship edit
    Route::get('mshipmanagement/{form}/page1/edit', [MilashashipManagementController::class, 'page1_edit'])->name('milashaship_management.page1_edit');
    Route::post('mshipmanagement/{form}/page1/update', [MilashashipManagementController::class, 'page1_update'])->name('milashaship_management.page1_update');
    Route::delete('mshipmanagement/{form}/delete', [MilashashipManagementController::class, 'delete'])->name('milashaship_management.delete');
    Route::get('mshipmanagement/{form}/download', [MilashashipManagementController::class, 'download'])->name('milashaship_management.download');

    //MILAHA SHIP MANAGEMENT CONFIDENTIAL DOCUMENT FORM
    Route::get('mshipmanagement/{form}/document/page1/create', [MilashashipManagementDocumentController::class, 'page1_create'] )->name('milashaship_menagement_document.page1_create');
    Route::get('mshipmanagement/{form}/document/page1/edit', [MilashashipManagementDocumentController::class, 'edit'])->name('milashaship_menagement_document.edit');
    Route::post('mshipmanagement/{form}/document/page1/store', [MilashashipManagementDocumentController::class, 'store'])->name('milashaship_menagement_document.store');
    Route::post('mshipmanagement/{form}/document/page1/update', [MilashashipManagementDocumentController::class, 'update'])->name('milashaship_menagement_document.update');

    //medical certificate for personnel service on board republic of panama (Physical test)
    Route::get('pfrmeofseafarer/form/index', [\App\Http\Controllers\PfrmeofseafarersController::class, 'index'])->name('pfrmeofseafarer.index');
    Route::get('pfrmeofseafarer/form/page1/create', [\App\Http\Controllers\PfrmeofseafarersController::class, 'page1_create'])->name('pfrmeofseafarer.page1_create');
    Route::get('pfrmeofseafarer/{form}/page2/create', [\App\Http\Controllers\PfrmeofseafarersController::class, 'page2_create'])->name('pfrmeofseafarer.page2_create');
    Route::get('pfrmeofseafarer/{form}/page3/create', [\App\Http\Controllers\PfrmeofseafarersController::class, 'page3_create'])->name('pfrmeofseafarer.page3_create');
    Route::get('pfrmeofseafarer/{form}/page4/create', [\App\Http\Controllers\PfrmeofseafarersController::class, 'page4_create'])->name('pfrmeofseafarer.page4_create');
    Route::post('pfrmeofseafarer/form/page1/store', [\App\Http\Controllers\PfrmeofseafarersController::class, 'page1_store'])->name('pfrmeofseafarer.page1_store');
    Route::post('pfrmeofseafarer/{form}/page2/store', [\App\Http\Controllers\PfrmeofseafarersController::class, 'page2_store'])->name('pfrmeofseafarer.page2_store');
    Route::post('pfrmeofseafarer/{form}/page3/store', [\App\Http\Controllers\PfrmeofseafarersController::class, 'page3_store'])->name('pfrmeofseafarer.page3_store');
    Route::post('pfrmeofseafarer/{form}/page4/store', [\App\Http\Controllers\PfrmeofseafarersController::class, 'page4_store'])->name('pfrmeofseafarer.page4_store');

    //pages editA
    Route::get('pfrmeofseafarer/{form}/page1/edit', [\App\Http\Controllers\PfrmeofseafarersController::class, 'page1_edit'])->name('pfrmeofseafarer.page1_edit');
    Route::post('pfrmeofseafarer/{form}/page1/update', [\App\Http\Controllers\PfrmeofseafarersController::class, 'page1_update'])->name('pfrmeofseafarer.page1_update');
    Route::delete('pfrmeofseafarer/{form}/delete', [\App\Http\Controllers\PfrmeofseafarersController::class, 'delete'])->name('pfrmeofseafarer.delete');

    //medical certificate for personnel service on board republic of panama (certificate)
    Route::get('mcfpservice_panama/{form}/create', [\App\Http\Controllers\McfpservicePanamaController::class, 'create'])->name('mcfpservice_panama.create');
    Route::post('mcfpservice_panama/{form}/ceritficate/store', [\App\Http\Controllers\McfpservicePanamaController::class, 'store'])->name('mcfpservice_panama.store');
    //edit
    Route::get('mcfpservice_panama/{form}/edit', [\App\Http\Controllers\McfpservicePanamaController::class, 'edit'])->name('mcfpservice_panama.edit');
    Route::post('mcfpservice_panama/{form}/edit', [\App\Http\Controllers\McfpservicePanamaController::class, 'update'])->name('mcfpservice_panama.update');
    //download
    Route::get('pfrmeofseafarer/{form}/download', [\App\Http\Controllers\PfrmeofseafarersController::class, 'download'])->name('pfrmeofseafarer.download');

    //MEDICAL CDERTIFICATE FOR SEVICE AT SEA
    Route::get('mcfsatsea/index', [\App\Http\Controllers\McfsatseaController::class, 'index'])->name('mcfsatsea.index');
    Route::get('mcfsatsea/create', [\App\Http\Controllers\McfsatseaController::class, 'create'])->name('mcfsatsea.create');
    Route::post('mcfsatsea/store', [\App\Http\Controllers\McfsatseaController::class, 'store'])->name('mcfsatsea.store');

    //edit
    Route::get('mcfsatsea/{form}/edit', [\App\Http\Controllers\McfsatseaController::class, 'edit'])->name('mcfsatsea.edit');
    Route::post('mcfsatsea/{form}/update', [\App\Http\Controllers\McfsatseaController::class, 'update'])->name('mcfsatsea.update');
    Route::delete('mcfsatsea/{form}/update', [\App\Http\Controllers\McfsatseaController::class, 'delete'])->name('mcfsatsea.delete');
    //download of mcfsatsea
    Route::get('mcfstsea/{form}/downlaod', [\App\Http\Controllers\McfsatseaController::class, 'download'])->name('mcfsatsea.download');

    //Physical examination report, Deputy commisoner of maritime affairs the republic of liberia
    Route::get('liberiapers/index', [\App\Http\Controllers\LiberiaperController::class, 'index'])->name('liberiapers.index');
    Route::get('liberiapers/create', [\App\Http\Controllers\LiberiaperController::class, 'create'])->name('liberiapers.create');
    Route::post('liberiapers/store', [\App\Http\Controllers\LiberiaperController::class, 'store'])->name('liberiapers.store');
    //edit
    Route::get('liberiapers/{form}/edit', [\App\Http\Controllers\LiberiaperController::class, 'edit'])->name('liberiapers.edit');
    //update
    Route::post('liberiapers/{form}/update', [\App\Http\Controllers\LiberiaperController::class, 'update'])->name('liberiapers.update');
    //delete
    Route::delete('liberiapers/{form}/delete', [\App\Http\Controllers\LiberiaperController::class, 'delete'])->name('liberiapers.delete');
    Route::get('liberiapers/{form}/download', [\App\Http\Controllers\LiberiaperController::class, 'download'])->name('liberiapers.download');

    //from for belgium
    Route::get('belgium/index', [\App\Http\Controllers\BelgiumController::class, 'index'])->name('belgium.index');
    Route::get('belgium/create', [\App\Http\Controllers\BelgiumController::class, 'create'])->name('belgium.create');
    Route::post('belgium/store', [\App\Http\Controllers\BelgiumController::class, 'store'])->name('belgium.store');
    Route::get('belgium/{form}/edit', [\App\Http\Controllers\BelgiumController::class, 'edit'])->name('belgium.edit');
    Route::post('belgium/{form}/update', [\App\Http\Controllers\BelgiumController::class, 'update'])->name('belgium.update');
    Route::delete('belgium/{form}/delete', [\App\Http\Controllers\BelgiumController::class, 'delete'])->name('belgium.delete');

    Route::get('belgium/{form}/download', [\App\Http\Controllers\BelgiumController::class, 'download'])->name('belgium.download');

    //form for malaysia
    Route::get('malaysia/index', [\App\Http\Controllers\MalaysiaController::class, 'index'])->name('malaysia.index');
    Route::get('malaysia/create', [\App\Http\Controllers\MalaysiaController::class, 'create'])->name('malaysia.create');
    Route::post('malaysia/store', [\App\Http\Controllers\MalaysiaController::class, 'store'])->name('malaysia.store');
    Route::get('malaysia/{form}/edit', [\App\Http\Controllers\MalaysiaController::class, 'edit'])->name('malaysia.edit');
    Route::post('malaysia/{form}/update', [\App\Http\Controllers\MalaysiaController::class, 'update'])->name('malaysia.update');
    Route::delete('malaysia/{form}/delete', [\App\Http\Controllers\MalaysiaController::class, 'delete'])->name('malaysia.delete');
    Route::get('malaysia/{form}/download', [\App\Http\Controllers\MalaysiaController::class, 'download'])->name('malaysia.download');

    //form for malaysia_report
    Route::get('malaysia_report/{form}/create', [MalaysiaReportController::class, 'create'])->name('malaysia_report.create');
    Route::post('malaysia_report/{form}/store', [MalaysiaReportController::class, 'store'])->name('malaysia_report.store');
    Route::get('malaysia_report/{form}/edit', [MalaysiaReportController::class, 'edit'])->name('malaysia_report.edit');
    Route::post('malaysia_report/{form}/update', [MalaysiaReportController::class, 'update'])->name('malaysia_report.update');

    //form for belize
    Route::get('belize/index', [\App\Http\Controllers\BelizeController::class, 'index'])->name('belize.index');
    Route::get('belize/create', [\App\Http\Controllers\BelizeController::class, 'create'])->name('belize.create');
    Route::post('belize/store', [\App\Http\Controllers\BelizeController::class, 'store'])->name('belize.store');
    Route::get('belize/{form}/edit', [\App\Http\Controllers\BelizeController::class, 'edit'])->name('belize.edit');
    Route::post('belize/{form}/update', [\App\Http\Controllers\BelizeController::class, 'update'])->name('belize.update');
    Route::delete('belize/{form}/delete', [\App\Http\Controllers\BelizeController::class, 'delete'])->name('belize.delete');
    Route::get('belize/{form}/download', [\App\Http\Controllers\BelizeController::class, 'download'])->name('belize.download');

   //form for MarshalIlands
    Route::get('marshall_islands/index', [\App\Http\Controllers\MarshallIslandController::class, 'index'])->name('marshall-islands.index');
    Route::get('marshall_islands/create', [\App\Http\Controllers\MarshallIslandController::class, 'create'])->name('marshall-island.create');
    Route::post('marshall_islands/store', [\App\Http\Controllers\MarshallIslandController::class, 'store'])->name('marshall-island.store');
    Route::get('marshall_islands/{form}/edit', [\App\Http\Controllers\MarshallIslandController::class, 'edit'])->name('marshall-island.edit');
    Route::post('marshall_islands/{form}/update', [\App\Http\Controllers\MarshallIslandController::class, 'update'])->name('marshall-island.update');
    Route::delete('marshall_islands/{form}/delete', [\App\Http\Controllers\MarshallIslandController::class, 'delete'])->name('marshall-island.delete');
    Route::get('marshall_islands/{form}/download', [\App\Http\Controllers\MarshallIslandController::class, 'download'])->name('marshall-island.download');

    //Halul offshore
    Route::get('halul_offshore/index', [\App\Http\Controllers\HaluloffshoreController::class, 'index'])->name('halul-offshore.index');
    Route::get('halul_offshore/create', [\App\Http\Controllers\HaluloffshoreController::class, 'create'])->name('halul-offshore.create');
    Route::post('halul_offshore/store', [\App\Http\Controllers\HaluloffshoreController::class, 'store'])->name('halul-offshore.store');
    Route::get('halul_offshore/{form}/edit', [\App\Http\Controllers\HaluloffshoreController::class, 'edit'])->name('halul-offshore.edit');
    Route::post('halul_offshore/{form}/update', [\App\Http\Controllers\HaluloffshoreController::class, 'update'])->name('halul-offshore.update');
    Route::delete('halul_offshore/{form}/delete', [\App\Http\Controllers\HaluloffshoreController::class, 'delete'])->name('halul-offshore.delete');
    Route::get('halul_offshore/{form}/download', [\App\Http\Controllers\HaluloffshoreController::class, 'download'])->name('halul-offshore.download');

    // McfPsonBoard
    Route::get('mcfps_board/index', [\App\Http\Controllers\McfpsboardController::class, 'index'])->name('mcfps-board.index');
    Route::get('mcfps_board/create', [\App\Http\Controllers\McfpsboardController::class, 'create'])->name('mcfps-board.create');
    Route::post('mcfps_board/store', [\App\Http\Controllers\McfpsboardController::class, 'store'])->name('mcfps-board.store');
    Route::get('mcfps_board/{form}/edit', [\App\Http\Controllers\McfpsboardController::class, 'edit'])->name('mcfps-board.edit');
    Route::post('mcfps_board/{form}/update', [\App\Http\Controllers\McfpsboardController::class, 'update'])->name('mcfps-board.update');
    Route::get('mcfps_board/{form}/download', [\App\Http\Controllers\McfpsboardController::class, 'download'])->name('mcfps-board.download');
    Route::delete('mcfps_board/{form}/delete', [\App\Http\Controllers\McfpsboardController::class, 'delete'])->name('mcfps-board.delete');
});

