@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <div style="height: 100px;" class="row justify-content-center">
        <div class="col-lg-6">
            <div style="font-weight: bold; font-size: 1.5rem; text-transform: capitalize" class="text-center">Seafarer Medical Certificate</div>
        </div>
    </div>
    <form method="post" action="{{ route('singapore-form.update', $form->id)}}">
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="form-label" for="name">Name:</label>
                                <input  type="text" name="name" class="form-control" id="name" value="{{ old('seafarers_name', $form->seafarers_name ?? '')}}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="nationality">Nationality:</label>
                    <input class="form-control" type="text" name="nationality" id="nationality" value="{{ old('nationality', $form->nationality ?? '') }}" disabled>
                </div>
                <div class="form-group">
                    <label class="form-label" for="dob" ><span class="text-danger">*</span>Date of Birth:(DD/MM/YYYY):</label>
                    <input type="date" name="date_of_birth" class="form-control" id="dob" value="{{ old('date_of_birth', $form->date_of_birth ?? '') }}" disabled>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="gender" ><span class="text-danger">*</span>Gender:(Male/Female):</label>
                    <select class="form-control" name="gender" id="gender" disabled>
                        <option>Select option</option>
                        <option value="Male" {{ $form->gender == 'Male'? 'selected' : '' }}>Male</option>
                        <option value="Female" {{ $form->gender == 'Female'? 'selected' : '' }}>Female</option>
                        <option value="Others" {{ $form->gender == 'Others'? 'selected' : '' }}>Others</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label" for="place_of_birth">Place of birth:</label>
                    <input class="form-control" type="text" name="place_of_birth" id="place_of_birth" value="{{ old('place_of_birth',  $form->place_of_birth ?? '')  }}" disabled>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="text-center">
                DECLAPATION 0F THE RECOGNIZED MEDICAL PRACTITIONER :
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="confirmation">1. Confirmation that identification documents were checked at the point of examination:</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="confirmation" value="1" {{ $form->certificate->confirmation == '1' ? 'checked' : '' }} >
                    <label style="font-size: 20px;" for="confirmation">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="confirmation" value="0" {{ $form->certificate->confirmation == '0' ? 'checked' : '' }}>
                    <label for="pregnancy" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="hearing_standard">2.Hearing meets the standards in section A-1 / 9 ?</label>
                    <br>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="hearing_standard" name="hearing_standard" value="1" {{ $form->certificate->hearing_standard == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="hearing_standard">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="hearing_standard" name="hearing_standard" value="0" {{ $form->certificate->hearing_standard == '0' ? 'checked' : '' }}>
                    <label for="hearing_standard" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="unaided_statisfactory">3.Unaided hearing stisfactory?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="unaided_statisfactory" name="unaided_statisfactory" value="1" {{ $form->certificate->unaided_statisfactory == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="unaided_statisfactory">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="unaided_statisfactory" name="unaided_statisfactory" value="0" {{ $form->certificate->unaided_statisfactory == '0' ? 'checked' : '' }}>
                    <label for="unaided_statisfactory" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="visual_acutity">4.Visual acutity meets standards in section A-1 / 9 ?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="visual_acutity" name="visual_acutity" value="1"  {{ $form->certificate->visual_acutity == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="visual_acutity">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="visual_acutity" value="0"  {{ $form->certificate->visual_acutity == '0' ? 'checked' : '' }}>
                    <label for="visual_acutity" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_vision_standard">5.Colour vision meets standards in section A-1 / 9 ?</label>
                    <br>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="color_vision_standard" name="color_vision_standard" value="1" {{ $form->certificate->color_vision_standard == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="color_vision_standard">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="color_vision_standard" name="color_vision_standard" value="0" {{ $form->certificate->color_vision_standard == '0' ? 'checked' : '' }}>
                    <label for="color_vision_standard" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_vision_test">Date of last colour vision test</label>
                    <input type="date" class="form-control" id="color_vision_test" name="color_vision_test" value="{{ old('color_vision_test', $form->certificate->color_vision_test ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="fit_lookout">6.Fit for lookoutduties?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="fit_lookout" value="1" {{ $form->certificate->fit_lookout == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="fit_lookout">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="fit_lookout" value="0" {{ $form->certificate->fit_lookout == '0' ? 'checked' : '' }}>
                    <label for="fit_lookout" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="unfit_for_service">7.Is the seafarer free from any medical condition likely to be aggravated by service at sea or the render the seafarer unfit for service or the render the health of any other persons on board ?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="visual_acutity" name="unfit_for_service" value="1" {{ $form->certificate->unfit_for_service == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="unfit_for_service">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="unfit_for_service" value="0" {{ $form->certificate->unfit_for_service == '0' ? 'checked' : '' }}>
                    <label for="unfit_for_service" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="any_restrictions">8.NO limitations or restictions on fitness ?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="any_restrictions" value="1" {{ $form->certificate->any_restrictions == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="any_restrictions">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="any_restrictions" value="0" {{ $form->certificate->any_restrictions == '0' ? 'checked' : '' }}>
                    <label for="any_restrictions" style="font-size: 20px;">No</label>
                    <br>
                    <label style="font-size: 20px;" for="restrinctions_des">lf NO, specify limitations or restrictions</label>
                </div>
                <div class="form-group">
                    <textarea class="form-control" type="text" id="restrinctions_des" name="restrinctions_des">{{ old('restrinctions_des', $form->certificate->restrinctions_des ?? '') }}</textarea>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="form-label" for="doe">9.Date of examination/Issue(DD/MM/YYY)</label>
                    <input class="form-control" type="date" name="doe" id="doe" value="{{ old('doe', $form->certificate->doe ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="doexpiry">10.Date of expiry(DD/MM/YYY)</label>
                    <input class="form-control" type="date" name="doexpiry" id="doexpiry" value="{{ old('doexpiry', $form->certificate->doexpiry ?? '') }}">
                    <small>No more than 2 years from the date of examination.</small>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="mb-3">
                <input type="submit" class="btn btn-primary" name="submit" value="Save">
            </div>
        </div>
    </form>
@endsection
