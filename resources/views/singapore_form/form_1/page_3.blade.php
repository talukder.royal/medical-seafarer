@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    <div class="row justify-content-center">
        @include('includes.flash-message')
        <div class="col-lg-6 offset-lg-1" style="font-weight: bold; font-size: 20px;">MEDICAL EXAMINATIONS OF SEAFARER</div>
    </div></br>
    <form method="POST" action="{{ route('form_page3.store', $form->id) }}">
        @csrf
        <p style="font-size: 20px;">Part B- Result of medical examinations</p>
        <div class="row">
            <div class="col-lg-6">
                <p>Eyesight</p>
                <div class="form-group">
                    <label class="form-label" for="contact_lenses">Use of glasses of contact lenses:</label>
                    <input type="checkbox" style="width: 20px;
    height: 20px;" id="contact_lenses" name="contact_lenses" value="0" {{ $form->contact_lenses == '0' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="contact_lenses">No</label>
                    <input type="checkbox" style="width: 20px;
    height: 20px;" id="contact_lenses" name="contact_lenses" value="1" {{ $form->contact_lenses == 1 ? 'checked' : '' }}>
                    <label for="contact_lenses" style="font-size: 20px;">Yes</label>
                </div>
            </div>

            <div class="col-lg-6">
                <div style="height: 45px">
                </div>
                <div>
                    <label style="font-size: 20px;" for="lense_type">Type</label>
                    <input type="text" class="form-control"  id="lense_type" name="lense_type"  value="{{ old('lense_type', $form->lense_type ?? '') }}">
                </div>

                <div>
                    <label style="font-size: 20px;" for="purpose">Purpose</label>
                    <input type="text" class="form-control"  id="purpose" name="purpose" value="{{ old('lense_type', $form->purpose ?? '') }}">
                </div>
            </div>
        </div>
        <p class="pt-2 pb-2">Visual Acuity</p>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <table>
                        <tr>
                            <th colspan="3">
                                Unaided
                            </th>
                        </tr>

                        <tr>
                            <td class="pr-3 pl-3">Label</td>
                            <td class="pr-3 pl-3">Right eye</td>
                            <td class="pr-3 pl-3">Left eye</td>
                            <td class="pr-3 pl-3">Binocular</td>
                        </tr>

                        <tr>
                            <td>Distant</td>
                            <td class="pr-3 pl-3 m-1" ><input style="width: 100%; border: 0;" type="text" name="right_eye_distant" value="{{ old('lense_type', $form->right_eye_distant ?? '') }}"></td>
                            <td class="pr-3 pl-3"><input style="width: 100%; border: 0;" type="text"  name="left_eye_distant" value="{{ old('lense_type', $form->left_eye_distant ?? '') }}"></td>
                            <td class="pr-3 pl-3"><input style="width: 100%; border: 0;" type="text"  name="binocular_eye_distant" value="{{ old('lense_type', $form->binocular_eye_distant ?? '') }}"></td>
                        </tr>

                        <tr>
                            <td>Near</td>
                            <td class="pr-3 pl-3 m-1" ><input style="width: 100%; border: 0;" type="text" name="right_eye_near" value="{{ old('lense_type', $form->right_eye_near ?? '') }}"></td>
                            <td class="pr-3 pl-3"><input style="width: 100%; border: 0;" type="text"  name="left_eye_near" value="{{ old('lense_type', $form->left_eye_near ?? '') }}"></td>
                            <td class="pr-3 pl-3"><input style="width: 100%; border: 0;" type="text"  name="binocular_eye_near" value="{{ old('lense_type', $form->binocular_eye_near ?? '') }}"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <table>
                        <tr>
                            <th colspan="3">
                                Aided
                            </th>
                        </tr>

                        <tr>
                            <td class="pr-3 pl-3">Label</td>
                            <td class="pr-3 pl-3">Right eye</td>
                            <td class="pr-3 pl-3">Left eye</td>
                            <td class="pr-3 pl-3">Binocular</td>
                        </tr>

                        <tr>
                            <td>Distant</td>
                            <td class="pr-3 pl-3 m-1" ><input style="width: 100%; border: 0;" type="text" name="aided_right_eye_distant" value="{{ old('lense_type', $form->aided_right_eye_distant ?? '') }}"></td>
                            <td class="pr-3 pl-3"><input style="width: 100%; border: 0;" type="text"  name="aided_left_eye_distant" value="{{ old('lense_type', $form->aided_left_eye_distant ?? '') }}"></td>
                            <td class="pr-3 pl-3"><input style="width: 100%; border: 0;" type="text"  name="aided_binocular_eye_distant" value="{{ old('lense_type', $form->aided_binocular_eye_distant ?? '') }}"></td>
                        </tr>

                        <tr>
                            <td>Near</td>
                            <td class="pr-3 pl-3 m-1" ><input style="width: 100%; border: 0;" type="text" name="aided_right_eye_near" value="{{ old('lense_type', $form->aided_right_eye_near ?? '') }}"></td>
                            <td class="pr-3 pl-3"><input style="width: 100%; border: 0;" type="text"  name="aided_left_eye_near" value="{{ old('lense_type', $form->aided_left_eye_near ?? '') }}"></td>
                            <td class="pr-3 pl-3"><input style="width: 100%; border: 0;" type="text"  name="aided_binocular_eye_near" value="{{ old('lense_type', $form->aided_binocular_eye_near ?? '') }}"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <p style="font-size: 20px;">Seafarer's Declarations (please tick)</p>
            <div class="row">
                <div class="col-lg-6">
                    <p>Visual fields</p>
                    <div class="form-group">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Normal</th>
                                <th scope="col">Defective</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Right Eye</th>
                                <td>
                                    <input type="checkbox" style="width: 25px; height: 20px; name="right_visual_field" name="visual_normal" value="1" {{ $form->visual_normal == 1 ? 'checked' : '' }}>
                                </td>
                                <td>
                                    <input type="checkbox" style="width: 25px; height: 20px; name="right_visual_field"   name="visual_normal" value="0" {{ $form->visual_normal == '0' ? 'checked' : '' }}>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Left Eye</th>
                                <td>
                                    <input type="checkbox" style="width: 25px; height: 20px; name="left_visual_field" name="visual_defective" value="1" {{ $form->visual_defective == 1 ? 'checked' : '' }}>
                                </td>
                                <td>
                                    <input type="checkbox" style="width: 25px; height: 20px; name="left_visual_field" name="visual_defective" value="0" {{ $form->visual_defective == '0' ? 'checked' : '' }}>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <p>Color Vision(please tick)</p>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="not_tested" name="color_vision" value="0" {{ $form->color_vision == '0' ? 'checked' : '' }}>
                        <label class="form-label" for="not_tested">Not Tested</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="normal" name="color_vision" value="1" {{ $form->color_vision == 1 ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="normal">Normal</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="normal" name="color_vision" value="2" {{ $form->color_vision == 2 ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="normal">Doubtful</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="normal" name="color_vision" value="3" {{ $form->color_vision == 3 ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="normal">Defective</label>
                    </div>
                    <div class="form-group">
                        <p>Hearing</p>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col" colspan="5">Pure tone adn audiometry (threshold values in dB) </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td>500HZ</td>
                                <td>1000HZ</td>
                                <td>2000HZ</td>
                                <td>3000HZ</td>
                            </tr>
                            <tr>
                                <th scope="row">Right ear</th>
                                <td><input style="width: 100%; border: 0;" type="text"  name="right_ear_500_hz" placeholder="10" value="{{ old('lense_type', $form->right_ear_500_hz ?? '') }}"></td>
                                <td><input style="width: 100%; border: 0;" type="text"  name="right_ear_1000_hz" placeholder="10" value="{{ old('lense_type', $form->right_ear_1000_hz ?? '') }}"></td>
                                <td><input style="width: 100%; border: 0;" type="text"  name="right_ear_2000_hz" placeholder="10" value="{{ old('lense_type', $form->right_ear_2000_hz ?? '') }}"></td>
                                <td><input style="width: 100%; border: 0;" type="text"  name="right_ear_3000_hz" placeholder="10" value="{{ old('lense_type', $form->right_ear_3000_hz ?? '') }}"></td>
                            </tr>
                            <tr>
                                <th scope="row">Left ear</th>
                                <td><input style="width: 100%; border: 0;" type="text"  name="left_ear_500_hz" placeholder="10" value="{{ old('lense_type', $form->left_ear_500_hz ?? '') }}"></td>
                                <td><input style="width: 100%; border: 0;" type="text"  name="left_ear_1000_hz" placeholder="10" value="{{ old('lense_type', $form->left_ear_1000_hz ?? '') }}"></td>
                                <td><input style="width: 100%; border: 0;" type="text"  name="left_ear_2000_hz" placeholder="10" value="{{ old('lense_type', $form->left_ear_2000_hz ?? '') }}"></td>
                                <td><input style="width: 100%; border: 0;" type="text"  name="left_ear_3000_hz" placeholder="10" value="{{ old('lense_type', $form->left_ear_3000_hz ?? '') }}"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <p>Speech and whisper test (metres)</p>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Normal</th>
                                <th scope="col">Whisper</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Right ear</th>
                                <td><input style="width: 100%; border: 0;" type="text"  name="right_ear_normal" value="{{ old('lense_type', $form->right_ear_normal ?? '') }}"></td>
                                <td><input style="width: 100%; border: 0;" type="text"  name="right_ear_whisper" value="{{ old('lense_type', $form->right_ear_whisper ?? '') }}"></td>
                            </tr>
                            <tr>
                                <th scope="row">Left ear</th>
                                <td><input style="width: 100%; border: 0;" type="text"  name="left_ear_normal" value="{{ old('lense_type', $form->left_ear_normal ?? '') }}"></td>
                                <td><input style="width: 100%; border: 0;" type="text"  name="left_ear_whisper" value="{{ old('lense_type', $form->left_ear_whisper ?? '') }}"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <p>Clinial Findings</p>
                        <table class="table">
                            <tbody>
                            <tr>
                                <th scope="row">Height (cm)</th>
                                <td class="p-0"><input style="border:0; width: 100%" type="text" name="clinical_height" value="{{ old('lense_type', $form->clinical_height ?? '') }}"></td>
                                <th scope="row">Weight(kg)</th>
                                <td class="p-0"><input style="border:0; width: 100%" type="text" name="clinical_weight" value="{{ old('lense_type', $form->clinical_weight ?? '') }}"></td>
                            </tr>
                            <tr>
                                <th scope="row">Pulse rate (per minute)</th>
                                <td class="p-0"><input style="border:0; width: 100%" type="text" name="clinical_pulse_rate" value="{{ old('lense_type', $form->clinical_pulse_rate ?? '') }}"></td>
                                <th scope="row">Rhythm</th>
                                <td class="p-0"><input style="border:0; width: 100%" type="text" name="clinical_rhythm" value="{{ old('lense_type', $form->clinical_rhythm ?? '') }}"></td>
                            </tr>
                            <tr>
                                <th scope="row">Blood Pressure Systolic (mm Hg)</th>
                                <td class="p-0"><input style="border:0; width: 100%" type="text" name="clinical_b_pressure" value="{{ old('lense_type', $form->clinical_b_pressure ?? '') }}"></td>
                                <th scope="row">Diastolic (mm Hg)</th>
                                <td class="p-0"><input style="border:0; width: 100%" type="text" name="clinical_diastolic" value="{{ old('lense_type', $form->clinical_diastolic ?? '') }}"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group d-flex justify-content-center">
                    <label class="form-label">
                        Urinalysis:
                    </label>
                    <label class for="urinalysis_glucose">Glucose:</label>
                    <input type="text" class="form-control m-1" id="urinalysis_glucose" name="urinalysis_glucose" value="{{ old('urinalysis_glucose', $form->urinalysis_glucose ?? '' ) }}">
                    <label class for="urinalysis_protin">Protein:</label>
                    <input type="text" class="form-control m-1"  id="urinalysis_protin" name="urinalysis_protin" value="{{ old('urinalysis_protin', $form->urinalysis_protin ?? '' ) }}">
                </div>
                <div class="d-flex justify-content-between">
                    <div class="mb-3">
                        <a href="{{ route('form_page2.create', $form->id) }}" class="btn btn-primary" >Previous</a>
                    </div>

                    <div class="mb-3">
                        <input type="submit" class="btn btn-primary" name="submit" value="Save & Next">
                    </div>
                </div>
            </div>
    </form>
@endsection
