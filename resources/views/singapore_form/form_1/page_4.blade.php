@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    <div class="row justify-content-center">
        @include('includes.flash-message')
        <div class="col-lg-6 offset-lg-1" style="font-weight: bold; font-size: 20px;">MEDICAL EXAMINATIONS OF SEAFARER</div>
    </div></br>
    <form method="POST" action="{{ route('form_page4.store', $form->id) }}">
        @csrf
        <p style="font-size: 20px;">Clinical Findings</p>
        <div class="row">
            <div class="col-lg-6">

                <div class="form-group">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="row"></th>
                            <th>Normal</th>
                            <th>Abnormal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Head</th>
                            <td class="p-0 text-center" ><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="head" value="1" {{ $form->head == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="head" value="0" {{ $form->head == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Sinus, nose, throat</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="sinus_nose_throat" value="1" {{ $form->sinus_nose_throat == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="sinus_nose_throat" value="0" {{ $form->sinus_nose_throat == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Mouth/teeth</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="mouth_teeth" value="1" {{ $form->mouth_teeth == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="mouth_teeth" value="0" {{ $form->mouth_teeth == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Ears (general)</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="ears" value="1" {{ $form->ears == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="ears" value="0" {{ $form->ears == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Tympanic membrane</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="tympanic_member" value="1" {{ $form->tympanic_member == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="tympanic_member" value="0" {{ $form->tympanic_member == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Eyes</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="eyes" value="1" {{ $form->eyes == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="eyes" value="0" {{ $form->eyes == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Ophthalmoscopy</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="ophthalmoscopy" value="1" {{ $form->ophthalmoscopy == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="ophthalmoscopy" value="0" {{ $form->ophthalmoscopy == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Pupils</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="pupils" value="1" {{ $form->pupils == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="pupils" value="0" {{ $form->pupils == '0' ? 'checked' : '' }}></td>
                        </tr>

                        <tr>
                            <th scope="row">Eye movement</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="eye_environment" value="1" {{ $form->eye_environment == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="eye_environment" value="0" {{ $form->eye_environment == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Lungs and chest</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="lungs_chest" value="1" {{ $form->lungs_chest == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="lungs_chest" value="0" {{ $form->lungs_chest == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Breast examination</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="breast_examination" value="1" {{ $form->breast_examination == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="breast_examination" value="0" {{ $form->breast_examination == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Heart</th>
                            <td class="p-0 text-center" ><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="heart" value="1" {{ $form->heart == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="heart" value="0" {{ $form->heart == '0' ? 'checked' : '' }}></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="row"></th>
                            <th>Normal</th>
                            <th>Abnormal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Skin</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="skin" value="1" {{ $form->skin == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="skin" value="0" {{ $form->skin == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Varicose Vein</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="varicose_vein" value="1" {{ $form->varicose_vein == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="varicose_vein" value="0" {{ $form->varicose_vein == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Vascular (inc. pedal pulse)</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="vascular_pulse" value="1" {{ $form->vascular_pulse == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="vascular_pulse" value="0" {{ $form->vascular_pulse == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Abdomen and viscera</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="abdomen_viscera" value="1" {{ $form->abdomen_viscera == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="abdomen_viscera" value="0" {{ $form->abdomen_viscera == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Hernia</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="hernia" value="1" {{ $form->hernia == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="hernia" value="0" {{ $form->hernia == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Anus (not rectal exam)</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="anus" value="1" {{ $form->anus == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="anus" value="0" {{ $form->anus == '0' ? 'checked' : '' }}></td>
                        </tr>

                        <tr>
                            <th scope="row">G-U system</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="g_u_system" value="1" {{ $form->g_u_system == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="g_u_system" value="0" {{ $form->g_u_system == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Upper and lower extremities</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="upper_lower_ext" value="1" {{ $form->upper_lower_ext == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="upper_lower_ext" value="0" {{ $form->upper_lower_ext == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Spine (C/s, T/S, L/S)</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="spine" value="1" {{ $form->spine == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="spine" value="0" {{ $form->spine == '0' ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Neurologic (full/brief)</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="neurologic" value="1" {{ $form->neurologic == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="neurologic" value="0" {{ $form->neurologic == '0' ? 'checked' : '' }}></td>
                        </tr>

                        <tr>
                            <th scope="row">Psychiatric</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="psychiatric" value="1" {{ $form->psychiatric == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="psychiatric" value="0" {{ $form->psychiatric == '0' ? 'checked' : '' }}></td>
                        </tr>

                        <tr>
                            <th scope="row">General appearance</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="general_appearance" value="1" {{ $form->general_appearance == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="general_appearance" value="0" {{ $form->general_appearance == '0' ? 'checked' : '' }}></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <p class="pt-2 pb-2">Chest X-ray</p>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="chest_xray" name="chest_xray" value="0" {{ $form->chest_xray == '0' ? 'checked' : '' }}>
                    <label class="form-label" for="chest_xray">Not performed</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="chest_xray" name="chest_xray" value="1" {{ $form->chest_xray == '0' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="chest_xray">Performed on</label>
                    <div class="d-inline">
                        <input type="date" name="date_of_performed_on" class="form-control d-inline" id="date_of_performed_on" value="{{ old('date_of_performed_on', $form->date_of_performed_on ?? '') }}">
                    </div>
                    <label  style="font-size: 20px;">Results</label>
                    <div class="d-inline">
                        <input type="text" name="date_of_results_on" class="form-control d-inline" id="date_of_results_on"  value="{{ old('date_of_results_on', $form->date_of_results_on ?? '') }}">
                    </div>
                </div>
                <div class="form-group">
                    <p>Other diagnostic test(s) and result(s):</p>
                    <label class="form-label d-inline" for="d-test">Test:</label>
                    <input  type="text" name="d_test" class="border-0 m-4"  value="{{ old('d_test', $form->d_test ?? '') }}">
                    <div class="form-group">
                        <label class="form-label" for="d-result">Results:</label>
                        <input type="text" name="d_result" class="border-0" value="{{ old('d_result', $form->d_test ?? '') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <p>Medical practitioner’s comments and assessment of fitness, with reasons for any limitations.</p>
                    <h3 class="text-center" style="color: #95a2af">Fit For Duty on board Ship</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <h3 style="color:#95a2af">Assessment of fitness for service at sea (please tick)</h3>
            <p>On the basis of the seafarer’s personal declaration, my clinical examination and diagnostic test results recorded above, I declare the seafarer medically:</p>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_for_lookout_duty" name="fit_for_lookout_duty" value="1" {{ $form->fit_for_lookout_duty == 1 ? 'checked' : '' }}>
                    <label class="form-label" for="fit_for_lookout_duty">Fit for look out duty</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="unfit_for_lookout_duty" name="fit_for_lookout_duty" value="0" {{ $form->fit_for_lookout_duty == '0' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="unfit_for_lookout_duty">Unfit for lookout duty</label>
                </div>

                <div class="form-group">
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="visual_aid" name="visual_aid" value="1" {{ $form->visual_aid == 1 ? 'checked' : '' }}>
                    <label class="form-label" for="visual_aid">Visual aid required</label>
                    <input type="checkbox" style="width: 25px;height: 20px;" id="not_visual_aid" name="visual_aid" value="0" {{ $form->visual_aid == '0' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="not_visual_aid">Visual aid not required</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <table class="table p-lg-0">
                        <thead>
                        <tr>
                            <th scope="row"></th>
                            <th>Deck Service</th>
                            <th>Engine Service</th>
                            <th>Catering Service</th>
                            <th>Other Service</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Fit</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="fit_desk" value="1" {{ $form->fit_desk == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="fit_engine" value="1" {{ $form->fit_engine == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="fit_catering" value="1" {{ $form->fit_catering == 1 ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="fit_other" value="1" {{ $form->fit_other == 1 ? 'checked' : '' }}></td>
                        </tr>
                        <tr>
                            <th scope="row">Unfit</th>
                            <td class="p-0 text-center"><input style="border:0; width: 25px; height: 20px;" type="checkbox" name="fit_desk" value="0" {{ $form->fit_desk == '0' ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="fit_engine" value="0" {{ $form->fit_engine == '0' ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="fit_catering" value="0" {{ $form->fit_catering == '0' ? 'checked' : '' }}></td>
                            <td class="p-0 text-center"><input style="border:0;  width: 25px; height: 20px;" type="checkbox" name="fit_other" value="0" {{ $form->fit_other == '0' ? 'checked' : '' }}></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <input style="width: 25px; height: 20px;" type="checkbox" name="with_restrictions" id="with_restrictions" value="1" {{ $form->with_restrictions == 1 ? 'checked' : '' }}>
                    <label style="font-size:20px" for="with_restrictions"> Without restrictions</label>
                    <input  style="width: 25px; height: 20px;" type="checkbox" name="with_restrictions" id="with_restrictions" value="0" {{ $form->with_restrictions == '0' ? 'checked' : '' }}>
                    <label style="font-size:20px" for="without_restrictions"> With restrictions</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="form-label" for="description_restrictions">Description of restrictions
                        (e.g. specific position, type of ship, trading area etc.)</label>
                    <textarea
                        class="form-control"
                        name="description_restrictions"
                        id="description_restrictions"
                        placeholder="Description of restrictions
                         (e.g. specific position, type of ship, trading area etc.)"
                    >
                        {{ $form->description_restrictions ?? '' }}
                    </textarea
                </div>
                <div class="d-flex justify-content-between">
                    <div class="mb-3">
                        <a href="{{ route('form_page3.create', $form->id) }}" class="btn btn-primary" >Previous</a>
                    </div>

                    <div class="mb-3">
                        <input type="submit" class="btn btn-primary" name="submit" value="Save">
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
