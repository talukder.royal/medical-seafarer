<!DOCTYPE html>
<html>
<head>
    <title>One</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assert('./public/css/all.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />


</head>
<body>
<div class="all">
    <div class="annex_all m_b_3">
        <div class="row_one">
            <p class="part_b_result">Part B- Result of medical examinations</p>
            <div class="eye">
                <p><b>Eyesight</b></p>
                <p>Use of glasses of contact lenses</p>
            </div>
            <div class="eye_yes_no">
                <p>
                    @if($form->contact_lenses == 0)
                        <i class="far fa-check-square"></i>
                    @else
                        <i class="far fa-square"></i>
                    @endif
                        No
                </p>
                <div class="eye_type">
                    <div class="type_yes s_float">
                        @if($form->contact_lenses == 1)
                            <i class="far fa-check-square"></i>
                        @else
                            <i class="far fa-square"></i>
                        @endif
                            Yes
                    </div>
                    <div class="type_text_row s_float">
                        <div class="type_text s_float">
                            Type
                        </div>
                        <div class="type_text_value s_float">
                            <p>{{ $form->lense_type }}</p>
                        </div>
                    </div>
                    <div class="type_purpose_row s_float">
                        <div class="purpose_text s_float">
                            Purpose
                        </div>
                        <div class="purpose_text_value s_float">
                            <p>{{ $form->purpose }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>	<!-- row one end -->

        <div class="row_two clear v_acuity">
            <p class="va">Visual Acuity</p>
            <div class="visual_table">
                <div class="unaided s_float">
                    <div class="unaided_row">
                        <p>Unaided</p>
                    </div>
                    <div class="unaided_row">
                        <div class="un_row s_float">
                            Right eye
                        </div>
                        <div class="un_row s_float">
                            left eye
                        </div>
                        <div class="un_row s_float" style="border: none;">
                            Binocular
                        </div>
                    </div>
                    <div class="unaided_row">
                        <div class="un_row s_float">
                            Distant: 6/6 {{ $form->right_eye_distant}}
                        </div>
                        <div class="un_row s_float">
                            6/6 {{ $form->left_eye_distant }}
                        </div>
                        <div class="un_row s_float" style="border: none;">
                            {{ $form->binocular_eye_distant }}
                        </div>
                    </div>
                    <div class="unaided_row" style="border: none;">
                        <div class="un_row s_float">
                            Near: 6/6 {{ $form->right_eye_near}}
                        </div>
                        <div class="un_row s_float">
                            6/6 {{ $form->left_eye_near}}
                        </div>
                        <div class="un_row s_float" style="border: none;">
                            {{ $form->binocular_eye_near}}
                        </div>
                    </div>

                </div>
                <div class="aided s_float">
                    <div class="aided_row">
                        <p>Aided</p>
                    </div>
                    <div class="unaided_row">
                        <div class="un_row s_float">
                            Right eye
                        </div>
                        <div class="un_row s_float">
                            left eye
                        </div>
                        <div class="un_row s_float" style="border: none">
                            Binocular
                        </div>
                    </div>
                    <div class="unaided_row">
                        <div class="un_row s_float">
                            Distant: {{ $form->aided_right_eye_distant }}
                        </div>
                        <div class="un_row s_float">
                            {{ $form->aided_left_eye_distant }}
                        </div>
                        <div class="un_row s_float" style="border: none;">
                            {{ $form->aided_binocular_eye_distant }}
                        </div>
                    </div>
                    <div class="unaided_row" style="border: none;">
                        <div class="un_row s_float">
                            Near: {{ $form->aided_right_eye_near }}
                        </div>
                        <div class="un_row s_float">
                            {{ $form->aided_left_eye_near }}
                        </div>
                        <div class="un_row s_float" style="border: none;">
                            {{ $form->aided_binocular_eye_near }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row two end -->
        <div class="row_three clear">
            <p class="v_field">Visual fields</p>
            <div class="v_field_table">
                <div class="v_field_row">
                    <div class="v_f_col_one s_float">

                    </div>
                    <div class="v_f_col_two s_float">
                        Normal
                    </div>
                    <div class="v_f_col_three s_float">
                        Defective
                    </div>
                </div>
                <div class="v_field_row">
                    <div class="v_f_col_one s_float">
                        Right Eye:
                    </div>
                    <div class="v_f_col_two s_float">
                        @if($form->visual_normal == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="v_f_col_three s_float">
                        @if($form->visual_normal == 0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="v_field_row" style="border: none;">
                    <div class="v_f_col_one s_float">
                        Left Eye:
                    </div>
                    <div class="v_f_col_two s_float">
                        @if($form->visual_normal == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="v_f_col_three s_float">
                        @if($form->visual_normal == 0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- row three end -->
        <div class="row_four">
            <p class="color_vision">Color Vision (Please tick)</p>
            <div class="color_v">
                <div class="not_t s_float">
                    @if($form->color_vision == 0)
                        <i class="far fa-check-square"></i>
                    @else
                        <i class="far fa-check-square"></i>
                    @endif
                        Not tested
                </div>
                <div class="not_t s_float">
                    @if($form->color_vision == 1)
                        <i class="far fa-check-square"></i>
                    @else
                        <i class="far fa-square"></i>
                    @endif
                    Normal
                </div>

                <div class="not_t s_float">
                    @if($form->color_vision == 2)
                        <i class="far fa-check-square"></i>
                    @else
                        <i class="far fa-square"></i>
                    @endif
                    Doubtful
                </div>

                <div class="not_t s_float">
                    @if($form->color_vision == 3)
                        <i class="far fa-check-square"></i>
                    @else
                        <i class="far fa-square"></i>
                    @endif
                    Defective
                </div>

            </div>
        </div>
        <!-- row four end -->

        <div class="row_five clear">
            <p class="haeaing_t">Hearing</p>
            <div class="hearing_table">
                <div class="heading_row">
                    <p class="pure_text"><b>Pure tone adn audiometry </b>(threshold values in dB)</p>
                </div>
                <div class="heading_row">
                    <div class="h_col s_float">

                    </div>
                    <div class="h_col s_float">
                        500 Hz
                    </div>
                    <div class="h_col s_float">
                        1000 Hz
                    </div>
                    <div class="h_col s_float">
                        2000 Hz
                    </div>
                    <div class="h_col s_float" style="border: none">
                        3000 Hz
                    </div>
                </div>
                <div class="heading_row">
                    <div class="h_col s_float">
                        Right ear
                    </div>
                    <div class="h_col s_float">
                        {{ $form->right_ear_500_hz }} dB
                    </div>
                    <div class="h_col s_float">
                        {{ $form->right_ear_1000_hz }} dB
                    </div>
                    <div class="h_col s_float">
                        {{ $form->right_ear_2000_hz }} dB
                    </div>
                    <div class="h_col s_float" style="border: none">
                        {{ $form->right_ear_3000_hz }} dB
                    </div>
                </div>
                <div class="heading_row" style="border: none;">
                    <div class="h_col s_float">
                        Left ear
                    </div>
                    <div class="h_col s_float">
                        {{ $form->left_ear_500_hz }} dB
                    </div>
                    <div class="h_col s_float">
                        {{ $form->left_ear_1000_hz }} dB
                    </div>
                    <div class="h_col s_float">
                        {{ $form->left_ear_2000_hz }} dB
                    </div>
                    <div class="h_col s_float" style="border: none">
                        {{ $form->left_ear_3000_hz }} dB
                    </div>
                </div>

            </div>
        </div>
        <!-- row five end -->
        <div class="row_six spc_row clear">
            <p class="speech_text">Speech and whisper test (metres)</p>
            <div class="speech_table">
                <div class="st_row">
                    <div class="sp_col s_float">

                    </div>
                    <div class="sp_col s_float">
                        Normal
                    </div>
                    <div class="sp_col s_float" style="border: none;">
                        Whisper
                    </div>
                </div>
                <div class="st_row">
                    <div class="sp_col s_float">
                        Right ear
                    </div>
                    <div class="sp_col s_float">
                        {{ $form->right_ear_normal }}.Mtr
                    </div>
                    <div class="sp_col s_float" style="border: none;">
                        {{ $form->right_ear_whisper }}.Mtr
                    </div>
                </div>
                <div class="st_row" style="border:none;">
                    <div class="sp_col s_float">
                        Left ear
                    </div>
                    <div class="sp_col s_float">
                        {{ $form->left_ear_normal }}.Mtr
                    </div>
                    <div class="sp_col s_float" style="border: none;">
                        {{ $form->left_ear_whisper }}.Mtr
                    </div>
                </div>
            </div>
        </div>
        <!-- row six end -->
        <div class="row_seven clear">
            <p class="clinial_text">Clinial Findings</p>
            <div class="clinial_table">
                <div class="cl_row">
                    <div class="cl_col_one s_float">
                        Height (cm)
                    </div>
                    <div class="cl_col_two s_float s_float">
                        {{ $form->clinical_height }}
                    </div>
                    <div class="cl_col_three s_float">
                        Weight (Kg)
                    </div>
                    <div class="cl_col_four s_float">
                        {{ $form->clinical_weight }}
                    </div>
                </div>
                <div class="cl_row">
                    <div class="cl_col_one s_float">
                        Pulse rate (per minute)
                    </div>
                    <div class="cl_col_two s_float s_float">
                        {{ $form->clinical_pulse_rate }}
                    </div>
                    <div class="cl_col_three s_float">
                        Rhythm							</div>
                    <div class="cl_col_four s_float">
                        {{ $form->clinical_rhythm }}
                    </div>
                </div>
                <div class="cl_row">
                    <div class="cl_col_one s_float">
                        Blood Pressure Systolic (mm Hg)
                    </div>
                    <div class="cl_col_two s_float s_float">
                        {{ $form->clinical_b_pressure }} mmHg
                    </div>
                    <div class="cl_col_three s_float">
                        Diastolic (mm Hg)
                    </div>
                    <div class="cl_col_four s_float">
                        {{ $form->clinical_diastolic }} mmHg
                    </div>
                </div>
                <div class="cl_row" style="border: none;">
                    <div class="cl_col_one_1 s_float">
                        <div class="urin s_float">
                            Urinalysis:
                        </div>
                        <div class="glucose s_float">
                            Glucose: {{ $form->urinalysis_glucose }}
                        </div>
                    </div>
                    <div class="cl_col_two_2 s_float s_float">
                        Protin: {{ $form->urinalysis_protin }}
                    </div>
                    <div class="cl_col_three_3 s_float">
                        Blood: {{ $form->urinalysis_blood }}
                    </div>

                </div>
            </div>
        </div>
        <!-- row six end -->
        <div class="row_seven">
            <div class="part_b_last_table">
                <div class="part_b_table_row">
                    <div  class="part_b_tb_col_1 s_float">
                        one
                    </div>
                    <div  class="part_b_tb_col_2 s_float">
                        <b>Normal</b>
                    </div>
                    <div  class="part_b_tb_col_3 s_float">
                        <b>Abnormal</b>
                    </div>
                </div>
                <div class="part_b_table_row">
                    <div  class="part_b_tb_col_1 s_float">
                        Head
                    </div>
                    <div  class="part_b_tb_col_2 s_float">
                        @if($form->head == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                       @endif
                    </div>
                    <div  class="part_b_tb_col_3 s_float">
                        @if($form->head == 0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="part_b_table_row">
                    <div  class="part_b_tb_col_1 s_float">
                        Sinus, nose, throat
                    </div>
                    <div  class="part_b_tb_col_2 s_float">
                        @if($form->sinus_nose_throat == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div  class="part_b_tb_col_3 s_float">
                        @if($form->sinus_nose_throat == 0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="part_b_table_row">
                    <div  class="part_b_tb_col_1 s_float">
                        Mouth/teeth
                    </div>
                    <div  class="part_b_tb_col_2 s_float">
                        @if($form->mouth_teeth == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div  class="part_b_tb_col_3 s_float">
                        @if($form->mouth_teeth == 0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <div class=" b_m_footer_m_3">
            <p class="m_foot_1">Page 1 of 5</p>
            <p class="m_foot_2">RECORD OF MEDICAL EXAMINTAION OF SEAFARERS - September 2021</p>
        </div>
    </div>

</div>
</body>
</html>
