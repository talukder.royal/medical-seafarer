<!DOCTYPE html>
<html>
<head>
    <title>one</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />


</head>
<body>
<div class="all">
    <div class="annex_all">
        <div class="row_one">
            <div class="additional_table_two">
                <div class="add_table_row">
                    <div class="add_no s_float">

                    </div>
                    <div  class="add_table_text s_float">
                        <b>Additional questions</b>
                    </div>
                    <div class="ad_yes s_float">
                        Yes
                    </div>
                    <div class="ad_no s_float">
                        No
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        35.
                    </div>
                    <div  class="add_table_text s_float">
                        Have you ever been signed off as sick or repatriated from a ship?
                    </div>
                    <div class="ad_yes s_float">
                       @if($form->signed_off_sick == 1)
                            <i class="fas fa-check" aria-hidden="true"></i>
                       @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->signed_off_sick == 0)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        36.
                    </div>
                    <div  class="add_table_text s_float">
                        Have you ever been hospitalized?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->hospitalized == 1)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->hospitalized == 0)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        37.
                    </div>
                    <div  class="add_table_text s_float">
                        Have you ever been declared unfit for sea duty?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->declared_for_sea_duty == 1)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->declared_for_sea_duty == 0)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        38.
                    </div>
                    <div  class="add_table_text s_float">
                        Has your medical certificate even been restricted or revoked?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->medical_certificate_revoked == 1)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->medical_certificate_revoked == 0)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        39.
                    </div>
                    <div  class="add_table_text s_float">
                        Are you aware that you have any medical problems, diseases or illnesses?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->medical_problems == 1)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->medical_problems == 0)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        40.
                    </div>
                    <div  class="add_table_text s_float">
                        Do you feel healthy and fit to perform the duties of your designated position/occupation?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->feel_healthy == 1)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->feel_healthy == 0)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        41.
                    </div>
                    <div  class="add_table_text s_float">
                        Are you allergic to any medication?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->allergic_medication == 1)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->allergic_medication == 0)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row" style="border-bottom: none;">
                    <div class="add_no s_float">
                        42.
                    </div>
                    <div  class="add_table_text s_float">
                        Are you using any non-prescription or prescription medication?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->prescription_medication == 1)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->prescription_medication == 0)
                            <i class="fas fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>

            </div>
        </div>
        <div class="row_two clear">
            <div class="if_answer">
                If you answer “yes”, please list the medications taken, the purpose(s) and the dosage:
                    {{ $form-> provide_details}}
            </div>
        </div>
        <div>
            <p class="declare_text">I hereby declare that the personal declaration above is a true statement to the best of my knowledge.</p>
        </div>

        <div class="row_three clear">
            <div class="date_sign">
                <div class=" s_float b_date">
                    <p class="date_value2">05-Jan-2021</p>
                    <p class="date">Date</p>
                </div>
                <div class="s_float b_sign">
                    <p class="signiture">Signature of Seafarer</p>
                </div>
                <div class=" s_float b_wit">
                    <p class="name_singiture">Name and Signature of Witness</p>
                </div>
            </div>
        </div>
        <div class="row_four dr_row clear">
            <p class="auth_text">I hereby authorize the release of all my previous medical records (including my last Seafarer Medical Certificate) from any health professional, health institutions and public authorities to<br> Dr. <span class="dr_field">SABRINA MOSTAPA</span></p>
        </div>

        <div class="row_three clear">
            <div class="date_sign b_date">
                <div class=" s_float">
                    <p class="date_value2">05-Jan-2021</p>
                    <p class="date">Date</p>
                </div>
                <div class="s_float b_sign">
                    <p class="signiture">Signature of Seafarer</p>
                </div>
                <div class=" s_float b_wit">
                    <p class="name_singiture">Name and Signature of Witness</p>
                </div>
            </div>
        </div>
        <div class=" b_m_footer">
            <p class="m_foot_1">Page 1 of 5</p>
            <p class="m_foot_2">RECORD OF MEDICAL EXAMINTAION OF SEAFARERS - March 2020</p>
        </div>
    </div>

</div>


</body>
</html>
