<!DOCTYPE html>
<html>
<head>
    <title>one</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('./public/css/all.css') }}">--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="all">
    <div class=" annex_all">
        <div class="annex_head an_row_one">
            <div class="annex_col_one s_float">
                <img src="{{ asset('public/img/an.png') }}">
            </div>
            <div class="annex_col_two s_float">
                <p class="m_text">MARITIME AND PORT AUTHORITY OF SINGAPORE </p>
                <p class="m_text_2">SHIPPING DIVISION </p>

                <p class="r_text">RECORD OF MEDICAL EXAMINATIONS OF SEAFARER</p>
            </div>
            <div class="annex_col_three s_float">
                <p>ANNEX B</p>
            </div>
        </div>
        <!-- row one end -->
        <div class="row_two row_part_a clear">
            <p class="part_a">Part A – to be completed by the Seafarer who is responsible for answering each question accurately.</p>
            <div class="ans">
                <div class="ans_col_one s_float">
                    <div class="ans_name s_float">
                        <p>Seafarer’s Name :(<i>Last, first, middle</i>)</p>
                        <p>(BLOCK CAPITALS)</p>
                    </div>
                    <div class="ans_name_value s_float">
                        <p>{{ strtoupper($form->seafarers_name) }}</p>
                    </div>
                    <div class="ans_gender s_float">
                        <p class="ans_gender">Gender:</p>
                        <p><i class="far fa-check-square"></i>{{ $form->gender }}</p>
                    </div>
                </div>
                <div class="ans_col_two clear">
                    <div class="ans_date s_float">
                        <p class="an_date_text">Date of Birth: day/month/year</p>
                        <p class="an_date_value">{{ $form->date_of_birth }}</p>
                    </div>
                    <div class="ans_place s_float">
                        <p>Place of Birth:</p>
                        <p class="an_place_vale">{{ $form->place_of_birth }}</p>
                    </div>
                    <div class="ans_natioin s_float">
                        <p>Nationality:<span class="nation_value">{{ $form->nationality }}</span></p>
                    </div>
                </div>
                <div class="ans_col_three clear">
                    <div class="ans_type s_float">
                        <p class="an_type_text">*Type of ID documents: NRIC No. for
                            Singaporeans and PRs (e.g. SXXXX567A)
                            / Passport No. for Foreigners:  <span class="an_type_value">{{ $form->passport_no }}</span></p>

                    </div>
                    <div class="ans_dept s_float">
                        <p>     Dept:
                            @if($form->dept == "desk")
                                <i class="far fa-check-square"></i>
                            @endif
                                Deck /
                            @if($form->dept == "engine")
                                <i class="far fa-check-square"></i>
                            @endif
                                Engine /
                            @if($form->dept == "catering")
                                <i class="far fa-check-square"></i>
                            @endif
                                Catering /
                            @if($form->dept == "others")
                            <i class="far fa-check-square"></i>
                            @endif
                            others Rank:<span class="ans_dept_vale">{{ $form->rank }}</span> </p>

                    </div>
                    <div class="ans_ship s_float">
                        <p>Type of ship:</p>
                        <p><span class="ship_value">{{ $form->type_of_ship }}</span> </p>
                    </div>
                </div>

                <div class="ans_col_four clear">
                    <div class="ans_address s_float">
                        <p>Home Address: <span class="ans_address_value">{{ $form->home_address }}</span></p>
                    </div>
                    <div class="ans_routine s_float">
                        <p>Routine and emergency duties: <span class="ans_routine_value">{{ $form->routine_duties}}</span></p>
                    </div>
                    <div class="ans_trading s_float">
                        <p>Trading area: <span class="ans_trading_value"> e.g
                                @if($form->trading_area == "coastal")
                                    <i class="far fa-check-square"></i>
                                @else
                                    <i class="far fa-square"></i>
                                @endif
                                 Coastal /
                                @if($form->trading_area == "world wide")
                                    <i class="far fa-check-square"></i>
                                 @else
                                    <i class="far fa-square"></i>
                                @endif
                                     World wide</span></p>
                    </div>
                </div>
            </div>
            <div>*For identity verification purposre</div>
        </div>

        <!-- row two end -->

        <div class="row_three clear">
            <p class="an_seafarer">Seafarer’s Declarations (<i>please tick</i>)</p>
            <p class="an_condition">Have you ever had any of the following conditions?</p>
            <div class="an_table">
                <div class="table_row">
                    <div class="table_col_one s_float">
                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                <p> </p>
                            </div>
                            <div class="tb_name s_float">

                            </div>
                            <div class="tb_yes s_float">
                                Yes
                            </div>
                            <div class="tb_no s_float">
                                No
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                1.
                            </div>
                            <div class="tb_name s_float">
                                Eye/vision problem
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->eye_problem == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->eye_problem == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                2.
                            </div>
                            <div class="tb_name s_float">
                                High blood pressure
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->high_blood_pressure == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->high_blood_pressure == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                3.
                            </div>
                            <div class="tb_name s_float">
                                Heart/vascular disease
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->heart_disease == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->heart_disease == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                4.
                            </div>
                            <div class="tb_name s_float">
                                Heart Surgery
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->heart_surgery == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->heart_surgery == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                5.
                            </div>
                            <div class="tb_name s_float">
                                Varicose veins/piles
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->varicose_veins == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->varicose_veins == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                6.
                            </div>
                            <div class="tb_name s_float">
                                Asthma/bronchitis
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->asthma_bronchitis == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                    1
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->asthma_bronchitis == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                    2
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                7.
                            </div>
                            <div class="tb_name s_float">
                                Blood disorder
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->blood_disorder == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->blood_disorder == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                8.
                            </div>
                            <div class="tb_name s_float">
                                Diabetes
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->diabetes == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->diabetes == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                9.
                            </div>
                            <div class="tb_name s_float">
                                Thyroid problem
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->thyroid_problem == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->thyroid_problem == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                10.
                            </div>
                            <div class="tb_name s_float">
                                Digestive disorder
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->digestive_disorder == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->digestive_disorder == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                11.
                            </div>
                            <div class="tb_name s_float">
                                Kidney problem
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->kidney_problem == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->kidney_problem == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                12.
                            </div>
                            <div class="tb_name s_float">
                                Skin Problem
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->skin_problem == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->skin_problem == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                13.
                            </div>
                            <div class="tb_name s_float">
                                Allergies
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->allergies == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->allergies == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one" style="height: 38px;">
                            <div class="no_one s_float">
                                14.
                            </div>
                            <div class="tb_name s_float" style="height: 38px">
                                Infectious / contagious diseases
                            </div>
                            <div class="tb_yes s_float" style="height: 38px">
                                @if($form->infectious_diseases == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float" style="height: 38px;">
                                @if($form->infectious_diseases == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                15.
                            </div>
                            <div class="tb_name s_float">
                                Hernia
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->hernia == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->hernia == 0 )
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                16.
                            </div>
                            <div class="tb_name s_float">
                                Genital disorder
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->genital_disorder == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->genital_disorder == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one br_none">
                            <div class="no_one s_float">
                                17.
                            </div>
                            <div class="tb_name s_float">
                                Pregnancy
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->pregnancy == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->pregnancy == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- table col_one end -->

                    <div class="table_col_two s_float">
                        <div class="tb_col_row_two">
                            <div class="no_two s_float">

                            </div>
                            <div class="tb_name_two s_float">

                            </div>
                            <div class="tb_yes_two s_float">
                                Yes
                            </div>
                            <div class="tb_no_two s_float">
                                No
                            </div>
                        </div>
                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                18.
                            </div>
                            <div class="tb_name_two s_float">
                                Sleep problem
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->sleep_problem == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->smoke_alcohol_drug == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>
                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                19.
                            </div>
                            <div class="tb_name_two s_float">
                                Do you smoke, use alcohol or drugs?
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->smoke_alcohol_drug == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->smoke_alcohol_drug == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                20.
                            </div>
                            <div class="tb_name_two s_float">
                                Operation/surgery
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->operation == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->operation == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                21.
                            </div>
                            <div class="tb_name_two s_float">
                                Epilesy/seizures
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->epilesy_seizures == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->epilesy_seizures == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                22.
                            </div>
                            <div class="tb_name_two s_float">
                                Epilesy/seizures
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->epilesy_seizures == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->epilesy_seizures == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                23.
                            </div>
                            <div class="tb_name_two s_float">
                                Loss of consciousness
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->loss_of_consciousness == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->loss_of_consciousness == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                24.
                            </div>
                            <div class="tb_name_two s_float">
                                Psychiatric problems
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->psychiatric_problem == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->psychiatric_problem == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                25.
                            </div>
                            <div class="tb_name_two s_float">
                                Depression
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->depression == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->depression == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                26.
                            </div>
                            <div class="tb_name_two s_float">
                                Attempted suicide
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->attempted_suicide == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->attempted_suicide == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                27.
                            </div>
                            <div class="tb_name_two s_float">
                                Loss of memory
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->loss_of_memory == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->loss_of_memory == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                28
                            </div>
                            <div class="tb_name_two s_float">
                                Balance problem
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->balance_problem == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->balance_problem == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                29.
                            </div>
                            <div class="tb_name_two s_float">
                                Severe headaches
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->severe_headache == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->severe_headache == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                30.
                            </div>
                            <div class="tb_name_two s_float">
                                Ear(hearing, tinnitus/nose/throat problem
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->hearing_throat_problem == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->hearing_throat_problem == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two" style="height: 38px;">
                            <div class="no_two s_float" style="height: 38px;">
                                31.
                            </div>
                            <div class="tb_name_two s_float" style="height: 38px;">
                                Restricted mobility
                            </div>
                            <div class="tb_yes_two s_float" style="height: 38px;">
                                @if($form->restricted_mobility == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float" style="height: 38px;">
                                @if($form->restricted_mobility == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                32.
                            </div>
                            <div class="tb_name_two s_float">
                                Back or joint problem
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->joint_problem == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->joint_problem == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                33.
                            </div>
                            <div class="tb_name_two s_float">
                                Amputation
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->amputation == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->amputation == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two br_none">
                            <div class="no_two s_float">
                                34.
                            </div>
                            <div class="tb_name_two s_float">
                                Fracture/dislocations
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->fracture == 1)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->fracture == 0)
                                    <i class="fas fa-check" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- row three end -->

        <div class="row_five">
            <div class="an_answer">
                <p class="answer_text">If you answer “yes” to any of the above questions, please provide details:</p>
                <p class="an_ans_value"></p>
            </div>
        </div>
        <!-- row five end -->


        <div class="m_footer">
            <p class="m_foot_1">Page 1 of 5</p>
            <p class="m_foot_2">RECORD OF MEDICAL EXAMINTAION OF SEAFARERS - September 2021</p>
        </div>
    </div>

</div>


</body>
</html>
