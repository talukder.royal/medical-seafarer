<!DOCTYPE html>
<html>
<head>
    <title>One</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assert('./public/css/all.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>
<div class="all">
    <div class="annex_all">
        <div class="row_one">
            <div class="w_rest_row">
                <div class="w_rest_col s_float">
                    <i @class([
                         'far fa-check-square' => ! $form->with_restrictions,
                         'far fa-square' => $form->with_restrictions,
                    ])></i>
                    Without restrictions
                </div>
                <div class="w_rest_col s_float">
                    <i @class([
                         'far fa-check-square' => $form->with_restrictions,
                         'far fa-square' => ! $form->with_restrictions,
                    ])></i>
                    With restrictions
                </div>
            </div>

            <div class="res_des clear">
                <p class="res_des_text">Description of restrictions (e.g. specific position, type of ship, trading area etc.)</p>
                <p class="res_des_text">{{ $form-> description_restrictions }}</p>
            </div>
        </div>

        <div class="row_two">
            <div class="res_date_sign">
                <div class="res_date s_float">
                    <p>Date</p>
                </div>
                <div class="res_sign s_float">
                    <p>Signature of Medical Practitioner</p>
                </div>
                <div class="res_medical s_float">
                    <p>Medical Practitioner’s name, licence number, address</p>
                </div>
            </div>
        </div>
        <div class=" b_m_footer_m_5">
            <p class="m_foot_1">Page 3 of 5</p>
            <p class="m_foot_2">RECORD OF MEDICAL EXAMINTAION OF SEAFARERS - September 2021</p>
        </div>
    </div>
</div>


</body>
</html>
