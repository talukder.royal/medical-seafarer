<!DOCTYPE html>
<html>
<head>
    <title>One</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ assert('./public/css/all.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>
<div class="all">
    <div class="all_in annex_all"  style="margin-top: 55px;">
        <div class="row_one">
            <div class="part_b_last_table_p2">
                <div class="part_b_table_row">

                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Ears (general)
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->ears == 1)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->ears == 0)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Tympanic membrane
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->tympanic_member == 1)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->tympanic_member == 0)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Eyes
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->eyes == 1)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->eyes == 0)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Ophthalmoscopy
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->ophthalmoscopy == 1)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->ophthalmoscopy == 0)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Pupils
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->pupils == 1)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->pupils == 0)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Eye movement
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->eye_environment == 1)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->eye_environment == 0)
                                <i class="fas fa-check"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Lungs and chest
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->lungs_chest == 1)
                                <i class="fas fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->lungs_chest == 0)
                                <i class="fas fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Breast examination
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->breast_examination == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->breast_examination == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Heart
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->heart == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->heart == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Skin
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->skin == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->skin == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Varicose Vein
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->varicose_vein == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->varicose_vein == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Vascular (inc. pedal pulse)
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->vascular_pulse == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->vascular_pulse == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Abdomen and viscera
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->abdomen_viscera == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->abdomen_viscera == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Hernia
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->hernia_2 == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->hernia_2 == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Anus (not rectal exam)
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->anus == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->anus == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            G-U system
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->g_u_system == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->g_u_system == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Upper and lower extremities
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->upper_lower_ext == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->upper_lower_ext == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Spine (C/s, T/S, L/S)
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->spine == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->spine == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Neurologic (full/brief)
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->neurologic == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->neurologic == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Psychiatric
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->psychiatric == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->psychiatric == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row" style="border: none;">
                        <div  class="part_b_tb_col_1 s_float">
                            General appearance
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->general_appearance == 1)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->general_appearance == 0)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- row one end -->
        <div class="row_two clear" style="height: 100px;">
            <div class="chest_x_ray">
                <p class="chest_text">Chest X-ray</p>
                <div class="ch_row">
                    <div class="not_per s_float">
                        @if($form->chest_xray == 0)
                            <i class="far fa-check-square"></i>
                        @else
                            <i class="far fa-square"></i>
                        @endif
                            Not performed
                    </div>
                    <div class="per s_float">
                        <div class="per_text s_float">
                            @if($form->chest_xray == 1)
                                <i class="far fa-check-square"></i>
                            @else
                                <i class="far fa-square"></i>
                            @endif
                            Performed on (day/month/year):
                        </div>
                        <div class="per_value s_float">
                            {{ $form->date_of_performed_on }}
                        </div>
                    </div>

                </div>
                <div class="ch_row" style="margin-top: 50px;">
                    <div class="not_result s_float">

                    </div>
                    <div class="per_2 s_float">
                        <div class="result_text s_float">
                            Result:
                        </div>
                        <div class="result_value s_float">
                            {{ $form->date_of_results_on }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- row two end -->

        <div class="row_three clear">
            <div class="other_sec">
                <p class="other_dio_text">Other diagnostic test(s) and result(s):</p>
                <div class="o_dio_row">
                    <div class="o_test s_float">
                        <div class="o_test_text s_float">
                            Test:
                        </div>
                        <div class="o_test_value s_float">
                            <p>{{ $form->d_test }}</p>
                        </div>
                    </div>
                    <div class="o_result s_float">
                        <div class="o_result_text s_float">
                            Results:
                        </div>
                        <div class="o_result_value s_float">
                            <p>{{  $form->d_result }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="medical_practition clear">
                <p class="medi_text">Medical practitioner’s comments and assessment of fitness, with reasons for any limitations.</p>
                <h2 class="fit_text">Fit For Duty on board Ship</h2>
            </div>
        </div>
        <!-- rwo three end -->
        <div class="row_four clear">
            <div class="assesment_sce">
                <p class="ass_text">Assessment of fitness for service at sea (please tick)</p>
                <p class="ass_text_2">On the basis of the seafarer’s personal declaration, my clinical examination and diagnostic test results recorded above, I declare the seafarer medically:</p>
                <div class="ass_select">
                    <div class="ass_select_row">
                        <div class="ass_fit s_float">
                            <i @class(['far fa-check-square' => $form->fit_for_lookout_duty, 'far fa-square' => ! $form->fit_for_lookout_duty])></i>
                            Fit for look out duty
                        </div>
                        <div class="ass_unfit s_float">
                            <i @class(['far fa-check-square' => !$form->fit_for_lookout_duty, 'far fa-square' => $form->fit_for_lookout_duty])></i>Unfit for lookout duty
                        </div>
                    </div>
                    <div class="ass_select_row" style="margin-top: 15px;">
                        <div class="ass_fit s_float">
                            <i @class([
                                'far fa-check-square' => $form->visual_aid,
                                'far fa-square' => !$form->visual_aid])>
                            </i>
                            Visual aid required
                        </div>
                        <div class="ass_unfit s_float">
                            <i @class([
                                'far fa-check-square' => !$form->visual_aid,
                                'far fa-square' => $form->visual_aid])>
                            </i>
                            Visual aid not required
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row four end -->
        <div class="row_five clear">
            <div class="fit_unfit_table">
                <div class="fit_unfit_row">
                    <div class="fit_unfit_col s_float">

                    </div>
                    <div class="fit_unfit_col s_float">
                        Deck Service
                    </div>
                    <div class="fit_unfit_col s_float">
                        Engine Service
                    </div>
                    <div class="fit_unfit_col s_float">
                        Catering Service
                    </div>
                    <div class="fit_unfit_col s_float" style="border: none;">
                        Other Service
                    </div>
                </div>
                <div class="fit_unfit_row_2">
                    <div class="fit_unfit_col s_float">
                        Fit
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_desk == 1)
                        <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_engine == 1)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_catering == 1)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float" style="border: none;">
                        @if($form->fit_other == 1)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                </div>
                <div class="fit_unfit_row_2">
                    <div class="fit_unfit_col s_float">
                        Unfit
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_desk == 0)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_engine == 0)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_catering == 0)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float" style="border: none;">
                        @if($form->fit_other == 0)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class=" b_m_footer_m_4">
            <p class="m_foot_1">Page 4 of 5</p>
            <p class="m_foot_2">RECORD OF MEDICAL EXAMINTAION OF SEAFARERS - September 2021</p>
        </div>
    </div>
</div>
<!--
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js" integrity="sha512-YSdqvJoZr83hj76AIVdOcvLWYMWzy6sJyIMic2aQz5kh2bPTd9dzY3NtdeEAzPp/PhgZqr4aJObB3ym/vsItMg==" crossorigin="anonymous"></script> -->

</body>
</html>
