<!DOCTYPE html>
<html>
<head>
    <title>Maritime Singapore</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{{ asset('public/js/style.js') }}"></script>
</head>
<body>
<div class="all">
    <div class=" annex_all">
{{--       start of  Qrcode --}}
        <div class="visible-print text-center" style="padding: 2px;">
            @php
                use SimpleSoftwareIO\QrCode\Facades\QrCode;
                $path  = 'qrcodes/_'.now().$form->id.'.svg';
                QrCode::generate(route('rmes_form.allForm.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
            @endphp
            <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
            <p>Scan me to return to the original page.</p>
        </div>
{{--       End of  Qrcode --}}
        <div class="annex_head an_row_one">
            <div class="annex_col_one s_float">
                <img src="{{ asset('public/img/an.png') }}">
            </div>
            <div class="annex_col_two s_float">
                <p class="m_text">MARITIME AND PORT AUTHORITY OF SINGAPORE </p>
                <p class="m_text_2">SHIPPING DIVISION </p>

                <p class="r_text">RECORD OF MEDICAL EXAMINATIONS OF SEAFARER</p>
            </div>
            <div class="annex_col_three s_float">
                <p>ANNEX B</p>
            </div>
        </div>
        <!-- row one end -->
        <div class="row_two row_part_a clear">
            <p class="part_a">Part A – to be completed by the Seafarer who is responsible for answering each question accurately.</p>
            <div class="ans">
                <div class="ans_col_one s_float">
                    <div class="ans_name s_float">
                        <p>Seafarer’s Name :(<i>Last, first, middle</i>)</p>
                        <p>(BLOCK CAPITALS)</p>
                    </div>
                    <div class="ans_name_value s_float">
                        <p>{{ strtoupper($form->seafarers_name) }}</p>
                    </div>
                    <div class="ans_gender s_float">
                        <p class="ans_gender">Gender:</p>
                        <p><i style="font-family: fontawesome;"  class="fa fa-check-square">&#xf046;</i>{{ $form->gender }}</p>
                    </div>
                </div>
                <div class="ans_col_two clear">
                    <div class="ans_date s_float">
                        <p class="an_date_text">Date of Birth: day/month/year</p>
                        <p class="an_date_value">{{ $form->date_of_birth }}</p>
                    </div>
                    <div class="ans_place s_float">
                        <p>Place of Birth:</p>
                        <p class="an_place_vale">{{ $form->place_of_birth }}</p>
                    </div>
                    <div class="ans_natioin s_float">
                        <p>Nationality:<span class="nation_value">{{ $form->nationality }}</span></p>
                    </div>
                </div>
                <div class="ans_col_three clear">
                    <div class="ans_type s_float">
                        <p class="an_type_text">*Type of ID documents: NRIC No. for
                            Singaporeans and PRs (e.g. SXXXX567A)
                            / Passport No. for Foreigners:  <span class="an_type_value">{{ $form->passport_no }}</span></p>

                    </div>
                    <div class="ans_dept s_float">
                        <p>     Dept:
                            @if($form->dept == "desk")
                                <i class="far fa-check">&#xf046;</i>
                            @else
                                <i class="far fa-check">&#xf096;</i>
                            @endif
                            Deck /
                            @if($form->dept == "engine")
                                <i class="far fa-check">&#xf046;</i>
                            @else
                                <i class="far fa-check">&#xf096;</i>
                            @endif
                            Engine /
                            @if($form->dept == "catering")
                                <i class="far fa-check">&#xf046;</i>
                            @else
                                <i class="far fa-check">&#xf096;</i>
                            @endif
                            Catering /
                            @if($form->dept == "others")
                                <i class="far fa-check">&#xf046;</i>
                            @else
                                <i class="far fa-check">&#xf096;</i>
                            @endif
                            others Rank:<span class="ans_dept_vale">{{ $form->rank }}</span> </p>

                    </div>
                    <div class="ans_ship s_float">
                        <p>Type of ship:</p>
                        <p><span class="ship_value">{{ $form->type_of_ship }}</span> </p>
                    </div>
                </div>

                <div class="ans_col_four clear">
                    <div class="ans_address s_float">
                        <p>Home Address: <span class="ans_address_value">{{ $form->home_address }}</span></p>
                    </div>
                    <div class="ans_routine s_float">
                        <p>Routine and emergency duties: <span class="ans_routine_value">{{ $form->routine_duties}}</span></p>
                    </div>
                    <div class="ans_trading s_float">
                        <p>Trading area: <span class="ans_trading_value"> e.g
                                @if($form->trading_area == "coastal")
                                    <i class="far fa-check-square"></i>
                                @else
                                    <i class="far fa-square"></i>
                                @endif
                                 Coastal /
                                @if($form->trading_area == "world wide")
                                    <i class="far fa-check-square"></i>
                                @else
                                    <i class="far fa-square"></i>
                                @endif
                                     World wide</span></p>
                    </div>
                </div>
            </div>
            <div>*For identity verification purposre</div>
        </div>

        <!-- row two end -->

        <div class="row_three clear">
            <p class="an_seafarer">Seafarer’s Declarations (<i>please tick</i>)</p>
            <p class="an_condition">Have you ever had any of the following conditions?</p>
            <div class="an_table">
                <div class="table_row">
                    <div class="table_col_one s_float">
                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                <p> </p>
                            </div>
                            <div class="tb_name s_float">

                            </div>
                            <div class="tb_yes s_float">
                                Yes
                            </div>
                            <div class="tb_no s_float">
                                No
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                1.
                            </div>
                            <div class="tb_name s_float">
                                Eye/vision problem
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->eye_problem == 1)
                                    <i class="fa fa-check" checked>&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->eye_problem == 0)
                                    <i class="fa fa-check" checked>&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                2.
                            </div>
                            <div class="tb_name s_float">
                                High blood pressure
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->high_blood_pressure == 1)
                                    <i class="fa fa-check" checked>&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->high_blood_pressure == 0)
                                    <i class="fa fa-check" checked>&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                3.
                            </div>
                            <div class="tb_name s_float">
                                Heart/vascular disease
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->heart_disease == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->heart_disease == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                4.
                            </div>
                            <div class="tb_name s_float">
                                Heart Surgery
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->heart_surgery == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->heart_surgery == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                5.
                            </div>
                            <div class="tb_name s_float">
                                Varicose veins/piles
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->varicose_veins == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->varicose_veins == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                6.
                            </div>
                            <div class="tb_name s_float">
                                Asthma/bronchitis
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->asthma_bronchitis == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->asthma_bronchitis == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                7.
                            </div>
                            <div class="tb_name s_float">
                                Blood disorder
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->blood_disorder == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->blood_disorder == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                8.
                            </div>
                            <div class="tb_name s_float">
                                Diabetes
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->diabetes == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->diabetes == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                9.
                            </div>
                            <div class="tb_name s_float">
                                Thyroid problem
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->thyroid_problem == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->thyroid_problem == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                10.
                            </div>
                            <div class="tb_name s_float">
                                Digestive disorder
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->digestive_disorder == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->digestive_disorder == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                11.
                            </div>
                            <div class="tb_name s_float">
                                Kidney problem
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->kidney_problem == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->kidney_problem == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                12.
                            </div>
                            <div class="tb_name s_float">
                                Skin Problem
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->skin_problem == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->skin_problem == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                13.
                            </div>
                            <div class="tb_name s_float">
                                Allergies
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->allergies == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->allergies == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one" style="height: 38px;">
                            <div class="no_one s_float">
                                14.
                            </div>
                            <div class="tb_name s_float" style="height: 38px">
                                Infectious / contagious diseases
                            </div>
                            <div class="tb_yes s_float" style="height: 38px">
                                @if($form->infectious_diseases == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float" style="height: 38px;">
                                @if($form->infectious_diseases == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                15.
                            </div>
                            <div class="tb_name s_float">
                                Hernia
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->hernia == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->hernia == 0 )
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one">
                            <div class="no_one s_float">
                                16.
                            </div>
                            <div class="tb_name s_float">
                                Genital disorder
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->genital_disorder == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->genital_disorder == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_one br_none">
                            <div class="no_one s_float">
                                17.
                            </div>
                            <div class="tb_name s_float">
                                Pregnancy
                            </div>
                            <div class="tb_yes s_float">
                                @if($form->pregnancy == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no s_float">
                                @if($form->pregnancy == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- table col_one end -->

                    <div class="table_col_two s_float">
                        <div class="tb_col_row_two">
                            <div class="no_two s_float">

                            </div>
                            <div class="tb_name_two s_float">

                            </div>
                            <div class="tb_yes_two s_float">
                                Yes
                            </div>
                            <div class="tb_no_two s_float">
                                No
                            </div>
                        </div>
                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                18.
                            </div>
                            <div class="tb_name_two s_float">
                                Sleep problem
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->sleep_problem == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->smoke_alcohol_drug == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>
                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                19.
                            </div>
                            <div class="tb_name_two s_float">
                                Do you smoke, use alcohol or drugs?
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->smoke_alcohol_drug == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->smoke_alcohol_drug == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                20.
                            </div>
                            <div class="tb_name_two s_float">
                                Operation/surgery
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->operation == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->operation == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                21.
                            </div>
                            <div class="tb_name_two s_float">
                                Epilesy/seizures
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->epilesy_seizures == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->epilesy_seizures == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                22.
                            </div>
                            <div class="tb_name_two s_float">
                                Epilesy/seizures
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->epilesy_seizures == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->epilesy_seizures == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                23.
                            </div>
                            <div class="tb_name_two s_float">
                                Loss of consciousness
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->loss_of_consciousness == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->loss_of_consciousness == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                24.
                            </div>
                            <div class="tb_name_two s_float">
                                Psychiatric problems
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->psychiatric_problem == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->psychiatric_problem == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                25.
                            </div>
                            <div class="tb_name_two s_float">
                                Depression
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->depression == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->depression == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                26.
                            </div>
                            <div class="tb_name_two s_float">
                                Attempted suicide
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->attempted_suicide == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->attempted_suicide == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                27.
                            </div>
                            <div class="tb_name_two s_float">
                                Loss of memory
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->loss_of_memory == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->loss_of_memory == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                28
                            </div>
                            <div class="tb_name_two s_float">
                                Balance problem
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->balance_problem == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->balance_problem == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                29.
                            </div>
                            <div class="tb_name_two s_float">
                                Severe headaches
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->severe_headache == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->severe_headache == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                30.
                            </div>
                            <div class="tb_name_two s_float">
                                Ear(hearing, tinnitus/nose/throat problem
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->hearing_throat_problem == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->hearing_throat_problem == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two" style="height: 38px;">
                            <div class="no_two s_float" style="height: 38px;">
                                31.
                            </div>
                            <div class="tb_name_two s_float" style="height: 38px;">
                                Restricted mobility
                            </div>
                            <div class="tb_yes_two s_float" style="height: 38px;">
                                @if($form->restricted_mobility == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float" style="height: 38px;">
                                @if($form->restricted_mobility == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                32.
                            </div>
                            <div class="tb_name_two s_float">
                                Back or joint problem
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->joint_problem == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->joint_problem == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two">
                            <div class="no_two s_float">
                                33.
                            </div>
                            <div class="tb_name_two s_float">
                                Amputation
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->amputation == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->amputation == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>

                        <div class="tb_col_row_two br_none">
                            <div class="no_two s_float">
                                34.
                            </div>
                            <div class="tb_name_two s_float">
                                Fracture/dislocations
                            </div>
                            <div class="tb_yes_two s_float">
                                @if($form->fracture == 1)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="tb_no_two s_float">
                                @if($form->fracture == 0)
                                    <i class="fa fa-check">&#xf00c;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- row three end -->

        <div class="row_five">
            <div class="an_answer">inf@shakil#!!!
                <p class="answer_text">If you answer “yes” to any of the above questions, please provide details:</p>
                <p class="an_ans_value">{{ $form->provide_details_1 }}</p>
            </div>
        </div>
        <!-- row five end -->


        <div class="m_footer">
            <p class="m_foot_1">Page 1 of 5</p>
            <p class="m_foot_2">RECORD OF MEDICAL EXAMINTAION OF SEAFARERS - September 2021</p>
        </div>
    </div>

</div>
<pagebreak />
{{--page2--}}
<div class="all">
    <div class="annex_all">
        <div class="row_one">
            <div class="additional_table_two">
                <div class="add_table_row">
                    <div class="add_no s_float">

                    </div>
                    <div  class="add_table_text s_float">
                        <b>Additional questions</b>
                    </div>
                    <div class="ad_yes s_float">
                        Yes
                    </div>
                    <div class="ad_no s_float">
                        No
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        35.
                    </div>
                    <div  class="add_table_text s_float">
                        Have you ever been signed off as sick or repatriated from a ship?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->signed_off_sick == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->signed_off_sick == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        36.
                    </div>
                    <div  class="add_table_text s_float">
                        Have you ever been hospitalized?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->hospitalized == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->hospitalized == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        37.
                    </div>
                    <div  class="add_table_text s_float">
                        Have you ever been declared unfit for sea duty?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->declared_for_sea_duty == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->declared_for_sea_duty == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        38.
                    </div>
                    <div  class="add_table_text s_float">
                        Has your medical certificate even been restricted or revoked?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->medical_certificate_revoked == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->medical_certificate_revoked == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        39.
                    </div>
                    <div  class="add_table_text s_float">
                        Are you aware that you have any medical problems, diseases or illnesses?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->medical_problems == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->medical_problems == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        40.
                    </div>
                    <div  class="add_table_text s_float">
                        Do you feel healthy and fit to perform the duties of your designated position/occupation?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->feel_healthy == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->feel_healthy == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row">
                    <div class="add_no s_float">
                        41.
                    </div>
                    <div  class="add_table_text s_float">
                        Are you allergic to any medication?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->allergic_medication == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->allergic_medication == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>
                <div class="add_table_row" style="border-bottom: none;">
                    <div class="add_no s_float">
                        42.
                    </div>
                    <div  class="add_table_text s_float">
                        Are you using any non-prescription or prescription medication?
                    </div>
                    <div class="ad_yes s_float">
                        @if($form->prescription_medication == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div class="ad_no s_float">
                        @if($form->prescription_medication == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>

            </div>
        </div>
        <div class="row_two clear">
            <div class="if_answer">
                If you answer “yes”, please list the medications taken, the purpose(s) and the dosage:
                {{ $form-> provide_details}}
            </div>
        </div>
        <div>
            <p class="declare_text">I hereby declare that the personal declaration above is a true statement to the best of my knowledge.</p>
        </div>

        <div class="row_three clear">
            <div class="date_sign">
                <div class="s_float b_date">
                    <p class="date_value2"></p>
                    <p class="date">Date</p>
                </div>
                <div class="s_float b_sign">
                    <p class="signiture">Signature of Seafarer</p>
                </div>
                <div class=" s_float b_wit">
                    <p class="name_singiture">Name and Signature of Witness</p>
                </div>
            </div>
        </div>
        <div class="row_four dr_row clear">
            <p class="auth_text">I hereby authorize the release of all my previous medical records (including my last Seafarer Medical Certificate) from any health professional, health institutions and public authorities to<br> Dr. <span class="dr_field">SABRINA MOSTAPA</span></p>
        </div>

        <div class="row_three clear">
            <div class="date_sign b_date">
                <div class="s_float b_date">
                    <p class="date_value2">05-Jan-2021</p>
                    <p class="date">Date</p>
                </div>
                <div class="s_float b_sign">
                    <p class="signiture">Signature of Seafarer</p>
                </div>
                <div class=" s_float b_wit">
                    <p class="name_singiture">Name and Signature of Witness</p>
                </div>
            </div>
        </div>
        <div class="b_m_footer clear">
            <p class="m_foot_1">Page 1 of 5</p>
            <p class="m_foot_2">RECORD OF MEDICAL EXAMINTAION OF SEAFARERS - March 2020</p>
        </div>
    </div>

</div>
<pagebreak />
{{--//page3--}}

<div class="all">
    <div class="annex_all m_b_3">
        <div class="row_one">
            <p class="part_b_result">Part B- Result of medical examinations</p>
            <div class="eye">
                <p><b>Eyesight</b></p>
                <p>Use of glasses of contact lenses</p>
            </div>
            <div class="eye_yes_no">
                <p>
                    @if($form->contact_lenses == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    No
                </p>
                <div class="eye_type">
                    <div class="type_yes s_float">
                        @if($form->contact_lenses == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        Yes
                    </div>
                    <div class="type_text_row s_float">
                        <div class="type_text s_float">
                            Type
                        </div>
                        <div class="type_text_value s_float">
                            <p>{{ $form->lense_type }}</p>
                        </div>
                    </div>
                    <div class="type_purpose_row s_float">
                        <div class="purpose_text s_float">
                            Purpose
                        </div>
                        <div class="purpose_text_value s_float">
                            <p>{{ $form->purpose }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>	<!-- row one end -->

        <div class="row_two clear v_acuity">
            <p class="va">Visual Acuity</p>
            <div class="visual_table">
                <div class="unaided s_float">
                    <div class="unaided_row">
                        <p>Unaided</p>
                    </div>
                    <div class="unaided_row">
                        <div class="un_row s_float">
                            Right eye
                        </div>
                        <div class="un_row s_float">
                            left eye
                        </div>
                        <div class="un_row s_float" style="border: none;">
                            Binocular
                        </div>
                    </div>
                    <div class="unaided_row">
                        <div class="un_row s_float">
                            Distant: 6/6 {{ $form->right_eye_distant}}
                        </div>
                        <div class="un_row s_float">
                            6/6 {{ $form->left_eye_distant }}
                        </div>
                        <div class="un_row s_float" style="border: none;">
                            {{ $form->binocular_eye_distant }}
                        </div>
                    </div>
                    <div class="unaided_row" style="border: none;">
                        <div class="un_row s_float">
                            Near: 6/6 {{ $form->right_eye_near}}
                        </div>
                        <div class="un_row s_float">
                            6/6 {{ $form->left_eye_near}}
                        </div>
                        <div class="un_row s_float" style="border: none;">
                            {{ $form->binocular_eye_near}}
                        </div>
                    </div>

                </div>
                <div class="aided s_float">
                    <div class="aided_row">
                        <p>Aided</p>
                    </div>
                    <div class="unaided_row">
                        <div class="un_row s_float">
                            Right eye
                        </div>
                        <div class="un_row s_float">
                            left eye
                        </div>
                        <div class="un_row s_float" style="border: none">
                            Binocular
                        </div>
                    </div>
                    <div class="unaided_row">
                        <div class="un_row s_float">
                            Distant: {{ $form->aided_right_eye_distant }}
                        </div>
                        <div class="un_row s_float">
                            {{ $form->aided_left_eye_distant }}
                        </div>
                        <div class="un_row s_float" style="border: none;">
                            {{ $form->aided_binocular_eye_distant }}
                        </div>
                    </div>
                    <div class="unaided_row" style="border: none;">
                        <div class="un_row s_float">
                            Near: {{ $form->aided_right_eye_near }}
                        </div>
                        <div class="un_row s_float">
                            {{ $form->aided_left_eye_near }}
                        </div>
                        <div class="un_row s_float" style="border: none;">
                            {{ $form->aided_binocular_eye_near }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row two end -->
        <div class="row_three clear">
            <p class="v_field">Visual fields</p>
            <div class="v_field_table">
                <div class="v_field_row">
                    <div class="v_f_col_one s_float">

                    </div>
                    <div class="v_f_col_two s_float">
                        Normal
                    </div>
                    <div class="v_f_col_three s_float">
                        Defective
                    </div>
                </div>
                <div class="v_field_row">
                    <div class="v_f_col_one s_float">
                        Right Eye:
                    </div>
                    <div class="v_f_col_two s_float">
                        @if($form->visual_normal == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="v_f_col_three s_float">
                        @if($form->visual_normal == 0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="v_field_row" style="border: none;">
                    <div class="v_f_col_one s_float">
                        Left Eye:
                    </div>
                    <div class="v_f_col_two s_float">
                        @if($form->visual_normal == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                    <div class="v_f_col_three s_float">
                        @if($form->visual_normal == 0)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- row three end -->
        <div class="row_four">
            <p class="color_vision">Color Vision (Please tick)</p>
            <div class="color_v">
                <div class="not_t s_float">
                    @if($form->color_vision == 0)
                        <i class="far fa-check-square"></i>
                    @else
                        <i class="far fa-check-square"></i>
                    @endif
                    Not tested
                </div>
                <div class="not_t s_float">
                    @if($form->color_vision == 1)
                        <i class="far fa-check-square"></i>
                    @else
                        <i class="far fa-square"></i>
                    @endif
                    Normal
                </div>

                <div class="not_t s_float">
                    @if($form->color_vision == 2)
                        <i class="far fa-check-square"></i>
                    @else
                        <i class="far fa-square"></i>
                    @endif
                    Doubtful
                </div>

                <div class="not_t s_float">
                    @if($form->color_vision == 3)
                        <i class="far fa-check-square"></i>
                    @else
                        <i class="far fa-square"></i>
                    @endif
                    Defective
                </div>

            </div>
        </div>
        <!-- row four end -->

        <div class="row_five clear">
            <p class="haeaing_t">Hearing</p>
            <div class="hearing_table">
                <div class="heading_row">
                    <p class="pure_text"><b>Pure tone adn audiometry </b>(threshold values in dB)</p>
                </div>
                <div class="heading_row">
                    <div class="h_col s_float">

                    </div>
                    <div class="h_col s_float">
                        500 Hz
                    </div>
                    <div class="h_col s_float">
                        1000 Hz
                    </div>
                    <div class="h_col s_float">
                        2000 Hz
                    </div>
                    <div class="h_col s_float" style="border: none">
                        3000 Hz
                    </div>
                </div>
                <div class="heading_row">
                    <div class="h_col s_float">
                        Right ear
                    </div>
                    <div class="h_col s_float">
                        {{ $form->right_ear_500_hz }} dB
                    </div>
                    <div class="h_col s_float">
                        {{ $form->right_ear_1000_hz }} dB
                    </div>
                    <div class="h_col s_float">
                        {{ $form->right_ear_2000_hz }} dB
                    </div>
                    <div class="h_col s_float" style="border: none">
                        {{ $form->right_ear_3000_hz }} dB
                    </div>
                </div>
                <div class="heading_row" style="border: none;">
                    <div class="h_col s_float">
                        Left ear
                    </div>
                    <div class="h_col s_float">
                        {{ $form->left_ear_500_hz }} dB
                    </div>
                    <div class="h_col s_float">
                        {{ $form->left_ear_1000_hz }} dB
                    </div>
                    <div class="h_col s_float">
                        {{ $form->left_ear_2000_hz }} dB
                    </div>
                    <div class="h_col s_float" style="border: none">
                        {{ $form->left_ear_3000_hz }} dB
                    </div>
                </div>

            </div>
        </div>
        <!-- row five end -->
        <div class="row_six spc_row clear">
            <p class="speech_text">Speech and whisper test (metres)</p>
            <div class="speech_table">
                <div class="st_row">
                    <div class="sp_col s_float">

                    </div>
                    <div class="sp_col s_float">
                        Normal
                    </div>
                    <div class="sp_col s_float" style="border: none;">
                        Whisper
                    </div>
                </div>
                <div class="st_row">
                    <div class="sp_col s_float">
                        Right ear
                    </div>
                    <div class="sp_col s_float">
                        {{ $form->right_ear_normal }}.Mtr
                    </div>
                    <div class="sp_col s_float" style="border: none;">
                        {{ $form->right_ear_whisper }}.Mtr
                    </div>
                </div>
                <div class="st_row" style="border:none;">
                    <div class="sp_col s_float">
                        Left ear
                    </div>
                    <div class="sp_col s_float">
                        {{ $form->left_ear_normal }}.Mtr
                    </div>
                    <div class="sp_col s_float" style="border: none;">
                        {{ $form->left_ear_whisper }}.Mtr
                    </div>
                </div>
            </div>
        </div>
        <!-- row six end -->
        <div class="row_seven clear">
            <p class="clinial_text">Clinial Findings</p>
            <div class="clinial_table">
                <div class="cl_row">
                    <div class="cl_col_one s_float">
                        Height (cm)
                    </div>
                    <div class="cl_col_two s_float s_float">
                        {{ $form->clinical_height }}
                    </div>
                    <div class="cl_col_three s_float">
                        Weight (Kg)
                    </div>
                    <div class="cl_col_four s_float">
                        {{ $form->clinical_weight }}
                    </div>
                </div>
                <div class="cl_row">
                    <div class="cl_col_one s_float">
                        Pulse rate (per minute)
                    </div>
                    <div class="cl_col_two s_float s_float">
                        {{ $form->clinical_pulse_rate }}
                    </div>
                    <div class="cl_col_three s_float">
                        Rhythm							</div>
                    <div class="cl_col_four s_float">
                        {{ $form->clinical_rhythm }}
                    </div>
                </div>
                <div class="cl_row">
                    <div class="cl_col_one s_float">
                        Blood Pressure Systolic (mm Hg)
                    </div>
                    <div class="cl_col_two s_float s_float">
                        {{ $form->clinical_b_pressure }} mmHg
                    </div>
                    <div class="cl_col_three s_float">
                        Diastolic (mm Hg)
                    </div>
                    <div class="cl_col_four s_float">
                        {{ $form->clinical_diastolic }} mmHg
                    </div>
                </div>
                <div class="cl_row" style="border: none;">
                    <div class="cl_col_one_1 s_float">
                        <div class="urin s_float">
                            Urinalysis:
                        </div>
                        <div class="glucose s_float">
                            Glucose: {{ $form->urinalysis_glucose }}
                        </div>
                    </div>
                    <div class="cl_col_two_2 s_float s_float">
                        Protin: {{ $form->urinalysis_protin }}
                    </div>
                    <div class="cl_col_three_3 s_float">
                        Blood: {{ $form->urinalysis_blood }}
                    </div>

                </div>
            </div>
        </div>
        <!-- row six end -->
        <div class="row_seven">
            <div class="part_b_last_table">
                <div class="part_b_table_row">
                    <div  class="part_b_tb_col_1 s_float">
                        one
                    </div>
                    <div  class="part_b_tb_col_2 s_float">
                        <b>Normal</b>
                    </div>
                    <div  class="part_b_tb_col_3 s_float">
                        <b>Abnormal</b>
                    </div>
                </div>
                <div class="part_b_table_row">
                    <div  class="part_b_tb_col_1 s_float">
                        Head
                    </div>
                    <div  class="part_b_tb_col_2 s_float">
                        @if($form->head == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div  class="part_b_tb_col_3 s_float">
                        @if($form->head == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>
                <div class="part_b_table_row">
                    <div  class="part_b_tb_col_1 s_float">
                        Sinus, nose, throat
                    </div>
                    <div  class="part_b_tb_col_2 s_float">
                        @if($form->sinus_nose_throat == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div  class="part_b_tb_col_3 s_float">
                        @if($form->sinus_nose_throat == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>
                <div class="part_b_table_row">
                    <div  class="part_b_tb_col_1 s_float">
                        Mouth/teeth
                    </div>
                    <div  class="part_b_tb_col_2 s_float">
                        @if($form->mouth_teeth == 1)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                    <div  class="part_b_tb_col_3 s_float">
                        @if($form->mouth_teeth == 0)
                            <i class="fa fa-check">&#xf00c;</i>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <div class=" b_m_footer_m_3">
            <p class="m_foot_1">Page 1 of 5</p>
            <p class="m_foot_2">RECORD OF MEDICAL EXAMINTAION OF SEAFARERS - September 2021</p>
        </div>
    </div>

</div>

{{--page4--}}
<div class="all">
    <div class="all_in annex_all"  style="margin-top: 55px;">
        <div class="row_one">
            <div class="part_b_last_table_p2">
                <div class="part_b_table_row">

                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Ears (general)
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->ears == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->ears == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Tympanic membrane
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->tympanic_member == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->tympanic_member == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Eyes
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->eyes == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->eyes == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Ophthalmoscopy
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->ophthalmoscopy == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->ophthalmoscopy == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Pupils
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->pupils == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->pupils == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Eye movement
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->eye_environment == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->eye_environment == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Lungs and chest
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->lungs_chest == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->lungs_chest == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Breast examination
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->breast_examination == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->breast_examination == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Heart
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->heart == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->heart == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Skin
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->skin == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->skin == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Varicose Vein
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->varicose_vein == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->varicose_vein == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Vascular (inc. pedal pulse)
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->vascular_pulse == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->vascular_pulse == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Abdomen and viscera
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->abdomen_viscera == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->abdomen_viscera == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Hernia
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->hernia_2 == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->hernia_2 == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Anus (not rectal exam)
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->anus == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->anus == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            G-U system
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->g_u_system == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->g_u_system == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Upper and lower extremities
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->upper_lower_ext == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->upper_lower_ext == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Spine (C/s, T/S, L/S)
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->spine == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->spine == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Neurologic (full/brief)
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->neurologic == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->neurologic == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row">
                        <div  class="part_b_tb_col_1 s_float">
                            Psychiatric
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->psychiatric == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->psychiatric == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>
                    <div class="part_b_table_row" style="border: none;">
                        <div  class="part_b_tb_col_1 s_float">
                            General appearance
                        </div>
                        <div  class="part_b_tb_col_2 s_float">
                            @if($form->general_appearance == 1)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                        <div  class="part_b_tb_col_3 s_float">
                            @if($form->general_appearance == 0)
                                <i class="fa fa-check">&#xf00c;</i>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- row one end -->
        <div class="row_two clear" style="height: 100px;">
            <div class="chest_x_ray">
                <p class="chest_text">Chest X-ray</p>
                <div class="ch_row">
                    <div class="not_per s_float">
                        @if($form->chest_xray == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        Not performed
                    </div>
                    <div class="per s_float">
                        <div class="per_text s_float">
                            @if($form->chest_xray == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                                    Performed on (day/month/year):
                                </div>
                                <div class="per_value s_float">
                            {{ $form->date_of_performed_on }}
                        </div>
                    </div>

                </div>
                <div class="ch_row" style="margin-top: 50px;">
                    <div class="not_result s_float">

                    </div>
                    <div class="per_2 s_float">
                        <div class="result_text s_float">
                            Result:
                        </div>
                        <div class="result_value s_float">
                            {{ $form->date_of_results_on }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- row two end -->

        <div class="row_three clear">
            <div class="other_sec">
                <p class="other_dio_text">Other diagnostic test(s) and result(s):</p>
                <div class="o_dio_row">
                    <div class="o_test s_float">
                        <div class="o_test_text s_float">
                            Test:
                        </div>
                        <div class="o_test_value s_float">
                            <p>{{ $form->d_test }}</p>
                        </div>
                    </div>
                    <div class="o_result s_float">
                        <div class="o_result_text s_float">
                            Results:
                        </div>
                        <div class="o_result_value s_float">
                            <p>{{  $form->d_result }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="medical_practition clear">
                <p class="medi_text">Medical practitioner’s comments and assessment of fitness, with reasons for any limitations.</p>
                <h2 class="fit_text">Fit For Duty on board Ship</h2>
            </div>
        </div>
        <!-- rwo three end -->
        <div class="row_four clear">
            <div class="assesment_sce">
                <p class="ass_text">Assessment of fitness for service at sea (please tick)</p>
                <p class="ass_text_2">On the basis of the seafarer’s personal declaration, my clinical examination and diagnostic test results recorded above, I declare the seafarer medically:</p>
                <div class="ass_select">
                    <div class="ass_select_row">
                        <div class="ass_fit s_float">
                            @if($form->chest_xray == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                            Fit for look out duty
                        </div>
                        <div class="ass_unfit s_float">
                            @if($form->fit_for_lookout_duty == '0')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                                Unfit for lookout duty
                        </div>
                    </div>
                    <div class="ass_select_row" style="margin-top: 15px;">
                        <div class="ass_fit s_float">
                            <i @class([
                                'far fa-check-square' => $form->visual_aid,
                                'far fa-square' => !$form->visual_aid])>
                            </i>
                            Visual aid required
                        </div>
                        <div class="ass_unfit s_float">
                            <i @class([
                                'far fa-check-square' => !$form->visual_aid,
                                'far fa-square' => $form->visual_aid])>
                            </i>
                            Visual aid not required
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row four end -->
        <div class="row_five clear">
            <div class="fit_unfit_table">
                <div class="fit_unfit_row">
                    <div class="fit_unfit_col s_float">

                    </div>
                    <div class="fit_unfit_col s_float">
                        Deck Service
                    </div>
                    <div class="fit_unfit_col s_float">
                        Engine Service
                    </div>
                    <div class="fit_unfit_col s_float">
                        Catering Service
                    </div>
                    <div class="fit_unfit_col s_float" style="border: none;">
                        Other Service
                    </div>
                </div>
                <div class="fit_unfit_row_2">
                    <div class="fit_unfit_col s_float">
                        Fit
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_desk == 1)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_engine == 1)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_catering == 1)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float" style="border: none;">
                        @if($form->fit_other == 1)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                </div>
                <div class="fit_unfit_row_2">
                    <div class="fit_unfit_col s_float">
                        Unfit
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_desk == 0)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_engine == 0)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float">
                        @if($form->fit_catering == 0)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                    <div class="fit_unfit_col s_float" style="border: none;">
                        @if($form->fit_other == 0)
                            <i class="fas fa-check"></i>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class=" b_m_footer_m_4">
            <p class="m_foot_1">Page 4 of 5</p>
            <p class="m_foot_2">RECORD OF MEDICAL EXAMINTAION OF SEAFARERS - September 2021</p>
        </div>
    </div>
</div>
{{--page5--}}
<div class="page-break"></div>
<div class="all">
    <div class="annex_all">
        <div class="row_one">
            <div class="w_rest_row">
                <div class="w_rest_col s_float">
                    <i @class([
                         'far fa-check-square' => ! $form->with_restrictions,
                         'far fa-square' => $form->with_restrictions,
                    ])></i>
                    Without restrictions
                </div>
                <div class="w_rest_col s_float">
                    <i @class([
                         'far fa-check-square' => $form->with_restrictions,
                         'far fa-square' => ! $form->with_restrictions,
                    ])></i>
                    With restrictions
                </div>
            </div>

            <div class="res_des clear">
                <p class="res_des_text">Description of restrictions (e.g. specific position, type of ship, trading area etc.)</p>
                <p class="res_des_text">{{ $form-> description_restrictions }}</p>
            </div>
        </div>

        <div class="row_two">
            <div class="res_date_sign">
                <div class="res_date s_float">
                    <p>Date</p>
                </div>
                <div class="res_sign s_float">
                    <p>Signature of Medical Practitioner</p>
                </div>
                <div class="res_medical s_float">
                    <p>Medical Practitioner’s name, licence number, address</p>
                </div>
            </div>
        </div>
        <div class=" b_m_footer_m_5">
            <p class="m_foot_1">Page 3 of 5</p>
            <p class="m_foot_2">RECORD OF MEDICAL EXAMINTAION OF SEAFARERS - September 2021</p>
        </div>
    </div>
</div>
<div class="all">
    <div class="all_in annex_all">
        <div class="annex_head an_row_one">
            <div class="annex_col_one s_float">
                <img src="img/an.png">
            </div>
            <div class="annex_col_two s_float">
                <p class="m_text">MARITIME AND PORT AUTHORITY OF SINGAPORE</p>
                <p class="r_text">SEAFARER'S MEDICAL CERTIFICATE</p>
            </div>
            <div class="annex_col_three s_float">
                <p>ANNEX C</p>
            </div>
        </div>
        <!-- row one end -->
        <div class="row_two clear">
            <div class="cer_text">
                <p>This certificate is issued by the undersigned recognized medical practitioner to the named seafarer on behalf of the <b>Maritime and Port Authority of Singapore</b> and meets both the requirements of the International Convention on Standards of Trainings, Certification and Watchkeeping for Seafarers, 1978, as amended (STCW Convention) and the Maritime Labour Convention, 2006.</p>
            </div>
        </div>
        <!-- row two end -->
        <div class="row_three">
            <div>
                <div class="annex_c_table_one">
                    <div class="annex_c_sea_row">
                        <div class="sea_l_f_name s_float">
                            Seafarer's Name :(Last, first, middle): {{ $form->seafarers_name }}
                        </div>
                        <div class="sea_gen s_float">
                            <p>Gender</p>
                            <p>
                                @if($form->gender == 'Male')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="far fa-check">&#xf096;</i>
                                @endif
                                Male/
                                @if($form->gender == 'Female')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="far fa-check">&#xf096;</i>
                                @endif
                                Female*
                            </p>
                        </div>
                    </div>
                    <div class="annex_c_sea_row_2">
                        <div class="an_c_date s_float">
                            Date of Birth: (Day/month/year): {{ date('d/m/Y', strtotime($form->date_of_birth ?? '')) }}
                        </div>
                        <div class="an_c_nation s_float">
                            Nationality: {{ $form->nationality }}
                        </div>
                        <div class="an_place_birth s_float">
                            Place of Birth: {{ $form->place_of_birth }}
                        </div>

                    </div>
                </div>


            </div>

        </div>
        <!-- row three end -->

        <div class="row_four clear">
            <p>Declaration of the recognized medical practitioner:</p>
            <div class="annex_c_table_two">
                <div class="ann_tab_two_row">
                    <div class="ann_tab_col_1 s_float">

                    </div>
                    <div class="ann_tab_col_2 s_float">

                    </div>
                    <div class="ann_tab_col_3 s_float">
                        Yes
                    </div>
                    <div class="ann_tab_col_4 s_float">
                        No
                    </div>
                </div>
                <div class="ann_tab_two_row">
                    <div class="ann_tab_col_1 s_float">
                        1
                    </div>
                    <div class="ann_tab_col_2 s_float">
                        Identification documents were checked at the point of examination?
                    </div>
                    <div class="ann_tab_col_3 s_float">
                        @if($form->certificate->confirmation == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="ann_tab_col_4 s_float">
                        @if($form->certificate->confirmation == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ann_tab_two_row">
                    <div class="ann_tab_col_1 s_float">
                        2
                    </div>
                    <div class="ann_tab_col_2 s_float">
                        Hearing meets the standards in STCW Code Section A-I/9?
                    </div>
                    <div class="ann_tab_col_3 s_float">
                        @if($form->certificate->hearing_standard == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="ann_tab_col_4 s_float">
                        @if($form->certificate->hearing_standard == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ann_tab_two_row">
                    <div class="ann_tab_col_1 s_float">
                        3
                    </div>
                    <div class="ann_tab_col_2 s_float">
                        Unaided hearing satisfactory?
                    </div>
                    <div class="ann_tab_col_3 s_float">
                        @if($form->certificate->unaided_statisfactory == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="ann_tab_col_4 s_float">
                        @if($form->certificate->unaided_statisfactory == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ann_tab_two_row">
                    <div class="ann_tab_col_1 s_float">
                        4
                    </div>
                    <div class="ann_tab_col_2 s_float">
                        Visual acuity meets the standards in STCW Code Section A-I/9?
                    </div>
                    <div class="ann_tab_col_3 s_float">
                        @if($form->certificate->visual_acutity == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="ann_tab_col_4 s_float">
                        @if($form->certificate->visual_acutity == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ann_tab_two_row">
                    <div class="ann_tab_col_1 s_float">
                        5
                    </div>
                    <div class="ann_tab_col_2 s_float">
                        Colour vision meets the standards in STCW Code Section A-I/9?
                    </div>
                    <div class="ann_tab_col_3 s_float">
                        @if($form->certificate->color_vision_standard == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="ann_tab_col_4 s_float">
                        @if($form->certificate->color_vision_standard == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ann_tab_two_row">
                    <div class="ann_tab_col_1 s_float">

                    </div>
                    <div class="ann_tab_col_2 s_float">
                        <p style="margin-left: 40px;">Date of last colour vision test: {{ date('d/m/Y', strtotime($form->certificate->color_vision_test ?? '')) }}</p>
                    </div>
                    <div class="ann_tab_col_3 s_float">

                    </div>
                    <div class="ann_tab_col_4 s_float">

                    </div>
                </div>
                <div class="ann_tab_two_row">
                    <div class="ann_tab_col_1 s_float">
                        6
                    </div>
                    <div class="ann_tab_col_2 s_float">
                        Fit for look-out duty?
                    </div>
                    <div class="ann_tab_col_3 s_float">
                        @if($form->certificate->fit_lookout == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="ann_tab_col_4 s_float">
                        @if($form->certificate->fit_lookout == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ann_tab_two_row ann_r_7">
                    <div class="ann_tab_col_1 s_float">
                        7
                    </div>
                    <div class="ann_tab_col_2 s_float">
                        Is the seafarer free from any medical condition likely to be aggravated by service at sea or
                        to render the seafarer unfit for such service or endanger the life of person onboard?

                    </div>
                    <div class="ann_tab_col_3 s_float">
                        @if($form->certificate->unfit_for_service == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="ann_tab_col_4 s_float">
                        @if($form->certificate->unfit_for_service == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ann_tab_two_row">
                    <div class="ann_tab_col_1 s_float">
                        8
                    </div>
                    <div class="ann_tab_col_2 s_float">
                        No limitations or restrictions on fitness?
                    </div>
                    <div class="ann_tab_col_3 s_float">
                        @if($form->certificate->any_restrictions == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="ann_tab_col_4 s_float">
                        @if($form->certificate->any_restrictions == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ann_tab_two_row ann_r_eight">
                    <div class="ann_tab_col_1 s_float">

                    </div>
                    <div class="ann_tab_ex_2 s_float" style="padding-left: 4px;">
                        If "no" specify limitations or restrictions {{ $form->certificate->restrinctions_des }}
                    </div>
                </div>
                <div class="ann_tab_two_row">
                    <div class="ann_tab_col_1 s_float">
                        9
                    </div>
                    <div class="ann_tab_col_2 s_float">
                        Date of examination: <i>(day/month/year)</i> {{ date('d/m/Y', strtotime($form->certificate->doe ?? '')) }}
                    </div>
                    <div class="ann_tab_col_3 s_float">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </div>
                    <div class="ann_tab_col_4 s_float">

                    </div>
                </div>
                <div class="ann_tab_two_row ann_tab_ex_3" style="border: none;">
                    <div class="ann_tab_col_1 s_float">
                        10
                    </div>
                    <div class="ann_tab_col_2 s_float">
                        <p>Expiry of certificate: <i>(day/month/year)</i> {{ date('d/m/Y', strtotime($form->certificate->doexpiry)) }}</p>
                        <p style="font-size: 14px;"><i>** Maximum two years from date of examination unless the seafarer is under the age of 18</i></p>
                    </div>
                    <div class="ann_tab_col_3 s_float">
                        <i class="fa fa-check" aria-hidden="true"></i>
                    </div>
                    <div class="ann_tab_col_4 s_float">

                    </div>
                </div>
            </div>
        </div>
        <!-- row four end -->
        <div class="row_six clear">
            <div class="res_date_sign">
                <div class="res_date s_float">
                    <p>Date</p>
                </div>
                <div class="ann_sign s_float">
                    <p>Signature of Authorised</p>
                    <p>Medical Practitioner	</p>
                </div>
                <div class="ann_medical s_float">
                    <p>Medical Practitioner's Official stamp</p>
                    <p>(name, licence number, address etc)</p>
                </div>
            </div>
        </div>

        <!-- row six end -->
        <div class="row_seven clear">
            <p class="ann_info">I have been informed of the content of the certificate and of the right to a review.</p>
            <div class="ann_last">
                <p>Signature of Seafarer</p>
            </div>
            <p class="c_del">*delte as appropriate</p>
        </div>
        <div class=" b_m_footer_c">
            <p class="m_foot_1">Page 1 of 1</p>
            <p class="m_foot_2">SEAFARER MEDICAL CERTIFICATE - September 2021</p>
        </div>
    </div>
</div>
</body>
</html>

