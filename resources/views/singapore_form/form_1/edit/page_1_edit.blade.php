@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-6 offset-lg-1" style="font-weight: bold; font-size: 20px;">MEDICAL EXAMINATIONS OF SEAFARER</div>
    </div></br>
    <form method="POST" action="{{ route('form_page1.update', $form->id) }}">
        @csrf
        <p style="font-size: 20px;">*For identity verification purpose</p>
        <div class="row">
            <div class="col-lg-10">
                <div class="form-group">
                    <label class="form-label" for="seafarer's_name">Seafarer's Name (Last, first, middle):</label>
                    <input type="text" name="seafarers_name" class="form-control" id="seafarer's_name" value="{{ old('seafarers_name', $form->seafarers_name?? '') }}">
                </div>
            </div>
            <div class="col-lg-2">
                <div class="form-group">
                    <label class="form-label" for="gender">Gender:</label><br>
                    <input type="radio" id="gender" name="gender" value="Male" {{ $form->gender == 'Male' ? 'checked' : '' }}>
                    <label for="gender">Male</label>
                    <input type="radio" id="gender" name="gender" value="Female" {{ $form->gender == 'Female' ? 'checked' : '' }}>
                    <label for="gender">Female</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="date_of_birth">Date of Birth (day/month/year):</label>
                    <input type="date" name="date_of_birth" class="form-control" id="date_of_birth" value="{{ old('date_of_birth', $form->date_of_birth ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="nationality">Nationality:</label>
                    <input type="text" name="nationality" class="form-control" id="nationality" value="{{ old('nationality', $form->nationality ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="dept">Dept:</label><br>
                    <input type="radio" id="ept" name="dept" value="deck " {{ $form->dept == "deck" ? 'checked' : '' }}>
                    <label for="dept">Deck</label>
                    <input type="radio" id="dept" name="dept" value="engine" {{ $form->dept == "engine" ?  'checked' : '' }}>
                    <label for="dept">Engine</label>
                    <input type="radio" id="dept" name="dept" value="catering" {{ $form->dept == "catering" ?  'checked' : '' }}>
                    <label for="dept">Catering</label>
                    <input type="radio" id="dept" name="dept" value="others" {{ $form->dept == "others" ?  'checked' : '' }}>
                    <label for="dept">others</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="type_of_ship:">Type of ship:</label>
                    <input type="text" name="type_of_ship" class="form-control" id="type_of_ship:" value="{{ old('type_of_ship' , $form->type_of_ship ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="routine_duties">Routine and emergency duties:</label>
                    <input type="text" name="routine_duties" class="form-control" id="routine_duties" value="{{ old('routine_duties', $form->routine_duties ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="place_of_birth">Place of Birth:</label>
                    <input type="text" name="place_of_birth" class="form-control" id="place_of_birth" value="{{ old('place_of_birth', $form->place_of_birth ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="passport_no">*Type of ID documents: NRIC No. / Passport No:</label>
                    <input type="text" name="passport_no" class="form-control" id="passport_no" value="{{ old('passport_no', $form->passport_no ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="rank">Rank:</label>
                    <input type="text" name="rank" class="form-control" id="rank" value="{{ old('rank', $form->rank ?? '') }}">
                </div>

                <div class="form-group">
                    <label class="form-label" for="home_address">Home Address:</label>
                    <input type="text" name="home_address" class="form-control" id="home_address" value="{{ old('home_address', $form->home_address ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="trading_area">Trading area:</label><br>
                    <input type="radio" id="trading_area" name="trading_area" value="coastal" {{ $form->trading_area == "coastal" ? 'checked' : '' }}>
                    <label for="trading_area">coastal</label>
                    <input type="radio" id="trading_area" name="trading_area" value="world wide" {{ $form->trading_area == "world wide" ? 'checked' : '' }}>
                    <label for="trading_area">world wide</label>
                </div>
            </div>
            <p style="font-size: 20px;">Seafarer's Declarations (please tick)</p>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-label" for="eye_problem">1. Eye/vision problem:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="eye_problem" name="eye_problem" value="1" {{ $form->eye_problem == 1 ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="eye_problem">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="eye_problem" name="eye_problem" value="0" {{ $form->eye_problem == 0 ? 'checked' : ''}}>
                        <label for="eye_problem" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="2.High_blood_pressure">2. High blood pressure:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="2.High_blood_pressure" name="high_blood_pressure" value="1" {{ $form->high_blood_pressure == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="2.High_blood_pressure">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="2.High_blood_pressure" name="high_blood_pressure" value="0" {{ $form->high_blood_pressure == '0' ? 'checked' : '' }}>
                        <label for="2.High_blood_pressure" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="heart_disease">3. Heart/vascular disease:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="heart_disease" name="heart_disease" value="1" {{ $form->heart_disease == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="heart_disease">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="heart_disease" name="heart_disease" value="0" {{ $form->heart_disease == '0' ? 'checked' : '' }}>
                        <label for="heart_disease" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="heart_surgery">4. Heart Surgery:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="heart_surgery" name="heart_surgery" value="1" {{ $form->heart_surgery == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="heart_surgery">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="heart_surgery" name="heart_surgery" value="0" {{ $form->heart_surgery == '0' ? 'checked' : '' }}>
                        <label for="heart_surgery" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="varicose_veins">5. Varicose veins/piles:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="varicose_veins" name="varicose_veins" value="1" {{ $form->varicose_veins == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="varicose_veins">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="varicose_veins" name="varicose_veins" value="0" {{ $form->varicose_veins == '0' ? 'checked' : ''}}>
                        <label for="varicose_veins" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="asthma_bronchitis">6. Asthma/bronchitis:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="asthma_bronchitis" name="asthma_bronchitis" value="1" {{ $form->asthma_bronchitis == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="asthma_bronchitis">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="asthma_bronchitis" name="asthma_bronchitis" value="0" {{ $form->asthma_bronchitis == '0' ? 'checked' : '' }}>
                        <label for="asthma_bronchitis" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="blood_disorder">7. Blood disorder:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="blood_disorder" name="blood_disorder" value="1" {{ $form->blood_disorder == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="blood_disorder">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="blood_disorder" name="blood_disorder" value="0" {{ $form->blood_disorder == '0' ? 'checked' : '' }}>
                        <label for="blood_disorder" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="diabetes">8. Diabetes:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="diabetes" name="diabetes" value="1" {{ $form->diabetes == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="diabetes">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="diabetes" name="diabetes" value="0" {{ $form->diabetes == '0' ? 'checked' : '' }}>
                        <label for="diabetes" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="thyroid_problem">9. Thyroid problem:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="thyroid_problem" name="thyroid_problem" value="1" {{ $form->thyroid_problem == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="thyroid_problem">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="thyroid_problem" name="thyroid_problem" value="0" {{ $form->thyroid_problem == '0' ? 'checked' : '' }}>
                        <label for="thyroid_problem" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="digestive_disorder">10. Digestive disorder:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="digestive_disorder" name="digestive_disorder" value="1" {{ $form->digestive_disorder == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="digestive_disorder">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="digestive_disorder" name="digestive_disorder" value="0" {{ $form->digestive_disorder == '0' ? 'checked' : '' }}>
                        <label for="digestive_disorder" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="kidney_problem">11. Kidney problem:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="kidney_problem" name="kidney_problem" value="1" {{ $form->kidney_problem == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="kidney_problem">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="kidney_problem" name="kidney_problem" value="0" {{ $form->kidney_problem == '0' ? 'checked' : '' }}>
                        <label for="kidney_problem" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="skin_Problem">12. Skin Problem:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="skin_Problem" name="skin_problem" value="1" {{ $form->skin_problem == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="skin_Problem">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="skin_problem" name="skin_Problem" value="0" {{ $form->skin_Problem == '0' ? 'checked' : '' }}>
                        <label for="skin_Problem" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="allergies">13. Allergies:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="allergies" name="allergies" value="1" {{ $form->allergies == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="allergies">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="allergies" name="allergies" value="0" {{ $form->allergies == '0' ? 'checked' : '' }}>
                        <label for="allergies" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="infectious_diseases">14. Infectious / contagious diseases:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="infectious_diseases" name="infectious_diseases" value="1" {{ $form->infectious_diseases == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="infectious_diseases">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="infectious_diseases" name="infectious_diseases" value="0" {{ $form->infectious_diseases == '0' ? 'checked' : '' }}>
                        <label for="infectious_diseases" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="hernia">15. Hernia:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="hernia" name="hernia" value="1" {{ $form->hernia == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="hernia">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="hernia" name="hernia" value="0" {{ $form->hernia == '0' ? 'checked' : '' }}>
                        <label for="hernia" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="genital_disorder">16. Genital disorder:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="genital_disorder" name="genital_disorder" value="1" {{ $form->genital_disorder == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="genital_disorder">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="genital_disorder" name="genital_disorder" value="0" {{ $form->genital_disorder == '0' ? 'checked' : '' }}>
                        <label for="genital_disorder" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="pregnancy">17. Pregnancy:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="pregnancy" name="pregnancy" value="1" {{ $form->pregnancy == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="pregnancy">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="pregnancy" name="pregnancy" value="0" {{ $form->pregnancy == '0' ? 'checked' : '' }}>
                        <label for="pregnancy" style="font-size: 20px;">No</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-label" for="sleep_problem">18. Sleep problem:</label>
                        <input type="checkbox" style="width: 20px;
    height: 20px;" id="sleep_problem" name="sleep_problem" value="1" {{ $form->sleep_problem == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="sleep_problem">Yes</label>
                        <input type="checkbox" style="width: 20px;
    height: 20px;" id="sleep_problem" name="sleep_problem" value="0" {{ $form->sleep_problem == '0' ? 'checked' : '' }}>
                        <label for="sleep_problem" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="2.smoke_alcohol_drug">19. Do you smoke, use alcohol or drugs?:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="2.smoke_alcohol_drug" name="smoke_alcohol_drug" value="1" {{ $form->smoke_alcohol_drug == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="2.smoke_alcohol_drug">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="2.smoke_alcohol_drug" name="smoke_alcohol_drug" value="0" {{ $form->smoke_alcohol_drug == '0' ? 'checked' : '' }}>
                        <label for="2.smoke_alcohol_drug" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="operation">20. Operation/surgery:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="operation" name="operation" value="1" {{ $form->operation == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="operation">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="operation" name="operation" value="0" {{ $form->operation == '0' ? 'checked' : '' }}>
                        <label for="operation" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="epilesy_seizures">21. Epilesy/seizures:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="epilesy_seizures" name="epilesy_seizures" value="1" {{ $form->epilesy_seizures == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="epilesy_seizures">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="epilesy_seizures" name="epilesy_seizures" value="0" {{ $form->epilesy_seizures == '0' ? 'checked' : '' }}>
                        <label for="epilesy_seizures" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="dizziness">22. Dizziness/fainting:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="dizziness" name="dizziness" value="1" {{ $form->dizziness == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="epilesy_seizures">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="dizziness" name="dizziness" value="0" {{ $form->dizziness == '0' ? 'checked' : '' }}>
                        <label for="dizziness" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="loss_of_consciousness">23. Loss of consciousness:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="loss_of_consciousness" name="loss_of_consciousness" value="1" {{ $form->loss_of_consciousness == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="loss_of_consciousness">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="loss_of_consciousness" name="loss_of_consciousness" value="0" {{ $form->loss_of_consciousness == '0' ? 'checked' : '' }}>
                        <label for="loss_of_consciousness" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="psychiatric_problem">24. Psychiatric problems:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="psychiatric_problem" name="psychiatric_problem" value="1" {{ $form->psychiatric_problem == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="psychiatric_problem">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="psychiatric_problem" name="psychiatric_problem" value="0" {{ $form->psychiatric_problem == '0' ? 'checked' : '' }}>
                        <label for="psychiatric_problem" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="depression">25. Depression:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="depression" name="depression" value="1" {{ $form->depression == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="depression">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="depression" name="depression" value="0" {{ $form->depression == '0' ? 'checked' : '' }}>
                        <label for="depression" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="attempted_suicide">26. Attempted suicide:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="attempted_suicide" name="attempted_suicide" value="1" {{ $form->attempted_suicide == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="attempted_suicide">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="attempted_suicide" name="attempted_suicide" value="0" {{ $form->attempted_suicide == '0' ? 'checked' : '' }}>
                        <label for="attempted_suicide" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="loss_of_memory">27.Loss of memory:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="loss_of_memory" name="loss_of_memory" value="1" {{ $form->loss_of_memory == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="loss_of_memory">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="loss_of_memory" name="loss_of_memory" value="0" {{ $form->loss_of_memory == '0' ? 'checked' : '' }}>
                        <label for="loss_of_memory" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="balance_problem">28. Balance problem:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="balance_problem" name="balance_problem" value="1" {{ $form->balance_problem == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="balance_problem">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="balance_problem" name="balance_problem" value="0" {{ $form->balance_problem == '0' ? 'checked' : '' }}>
                        <label for="balance_problem" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="severe_headache">29. Severe headaches:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="severe_headache" name="severe_headache" value="1" {{ $form->severe_headache == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="severe_headache">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="severe_headache" name="severe_headache" value="0" {{ $form->severe_headache == '0' ? 'checked' : '' }}>
                        <label for="severe_headache" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="hearing_throat_problem">30. Ear(hearing, tinnitus/nose/throat problem:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="hearing_throat_problem" name="hearing_throat_problem" value="1" {{ $form->hearing_throat_problem == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="hearing_throat_problem">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="hearing_throat_problem" name="hearing_throat_problem" value="0" {{ $form->hearing_throat_problem == '0' ? 'checked' : '' }}>
                        <label for="hearing_throat_problem" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="restricted_mobility">31. Restricted mobility:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="restricted_mobility" name="restricted_mobility" value="1" {{ $form->restricted_mobility == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="restricted_mobility">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="restricted_mobility" name="restricted_mobility" value="0" {{ $form->restricted_mobility == '0' ? 'checked' : '' }}>
                        <label for="restricted_mobility" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="joint_problem">32. Back or joint problem:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="joint_problem" name="joint_problem" value="1" {{ $form->joint_problem == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="joint_problem">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="joint_problem" name="joint_problem" value="0" {{ $form->joint_problem == '0' ? 'checked' : '' }}>
                        <label for="joint_problem" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="amputation">33. Amputation:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="amputation" name="amputation" value="1" {{ $form->amputation == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="amputation">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="amputation" name="amputation" value="0" {{ $form->amputation == '0' ? 'checked' : '' }}>
                        <label for="amputation" style="font-size: 20px;">No</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="fracture">34. Fracture/dislocations:</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="fracture" name="fracture" value="1" {{ $form->fracture == '1' ? 'checked' : '' }}>
                        <label style="font-size: 20px;" for="fracture">Yes</label>
                        <input type="checkbox" style="width: 25px;
    height: 20px;" id="fracture" name="fracture" value="0" {{ $form->fracture == '0' ? 'checked' : '' }}>
                        <label for="fracture" style="font-size: 20px;">No</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="form-label" for="provide_details">If you answer "yes" to any of the above questions, please provide details:</label>
                        <textarea class="form-control" name="provide_details_1" id="provide_details">{{ $form->provide_details_1 ?? '' }}</textarea>
                    </div>
                </div>
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save & Next">
                </div>
            </div>
    </form>
@endsection
