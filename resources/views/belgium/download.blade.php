<!DOCTYPE html>
<html lang="en">
<head>
    <title>Belgium</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous">
</head>
<body>
<div class="kon_full">
    <div class="kon_body">
{{--        <--start of the Qr code -- >--}}
        <div class="visible-print text-center" style="padding: 2px;">
            @php
                use SimpleSoftwareIO\QrCode\Facades\QrCode;
                $path  = 'qrcodes/_'.now().$form->id.'.svg';
                QrCode::generate(route('smcseafarer_recrord.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
            @endphp
            <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
            <p>Scan me to return to the original page.</p>
        </div>
 {{--        <--end of the Qr code -- >--}}
        <div class="kon_head">
            <div class="kon_head_row_1">
                <div class="kon_head_title s_float">
                    KONINKRIJK BELGIË/ KINGDOM OF BELGIUM
                </div>
                <div class="kon_nr s_float">
                    <p>Nr/N°:</p>
                </div>
            </div>
            <div class="kon_head_row_2">
                <div class="kon_fed s_float">
                    <p class="kon_fed_title">Federale Overheidsdienst Mobiliteit en Vervoer</p>
                    <p class="kon_fed_title_2">Federal Public Service Mobility and Transport</p>
                </div>
                <div class="kon_logo s_float">
                    <img src="img/k.PNG">
                </div>
                <div class="kon_fed_2 s_float">
                    <p class="kon_fed_title">Directoraat-generaal Scheepvaart</p>
                    <p class="kon_fed_title_2">Directorate General Shipping</p>
                </div>
            </div>
        </div>

        <div class="kon_sec_1 clear">
            <h4 class="kons_sec_1_title">CERTIFICAAT VAN MEDISCHE GESCHIKTHEID / CERTIFICATE OF MEDICAL FITNESS</h4>
            <div class="perso_table">
                <div class="perso_tab_row_f" style="padding-left: 8px;">
                    Persoonsgegevens / Personal details
                </div>
                <div class="perso_tab_row_s">
                    <div class="perso_tab_sec_1 s_float">
                        Naam / Surname
                    </div>
                    <div class="perso_tab_sec_value s_float">
                        {{ $form->last_name }}
                    </div>
                </div>
                <div class="perso_tab_row_s">
                    <div class="perso_tab_sec_1 s_float">
                        Voorna(a)m(en) / First name(s)
                    </div>
                    <div class="perso_tab_sec_value s_float">
                        {{ $form->first_name }}
                    </div>
                </div>
                <div class="perso_tab_row_s">
                    <div class="perso_tab_sec_1 s_float">
                        Geboorteplaats en –datum / Place and date of birth
                    </div>
                    <div class="perso_tab_sec_value s_float">
                        <div class="perso_tab_sec_value_p s_float">
                            {{ $form->place_of_birth }}
                        </div>
                        <div class="perso_tab_sec_value_p s_float">

                        </div>
                    </div>
                </div>
                <div class="perso_tab_row_s">
                    <div class="perso_tab_sec_1 s_float">
                        Geslacht / <i>Gender</i>
                    </div>
                    <div class="perso_tab_sec_value s_float">
                        <div class="perso_tab_sec_value_p s_float">
                            @if($form->gender == 'Male')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                                M
                        </div>
                        <div class="perso_tab_sec_value_p s_float" style="border: none">
                            @if($form->gender == 'Female')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                                V/F
                        </div>
                    </div>
                </div>
                <div class="perso_tab_row_s">
                    <div class="perso_tab_sec_1 s_float">
                        Functie aan boord / <I>Function on board</I>
                    </div>
                    <div class="perso_tab_sec_value s_float">
                        {{ $form->function_on_board }}
                    </div>
                </div>
                <div class="perso_tab_row_s">
                    <div class="perso_tab_sec_1 s_float">
                        Nationaliteit / <i>Nationality</i>
                    </div>
                    <div class="perso_tab_sec_value s_float" style="border:none;">
                        {{ $form->nationality }}
                    </div>
                </div>
                <div class="perso_tab_row_s">
                    <div class="perso_tab_sec_1 s_float">
                        Nummer identiteitskaart / <i>Identification card number</i>
                    </div>
                    <div class="perso_tab_sec_value s_float" style="border:none;">
                        {{ $form->identification_number }}
                    </div>
                </div>
                <div class="perso_tab_row_s">
                    <div class="perso_tab_sec_1 s_float">
                        Nummer paspoort / <i>Passport number</i>
                    </div>
                    <div class="perso_tab_sec_value s_float" style="border:none;">
                        {{ $form->passport_number }}
                    </div>
                </div>
                <div class="perso_tab_row_s">
                    <div class="perso_tab_sec_1 s_float">
                        Nummer zeemansboekje / <i>Number seaman’s book</i>
                    </div>
                    <div class="perso_tab_sec_value s_float" style="border:none;">
                        {{ $form->number_of_seaman_book }}
                    </div>
                </div>
                <div class="perso_tab_row_s" style="border: none;">
                    <div class="perso_tab_sec_1 s_float" style="height: 30px;">
                        De identiteitsdocumenten werden gecontroleerd bij de keuring / <i>
                            Identification documents were checked at the point of examination</i>
                    </div>
                    <div class="perso_tab_sec_value s_float" style="border:none;">
                        <div class="perso_tab_sec_value_p s_float" style="height: 22px; padding-top:8px;">
                            @if($form->documents_checked == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                            JA / Yes
                        </div>
                        <div class="perso_tab_sec_value_p s_float" style="border: none; padding-top: 8px;">
                            @if($form->documents_checked == '0')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                            Nee / No
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="kon_secc_2 clear">
            <div class="kon_geg_table">
                <div class="kon_geg_tab_row_n_c" style="background: #d6e3bc; padding-left: 8px;">
                    Gegevens keuring / <i>Examination results</i>
                </div>
                <div class="kon_geg_tab_row_n_c" style="padding-left: 8px;">
                    Betreft / Concerning
                </div>
                <div class="kon_geg_tab_row_b">
                    <div class="kon_geg_tab_row_b_col s_float">
                        <div class="kon_geg_tab_icon s_float">
                            @if($form->service_watch_b == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class="kon_geg_tab_details s_float">
                            <p>Keuring dienst of wacht op brug /<i> Examination service or watch on the bridge</i></p>
                        </div>
                    </div>
                    <div class="kon_geg_tab_row_b_col s_float">
                        <div class="kon_geg_tab_icon s_float">
                            @if($form->service_watch_m == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class="kon_geg_tab_details s_float">
                            <p>Keuring dienst of wacht inde machinekamer / <i>Examination service or watch in the machinery spac</i></p>
                        </div>
                    </div>
                    <div class="kon_geg_tab_row_b_col s_float">
                        <div class="kon_geg_tab_icon s_float">
                            @if($form->seafarer_w_watch_services == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class="kon_geg_tab_details s_float">
                            <p>Keuring bemanningslid zonder wachtfunctie / <i>Examination seafarer
                                    without watchservice</i> </p>
                        </div>
                    </div>
                    <div class="kon_geg_tab_row_b_col s_float" style="border:none;">
                        <div class="kon_geg_tab_icon s_float">
                            @if($form->remainig_seafarers == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class="kon_geg_tab_details s_float">
                            <p>Keuring overige bemanningsleden / <i>Examination remaining seafarers</i></p>
                        </div>
                    </div>
                </div>
                <div class="kon_geg_tab_row_n_c" style="padding-left: 8px;">
                    Gehoor/Zicht geschikt voor / Hearing/Sight fit for
                </div>
                <div class="kon_geg_tab_row_m">
                    <div class="kon_geg_row_m_col_1 s_float">
                        <div class="kon_geg_row_m_icon s_float">
                            @if($form->sight_s_watch == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class="kon_geg_tab_row_m_details s_float">
                            <p>Dienst of wacht op de brug / <i>or watch in the machinery spaces</i></p>
                        </div>
                    </div>
                    <div class="kon_geg_row_m_col_2 s_float">
                        <div class="kon_geg_row_m_icon s_float">
                            @if($form->sight_s_watch_mach == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class="kon_geg_tab_row_m_details s_float">
                            <p>Dienst of wacht in de machinekamer /<i>Service or watch on the bridge</i></p>
                        </div>
                    </div>
                    <div class="kon_geg_row_m_col_3 s_float">
                        <div class="kon_geg_row_m_icon s_float">
                            @if($form->sight_remaing_sea == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class="kon_geg_tab_row_m_details s_float">
                            <p>Overige bemanningsleden <i>Remaining seafarers</i></p>
                        </div>
                    </div>
                </div>

                <div class="kon_geg_tab_row_n_c "  style="padding-left: 8px;">
                    Audio- of visuele hulpmiddelen / <i>Audio or visual tools</i>
                </div>
                <div class="kon_geg_tab_row_n_c" style="background: none; padding-left: 8px;">
                    <div class="kon_ver_text s_float">
                        @if($form->required_namely == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Vereist, namelijk / <i>Required, namely: </i>
                    </div>
                    <div class="kon_ver_text_2 s_float">
                        @if($form->required_namely == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Niet vereist / <i>Not required</i>
                    </div>
                </div>

                <div class="kon_geg_tab_row_n_c"  style="padding-left: 8px;">
                    Kleurenblindheid die het werk negatief kan beïnvloeden /<i>Colour blindness that can affect the work in a negative way</i>
                </div>

                <div class="kon_geg_tab_row_n_c" style="background: none; height: 24px; ">
                    <div class="kon_dat_text s_float" style="height: 24px;">
                        @if($form->color_blindness == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Ja /  <i>Yes </i> <span>

                        @if($form->color_blindness == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                              Nee / <i>No </i></span>
                    </div>
                    <div class="kon_dat_text_2 s_float">
                        <p>Datum laatste kleurenblindheidtest / <i>Date of last colour vision test</i></p>
                        <p class="kon_var_sm_text">De kleurenblindheidtest heeft een maximumgeldigheidsduur van zes jaar / <i>Colour vision assessment only needs to be conducted every six years</i></p>
                    </div>
                </div>
                <div class="kon_geg_tab_row_n_c" style="padding-left: 8px;">
                    Geldigheidsduur / Validity
                </div>
                <div class="kon_geg_tab_row_m">
                    <div class="kon_mar_icon s_float">
                        @if($form->validity_18_plus == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="kon_mar_details s_float">
                        Maximum <b>twee jaar</b> na datum van afgifte, tenzij de zeevarende jonger is dan 18 jaar / <i>Maximum period of <b>two years</b> after date of issue unless the seafarer is under the age of 18</i>
                    </div>

                </div>
                <div class="kon_geg_tab_row_m">
                    <div class="kon_mar_icon s_float">
                        @if($form->validity_18_under == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="kon_mar_details s_float">
                        Maximum <b>één jaar </b> na datum van afgifte voor bemanningsleden jonger dan 18 jaar <i>Maximum period of <b>one year</b> after date of issue if the seafarer is under the age of 18</i>
                    </div>

                </div>
                <div class="kon_geg_tab_row_n_c" style="padding-left: 0px; background: none;">
                    <div class="kon_mar_icon s_float" style="padding-top: 0px;">
                        @if($form->validity_other == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="kon_mar_details s_float">
                        <b>Andere</b>, namelijk / <i><b>Others</b>, namely</i>
                    </div>

                </div>

                <div class="kon_geg_tab_row_m" style="background: #c6d9f1;">
                    <p class="kon_af_text"> Afwijkingen of beperkende voorwaarden (vb. tijd, bijzondere functie, soort werk, vaargebied) / <i>Exemptions or restrictive conditions (e.g. time, special function, field of work, navigation area):</i></p>
                </div>

                <div class="kon_geg_tab_row_m" style="background: none; height: 24px;">
                    <div class="kon_dat_text s_float" style="height: 24px; padding-left: 8px;">
                        @if($form->restrictive_condition == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                            @endif
                            Ja /  <i>Yes </i>
                        <span>
                           @if($form->restrictive_condition == '0')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif

                        Nee / <i>No </i></span>
                    </div>
                    <div class="kon_dat_text_2 s_float">
                        Omschrijving / Description: {{ $form->description }}
                    </div>
                    <div class="kon_geg_tab_row_lg ">
                        <div class="kon_de_bet_details s_float">
                            De betrokkene lijdt aan een medische aandoening die mogelijk wordt verergerd door werkzaamheden op zee of hem/haar ongeschikt maakt voor dergelijke werkzaamheden of de gezondheid van andere opvarenden in gevaar kan brengen / <i>The person concerned is suffering from any medical condition likely to be aggravated by service at sea or to render him/her unfit for such service or to endanger the health of other persons on board</i>
                        </div>
                        <div class="kon_de_bet_details_value_d s_float">
                            <p>
                                @if($form->person_suffering == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                                     Ja /  <i>Yes </i> </p>
                                <p>
                                     @if($form->person_suffering == '0')
                                         <i class="fa fa-check">&#xf046;</i>
                                     @else
                                <i class="fa fa-check">&#xf096;</i>
                                @endif
                                Nee /  <i>NO </i> </p>
                        </div>
                    </div>

                    <div class="kon_geg_tab_row_n_c" style="padding-left: 8px;">
                        <b>Aanbevelingen vaccinatiestatus en/of opmerkingen / <i>Recommendations vaccination status and/or remarks</i></b>
                    </div>
                </div>

            </div>
        </div>
        <!-- sec_2 end -->

        <div class="kon_sec_3 clear">
            <div class="kon_ond_table">
                <div class="ond_table_row_1">
                    <P>Ondergetekende geneesheer, erkend door de met de scheepvaartcontrole belaste ambtenaar die daartoe aangesteld is, verklaart dat / <i>The undersigning medical practitioner, recognised by a duly authorised official, declares that:</i></P>
                </div>
                <div class="ond_table_row_2">
                    <p>de heer/mevrouw /<i> mister/misses</i></p>
                </div>
                <div class="ond_table_row_3">
                    <p><b><u>medisch geschikt is</u></b> overeenkomstig artikel 102 en bijlage XX van het koninklijk besluit van 20 juli 1973 houdende zeevaart-inspectiereglement, het STCW-verdrag, in het bijzonder afdeling A-I/9 van de STCW-code en MLC 2006, in het bijzonder voorschrift 1.2 / <i><b>is medically fit</b> in accordance with article 102 and annex XX of the royal decree of 20 July 1973 concerning the maritime inspection code, the STCW convention, in particular section A-I/9 of the STCW-code and MLC 2006, in particular regulation 1.2.</i></p>
                </div>
                <div class="ond_table_row_4">
                    <div class="ond_row_sec_1 s_float">
                        <p>Het bemanningslid bevestigt dat hij/zij werd geïnformeerd over de inhoud van het certificaat en over het recht tot een herkeuring in overeenstemming met artikel 4 van bijlage XX van het koninklijk besluit van 20 juli 1973 houdende zeevaartinspectiereglement, paragraaf 6 van sectie A-1/9 van de STCW code en norm A1.2.5 van MLC 2006 / The seafarer confirms that he/she has been informed of the content of the certificate and of the right to a review in accordance with article 4 of annex XX of the royal decree of 20 July 1973 concerning the maritime inspection code, paragraph 6 of section A-I/9 of the STCW-code and standard A1.2.5 of MLC 2006.</p>
                    </div>
                    <div class="ond_row_sec_2 s_float">
                        <p>
                            @if($form->maritime_inspection == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                            Ja /  <i>Yes </i> </p>
                        <p>
                            @if($form->maritime_inspection == '0')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                            Nee /  <i>NO </i> </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- sec_3 end -->
        <div class="kon_sec_4">
            <div class="kon_foot_table">
                <div class="kon_foot_tab_row_1">
                    <div class="fot_tab_dat s_float">
                        <p>Keuringsplaats en –datum /<i> Place and date of examination:</i> {{ $form->date_of_exam }}</p>
                    </div>
                    <div class="fot_tab_dat_value s_float">
                        <p>Geldig tot /<i> Valid until:</i> {{ $form->date_of_validity }}</p>
                    </div>
                </div>
                <div class="kon_foot_tab_row_2">
                    <div class="fot_tab_na s_float">
                        <p>Naamstempel en handtekening geneesheer / </p>
                        <p><i>Name stamp and signature medical practitioner</i></p>
                    </div>
                    <div class="fot_tab_na_value s_float">
                        <p>Handtekening bemanningslid / <i>Signature seafarer</i></p>
                    </div>
                </div>
                <div class="kon_foot_tab_row_3">
                    <div class="fot_tab_nan s_float">

                    </div>
                    <div class="fot_tab_nan_value s_float">

                    </div>
                </div>

            </div>
        </div>
        <!-- sec_4 end -->
        <div class="kon_sec_five">
            <div class="kon_web s_float ">
                <a href="">www.mobilit.belguum.be</a>
            </div>
            <div class="kon_foot_date s_float">
                <p>19-04-2018</p>
            </div>
            <div class="kon_be s_float">
                <p>.be</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
