@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <div class="row">
        <div class="col-lg-12 text-center">
            <div>
                SEAFARER'S PHYSICAL EXAMINATION REPORT/CERTIFICATE
            </div>
        </div>
    </div>
    <form class="form-control" action="{{ route('belgium.update', $form->id) }}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="last_name"><span class="text-danger">*</span>Naam/Surname</label>
                    <input type="text" class="form-control" name="last_name" id="last_name" value="{{ old('last_name', $form->last_name ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="first_name"><span class="text-danger">*</span>Voorna(a)m(en) /First name(s)</label>
                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{ old('first_name', $form->first_name ?? '') }}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="">
                        <label class="form-label" for="place_of_birth">Place of birth</label>
                        <input type="text" class="form-control" name="place_of_birth" id="place_of_birth" value="{{ old('place_of_birth', $form->place_of_birth ?? '') }}">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="dob">Date of birth:</label>
                    <input type="date" class="form-control" name="dob" id="dob" value="{{ old('dob', $form->dob ?? '') }}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="document_no"><span class="text-danger">*</span>Gender:</label>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="male">Male:</label>
                    <input style="width: 25px; height: 20px;" type="checkbox"  name="gender" id="male" value="Male" {{ $form->gender == 'Male' ? 'checked' : '' }}>
                    <label class="form-label text-capitalize" for="female">Female:</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="gender" id="female" value="Female" {{ $form->gender == 'Female' ? 'checked' : '' }}>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-class" for="function_on_board">Function on board</label>
                    <input class="form-control" type="text" name="function_on_board" id="function_on_board" value="{{ old('function_on_board', $form->function_on_board ?? '') }}">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="nationality">Nationality:</label>
                    <input  class="form-control" type="text" name="nationality" id="nationality" value="{{ old('nationality', $form->nationality ?? '') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-class" for="identification_number">Identification Card Number</label>
                    <input class="form-control" type="text" name="identification_number" id="identification_number" value="{{ old('identification_number', $form->identification_number ?? '') }}">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="passport_number">Passport Number:</label>
                    <input  class="form-control" type="text" name="passport_number" id="passport_number" value="{{ old('passport_number', $form->passport_number ?? '') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="number_of_seaman_book"><span class="text-danger">*</span>Number Seaman's Book:</label>
                    <input type="text" class="form-control" name="number_of_seaman_book" id="number_of_seaman_book"  value="{{ old('number_of_seaman_book', $form->number_of_seaman_book ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <label class="form-label" style="text-transform: uppercase" for=""> Identification documents were checked at the point of examination:</label>
                <input style="width: 25px; height: 20px;" type="checkbox" name="documents_checked" value="1" id="identi_yes" {{ $form->documents_checked == '1' ? 'checked' : '' }}>
                <label class="form-label" for="identi_yes">Yes</label>
                <input style="width: 25px; height: 20px;" type="checkbox" name="documents_checked" value="0" id="identi_yes" {{ $form->documents_checked == '0' ? 'checked' : '' }}>
                <label class="form-label" for="identi_yes">No</label>
            </div>
            <div class="col-lg-12">
                <hr>
                <h6 class="text-center" style="color: #95a2af; text-transform: uppercase">  Examination results</h6>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group text-center" style="font-size: 1.5em;">
                </div>
                <div class="form-group">
                    Concerning
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="service_watch_b" id="service_watch_b" value="1" {{ $form->service_watch_b == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="service_watch_b">Examination service or watch on the bridge</label>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="service_watch_m" id="service_watch_m" value="1" {{ $form->service_watch_m == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="service_watch_m">Examination service or watch in the machinery space</label>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="seafarer_w_watch_services" id="seafarer_w_watch_services" value="1" {{ $form->seafarer_w_watch_services == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="seafarer_w_watch_services">Examination seafarer without watchservice</label>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="remainig_seafarers"  id="remainig_seafarers" value="1" {{ $form->remainig_seafarers == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="remainig_seafarers">Examination Remaining seafarers</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                Hearing/Sight fit for
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="sight_s_watch" id="sight_s_watch" value="1" {{ $form->sight_s_watch == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="sight_s_watch">Service or watch on the bridge</label>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="sight_s_watch_mach" id="sight_s_watch_mach" value="1" {{ $form->sight_s_watch_mach == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="sight_s_watch_mach">Service or watch int machinery space</label>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="sight_remaing_sea" id="sight_remaing_sea" value="1" {{ $form->sight_remaing_sea == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="sight_remaing_sea">Remaining Seafarers</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                Audio or visual tools
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="required_namely" value="1" id="required_namely_yes" {{ $form->required_namely == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="required_namely_yes">Required, namely:</label>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="required_namely" value="0" id="required_namely_no" {{ $form->required_namely == '0' ? 'checked' : '' }}>
                    <label class="form-label" for="required_namely_no">Not Required</label>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="color_blindness">
                        Colour blindness that can affect the work in a negative way
                    </label>
                    <br>
                    <label class="form-label" for="color_blindness">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="color_blindness" id="color_blindness" value="1" {{ $form->color_blindness == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="color_blindness_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="color_blindness" id="color_blindness_no" value="0" {{ $form->color_blindness == '0' ? 'checked' : '' }}>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="color_v_test">Date of last color vision test:</label>
                    <input class="form-control" type="date"  name="color_v_test" id="color_v_test" value="{{ old('color_v_test', $form->color_v_test ?? '') }}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <label class="form-label">
                    Validity
                </label>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="validity_18_plus" id="validity_18_plus" value="1" {{ $form->validity_18_plus == '1' ? 'checked' : '' }}>
                    <label class="form-group" for="validity_18_plus">Maximum period of two years after date of issue unless the seafarer is under the age of 18</label>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="validity_18_under" id="validity_18_under" value="1" {{ $form->validity_18_under == '1' ? 'checked' : '' }}>
                    <label class="form-group" for="validity_18_under">Maximum period of one year after date of issue if the seafarer is under the age of 18</label>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group">
                    <input type="checkbox" style="width: 25px; height: 20px;" name="validity_other" id="validity_other" value="1" {{ $form->validity_other == '1' ? 'checked' : '' }}>
                    <label class="form-group" for="validity_other">Validity others</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="restrictive_condition">Exemptions of restrictive conditions:</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="restrictive_condition">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="restrictive_condition" id="restrictive_condition" value="1" {{ $form->restrictive_condition == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="restrictive_condition_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="restrictive_condition" id="restrictive_condition_no" value="0" {{ $form->restrictive_condition == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label class="form-label" for="description">Description</label>
                <textarea class="form-control" name="description" id="description">{{ old('description', $form->description ?? '') }}</textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="person_suffering">The person concerned is suffering from any medical condition likely to be
                        aggravated by service at sea or to render him/her unfit for such service or to endanger the health of the other persons on board
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="person_suffering">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="person_suffering" id="person_suffering" value="1" {{ $form->person_suffering == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="person_suffering_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="person_suffering" id="person_suffering_no" value="0" {{ $form->person_suffering == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="maritime_inspection">The seafarer confirms that he/she has been informed of the content of the certificate and of the right to a review in accordance with article 4 of annex XX of the royal decree of 20 july 1973 concerning the
                        the maritime inspection code, paragraph 6 of section A-I/9 of the STCW-code and standard A1.2.5 of MLC 2006.
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="maritime_inspection">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="maritime_inspection" id="maritime_inspection" value="1" {{ $form->maritime_inspection == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="maritime_inspection_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="maritime_inspection" id="maritime_inspection_no" value="0" {{ $form->maritime_inspection == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="date_of_exam">Date of Examination:</label>
                    <input class="form-control" type="date" name="date_of_exam" id="date_of_exam" value="{{ old('date_of_exam', $form->date_of_exam ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="date_of_expiration">Date of Expiration:</label>
                    <input class="form-control" type="date" name="date_of_validity" id="date_of_validity" value="{{ old('date_of_validity', $form->date_of_validity ?? '') }}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>
    </form>
@endsection
