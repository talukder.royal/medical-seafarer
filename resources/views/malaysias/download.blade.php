<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{{ asset('public/js/style.js') }}"></script>
</head>
<body>
<div class="mal_all">
    <div class="mal_body">
        <div class="mal_head">
            <div class="visible-print text-center" style="padding: 2px;">
                @php
                    use SimpleSoftwareIO\QrCode\Facades\QrCode;
                    $path  = 'qrcodes/_'.now().$form->id.'.svg';
                    QrCode::generate(route('smcseafarer_recrord.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
                @endphp
                <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
                <p>Scan me to return to the original page.</p>
            </div>
            <div class="mal_logo s_float">
                <img src="img/ma.PNG">
            </div>
            <div class="mal_t_r_sec_1 s_float">
                <h1 class="mal_heac_t_1">KERAJAAN MALAYSIA</h1>
                <h1 class="mal_heac_t_1_2"><i>GOVERNMENT OF MALAYSIA</i></h1>
                <p class="mal_head_ad_1">Marine Headquarters, Marine Department Malaysia, P.O Box 12,42007 Port Klang</p>
                <p class="mal_head_ad_2">Tel: 03 - 3346 7777 , Fax: 03 - 3168 5289, E-mail: kpgr@marine.gov.mv Website: http://www.marine.gov.my</p>
            </div>
        </div>
        <div class="mal_head_2 clear">
            <h1 class="mal_sij">SIJ IL PEMERIKSAAN PERU BATAN</h1>
            <h2 class="mal_med_exam"><i>MEDICAL EXAMINATION CERTIFICATE</i></h2>
        </div>

        <div class="mal_row_1">
            <div class="no_and_text s_float">
                <div class="mal_no s_float">
                    1)
                </div>
                <div class="meal_nam_hold s_float">
                    <p>Nama pemegang sijil (seperti dalam passport):</p>
                    <p><i>Name oJ holder oJ Certificate (as in possport):</i></p>
                </div>
            </div>
            <div class="mal_family_name s_float">
                <p class="fam_name_text"><i>Family name</i></p>
                <p class="fam_name_value">{{ $form->family_name }}</p>
            </div>
            <div class="mal_giv_name s_float">
                <p class="giv_name_text"><i>Given name(s)</i></p>
                <p class="giv_name_vlue">{{ $form->given_name }}</p>
            </div>
        </div>

        <div class="mal_row_2">
            <div class="no_and_text_2 s_float">
                <div class="mal_no s_float">
                    2)
                </div>
                <div class="meal_gen s_float">
                    <p>*Jantina: Lelaki / Wanita</p>
                    <p><i>Gentler:
                            @if($form->gender == 'Male')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                                @endif
                            Male I
                            @if($form->gender == 'Female')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                                @endif
                            Female</i></p>
                </div>
            </div>
            <div class="mal_nat_name s_float">
                <p class="fam_nat_text">3) Warganegara:</p>
                <p class="fam_nat_text_2"><i>Nationalit:</i></p>
            </div>
            <div class="mal_nat_value s_float">
                 {{ $form->nationality }}
            </div>
            <div class="mal_no_kad_name s_float">
                <p class="giv_no_text">4) No. Kad Pelaut:</p>
                <p class="giv_no_text_2"><i>Seaman Card l,{o.:</i></p>
            </div>
            <div class="mal_no_kad_value s_float">
                {{ $form->seaman_id }}
            </div>
        </div>
        <div class="mal_row_3">
            <div class="mal_no s_float">
                5)
            </div>
            <div class="mal_id_card s_float">
                <p>No. Kad Penger.ralan/Passport:</p>
                <p><i>Identity' Card NolPassport .:</i></p>
            </div>
            <div class="mal_id_card_value s_float">
                {{ $form->passport_no }}
            </div>


            <div class="mal_d_birth s_float">
                <p>6) Tarikh Lahir (dd/mm/yyyy):</p>
                <p><i>Date of Birth:</i></p>
            </div>
            <div class="mal_d_birth_value s_float">
                {{ date('d/m/Y', strtotime($form->dob ?? ''))}}
            </div>
        </div>
        <div class="mal_row_4">
            <div class="mal_no s_float">
                7)
            </div>
            <div class="mal_pro s_float">
                <p>Jawatan:</p>
                <p><i>Profession:</i></p>
            </div>
            <div class="mal_pro_value s_float">
                {{ $form->occupation }}
            </div>

        </div>

        <div class="mal_row_8">
            <div class="mal_no s_float">
                8)
            </div>
            <div class="mal_pro s_float">
                <p>Pengakuan oleh Pengamal Perubatan yang Diiktiraf:</p>
                <p><i>Declaration of the Recognized Metlical Practitioner:</i></p>
            </div>


        </div>
        <div class="mal_row_9 clear">
            <div class="no_con s_float">

            </div>
            <div class="mal_y_n s_float">
                <p class="mal_yes_r s_float">Yes/Ya</p>
                <p class="mal_no_r s_float">No/Tidak</p>
            </div>
        </div>

        <div class="mal_row_same clear">
            <div class="mal_no s_float">
                8.1
            </div>
            <div class="mal_same_con s_float">
                <p>Pengesahan dokumen pengenalan telah disemak ketika pemeriksaan</p>
                <p><i>Confirmation that identiticcrtion docwnetlts were chec'ketl at the point oJ erarnination</i></p>
            </div>
            <div class="mal_y_n s_float">
                <p class="mal_yes_i s_float">
                    @if($form->confirmation == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
                <p class="mal_no_i s_float">
                    @if($form->confirmation == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
            </div>
        </div>

        <div class="mal_row_same clear">
            <div class="mal_no s_float">
                8.2
            </div>
            <div class="mal_same_con s_float">
                <p>Pendengaran menepati piawaian mengikut seksyen A-I/9 Konvensyen STCW 78 seperti dipinda</p>
                <p><i>Hearing meets the stdndards in sectiott A-l/9 of the STC\Y 78 o.s amended</i></p>
            </div>
            <div class="mal_y_n s_float">
                <p class="mal_yes_i s_float">
                    @if($form->hearing_standard == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
                <p class="mal_no_i s_float">
                    @if($form->hearing_standard == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
            </div>
        </div>

        <div class="mal_row_same clear">
            <div class="mal_no s_float">
                8.3
            </div>
            <div class="mal_same_con s_float">
                <p>Pendengaran memuaskan tanpa apa-apa bantuan?</p>
                <p><i>Unaided hearings satisfactory? </i></p>
            </div>
            <div class="mal_y_n s_float">
                <p class="mal_yes_i s_float">
                    @if($form->unaided_statisfactory == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
                <p class="mal_no_i s_float">
                    @if($form->unaided_statisfactory == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
            </div>
        </div>

        <div class="mal_row_same clear">
            <div class="mal_no s_float">
                8.4
            </div>
            <div class="mal_same_con s_float">
                <p>Ketajaman penglihatan menepati piar,vaian mengikut seksyen A-I/9 Konvensyen STCW 78 seperti dipinda?</p>
                <p><i>Visual acttirl- meets standords in section A-ll9 o.f the S'|CW 78 as amended?</i></p>
            </div>
            <div class="mal_y_n s_float">
                <p class="mal_yes_i s_float">
                    @if($form->visual_acutity == '1')
                       <i class="fa fa-check">&#xf046;</i>
                    @else
                       <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
                <p class="mal_no_i s_float">
                    @if($form->visual_acutity == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
            </div>
        </div>
        <div class="mal_row_same clear">
            <div class="mal_no s_float">
                8.5
            </div>
            <div class="mal_same_con s_float">
                <p>Penglihatan warna menepati piaivaian mengikut seksyen A-I/9 Konvensyen STCW 78 seperti dipinda?</p>
                <p><i>Colour ,-ision m.eets standttrcls in section A-ll9 o.f the STCW 78 as omendetl?</i></p>
            </div>
            <div class="mal_y_n s_float">
                <p class="mal_yes_i s_float">
                    @if($form->color_vision_standard == '1')
                         <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
                <p class="mal_no_i s_float">
                    @if($form->color_vision_standard == '0')
                       <i class="fa fa-check">&#xf046;</i>
                    @else
                       <i class="fa fa-check">&#xf096;</i>
                   @endif
                </p>
            </div>
        </div>
        <div class="mal_row_same clear">
            <div class="mal_no s_float">

            </div>
            <div class="mal_same_con s_float">
                <div class="mal_d_l_color s_float">
                    <p>Tarikh terakhir ujian penglihatan warna:</p>
                    <p><i>Date of las colour vission text</i></p>
                </div>
                <div class="mal_d_l_color_value">
                     {{ date('d/m/Y', strtotime($form->color_vision_test ?? '')) }}
                </div>
            </div>
            <div class="mal_y_n s_float">

            </div>
        </div>

        <div class="mal_row_same clear">
            <div class="mal_no s_float">
                8.6
            </div>
            <div class="mal_same_con s_float">
                <p>Layak untuk tugas peninjauan?</p>
                <p><i>Fit for look-oLrt duties?</i></p>
            </div>
            <div class="mal_y_n s_float">
                <p class="mal_yes_i s_float">
                    @if($form->fit_lookout == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
                <p class="mal_no_i s_float">
                    @if($form->fit_lookout == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
            </div>
        </div>

        <div class="mal_row_same clear">
            <div class="mal_no s_float">
                8.7
            </div>
            <div class="mal_same_con s_float">
                <p>Tiada had atau sekatan dari aspek kecergasan?</p>
                <p><i>No \imitcttiorts or restriction on.fitness?</i></p>
            </div>
            <div class="mal_y_n s_float">
                <p class="mal_yes_i s_float">
                    @if($form->any_restrictions == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
                <p class="mal_no_i s_float">
                    @if($form->any_restrictions == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
            </div>
        </div>
        <div class="mal_row_same clear">
            <div class="mal_no s_float">

            </div>
            <div class="mal_same_con s_float">
                <div class="mal_d_l_color_2 s_float">
                    <p>Jika "Tidak", nyatakan had dan sekatan:</p>
                    <p><i>If "No", specify limitations or reslrictions:</i></p>
                </div>
                <div class="mal_d_l_color_2_value">
                        {{ $form->limit_restriction }}
                </div>
            </div>
            <div class="mal_y_n s_float">

            </div>
        </div>

        <div class="mal_row_same clear">
            <div class="mal_no s_float">
                8.8
            </div>
            <div class="mal_same_con s_float">
                <p>Adakah pelaut bebas dari apa-apa keadaan perubatan yang mungkin dimudharatkan melalui perkhidmatan iaut atau boleh menyebabkan seseorang pelaut tidak layak untuk perkhidmatan sedemikian atau mungkin membahayakan kesihatan mana-mana orang di atas kapal?</p>

            </div>
            <div class="mal_y_n s_float">

            </div>
        </div>
        <div class="mal_row_same clear">
            <div class="mal_no s_float">

            </div>
            <div class="mal_same_con s_float">
                <p><i>is the seafctrerJree Jrom anv- medical condition likely to be nggravatedby servic:e at sea or to render
                        the seafarer unfit.for snc'h seryice or to endanger the o.fother persons on board?</i></p>

            </div>
            <div class="mal_y_n s_float">
                <p class="mal_yes_i s_float">
                    @if($form->unfit_for_service == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
                <p class="mal_no_i s_float">
                    @if($form->unfit_for_service == '0')
                        <i class="fa fa-check">&#xf046;</i>
                     @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
            </div>
        </div>
    </div>
</div>
<div class="mal_all">
    <div class="mal_body_2">
        <div class="mal_head_3">
            <p>JLAHEPP/D/09 - 1</p>
            <p>No:JLM <span class="mal_h_2_l_no">153809</span></p>
        </div>
        <div class="mal_cer_detail">
            <p>Saya mengesahkan bahawa saya telah memeriksa pelaut seperti di atas mengikut standard perubatan dan penglihatan Malaysia</p>
            <p><i>I certfy that I have examined the above-named seafarer to standards of the medical and eyesight of Malaysi</i></p>
            <p>sepertimana dalam Kaedah-Kaedah Perkapalan Saudagar (Pemeriksaan Perubatan) 1999 seperti pindaan,</p>
            <p><i>as in the Merchant Shipping (Medical Examination) Rules 1999 as amended,</i></p>
            <p>dan didapati beliau *layak atau tidak layak untuk menjalankan tugas pelaut dengan pembatasan-pembatasan berikut:</p>
            <p><i>and have found him to be xfit or unfit for seafaring subject to the following restrictions:</i></p>
        </div>
        <div class="mal_2_res_details">

        </div>
        <div class="mal_row_9_1">
            <div class="mal_no s_float">
                9)
            </div>
            <div class="mal_cat_text s_float">
                <p>Kategori Kecergasan Perubatan:</p>
                <p><i>Category of Medical Fitness:</i></p>
            </div>
            <div class="mal_cat_value s_float">
                {{ $form->medical_fitness }}
            </div>

        </div>

        <div class="mal_row_10">
            <div class="mal_no s_float">
                10)
            </div>
            <div class="mal_2_d_exam s_float">
                <p>Tarikh pemeriksaan (dd/mm/yyyy)</p>
                <p><i>Date of Examination</i></p>
            </div>
            <div class="mal_2_d_exam_value s_float">
                 {{ date('d/m/Y', strtotime( $form->doe ?? '')) }}
            </div>


            <div class="mal_2_e_d s_float">
                <p>11) Tarikh luput sijil (dd/mm/yyyy):</p>
                <p><i>Expiry date of certificate:</i></p>
            </div>
            <div class="mal_2_e_d_value s_float">
                {{ date('d/m/Y', strtotime( $form->doexpiry ?? '')) }}
            </div>
        </div>

        <div class="mal_row_11 clear">
            <div class="mal_no s_float">
                12)
            </div>
            <div class="mal_2_sign s_float">
                <p>Tandatangan pelaut:</p>
                <p><i>Signature of seafarer:</i></p>
            </div>
            <div class="mal_2_sign_value s_float">

            </div>
        </div>
        <div class="mal_row_12 clear">
            <div class="mal_no s_float">
                13)
            </div>
            <div class="mal_2_sign_m s_float">
                <p>Nama pengamal perubatan:</p>
                <p><i>Name of medical practitioner:</i></p>
            </div>
            <div class="mal_2_sign_m_value s_float">

            </div>
        </div>
        <div class="mal_row_12 clear">
            <div class="mal_no s_float">
                14)
            </div>
            <div class="mal_2_sign_m_m s_float">
                <p>Tandatangan pengamal perubatan:</p>
                <p><i>Signature of medical practitioner:</i></p>
            </div>
            <div class="mal_2_sign_m_mvalue s_float">

            </div>
        </div>
        <div class="mal_row_12 clear">
            <div class="mal_no s_float">
                15)
            </div>
            <div class="mal_2_mmc s_float">
                <p>Pendaftaran MMC:</p>
                <p><i>Pendaftaran MMC:</i></p>
            </div>
            <div class="mal_2_stamp s_float">
                <p>16) Cop rasmi:</p>
                <p><i>Official stqmp:</i></p>
            </div>
        </div>
        <div class="mal_2_row_13">
            <p><i>-The certificate is issued by the Government of malaysia in compliance with the reqirments of The 1. Regulation 1.2 under Maritime Labour Convention (200)</i></p>
            <p><i>- The Maximum Validity of this certificate is ony Two (2) yeas.</i></p>
            <p style="margin-top: 20px;"><i><b>stikethrouth whiever not applicable</b></i></p>

        </div>


    </div>
</div>
<div class="mal_all">
    <div class="mal_body_p3">
        <div class="mal_head_p3">
            <div class="mal_3_logo s_float">
                <img src="img/ma.PNG">
            </div>
            <div class="mal_t_r_sec s_float" style="font-size: 9px;">
                <p class="mal_jl_hep">JL/HEPP/D/16</p>
                <h1 class="mal_heac_t_1_p3">JABATAN I,AITT MALAYSIA</h1>
                <p class="mal_head_ad_1">Ibu Pe-iabat Laut Senenaniunll Malaysia. Ireti Surat 12, 42007 Pelabuhan Klang</p>
                <p class="mal_head_ad_2">Tel: 03-.169-i l00. Fax 0l-1685289. ti-ilai1: kpgrl(rnarine.gqr.11 hllp., www marrne gor rn)</p>
            </div>
        </div>
        <div class="mal_head_2_P3 clear">
            <h1 class="mal_sij_p3">LAPORAN PENGAMAL PERUBATAN</h1>
            <h2 class="mal_med_exam_p3"><i>MEDICAL PRACTITIONER'S REPORT</i></h2>
        </div>
        <div class="mal_p_row_1">
            <div class="mal_p_exam_">
                <p> saya telah memeriksa <span class="mal_s_t_p3_value">{{ $form->given_name }} {{ $form->family_name }} </span></p>
                <p>I have Examined</p>
            </div>
            <div class="mal_p_3_ic">
                <p>No l(P/Paspot: <span class="mal_p_3_pass_value">{{ $form->passport_no }}</span></p>
                <p>lC.iPttssport No.</p>
            </div>
        </div>
        <div class="mal_p_row_2">
            <p>mengikut standard perubatan Jabatan Laut Malaysia JL/P/02/98 dan keputusannya adalah berikut:</p>
            <p>as per the Malaysian Marine Department medical standards JLP02 98 and the results are as follows:</p>
        </div>

        <div class="mal_p_row_3">
            <div class="mal_p3_r_3_p1 s_float">
                <div class="mal_t_row">
                    <div class="mal_t_1 s_float">
                        <p>Tinggi/Berat</p>
                        <p>Height/weight</p>
                    </div>
                    <div class="mal_t_2 s_float">
                        <span class="mal_met_value">{{ $form->report->height }}</span> Metres
                    </div>
                    <div class="mal_t_3 s_float">
                        <span class="mal_kg_value">{{ $form->report->height }}</span> Kg
                    </div>
                </div>

                <div class="mal_t_row">
                    <div class="mal_t_1 s_float">
                        <p>Pcn den garan</p>
                        <p><i>Hearing</i></p>
                    </div>
                    <div class="mal_t_2 s_float">
                        <p class="mal_h_value">{{ $form->report->hearing_right_ear }}</p>
                        <p class="mal_h_text">kanan <i>Right</i></p>
                    </div>
                    <div class="mal_t_3 s_float">
                        <p class="mal_h_value">{{ $form->report->hearing_left_ear }}</p>
                        <p class="mal_h_text">kiri <i>left</i></p>
                    </div>
                </div>

                <div class="mal_t_row">
                    <div class="mal_t_1 s_float">
                        <p>Pengl ihatan</p>
                        <p><i>Eyesight</i></p>
                    </div>
                    <div class="mal_t_2 s_float">
                        <p class="mal_h_value">{{ $form->report->eye_sight_right }}</p>
                        <p class="mal_h_text">kanan <i>Right</i></p>
                    </div>
                    <div class="mal_t_3 s_float">
                        <p class="mal_h_value">{{ $form->report->eye_sight_left }}</p>
                        <p class="mal_h_text">kiri <i>left</i></p>
                    </div>
                </div>

                <div class="mal_t_row">
                    <div class="mal_t_1 s_float">
                        <p>Penglihutun dgn kaeurnata</p>
                        <p><i>Eyesight with visual aids</i></p>
                    </div>
                    <div class="mal_t_2 s_float">
                        <p class="mal_h_value">{{ $form->report->eye_right_with_aid}}</p>
                        <p class="mal_h_text">kanan <i>Right</i></p>
                    </div>
                    <div class="mal_t_3 s_float">
                        <p class="mal_h_value">{{ $form->report->eye_left_with_aid}}</p>
                        <p class="mal_h_text">kiri <i>left</i></p>
                    </div>
                </div>

                <div class="mal_t_row">
                    <div class="mal_t_1 s_float">
                        <p>Pertglihatan Warna</p>
                        <p><i>Colour Vision</i></p>
                    </div>
                    <div class="mal_t_2 s_float">
                        <p class="mal_h_value">{{ $form->report->color_vision }}</p>
                    </div>
                    <div class="mal_t_3 s_float">
                        <p class="mal_h_value"></p>
                    </div>
                </div>

                <div class="mal_t_row">
                    <div class="mal_t_1 s_float">
                        <p>ujian Kencing <i>Urinalyysis</i></p>
                    </div>
                    <div class="mal_t_2 s_float">
                        <span class="mal_met_value">{{ $form->report->urinalysis }}</span> gula sugar
                    </div>
                    <div class="mal_t_3 s_float">
                        <span class="mal_kg_value">{{ $form->report->sugar }}</span> albumin
                    </div>
                </div>

                <div class="mal_t_row">
                    <div class="mal_t_1 s_float">
                        <p>Naki <i>Pulse</i></p>
                    </div>
                    <div class="mal_t_2 s_float">
                        <span class="mal_met_value">{{ $form->report->pulse }}</span> /min
                    </div>
                    <div class="mal_t_3 s_float">
                    </div>
                </div>
                <div class="mal_t_row">
                    <div class="mal_t_1 s_float">
                        <p>Tekanan darah</p>
                        <p><i>Blood pressure</i></p>
                    </div>
                    <div class="mal_t_2 s_float">
                        <span class="mal_met_value_2">{{ $form->report->blood_pressure }}</span>
                    </div>
                    <div class="mal_t_3 s_float">
                    </div>
                </div>

            </div>
            <div class="mal_p3_r_3_p2 s_float">
                <p class="mal_p3_ke">KEPUTUSAN PEPERIKSAAN</p>
                <P class="mal_p3_ke"><I>EXAMINATION RESULTS</I></P>
                <div class="ps_l_p_2_row">
                    <div class="mal_p3_fit s_float">
                        <P>LAYAK</P>
                        <P><I>FIT</I></P>
                    </div>
                    <div class="mal_p3_fit_icon s_float">
                        @if($form->report->fit == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ps_l_p_2_row">
                    <div class="mal_p3_fit s_float">
                        <P>TIDAK SAYAK</P>
                        <P><I>UNFIT</I></P>
                    </div>
                    <div class="mal_p3_fit_icon s_float">
                        @if($form->report->fit == '2')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ps_l_p_2_row">
                    <div class="mal_p3_fit s_float">
                        <P>TIDAK LAYAK SEMENTARA</P>
                        <P><I>TEMPORARIL UNFIT</I></P>
                    </div>
                    <div class="mal_p3_fit_icon s_float">
                        @if($form->report->fit == '3')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="mal_p_3_row_4 clear">
            <div class="mal_p3_ce_text s_float">
                Chest X ray
            </div>
            <div class="mal_p3_c_nor s_float">
                @if($form->report->chest_x_ray == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                Normal/
                @if($form->report->chest_x_ray == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                Abnormal
            </div>
            <div class="mal_p3_xray_num  s_float">
                X-ray Number<span class="xray_no_value">{{ $form->report->x_ray_number }}</span>
            </div>

        </div>

        <div class="mal_p_3_row_5 clear">
            <div class="mal_p3_ecg_text s_float">
                ECG
            </div>
            <div class="mal_p3_ecg_nor s_float">
                @if($form->report->ecg == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                  Normal
                  /
                @if($form->report->ecg == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                    Abnormal
            </div>
        </div>
        <div class="mal_p_3_row_6">
            <div class="na_row_d clear">
                <div class="mal_na_no s_float">

                </div>
                <div class="mal_na_name s_float">

                </div>
                <div class="mal_na_name_icon_1 s_float">
                    Normal
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    Abnormal
                </div>
                <div class="mal_na_name_remark s_float">
                    Remarks
                </div>
            </div>
            <div class="na_row_d clear">
                <div class="mal_na_no s_float">
                    1
                </div>
                <div class="mal_na_name s_float">
                    Infection diseases
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->infectios == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->infectios == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{ $form->report->infectios_remarks }}</p>
                </div>
            </div>
            <div class="na_row_d clear">
                <div class="mal_na_no s_float">
                    2
                </div>
                <div class="mal_na_name s_float">
                    Mlinant Neoplasm
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->malignant == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->malignant == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value"> {{ $form->report->malignant_remarks }}</p>
                </div>
            </div>
            <div class="na_row_d clear">
                <div class="mal_na_no s_float">
                    3
                </div>
                <div class="mal_na_name s_float">
                    Endocrine and Metabolic Disease
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->endocrine == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->endocrine == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{ $form->report->disease_blood }}</p>
                </div>
            </div>
            <div class="na_row_d m_r clear">
                <div class="mal_na_no s_float">
                    4
                </div>
                <div class="mal_na_name s_float">
                    Desease of the blood and bolld forming organss
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->disease_blood == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->disease_blood == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{ $form->disease_blood_remarks }}</p>
                </div>
            </div>
            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    5
                </div>
                <div class="mal_na_name s_float">
                    Mental Disorders
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->mental_disorder == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->mental_disorder == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{ $form->report->mental_disorder_remarks }}</p>
                </div>
            </div>

            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    6
                </div>
                <div class="mal_na_name s_float">
                    Central Nervous system
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->central_nervious == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->central_nervious == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{ $form->report->central_nervious_rm }}</p>
                </div>
            </div>

            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    7
                </div>
                <div class="mal_na_name s_float">
                    Cardiovascular system
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->cardio_vascular == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->cardio_vascular == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value"> {{$form->report->cardio_vascular_remarks}}</p>
                </div>
            </div>
            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    8
                </div>
                <div class="mal_na_name s_float">
                    Respiratory system
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->rs_system == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->rs_system == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{ $form->report->rs_system_remarks }}</p>
                </div>
            </div>
            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    9
                </div>
                <div class="mal_na_name s_float">
                    Desestive system
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->digestiv_sm == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->digestiv_sm == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{ $form->report->digestiv_sm_remarks }}</p>
                </div>
            </div>
            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    10
                </div>
                <div class="mal_na_name s_float">
                    Genio-urinary System
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->genito_sm == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->genito_sm == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{$form->report->genito_sm_remarks}}</p>
                </div>
            </div>
            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    11
                </div>
                <div class="mal_na_name s_float">
                    Pregnancy
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->pregnancy == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    NO
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->pregnancy == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    Yes
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">(Week ______){{$form->report->pregnancy_rm  }}</p>
                </div>
            </div>
            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    12
                </div>
                <div class="mal_na_name s_float">
                    Skin
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->skin == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->skin == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{ $form->report->skin_rm }}</p>
                </div>
            </div>
            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    13
                </div>
                <div class="mal_na_name s_float">
                    Musculo-skeletal system
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->musculo == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->musculo == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{ $form->report->musculo_rm }}</p>
                </div>
            </div>
            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    14
                </div>
                <div class="mal_na_name s_float">
                    Specch Defects
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->speech_defects == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->speech_defects == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    <p class="mal_p3_remark_value">{{ $form->report->speech_defects_rm }}</p>
                </div>
            </div>
            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    15
                </div>
                <div class="mal_na_name s_float">
                    Ear/Nose.Throat
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->nose_throat == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->nose_throat == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    {{ $form->report->nose_throat_rm }}
                </div>
            </div>
            <div class="na_row_d  clear">
                <div class="mal_na_no s_float">
                    16
                </div>
                <div class="mal_na_name s_float">
                    Eyes
                </div>
                <div class="mal_na_name_icon_1 s_float">
                    @if($form->report->eyes == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_icon_2 s_float">
                    @if($form->report->eyes == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_na_name_remark s_float">
                    {{ $form->report->eyes_rm }}
                </div>
            </div>
        </div>
        <!-- row_6 end -->
        <div class="mal_p3_row_7">
            <p><b>Perakuan ini sah sehingga</b><span class="mal_p3_cer_value">{{ $form->doexpiry }}</span></p>
            <p><i>This certificate is valid until</i></p>
        </div>
        <div class="mal_p3_row_8">
            <div class="mal_p3_sign_all">
                <p class="mal_p3_sign_value"></p>
                <p class="mal_p3_sign">Signature of Medical Practitioner</p>
                <p>MMC No.</p>
            </div>
        </div>
        <div class="mal_p3_row_9 clear">
            <p>Tarikkh</p>
            <p><i>Date</i></p>
        </div>



    </div>
</div>

<div class="mal_all">
    <div class="mal_body_p3">
        <div class="mal_p_4_head">
            <P class="mal_p_4_h_l">JL/HEALPP/D/16</P>
            <h5>PLN(iAKtJAN P EI.,AU1 Y,\NC ING I N MEN.IALANI PF]MERIKSAAN PERUBATAN</h5>
            <h5>TESTIMONIAL OF SEAMAN UNDERGOING MEDICAL EAMINATION</h5>
        </div>
        <div class="mal_p4_row_1">
            <p>Sila .jarvab soalan-soalan berrkut berhubung dengan sejarah kesihatan anda. Tandakan X dalam kotak ruangan yangscsuai 'Ya' atau 'l'idak'. .lika 'Ya'.jelaskan dalam ruangan catitan.</p>
            <p><i>Please answer tha following with reference to your health. tick X in the appropriate 'Yes' or 'No' column. If ticked 'Yes' please elaborate the markes column</i></p>
            <p class="mal_p4_adakah">Adaliah anda mempnysi sejlarah atau sedang mengalami penyyakit berikut bcrikut:</p>
            <p><i>Do yu have an history or are undergoing treatment in any of the folowing:</i></p>
        </div>
        <div class="mal_p4_row_2">
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    No
                </div>
                <div class="mal_p4_perihal s_float" style="text-align: center;">
                    <p>Perihal <i>Regarding</i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    <p>Ya <i>Yes</i></p>
                </div>
                <div class="mal_p4_no_t s_float">
                    <p>Tidak <i>No</i></p>
                </div>
                <div class="mal_p4_remark s_float">
                    <p>catintan <i>Remaks</i></p>
                </div>
            </div>

            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    1
                </div>
                <div class="mal_p4_perihal s_float">
                    <p>Masalah mata <i>Eye disorders</i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->eye_disorders == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->eye_disorders == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->eye_disorders_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">

                </div>
                <div class="mal_p4_perihal s_float">
                    -Katark <i>Cataract</i>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->cataract == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->cataract == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->cataract_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">

                </div>
                <div class="mal_p4_perihal s_float">
                    -Pandangan monocular <i>Monocular sight</i>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->monocular_sight == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->monocular_sight == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->monocular_sight_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row mal_p4_ex">
                <div class="mal_p4_no s_float">

                </div>
                <div class="mal_p4_perihal s_float ">
                    <p> -Lain-lain yang menyebabkan halangan pandangan</p>
                    <p><i>Other foctors which hinder vision</i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->hinder_vision == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->hinder_vision == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->hinder_vision_rm }}
                </div>
            </div>

            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    2
                </div>
                <div class="mal_p4_perihal s_float">
                    Buta warna <i>colourblind</i>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->color_bind == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->color_bind == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->color_bind_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    3
                </div>
                <div class="mal_p4_perihal s_float">
                    Sukar melihat dalam gelap <i>Night blindness</i>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->night_blind == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->night_blind == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->night_blind_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    4
                </div>
                <div class="mal_p4_perihal s_float">
                    Apa-apa jenis sawan atau kekejanagan <i>convulsion or fits</i>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->convulsion == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->convulsion == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->convulsion_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    5
                </div>
                <div class="mal_p4_perihal s_float">
                    kecederaan bearat dikepala <i>Heavy injuries to head</i>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->head_injuris == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->head_injuris == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->head_injuris_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    6
                </div>
                <div class="mal_p4_perihal s_float">
                    Sceangan pening atau pening <i>Dezziness</i>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->dizziness == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->dizziness == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->dizziness_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row mal_p4_ex">
                <div class="mal_p4_no s_float">
                    7
                </div>
                <div class="mal_p4_perihal s_float ">
                    <p>Sakit kepala yang berat atau 'migraaine' </p>
                    <p><i>Severe headache or migraine</i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->headache_migrane == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->headache_migrane == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->headache_migrane_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    8
                </div>
                <div class="mal_p4_perihal s_float">
                    Pembedahan otak yang 'Major' <i>Major brain operation</i>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->brain_operation == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->brain_operation == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->brain_operation_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row mal_p4_ex">
                <div class="mal_p4_no s_float">
                    9
                </div>
                <div class="mal_p4_perihal s_float ">
                    <p>Kencing mains dalam rawatam insulin</p>
                    <p><i>Dabeis undergong insulin treatment</i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->diabetis_trt == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->diabetis_trt == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->diabetis_trt_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    10
                </div>
                <div class="mal_p4_perihal s_float">
                    Penyakit mental  <i>Mental Disorder</i>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->mental_dis == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->mental_dis == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->mental_dis_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row mal_p4_ex">
                <div class="mal_p4_no s_float">
                    11
                </div>
                <div class="mal_p4_perihal s_float ">
                    <p>Penalahgunaan arak/dadah dalam masa 5 tahun yang lalu <i>Misuse of alcohol/drugs within last 5 years</i></p>

                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->alcohol_drugs == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->alcohol_drugs == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->alcohol_drugs_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    12
                </div>
                <div class="mal_p4_perihal s_float">
                    Kecacatan tulang belakang <i>Spinal Disformityyy</i>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->spinal == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->spinal == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->spinal_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row mal_p4_ex">
                <div class="mal_p4_no s_float">
                    13
                </div>
                <div class="mal_p4_perihal s_float ">
                    <p>Penyakit jantung/tetanan darah tinggi/debaran jantung <i>Heart disease/hypertension/heart palpitatiions</i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->heart_pal == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->heart_pal == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->heart_pal_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row mal_p4_ex">
                <div class="mal_p4_no s_float">
                    14
                </div>
                <div class="mal_p4_perihal s_float ">
                    <p>Sesak nafas/muntah darah/batuk kronik</p>
                    <p><i>Brdifficulty/blood vomitting/chronic cough </i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->breathing == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->breathing == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->breathing_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    15
                </div>
                <div class="mal_p4_perihal s_float">
                    <p>Pekak <i>Deafness</i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->deafiness == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->deafiness == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->deafiness_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row">
                <div class="mal_p4_no s_float">
                    16
                </div>
                <div class="mal_p4_perihal s_float">
                    <p>Penyakit buah pinggang <i>Kidney disease</i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->kidney_dis == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->kidney_dis == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->kidney_dis_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row mal_p4_ex">
                <div class="mal_p4_no s_float">
                    17
                </div>
                <div class="mal_p4_perihal s_float ">
                    <p>Apa-apa rawatan yang berulang</p>
                    <p><i>Any regular medical treatment</i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->medical_treat == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->medical_treat == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->medical_treat_rm }}
                </div>
            </div>
            <div class="mal_p4_treatment_row mal_p4_ex">
                <div class="mal_p4_no s_float">
                    18
                </div>
                <div class="mal_p4_perihal s_float ">
                    <p>Apa-apa penyakit/kecederaan yang tidak dinyatakan diatas <i>Any injury/disease not stated above</i></p>
                </div>
                <div class="mal_p4_yes s_float">
                    @if($form->report->disease_stated == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_no_t s_float">
                    @if($form->report->disease_stated == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="mal_p4_remark s_float">
                    {{ $form->report->disease_stated_rm }}
                </div>
            </div>
        </div>
        <div class="mal_p4_row_3">
            <p>Saya dengan ini mengisytiharkan bahawa saya telah dengan teliti mengambilkira kenyataan yang dibuat diatas dan saya
                percaya ianya lengkap dan tepat Saya seterusnya mengisytiharkan bahawa saya tidak menyembunyikan apa-apa maklumat atau membuat apa-apa kenyataan palsu yang boleh menjejaskan prestasi kerja saya. Saya memberi izin kepada pengamal perubatan yang memeriksa untuk berkomunikasi dengan mana-mana pengamal perubatan yang memeriksa saya dan Jabatan Laut dalam hal-hal yang boleh memberikan kesan atas kesesuaian untuk bekerja diatas</p>
            <p style="font-size: 9.8px;"><i>I declare that the information given above is correct to the best of my knowledge. i farther diclare that I have not hidden any information or made false statement which can jeopardize my work. I do give permission for the medical practitioner to communicate with any other medical practitioners or the Marine Department in any matter which can affect my placement on board a vessel.</i></p>
        </div>
        <div class="mal_p4_row_4">
            <div class="mal_p4_app_sign s_float">
                <p>Tandatangan pemohon<span class="mal_p4_app_sign_value"></span></p>
                <p><i>Applicants Singature:</i></p>
            </div>
            <div class="mal_p4_semanan s_float">
                <p>No Kad Peleaut: <span class="mal_p4_semanan_value"></span></p>
                <p>Seman Cart No:</p>
            </div>
        </div>

        <div class="mal_p4_row_5 clear">
            <div class="mal_p4_f_name s_float">
                <p>Name (di huruf besar)<span class="mal_p4_app_name_value"></span></p>
                <p><i>Name (in capital letters):</i></p>
            </div>
            <div class="mal_p4_nric s_float">
                <p>No. kad pengenalan: <span class="mal_p4_nric_value"></span></p>
                <p>NRIC Passport Number:</p>
            </div>
        </div>

        <div class="mal_p4_row_6 clear">
            <div class="mal_p4_f_dr s_float">
                <p>Disaksikan oleh:(Dr)<span class="mal_p4_app_dr_value"></span></p>
                <p><i>Witnessed byy</i></p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
