@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    <div style="height: 100px;" class="row justify-content-center">
        <div class="col-lg-6">
            <div style="font-weight: bold; font-size: 1.5rem; text-transform: capitalize" class="text-center">Seafarer Medical Certificate</div>
        </div>
    </div>
    <form method="post" action="{{ route('malaysia.update', $form->id)}}">
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label" for="family_name"><span class="text-danger">*</span>Family Name:</label>
                                <input  type="text" name="family_name" class="form-control" id="family_name" value="{{ old('family_name', $form->family_name ?? '') }}">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label" for="given_name"><span class="text-danger">*</span>Given Name:</label>
                                <input type="text" name="given_name" class="form-control" id="given_name" value="{{ old('given_name', $form->given_name ?? '') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="gender" ><span class="text-danger">*</span>Gender:(Male/Female):</label>
                    <select class="form-control" name="gender" id="gender">
                        <option>Select option</option>
                        <option value="Male" {{ $form->gender == 'Male' ? 'selected' : '' }}>Male</option>
                        <option value="Female" {{ $form->gender == 'Female' ? 'selected' : '' }}>Female</option>
                        <option value="Others" {{ $form->gender == 'Others' ? 'selected' : '' }}>Others</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label" for="seaman_id">Seaman Card No:</label>
                    <input class="form-control" type="text" name="seaman_id" id="seaman_id" value="{{ old('seaman_id', $form->seaman_id ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="dob" ><span class="text-danger">*</span>Date of Birth:(DD/MM/YYYY):</label>
                    <input type="date" name="dob" class="form-control" id="dob" value="{{ old('dob', $form->dob ?? '') }}">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="nationality">Nationality:</label>
                    <input class="form-control" type="text" name="nationality" id="nationality" value="{{ old('nationality', $form->nationality ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="passport_no">Passport/NID No::</label>
                    <input class="form-control" type="text" name="passport_no" id="passport_no" value="{{ old('passport_no', $form->passport_no ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="occupation">Profession:</label>
                    <input class="form-control" type="text" name="occupation" id="occupation" value="{{ old('occupation', $form->occupation ?? '') }}">
                </div>
            </div>

        </div>
        <div class="row">
            <div class="text-center">
                DECLAPATION 0F THE RECOGNIZED MEDICAL PRACTITIONER :
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="confirmation">1. Confirmation that identification documents were checked at the point of examination:</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="confirmation" value="1" {{ $form->confirmation == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="confirmation">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="confirmation" value="0" {{ $form->confirmation == '0' ? 'checked' : '' }}>
                    <label for="pregnancy" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="hearing_standard">2.Hearing meets the standards in section A-1 /9 of the
                        STCW 78 as amended</label>
                    <br>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="hearing_standard" name="hearing_standard" value="1" {{ $form->hearing_standard == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="hearing_standard">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="hearing_standard" name="hearing_standard" value="0" {{ $form->hearing_standard == '0' ? 'checked' : '' }}>
                    <label for="hearing_standard" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="unaided_statisfactory">3.Unaided hearing stisfactory?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="unaided_statisfactory" name="unaided_statisfactory" value="1" {{ $form->unaided_statisfactory == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="unaided_statisfactory">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="unaided_statisfactory" name="unaided_statisfactory" value="0" {{ $form->unaided_statisfactory == '0' ? 'checked' : '' }}>
                    <label for="unaided_statisfactory" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="visual_acutity">4.Visual acutity meets standards in section A-1 /9 of the
                        STCW 78 as amended</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="visual_acutity" name="visual_acutity" value="1" {{ $form->visual_acutity == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="visual_acutity">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="visual_acutity" value="0" {{ $form->visual_acutity == '0' ? 'checked' : '' }}>
                    <label for="visual_acutity" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_vision_standard">5.Colour vision meets standards in section A-1 / 9 ?</label>
                    <br>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="color_vision_standard" name="color_vision_standard" value="1" {{ $form->color_vision_standard == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="color_vision_standard">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="color_vision_standard" name="color_vision_standard" value="0" {{ $form->color_vision_standard == '0' ? 'checked' : '' }}>
                    <label for="color_vision_standard" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_vision_test">Date of last colour vision test</label>
                    <input type="date" class="form-control" id="color_vision_test" name="color_vision_test" value="{{ old('color_vision_test', $form->color_vision_test ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="fit_lookout">6.Fit for lookoutduties?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="fit_lookout" value="1" {{ $form->fit_lookout == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="fit_lookout">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="fit_lookout" value="0" {{ $form->confirmation == '0' ? 'checked' : '' }}>
                    <label for="fit_lookout" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="any_restrictions">7.Any limitations or restictions on fitness ?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="any_restrictions" value="1" {{ $form->any_restrictions == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="any_restrictions">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="any_restrictions" value="0" {{ $form->any_restrictions == '0' ? 'checked' : '' }}>
                    <label for="any_restrictions" style="font-size: 20px;">No</label>
                    <br>
                    <label  style="font-size: 20px;" for="limit_restriction">lf NO, specify limitations or restrictions</label>
                </div>
                <div class="form-group">
                    <textarea class="form-control" type="text" id="limit_restriction" name="limit_restriction">{{ old('limit_restriction', $form->limit_restriction ?? '')  }}</textarea>
                </div>
                <div class="form-group">
                    <label class="form-label" for="unfit_for_service">8.Is the seafarer free from any medical condition likely to be aggravated by service at sea or the render the seafarer unfit for service or the render the health of any other persons on board ?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="visual_acutity" name="unfit_for_service" value="1" {{ $form->unfit_for_service == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="unfit_for_service">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="unfit_for_service" value="0" {{ $form->unfit_for_service == '0' ? 'checked' : '' }}>
                    <label for="unfit_for_service" style="font-size: 20px;">No</label>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="form-label" for="medical_fitness">Category of Medical Fitness:</label>
                    <input class="form-control" type="text" name="medical_fitness" id="medical_fitness" value="{{ old('medical_fitness', $form->medical_fitness ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="doe">Date of examination/Issue(DD/MM/YYY)</label>
                    <input class="form-control" type="date" name="doe" id="doe" value="{{ old('doe', $form->doe ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="doexpiry">Date of expiry(DD/MM/YYY)</label>
                    <input class="form-control" type="date" name="doexpiry" id="doexpiry" value="{{ old('doexpiry', $form->doexpiry ?? '') }}">
                    <small>No more than 2 years from the date of examination.</small>
                </div>
            </div>
        </div>

        <div class="row">
            @if(!empty($form))
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="update">
                </div>
            @else
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            @endif

        </div>
    </form>
@endsection
