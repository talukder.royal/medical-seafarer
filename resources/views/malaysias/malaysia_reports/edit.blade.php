
@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    <form class="form-control" action="{{ route('malaysia_report.update', $form->id) }}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-12 text-center text-black-50 font-sans">
                MEDICAL PRACTITIONER'S REPORT
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-label" for="name">Name</label>
                        <p>{{ $form->given_name }} {{ $form->family_name }}</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="form-label" for="passport_no">Passport No</label>
                        <input class="form-control" type="text" name="passport_no" id="passport_no" value="{{ old('passport_no', $form->passport_no ?? '') }}" disabled>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="height_weight">Height/Weight</label>
                        <input class="d-inline-block w-25 form-control" type="text" name="height" id="height" value="{{ old('height', $form->report->height ?? '') }}">
                        <label>meters</label>
                        <input class="d-inline-block w-25 form-control" type="text" name="weight" id="weight" value="{{ old('weight', $form->report->weight ?? '') }}">
                        <label>kg</label>
                    </div>
                    <div class="form-group">
                        <label for="height_weight">Hearing</label>
                        <input class="form-control d-inline-block w-25 " type="text" name="hearing_right_ear" id="hearing_right_ear" value="{{ old('hearing_right_ear', $form->report->hearing_right_ear ?? '') }}">
                        <label class="form-label">right</label>
                        <input class="form-control d-inline-block w-25 " type="text" name="hearing_left_ear" id="hearing_left_ear" value="{{ old('hearing_left_ear', $form->report->hearing_left_ear ?? '') }}">
                        <label class="form-label">left</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="eyesight">Eyesight</label>
                        <input class="form-control d-inline-block w-25" type="text" name="eye_sight_right" id="eye_sight_right" value="{{ old('eye_sight_right', $form->report->eye_sight_right ?? '') }}">
                        <label class="form-label" for="eye_sight_right">Right</label>
                        <input class="form-control d-inline-block w-25" type="text" name="eye_sight_left" id="eye_sight_left" value="{{ old('eye_sight_left', $form->report->eye_sight_left ?? '') }}">
                        <label class="form-label" for="eye_sight_left">Left</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="eye_right_with_aid">ES. visual aids</label>
                        <input class="form-control d-inline-block w-25" type="text" name="eye_right_with_aid" id="eye_right_with_aid" value="{{ old('eye_right_with_aid', $form->report->eye_right_with_aid ?? '') }}">
                        <label class="form-label" for="eye_right_with_aid">Right</label>
                        <input class="form-control  d-inline-block w-25" type="text" name="eye_left_with_aid" id="eye_left_with_aid" value="{{ old('eye_right_with_aid', $form->report->eye_left_with_aid ?? '') }}">
                        <label class="form-label" for="eye_left_with_aid">Left</label>
                    </div>
{{--                    --}}
                    <div class="form-group">
                        <label class="form-label" for="color_vision">Color vision</label>
                        <input class="form-control" type="text" name="color_vision" id="color_vision" value="{{ old('color_vision', $form->report->color_vision ?? '') }}">
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="urinalysis">Urinalysis</label>
                        <input class="form-control d-inline-block w-25" type="text" name="urinalysis" id="urinalysis" value="{{ old('urinalysis', $form->report->urinalysis ?? '') }}">
                        <label class="form-label" for="sugar">Sugar</label>
                        <input class="form-control d-inline-block w-25" type="text" name="sugar" id="sugar" value="{{ old('sugar', $form->report->sugar ?? '') }}">
                        <label class="form-label" for="sugar">Albumin</label>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="pulse">Pulse(per minute)</label>
                        <input class="form-control" type="text" name="pulse" id="pulse" value="{{ old('pulse', $form->report->pulse ?? '') }}">
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="blood_pressure">Blood pressure</label>
                        <input class="form-control" type="text" name="blood_pressure" id="blood_pressure" value="{{ old('pulse', $form->report->blood_pressure ?? '') }}">
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="chest_x_ray">Chest X-ray</label>
                        <select class="form-control" name="chest_x_ray" id="chest_x_ray">
                            <option>Select</option>
                            <option value="1"  {{ $form->report->chest_x_ray == '1' ? 'selected' : ''}}>Normal</option>
                            <option value="0"  {{ $form->report->chest_x_ray == '0' ? 'selected' : ''}}>Abnormal</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="x_ray_number">X-ray Number</label>
                        <input class="form-control" type="text" name="x_ray_number" id="x_ray_number" value="{{ old('x_ray_number', $form->report->x_ray_number ?? '') }}" >
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="ecg">ECG</label>
                        <select class="form-control" name="ecg" id="ecg">
                            <option>Select</option>
                            <option value="1"  {{ $form->report->ecg == '1' ? 'selected' : ''}}>Normal</option>
                            <option value="0"  {{ $form->report->ecg == '0' ? 'selected' : ''}}>Abnormal</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <p class="h3 text-lg-center" style="color:lightslategray">Examination Results</p>
                    </div>
                    <div class="form-group">
                        <pre><label class="form-label" for="fit">FIT</label>                       <input type="checkbox" style="width: 25px; height: 20px;" name="fit" id="fit" value="1" {{ $form->report->fit == '1' ? 'checked' : '' }}></pre>
                        <pre><label class="form-label" for="unfit">UNFIT</label>                     <input type="checkbox" style="width: 25px; height: 20px;" name="fit" id="unfit" value="2" {{ $form->report->fit == '2' ? 'checked' : '' }}></pre>
                        <pre><label class="form-label" for="tem_unfit">TEMPORARILY UNFIT</label>         <input type="checkbox" style="width: 25px; height: 20px;" name="fit" id="tem_unfit" value="3" {{ $form->report->fit == '3' ? 'checked' : '' }}></pre>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">
                            <div class="form-group">
                                <label class="form-label">Normal</label>
                            </div>
                        </th>
                        <th class="sm:text-left">
                            <div class="form-group">
                                <label class="form-label">Normal</label>
                            </div>
                        </th>
                        <th>
                            <div class="form-group">
                                <label class="form-label">Remarks</label>
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="infectios_disease">1. Infectious disease</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="infectios" id="infectios_disease" value="1" {{ $form->report->infectios == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="infectios" id="infectios_disease" value="0" {{ $form->report->infectios == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="infectios_remarks" id="infectios_disease" value="{{ old('infectios_remarks', $form->report->infectios_remarks ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr style="margin: 0">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="mal_neop">2.Malignant Neoplasm</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="malignant" id="mal_neop" value="1" {{ $form->report->malignant == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="malignant" id="mal_neop" value="0" {{ $form->report->malignant == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="malignant_remarks" id="mal_neop" value="{{ old('malignant_remarks', $form->report->malignant_remarks ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="endocrine">3.Endocrine and metabolic disease</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="endocrine" id="endocrine" value="1" {{ $form->report->endocrine == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="endocrine" id="endocrine" value="0" {{ $form->report->endocrine == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="endocrine_remarks" id="endocrine" value="{{ old('endocrine_remarks', $form->report->endocrine_remarks ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="disease_blood">4.Disease of the blood and blood forming organs</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="disease_blood" id="disease_blood" value="1" {{ $form->report->disease_blood == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="disease_blood" id="disease_blood" value="0" {{ $form->report->disease_blood == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="disease_blood_remarks" id="disease_blood" value="{{ old('disease_blood_remarks', $form->report->disease_blood_remarks ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="mental_disorder">5.Mental Disorder</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="mental_disorder" id="mental_disorder" value="1" {{ $form->report->mental_disorder == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="mental_disorder" id="mental_disorder" value="0" {{ $form->report->mental_disorder == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="mental_disorder_remarks" id="endocrine" value="{{ old('mental_disorder_remarks', $form->report->mental_disorder_remarks ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="central_nervious">6.Central Nervous system</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="central_nervious" id="central_nervious" value="1" {{ $form->report->central_nervious == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="central_nervious" id="central_nervious" value="0" {{ $form->report->central_nervious == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="central_nervious_rm" id="endocrine" value="{{ old('central_nervious_rm', $form->report->central_nervious_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="cardio_vascular">7. Cardiovascular system</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="cardio_vascular" id="cardio_vascular" value="1" {{ $form->report->cardio_vascular == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="cardio_vascular" id="cardio_vascular" value="0" {{ $form->report->cardio_vascular == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="cardio_vascular_remarks" id="cardio_vascular" value="{{ old('cardio_vascular_remarks', $form->report->cardio_vascular_remarks ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="rs_system">Respiratory system</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="rs_system" id="rs_system" value="1" {{ $form->report->rs_system == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="rs_system" id="rs_system" value="0" {{ $form->report->rs_system == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="rs_system_remarks" id="rs_system" value="{{ old('rs_system_remarks', $form->report->rs_system_remarks ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="digestiv_sm">Digestive System</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="digestiv_sm" id="digestiv_sm" value="1" {{ $form->report->digestiv_sm == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="digestiv_sm" id="digestiv_sm" value="0" {{ $form->report->digestiv_sm == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="digestiv_sm_remarks" id="digestiv_sm" value="{{ old('digestiv_sm_remarks', $form->report->digestiv_sm_remarks ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="genito_sm">10.Genito-Urinary System</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="genito_sm" id="genito_sm" value="1" {{ $form->report->genito_sm == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="genito_sm" id="genito_sm" value="0" {{ $form->report->genito_sm == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="genito_sm_remarks" id="genito_sm" value="{{ old('genito_sm_remarks', $form->report->genito_sm_remarks ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="pregnancy">11.Prgnancy</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label">No</label>
                                <input style="width: 25px; height: 20px;" type="checkbox" name="pregnancy" id="pregnancy" value="0" {{ $form->report->pregnancy == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <label class="form-label">Yes</label>
                                <input style="width: 25px; height: 20px;" type="checkbox" name="pregnancy" id="pregnancy" value="1" {{ $form->report->pregnancy == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label">Week</label>
                                <input style="width: 87%; display: inline-block" class="form-control" type="text" name="pregnancy_rm" id="pregnancy" value="{{ old('pregnancy_rm', $form->report->pregnancy_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="skin">12.Skin</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="skin" id="skin" value="1" {{ $form->report->skin == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="skin" id="skin" value="0" {{ $form->report->skin == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="skin_rm" id="skin" value="{{ old('skin_rm', $form->report->skin_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="musculo">13.Musculo-skeletal system</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="musculo" id="musculo" value="1" {{ $form->report->musculo == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="musculo" id="musculo" value="0" {{ $form->report->musculo == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="musculo_rm" id="musculo" value="{{ old('musculo_rm', $form->report->musculo_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>

                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="speech_defects">14.Speech Defects</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="speech_defects" id="speech_defects" value="1" {{ $form->report->speech_defects == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="speech_defects" id="speech_defects" value="0" {{ $form->report->speech_defects == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="speech_defects_rm" id="speech_defects" value="{{ old('speech_defects_rm', $form->report->speech_defects_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="nose_throat">15.Ears/Nose/Throat</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="nose_throat" id="nose_throat" value="1" {{ $form->report->nose_throat == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="nose_throat" id="nose_throat" value="0" {{ $form->report->nose_throat == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="nose_throat_rm" id="nose_throat" value="{{ old('nose_throat_rm', $form->report->nose_throat_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="musculo">16.Eyes</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="eyes" id="eyes" value="1" {{ $form->report->eyes == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="eyes" id="eyes" value="0" {{ $form->report->eyes == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="eyes_rm" id="eyes" value="{{ old('eyes_rm', $form->report->eyes_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">TESTIMONIAL OF SEAMAN UNDERGOING MEDICAL EXAMINATION</div>
        </div>

        <div class="row">
            <div class="form-group">
                <label class="form-label">Do you have any history or are undergoing treatment in any of the folowing:</label>
            </div>
            <div class="col-lg-12">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>
                            <div class="form-group">
                                <label class="form-label">Regarding</label>
                            </div>
                        </th>
                        <th class="text-center">
                            <div class="form-group">
                                <label class="form-label">Yes</label>
                            </div>
                        </th>
                        <th class="sm:text-left">
                            <div class="form-group">
                                <label class="form-label">No</label>
                            </div>
                        </th>
                        <th>
                            <div class="form-group">
                                <label class="form-label">Remarks</label>
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="eye_disorders">1. Eye disorders</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="eye_disorders" id="eye_disorders" value="1" {{ $form->report->eye_disorders == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="eye_disorders" id="eye_disorders" value="0" {{ $form->report->eye_disorders == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="eye_disorders_rm" id="eye_disorders" value="{{ old('eye_disorders_rm', $form->report->eye_disorders_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr style="margin: 0">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="cataract">Cataract</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="cataract" id="cataract" value="1" {{ $form->report->cataract == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="cataract" id="cataract" value="0" {{ $form->report->cataract == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="cataract_rm" id="cataract" value="{{ old('cataract_rm', $form->report->cataract_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="monocular_sight">Monocular sight</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="monocular_sight" id="monocular_sight" value="1" {{ $form->report->monocular_sight == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="monocular_sight" id="monocular_sight" value="0" {{ $form->report->monocular_sight == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="monocular_sight_rm" id="monocular_sight" value="{{ old('monocular_sight_rm', $form->report->monocular_sight_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="hinder_vision">Other factors which hinder vision</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="hinder_vision" id="hinder_vision" value="1" {{ $form->report->hinder_vision == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="hinder_vision" id="hinder_vision" value="0" {{ $form->report->hinder_vision == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="hinder_vision_rm" id="hinder_vision" value="{{ old('hinder_vision_rm', $form->report->hinder_vision_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="color_bind">2. Colour bind</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="color_bind" id="color_bind" value="1" {{ $form->report->color_bind == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="color_bind" id="color_bind" value="0" {{ $form->report->color_bind == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="color_bind_rm" id="color_bind" value="{{ old('color_bind_rm', $form->report->color_bind_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="night_blind">Night blindness</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="night_blind" id="night_blind" value="1" {{ $form->report->night_blind == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="night_blind" id="night_blind" value="0" {{ $form->report->night_blind == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="night_blind_rm" id="night_blind" value="{{ old('night_blind_rm', $form->report->night_blind_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="convulsion">4. Convulsion or fits</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="convulsion" id="convulsion" value="1" {{ $form->report->convulsion == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="convulsion" id="convulsion" value="0" {{ $form->report->convulsion == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="convulsion_rm" id="convulsion" value="{{ old('convulsion_rm', $form->report->convulsion_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="head_injuris">Heavy injuries to head</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="head_injuris" id="head_injuris" value="1" {{ $form->report->head_injuris == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="head_injuris" id="head_injuris" value="0" {{ $form->report->head_injuris == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="head_injuris_rm" id="head_injuris" value="{{ old('head_injuris_rm', $form->report->head_injuris_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="dizziness">6.Dizziness</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="dizziness" id="dizziness" value="1" {{ $form->report->dizziness == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="dizziness" id="dizziness" value="0" {{ $form->report->dizziness == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="dizziness_rm" id="dizziness" value="{{ old('dizziness_rm', $form->report->dizziness_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="headache_migrane">7. Severe headache or migrane</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="headache_migrane" id="headache_migrane" value="1" {{ $form->report->headache_migrane == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="headache_migrane" id="headache_migrane" value="0" {{ $form->report->headache_migrane == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="headache_migrane_rm" id="headache_migrane" value="{{ old('headache_migrane_rm', $form->report->headache_migrane_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="brain_operation">8.Major brain operation</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="brain_operation" id="brain_operation" value="1" {{ $form->report->brain_operation == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="brain_operation" id="brain_operation" value="0" {{ $form->report->brain_operation == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input  class="form-control" type="text" name="brain_operation_rm" id="brain_operation" value="{{ old('brain_operation_rm', $form->report->brain_operation_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="diabetis_trt">9.Diabetis undergoing insulin treatment</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="diabetis_trt" id="diabetis_trt" value="1" {{ $form->report->diabetis_trt == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="diabetis_trt" id="diabetis_trt" value="0" {{ $form->report->diabetis_trt == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="diabetis_trt_rm" id="diabetis_trt" value="{{ old('diabetis_trt_rm', $form->report->diabetis_trt_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="mental_dis">10. Mental disorder</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="mental_dis" id="mental_dis" value="1" {{ $form->report->mental_dis == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="mental_dis" id="mental_dis" value="0" {{ $form->report->mental_dis == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="mental_dis_rm" id="mental_dis" value="{{ old('mental_dis_rm', $form->report->mental_dis_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>

                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="alcohol_drugs">11.Misuse of alcohol/drugs within last 5 years</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="alcohol_drugs" id="alcohol_drugs" value="1" {{ $form->report->alcohol_drugs == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="alcohol_drugs" id="alcohol_drugs" value="0" {{ $form->report->alcohol_drugs == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="alcohol_drugs_rm" id="alcohol_drugs" value="{{ old('alcohol_drugs_rm', $form->report->alcohol_drugs_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="spinal">12.Spinal disformity</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="spinal" id="spinal" value="1" {{ $form->report->spinal == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="spinal" id="spinal" value="0" {{ $form->report->spinal == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="spinal_rm" id="spinal" value="{{ old('spinal_rm', $form->report->spinal_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="heart_pal">13.Heart disease/hypertension/heart palpitations</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="heart_pal" id="heart_pal" value="1" {{ $form->report->heart_pal == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="heart_pal" id="heart_pal" value="0" {{ $form->report->heart_pal == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="heart_pal_rm" id="heart_pal" value="{{ old('heart_pal_rm', $form->report->heart_pal_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>

                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="breathing">14.Breathing difficulty/blood vomitting / chronic cough</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="breathing" id="breathing" value="1" {{ $form->report->breathing == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="breathing" id="breathing" value="0" {{ $form->report->breathing == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="breathing_rm" id="breathing" value="{{ old('breathing_rm', $form->report->breathing_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>

                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="deafiness">15.Deafiness</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="deafiness" id="deafiness" value="1" {{ $form->report->deafiness == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="deafiness" id="deafiness" value="0" {{ $form->report->deafiness == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="deafiness_rm" id="deafiness" value="{{ old('deafiness_rm', $form->report->deafiness_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="kidney_dis">16.Kidney disease</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="kidney_dis" id="kidney_dis" value="1" {{ $form->report->kidney_dis == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="kidney_dis" id="kidney_dis" value="0" {{ $form->report->kidney_dis == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="kidney_dis_rm" id="kidney_dis" value="{{ old('kidney_dis_rm', $form->report->kidney_dis_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="medical_treat">17.Any regular medical treatment?</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="medical_treat" id="medical_treat" value="1" {{ $form->report->medical_treat == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="medical_treat" id="medical_treat" value="0" {{ $form->report->medical_treat == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="medical_treat_rm" id="medical_treat" value="{{ old('medical_treat_rm', $form->report->medical_treat_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    <tr  style="margin: 0; padding: 0;">
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <label class="form-label" style="color: #95a2af;" for="disease_stated">18. Any injury/disease not stated above</label>
                            </div>
                        </td>
                        <td class="text-center"  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="disease_stated" id="disease_stated" value="1" {{ $form->report->disease_stated == '1' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group" style="padding-left: 1em;">
                                <input style="width: 25px; height: 20px;" type="checkbox" name="disease_stated" id="disease_stated" value="0" {{ $form->report->disease_stated == '0' ? 'checked' : '' }}>
                            </div>
                        </td>
                        <td  style="margin: 0; padding: 0;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="disease_stated_rm" id="disease_stated" value="{{ old('disease_stated_rm', $form->report->disease_stated_rm ?? '') }}">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>
    </form>
@endsection
