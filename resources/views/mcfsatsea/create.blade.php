@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <div class="row">
        <div class="col-lg-12 text-center">
            <div>
                MDECAL CERTIFICATE FOR SERVICE AT SEA
            </div>
        </div>
    </div>
    <form class="form-control" action="{{ route('mcfsatsea.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="surname"><span class="text-danger">*</span>Last/Family Name</label>
                    <input type="text" class="form-control" name="surname" id="surname" value="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="given_name"><span class="text-danger">*</span>First/ Given Name(s)</label>
                    <input type="text" class="form-control" name="given_name" id="given_name" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="po_applied_for">Position applied for</label>
                    <select class="form-control" name="po_applied_for" id="po_applied_for">
                        <option >Select</option>
                        <option value="1">Deck officer</option>
                        <option value="2">Engineering Officer</option>
                        <option value="3">Rating-Deck</option>
                        <option value="4">Rating-Engine</option>
                        <option value="5">Catering Department</option>
                    </select>
                </div>

                <div class="form-group mt-4">
                    <label class="form-label" for="cdc_no">CDC number</label>
                    <input class="form-control" type="text"  name="cdc_no" id="cdc_no" value="">
                </div>
                <div class="form-group">
                    <label class="form-label">Sex</label>
                    <br>
                    <label class="form-label" for="male">Male</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="gender" id="male" value="Male" >
                    <label class="form-label" for="female">Female</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="gender" id="female" value="Female">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="dob">Date of birth:</label>
                    <input type="date" class="form-control" name="dob" id="dob" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="passport_no">Passport No</label>
                    <input class="form-control" type="text"  name="passport_no" id="passport_no">
                </div>
                <div class="form-group">
                    <label class="form-label" for="avatar">Photo</label>
                    <input class="form-control" type="file"  name="avatar" id="avatar" value="">
                    <small class="text-warning">Must be passport size</small>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <label class="from-group" style="text-transform: uppercase">Declaration of approved medical practioner:</label>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="hearing_sight_color">The hearing, sight and color vision of the seafarer meets the required
                        standards for his / her rank,
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="hearing_sight_color_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="hearing_sight_color" id="hearing_sight_color_yes" value="1">
                    <label class="form-label" for="hearing_sight_color_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="hearing_sight_color" id="hearing_sight_color_no" value="0">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="seafarer_is_suffering">
                        The seafarer is not suffering from any disease likely to be aggravated by, or render him/her
                        unfit for, service at sea or likely to endanger the health of other persons on board ships, and specifically that the examinee is free from
                        all communicable diseases (including without limitation Tuberculosis).
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="seafarer_is_suffering_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="seafarer_is_suffering" id="seafarer_is_suffering_yes" value="1">
                    <label class="form-label" for="seafarer_is_suffering_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="seafarer_is_suffering" id="seafarer_is_suffering_no" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="visual_aid">The seafarer needs visual aids to perform duties.</label>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="form-group">
                    <label class="form-label" for="visual_aid_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="visual_aid" id="visual_aid_yes" value="1">
                    <label class="form-label" for="visual_aid_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="visual_aid" id="visual_aid_no" value="0">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="regular_medication">The seafarer is taking regular medication.(Place comments in remark*,_Concession form to be used).</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="regular_medication_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="regular_medication" id="regular_medication_yes" value="1">
                    <label class="form-label" for="regular_medication_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="regular_medication" id="regular_medication_no" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="fit_for_duty">The seafarer is FIT FOR LOOKOUT DUTY.</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="fit_for_duty_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="fit_for_duty" id="fit_for_duty_yes" value="1">
                    <label class="form-label" for="fit_for_duty_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="fit_for_duty" id="fit_for_duty_no" value="0">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="fit_for_duty_restrictions">The seafarer is FIT FOR DUTY without restrictions.</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="fit_for_duty_restrictions_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="fit_for_duty_restrictions" id="fit_for_duty_restrictions_yes" value="1">
                    <label class="form-label" for="fit_for_duty_restrictions_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="fit_for_duty_restrictions" id="fit_for_duty_restrictions_no" value="0">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label">The examinee medically fit to perform duties as:</label>
                </div>
                <div class="form-group" style="padding-left: 100px;">
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="deck_officer">Deck Officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="performed_duty" id="deck_officer" value="0">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="engineering_officer">Engineering officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="performed_duty" id="engineering_officer" value="1">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="rating_deck">Rating-deck</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="performed_duty" id="rating_deck" value="2">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="rating_engine">Rating-Engine</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="performed_duty" id="rating_engine" value="3">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="catering_department">Catering department</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="performed_duty" id="catering_department" value="4">
                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="issued_date">
                        Issued Date
                    </label>
                    <input class="form-control" type="date"  name="issued_date" id="issued_date" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="valid_till">
                        Valid Till
                    </label>
                    <input class="form-control" type="date"  name="valid_till" id="valid_till" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>

    </form>
@endsection
