<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{{ asset('public/js/style.js') }}"></script>
</head>
<body>
<div class="ser_sea_full">
    <div class="ser_sea_body">
        <div class="visible-print text-center" style="padding: 2px;">
            @php
                use SimpleSoftwareIO\QrCode\Facades\QrCode;
                $path  = 'qrcodes/_'.now().$form->id.'.svg';
                QrCode::generate(route('smcseafarer_recrord.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
            @endphp
            <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
            <p>Scan me to return to the original page.</p>
        </div>
        <h4 class="ser_sea_title_1">MEDICAL CERTIFICATE FOR SERVICE AT SEA</h4>
        <div class="ser_sea_row_1">
            <div class="ser_sea_familyy_name_row s-float">
                <div class="ser_sea_1_inner_row">
                    <div class="ser_sea_family_value s_float">
                        Last/Family Name
                    </div>
                    <div class="ser_sea_family_value s_float">
                           {{ $form->surname }}
                    </div>
                    <div class="ser_sea_family_value s_float">
                        First/Given Name
                    </div>
                    <div class="ser_sea_family_value s_float" style="border-right: none;">
                        {{ $form->given_name }}
                    </div>
                </div>
                <div class="ser_sea_1_inner_row">
                    <div class="ser_sea_family_value s_float">
                        Position applied for
                    </div>
                    <div class="ser_sea_family_value s_float">
                         {{ $form->po_applied_for}}
                    </div>
                    <div class="ser_sea_family_value s_float">
                        Date of Birth
                    </div>
                    <div class="ser_sea_family_value s_float" style="border-right: none;">
                        {{ $form->dob }}
                    </div>
                </div>
                <div class="ser_sea_1_inner_row">
                    <div class="ser_sea_family_value s_float">
                        CDC Number
                    </div>
                    <div class="ser_sea_family_value s_float">
                        {{ $form->cdc_no }}
                    </div>
                    <div class="ser_sea_family_value s_float">
                        Passport No
                    </div>
                    <div class="ser_sea_family_value s_float" style="border-right: none;">
                        {{ $form->passport_no }}
                    </div>
                </div>
                <div class="ser_sea_1_inner_row">
                    <div class="ser_sea_family_value s_float">
                        Sex
                    </div>
                    <div class="ser_sea_family_value s_float">
                           {{ $form->gender }}
                    </div>
                    <div class="ser_sea_family_value s_float">

                    </div>
                    <div class="ser_sea_family_value s_float" style="border-right: none;">

                    </div>
                </div>

            </div>
            <div class="ser_sea_photo s_float">

            </div>
        </div>

        <div class="ser_sea_row_2">
            <h4 class="ser_sea_ass_fit">Assessment of fitness for service at sea</h4>
            <p class="ser_sea_ass_text">I have evaluated the above-named examinee according to medical standards and regulations of Merchant Shipping Medical Examination Rules (2010), ILOM/HO/STCW 1/9 and MLC2006 Reg 8.1.2 Guidelines for Conducting Pre-Sea and Periodic Medical Examinations of Seafarers. On the basis of the examinees pesonal declaration, my clinical examination, and the diagnostic test results obtained, and in consideration of the essential requirements of the position applied for, my opinion is:</p>
        </div>
        <div class="ser_sea_row_3 clear">
            <div class="ser_sea_row_3_no_1">
                <div class="ser_sea_no s_float">

                </div>
                <div class="ser_sea_option_text s_float">

                </div>
                <div class="ser_sea_yes s_float">
                    Yes
                </div>
                <div class="ser_sea_no_s s_float">
                    No
                </div>
            </div>

            <div class="ser_sea_row_3_no_1">
                <div class="ser_sea_no s_float">
                    1
                </div>
                <div class="ser_sea_option_text s_float">
                    The hearing, sight and eolsr vision sf the seafarer meets the required standards for his / her rank,
                </div>
                <div class="ser_sea_yes s_float">
                    @if($form->hearing_sight_color == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="ser_sea_no_s s_float">
                    @if($form->hearing_sight_color == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
            </div>

            <div class="ser_sea_row_3_no_2">
                <div class="ser_sea_no s_float ser_sea_ex_2">
                    2
                </div>
                <div class="ser_sea_option_text ser_sea_ex_2 s_float">
                    The seafarer is not suffering from any disease likely to be aggravated by, or render him/her
                    unfit for, service at sea or likely to endanger the health of other persons on board ships, and
                    specifically that the examinee is free from all communicable diseases {including without
                    limitation Tuberculosis).
                </div>
                <div class="ser_sea_yes s_float ser_sea_ex_2">
                    @if($form->seafarer_is_suffering == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="ser_sea_no_s s_float ser_sea_ex_2">
                    @if($form->seafarer_is_suffering == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
            </div>

            <div class="ser_sea_row_3_no_3">
                <div class="ser_sea_no s_float ser_sea_ex_3">
                    3
                </div>
                <div class="ser_sea_option_text s_float ser_sea_ex_3">
                    The seafarer needs visual aids to perform duties.
                </div>
                <div class="ser_sea_yes s_float ser_sea_ex_3">
                    @if($form->visual_aid == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="ser_sea_no_s s_float ser_sea_ex_3">
                    @if($form->visual_aid == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
            </div>

            <div class="ser_sea_row_3_no_1">
                <div class="ser_sea_no s_float">
                    4
                </div>
                <div class="ser_sea_option_text s_float">
                    The seafarer is taking regular medication. <span style="font-size: 11px;">(Place comments in remark*,-Concession Declaration form to be used).</span>
                </div>
                <div class="ser_sea_yes s_float">
                    @if($form->regular_medication == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="ser_sea_no_s s_float">
                    @if($form->regular_medication == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
            </div>

            <div class="ser_sea_row_3_no_3">
                <div class="ser_sea_no s_float ser_sea_ex_3">
                    5
                </div>
                <div class="ser_sea_option_text s_float ser_sea_ex_3">
                    The seafarer is <span class="ser_sea_fit_lookout_value"> FIT FOR LOOKOUT DUTY.</span>
                </div>
                <div class="ser_sea_yes s_float ser_sea_ex_3">
                    @if($form->fit_for_duty == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="ser_sea_no_s s_float ser_sea_ex_3">
                    @if($form->fit_for_duty == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
            </div>
            <div class="ser_sea_row_3_no_3">
                <div class="ser_sea_no s_float ser_sea_ex_3">
                    6
                </div>
                <div class="ser_sea_option_text s_float ser_sea_ex_3">
                    The seafarer is <span class="fit_without_res">FIT FOR DUTY</span> without restrictions.
                </div>
                <div class="ser_sea_yes s_float ser_sea_ex_3">
                    @if($form->fit_for_duty_restrictions == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="ser_sea_no_s s_float ser_sea_ex_3">
                    @if($form->fit_for_duty_restrictions == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
            </div>

        </div>

        <div class="ser_sea_row_4">
            <p class="personal_dec">On the basis of the examinee's personal declaration, my clinical examination and the diagnostic test results recorded above, I declare the examinee medically fit to perform duties as:</p>
            <div class="ser_sea_deck_sec">
                <div class="ser_sea_deck_row">
                    <div class="ser_sea_deck_no s_float">
                        1
                    </div>
                    <div class="ser_sea_deck_text s_float">
                        DECK OFFICER
                    </div>
                    <div class="ser_sea_deck_check s_float">
                        @if($form->performed_duty == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ser_sea_deck_row clear">
                    <div class="ser_sea_deck_no s_float">
                        2
                    </div>
                    <div class="ser_sea_deck_text s_float">
                        ENGINEERING OFFICER
                    </div>
                    <div class="ser_sea_deck_check s_float">
                        @if($form->performed_duty == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ser_sea_deck_row clear">
                    <div class="ser_sea_deck_no s_float">
                        3
                    </div>
                    <div class="ser_sea_deck_text s_float">
                        RAING- DECK
                    </div>
                    <div class="ser_sea_deck_check s_float">
                        @if($form->performed_duty == '2')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ser_sea_deck_row clear">
                    <div class="ser_sea_deck_no s_float">
                        4
                    </div>
                    <div class="ser_sea_deck_text s_float">
                        RATING- ENGINE
                    </div>
                    <div class="ser_sea_deck_check s_float">
                        @if($form->performed_duty == '3')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="ser_sea_deck_row clear">
                    <div class="ser_sea_deck_no s_float">
                        5
                    </div>
                    <div class="ser_sea_deck_text s_float">
                        CATERING DEPARTMEN
                    </div>
                    <div class="ser_sea_deck_check s_float">
                        @if($form->performed_duty == '4')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="ser_sea_row_5">
            <div class="ser_sea_reason s_float">Reason for unit:</div>
            <div class="ser_sea_reason_value s_float">N/A</div>
        </div>
        <div class="ser_sea_row_6 clear">
            <div class="ser_sea_remark s_float">*Remark:</div>
            <div class="ser_sea_remark_value s_float">FIT FOR SEA SERVICES</div>
        </div>

        <div class="ser_sea_row_7">
            <div class="ser_sea_phy_sign_sec s_float">
                <p class="ser_sea_phy_sign">Physician Signature: <span class="ser_sea_phy_sign_value"></span></p>
                <p class="ser_sea_phy_printed_text">Physician name Printed: <span class="ser_sea_phy_printed_value"></span></p>
                 @php
                     $date = \Carbon\Carbon::parse($form->issued_date);
                 @endphp
                <div class="ser_sea_date_valid s_float">Date <span class="ser_sea_day">{{$date->day}}</span> <span class="ser_sea_month">{{$date->month}}</span> <span class="ser_sea_year">{{$date->year}}</span></div>

                @php
                    $date = \Carbon\Carbon::parse($form->issued_date);
                @endphp
                <div class="ser_sea_date_valid s_float">Date <span class="ser_sea_day">{{$date->day}}</span> <span class="ser_sea_month">{{$date->month}}</span> <span class="ser_sea_year">{{$date->year}}</span></div>
            </div>
            <div class="ser_sea_clinic_stamp">
                <p style="margin-left: 10px;">Clinic stamp</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
