<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
</head>
<body>
<div class="all">
    <div class="all_in">
        <div>
            <div class="visible-print text-center" style="padding: 2px;">
                @php
                    use SimpleSoftwareIO\QrCode\Facades\QrCode;
                    $path  = 'qrcodes/_'.now().$smCertificate->id.'.svg';
                    QrCode::generate(route('smCertificate.index.guest') . '?search=' . $smCertificate->id, public_path('qrcodes/_'.now().$smCertificate->id.'.svg'));
                @endphp
                <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
                <p>Scan me to return to the original page.</p>
            </div>
        </div>
        <div class="row row_one">
            <p class="head_one">
                lSSUED 0N BEHALF OF THE DEPARTMENT OF SHIPPING
                GOVERNMENT 0F THE PEOPLE[S REPUBLIC 0F BANGLADESH
            </p>
        </div>
        <!-- row_one end -->

        <div class="row_two">
            <div class="from_no s_float">
                <p>Form No: SMC</p>
            </div>
            <div class="bd_logo s_float">
                <center><img src="img/bd.svg" class="bd"></center>
            </div>
            <div class="sl_no s_float">
                <div class="sl_text s_float">
                    SL NO:
                </div>
                <div class="sl_value s_float">
                    08.21.2010
                </div>

            </div>
        </div>
        <!-- row_two end -->

        <div class="row_three">
            <h2 class="med_cer">SEAFARER MEDICAL CERTIFICATE</h2>
            <p class="cer_detail">
                This certificate is issued in accordance with Bangladesh' Merchant Shipping Ordinance, 1983 and
                Bangladesh Merchant Shipping Officers and F`atings Training, Certification, Recruitment, Work Hours and
                Watch keeping Plules, 2011 in compliance with the International .Convention on Standards of Traing
                Certificate and Watch keeping for Seafarers,1978 as amended (STOW.78) and F]egulation 1.2 of the
                Maritime Labour Convention, 2006

            </p>
        </div>
        <!-- row_three end -->

        <div class="row_four">
            <h4 class="sea_info">SEAFARER INFORMATION:</h4>
            <div class="name_row info_defalt">
                <div class="last s_float">
                    <div class="last_name s_float">
                        <p>Name: Last:</p>
                    </div>
                    <div class="last_value s_float">
                        <p>{{ $smCertificate->lastName ?? ''}}</p>
                    </div>
                </div>
                <div class="first s_float">
                    <div class="first_name s_float">
                        <p>First:</p>
                    </div>
                    <div class="first_value s_float">
                        <p>{{ $smCertificate->firstName ?? ''}}</p>
                    </div>
                </div>
                <div class="middle s_float">
                    <div class="middle_name s_float">
                        <p>Middle:</p>
                    </div>
                    <div class="midle_value s_float">
                        <p>{{ $smCertificate->middleName ?? ''}}</p>
                    </div>
                </div>
            </div>
            <div class="birth_date_row clear info_defalt">
                <div class="date_text">
                    <p>Date of Birth:(DD/MM/YYYY):</p>
                </div>
                <div class="date_value">
                    <p>{{date('d-m-Y', strtotime($smCertificate->dob))}}</p>
                </div>
            </div>
            <div class="gender_row info_defalt">
                <div class="gender_text">
                    <p>Gender:(Male/Female):</p>
                </div>
                <div class="gender_value">
                    <p>{{ $smCertificate->gender }}</p>
                </div>
            </div>
            <div class="nationality_row info_defalt">
                <div class="nationality_col s_float">
                    <div class="nationality_text s_float">
                        <p>Nationality: </p>
                    </div>
                    <div class="nationality_value s_float">
                        <p>{{ $smCertificate->nationality }}</p>
                    </div>
                </div>
                <div class="passport s_float">
                    <div class="passport_text s_float">
                        <p>Passport/NID No:</p>
                    </div>
                    <div class="passport_value s_float">
                        <p>{{ $smCertificate->passport_no }}</p>
                    </div>
                </div>
            </div>
            <div class="cdc_row info_defalt">
                <div class="cdc_col s_float">
                    <div class="cdc_text s_float">
                        <p>CDC No:</p>
                    </div>
                    <div class="cdc_value s_float">
                        <p>{{ $smCertificate->cdc_no }}</p>
                    </div>
                </div>
                <div class="seaman_col s_float">
                    <div class="seaman_text s_float">
                        <p>Seaman ID No: </p>
                    </div>
                    <div class="seaman_value s_float">
                        <p>{{ $smCertificate->seaman_id }}</p>
                    </div>
                </div>
            </div>
            <div class="occupation_row info_defalt">
                <div class="occupation_text s_float">
                    <p>Occupation : Deck/Engine/Catering/Other (specify):</p>
                </div>
                <div class="occupation_value s_float">
                    <p>{{ $smCertificate->occupation }}</p>
                </div>
            </div>
            <div class="fathers_name_row info_defalt clear">
                <div class="father_text s_float">
                    <p>Father.s/Husband.s Name:</p>
                </div>
                <div class="father_value s_float">
                    <p>{{ $smCertificate->father_husband_name }}</p>
                </div>
            </div>
            <div class="mothers_name_row info_defalt clear">
                <div class="mother_text s_float">
                    <p>Mother's Name:</p>
                </div>
                <div class="mother_value s_float">
                    <p>{{ $smCertificate->mother_name }}</p>
                </div>
            </div>
            <div class="mailing_row info_defalt">
                <div class="mailing_col s_float">
                    <div class="mailing_text s_float">
                        <p>Mailing Address:</p>
                    </div>
                    <div class="mailing_value s_float">
                        <p>{{ $smCertificate->mailing_address }}</p>
                    </div>
                </div>
                <div class="house_col s_float">
                    <div class="house_text s_float">
                        <p>House No.:</p>
                    </div>
                    <div class="house_value s_float">
                        <p>{{ $smCertificate->house_no }}</p>
                    </div>
                </div>
                <div class="street_col s_float">
                    <div class="street_text s_float">
                        <p>Street/Road No.:</p>
                    </div>
                    <div class="street_value s_float">
                        <p>{{ $smCertificate->street_no }}</p>
                    </div>
                </div>
            </div>
            <div class="locality_row info_defalt">
                <div class="locality_col s_float">
                    <div class="locality_text s_float">
                        <p>Locality/Village:</p>
                    </div>
                    <div class="locality_value s_float">
                        <p>{{$smCertificate->village}}</p>
                    </div>
                </div>
                <div class="po_col s_float">
                    <div class="po_text s_float">
                        <p>P.O:</p>
                    </div>
                    <div class="po_value s_float">
                        <p>{{ $smCertificate->p_o ?? '' }}</p>
                    </div>
                </div>
            </div>
            <div class="ps_row info_defalt">
                <div class="ps_col s_float">
                    <div class="ps_text s_float">
                        <p>P.S:</p>
                    </div>
                    <div class="ps_value s_float">
                        <p>{{ $smCertificate->p_s ?? '' }}</p>
                    </div>
                </div>
                <div class="district_col s_float">
                    <div class="district_text s_float">
                        <p>District</p>
                    </div>
                    <div class="district_value s_float">
                        <p>{{ $smCertificate->district }}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- row_four end -->

        <div class="row_five clear">
            <h2 class="diclaration">DECLAPATION 0F THE RECOGNIZED MEDICAL PRACTITIONER : </h2>
            <p class="dec_des">I am duly authorized by he Department of Shipping, Government of the Perople.s Republic of Bangladesh and confirm the followings:</p>
            <div class="yes_no_row">
                <div class="y_row">
                    <div class="no s_float">
                        1.
                    </div>
                    <div class="y_col s_float">
                        <p>Confirmation that identification documents were checked at the point of examination:</p>
                    </div>
                    <div class="y_value">
                        @if($smCertificate->confirmation == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            YES/
                        @if($smCertificate->confirmation == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            NO
                    </div>
                </div>
                <div class="y_row clear">
                    <div class="no s_float">
                        2.
                    </div>
                    <div class="y_col s_float">
                        <p>Hearing meets the standards in section A-1 / 9 ?</p>
                    </div>
                    <div class="y_value">
                        @if($smCertificate->hearing_standard == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        YES/
                        @if($smCertificate->hearing_standard == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        NO
                    </div>
                </div>
                <div class="y_row">
                    <div class="no s_float">
                        3.
                    </div>
                    <div class="y_col s_float">
                        <p>Unaided hearing stisfactory?</p>
                    </div>
                    <div class="y_value">
                        @if($smCertificate->unaided_statisfactory == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                          YES/
                        @if($smCertificate->unaided_statisfactory == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                          NO
                    </div>
                </div>
                <div class="y_row">
                    <div class="no s_float">
                        4.
                    </div>
                    <div class="y_col s_float">
                        <p>Visual acutity meets standards in section A-1 / 9 ?</p>
                    </div>
                    <div class="y_value">
                        @if($smCertificate->visual_acutity == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        YES/
                        @if($smCertificate->visual_acutity == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        NO
                    </div>
                </div>
                <div class="y_row">
                    <div class="no s_float">
                        5.
                    </div>
                    <div class="y_col s_float">
                        <p>Colour vision meets standards in section A-1 / 9 ?</p>
                    </div>
                    <div class="y_value">
                        @if($smCertificate->color_vision_standard == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        YES/
                        @if($smCertificate->color_vision_standard == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        NO
                    </div>
                </div>
                <div class="y_row clear">
                    <div class="color">
                        <p>Date of last colour vision test</p>
                    </div>
                    <div class="color_value">
                        <p>{{date('d-m-Y', strtotime($smCertificate->color_vision_test))}}</p>
                    </div>
                </div>
                <div class="y_row">
                    <div class="no s_float">
                        6.
                    </div>
                    <div class="y_col s_float">
                        <p>Fit for lookoutduties?</p>
                    </div>
                    <div class="y_value">
                        @if($smCertificate->fit_lookout == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        YES/
                        @if($smCertificate->fit_lookout == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        NO
                    </div>
                </div>
                <div class="y_row">
                    <div class="no s_float">
                        7.
                    </div>
                    <div class="y_col s_float">
                        <p>Is the seafarer free from any medical condition likely to be aggravated by service at sea or the render the seafarer unfit for service or the render the health of any other persons on board ?</p>
                    </div>
                    <div class="y_value">
                        @if($smCertificate->unfit_for_service == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        YES/
                        @if($smCertificate->unfit_for_service == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        NO
                    </div>
                </div>
                <div class="y_row">
                    <div class="no s_float">
                        8.
                    </div>
                    <div class="y_col s_float">
                        <p>Any limitations or restictions on fitness ?</p>
                    </div>
                    <div class="y_value">
                        @if($smCertificate->any_restrictions == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        YES/
                        @if($smCertificate->any_restrictions == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        NO
                    </div>
                </div>
                <div class="y_row">
                    <div class="y_col s_float">
                        <p class="if_yes">lf YES, specify limitations or restrictions</p>
                    </div>
                    <div class="y_value">

                    </div>
                </div>

            </div>

            <div class="duties_row clear">
                <div class="duties">
                    <p>Duties:{{ $smCertificate->duties }}</p>
                    <p>Location:{{ $smCertificate->location }}</p>
                    <p>Medical/Other:{{ $smCertificate->medical_other }}</p>
                </div>
            </div>
            <div class="medical_fitness info_defalt">
                <div class="no s_float">
                    9.
                </div>
                <div class="fitness s_float">
                    Medical fitness category:
                </div>
                <div class="fit s_float">
                    <p>
                        @if($smCertificate->any_restrictions == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Fit-No restriction
                    </p>
                </div>
                <div class="fit_subject s_float">
                    @if($smCertificate->any_restrictions == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        Fit-subject to restrictions
                </div>
                <div class="unfit s_float">
                    @if($smCertificate->any_restrictions == '2')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        Unfit
                </div>
            </div>

            <div class="date_of_exam info_defalt clear">
                <div class="no s_float">
                    10.
                </div>
                <div class="exam_text s_float">
                    <p> Date of examination/Issue (DD/MM/YYYY):</p>
                </div>
                <div class="exam_value s_float">
                    <p>{{ date('d-m-Y', strtotime($smCertificate->doe)) }}</p>
                </div>
            </div>
            <div class="date_of_expiry info_defalt clear">
                <div class="no s_float">
                    11.
                </div>
                <div class="expiry_text s_float">
                    <p>Date of expiry (DD/MM/YYYY):</p>
                </div>
                <div class="expiry_value s_float">
                    <p>{{ date('d-m-Y', strtotime($smCertificate->doexpiry)) }}</p>
                </div>
                <div class="year s_float">
                    <p>"No more than 2 years from the date of examination"</p>
                </div>
            </div>
        </div>


        <!-- row_five end -->
        <div class="row_six clear">
            <div class="foot_one s_float">
                <p>I have read the contents of the certificate and have been informed or the right to review.</p>
                <p class="sign">Seafarer's Signature</p>
            </div>
            <div class="foot_two s_float">
                <p>Offical Stamp</p>
            </div>
            <div class="foot_three s_float">
                <div class="dr">

                </div>
                <p>Name & Signature of the practitioner :</p>
            </div>
        </div>
        <!-- row_six end -->

    </div>
</div>
</body>
</html>
