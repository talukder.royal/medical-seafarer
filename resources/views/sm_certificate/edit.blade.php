@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <div style="height: 100px;" class="row justify-content-center">
        <div class="col-lg-6">
            <div style="font-weight: bold; font-size: 1.5rem; text-transform: capitalize" class="text-center">Seafarer Medical Certificate</div>
        </div>
    </div>
    <form method="post" action="{{ route('smCertificate.update', $smCertificate->id)}}">
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-label" for="firstName">First Name:</label>
                                <input type="text" name="firstName" class="form-control" id="firstName" value="{{ old('firstName', $smCertificate->firstName ?? '') }}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-label" for="middleName">Middle Name:</label>
                                <input type="text" name="middleName" class="form-control" id="middleName"  value="{{ old('middleName', $smCertificate->middleName ?? '')}}">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-label" for="lastName">Last Name:</label>
                                <input  type="text" name="lastName" class="form-control" id="lastName" value="{{ old('lastName', $smCertificate->lastName ?? '')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="dob" > Date of Birth:(DD/MM/YYYY):</label>
                    <input type="date" name="dob" class="form-control" id="dob" value="{{ old('dob', $smCertificate->dob ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="gender" >Gender:(Male/Female):</label>
                    <select class="form-control" name="gender" id="gender">
                        <option>Select option</option>
                        <option value="Male" {{ $smCertificate->gender == 'Male' ? 'selected' : '' }}>Male</option>
                        <option value="Female" {{ $smCertificate->gender == 'Female' ? 'selected' : '' }}>Female</option>
                        <option value="Others" {{ $smCertificate->gender == 'Others' ? 'selected' : '' }}>Others</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label" for="nationality">Nationality:</label>
                    <input class="form-control" type="text" name="nationality" id="nationality" value="{{ old('nationality', $smCertificate->nationality ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="passport_no">Passport/NID No::</label>
                    <input class="form-control" type="text" name="passport_no" id="passport_no" value="{{ old('passport_no', $smCertificate->passport_no ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="cdc_no">CDC No::</label>
                    <input class="form-control" type="text" name="cdc_no" id="cdc_no" value="{{ old('cdc_no', $smCertificate->cdc_no ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="seaman_id">Seaman ID No:</label>
                    <input class="form-control" type="text" name="seaman_id" id="seaman_id"  value="{{ old('seaman_id', $smCertificate->seaman_id ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="occupation">Occupation : Deck/Engine/Catering/Other (specify):</label>
                    <input class="form-control" type="text" name="occupation" id="occupation"  value="{{ old('occupation', $smCertificate->occupation ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="father_husband_name">Father.s/Husband.s Name:</label>
                    <input class="form-control" type="text" name="father_husband_name" id="father_husband_name"  value="{{ old('father_husband_name', $smCertificate->father_husband_name ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="mother_name">Mother's Name:</label>
                    <input class="form-control" type="text" name="mother_name" id="mother_name"  value="{{ old('mother_name', $smCertificate->mother_name ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="seaman_id">Mailing Address:</label>
                    <input class="form-control" type="text" name="mailing_address" id="mailing_address"  value="{{ old('mailing_address', $smCertificate->mailing_address ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="house_no">House No:</label>
                    <input class="form-control" type="text" name="house_no" id="house_no"  value="{{ old('house_no', $smCertificate->house_no ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="street_no">Street/Road No:</label>
                    <input class="form-control" type="text" name="street_no" id="street_no"  value="{{ old('street_no', $smCertificate->street_no ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="village">Locality/Village:</label>
                    <input class="form-control" type="text" name="village" id="village"  value="{{ old('village', $smCertificate->village ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="p_o">P.O:</label>
                    <input class="form-control" type="text" name="p_o" id="p_o"  value="{{ old('p_o', $smCertificate->p_o ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="p_s">P.S:</label>
                    <input class="form-control" type="text" name="p_s" id="p_s"  value="{{ old('p_s', $smCertificate->p_s ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="district">District:</label>
                    <input class="form-control" type="text" name="district" id="district" value="{{ old('district', $smCertificate->district ?? '') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="text-center">
                DECLAPATION 0F THE RECOGNIZED MEDICAL PRACTITIONER :
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="confirmation">1. Confirmation that identification documents were checked at the point of examination:</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="confirmation" value="1" {{ $smCertificate->confirmation == '1' ? 'checked' : '' }} >
                    <label style="font-size: 20px;" for="confirmation">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="confirmation" value="0" {{ $smCertificate->confirmation == '0' ? 'checked' : '' }}>
                    <label for="pregnancy" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="hearing_standard">2.Hearing meets the standards in section A-1 / 9 ?</label>
                    <br>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="hearing_standard" name="hearing_standard" value="1" {{ $smCertificate->hearing_standard == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="hearing_standard">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="hearing_standard" name="hearing_standard" value="0" {{ $smCertificate->hearing_standard == '0' ? 'checked' : '' }}>
                    <label for="hearing_standard" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="unaided_statisfactory">3.Unaided hearing stisfactory?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="unaided_statisfactory" name="unaided_statisfactory" value="1" {{ $smCertificate->unaided_statisfactory == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="unaided_statisfactory">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="unaided_statisfactory" name="unaided_statisfactory" value="0" {{ $smCertificate->unaided_statisfactory == '0' ? 'checked' : '' }}>
                    <label for="unaided_statisfactory" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="visual_acutity">4.Visual acutity meets standards in section A-1 / 9 ?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="visual_acutity" name="visual_acutity" value="1"  {{ $smCertificate->visual_acutity == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="visual_acutity">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="visual_acutity" value="0"  {{ $smCertificate->visual_acutity == '0' ? 'checked' : '' }}>
                    <label for="visual_acutity" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_vision_standard">5.Colour vision meets standards in section A-1 / 9 ?</label>
                    <br>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="color_vision_standard" name="color_vision_standard" value="1" {{ $smCertificate->color_vision_standard == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="color_vision_standard">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="color_vision_standard" name="color_vision_standard" value="0" {{ $smCertificate->color_vision_standard == '0' ? 'checked' : '' }}>
                    <label for="color_vision_standard" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_vision_test">Date of last colour vision test</label>
                    <input type="date" class="form-control" id="color_vision_test" name="color_vision_test" value="{{ old('color_vision_test', $smCertificate->color_vision_test ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="fit_lookout">6.Fit for lookoutduties?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="fit_lookout" value="1" {{ $smCertificate->fit_lookout == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="fit_lookout">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="fit_lookout" value="0" {{ $smCertificate->fit_lookout == '0' ? 'checked' : '' }}>
                    <label for="fit_lookout" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="unfit_for_service">7.Is the seafarer free from any medical condition likely to be aggravated by service at sea or the render the seafarer unfit for service or the render the health of any other persons on board ?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="visual_acutity" name="unfit_for_service" value="1" {{ $smCertificate->unfit_for_service == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="unfit_for_service">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="unfit_for_service" value="0" {{ $smCertificate->unfit_for_service == '0' ? 'checked' : '' }}>
                    <label for="unfit_for_service" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="any_restrictions">8.Any limitations or restictions on fitness ?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="any_restrictions" value="1" {{ $smCertificate->any_restrictions == '1' ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="any_restrictions">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="fit_lookout" name="any_restrictions" value="0" {{ $smCertificate->any_restrictions == '0' ? 'checked' : '' }}>
                    <label for="any_restrictions" style="font-size: 20px;">No</label>
                    <br>
                    <label for="" style="font-size: 20px;">lf YES, specify limitations or restrictions</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="duties">Duties:</label>
                    <input class="form-control" type="text" id="duties" name="duties"  value="{{ old('duties', $smCertificate->duties ?? '') }}">
                    <label class="form-label" for="location">Location:</label>
                    <input class="form-control" type="text" id="location" name="location"  value="{{ old('location', $smCertificate->location ?? '') }}">
                    <label class="form-label" for="medical_other">Medical/Other:</label>
                    <input class="form-control" type="text" id="medical_other" name="medical_other"  value="{{ old('medical_other', $smCertificate->medical_other ?? '') }}">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="d-flex justify-content-between">
                    <div>
                        9.Medical fitness category:
                    </div>
                    <div class="form-group">
                        <input type="checkbox" style="width: 25px; height: 20px;" name="fit_restrictions" value="0" {{ $smCertificate->fit_restrictions == '0' ? 'checked' : '' }}><label style="font-size: 20px;">Fit-No restriction</label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" style="width: 25px; height: 20px;" name="fit_restrictions" value="1" {{ $smCertificate->fit_restrictions == '1' ? 'checked' : '' }}><label style="font-size: 20px;">Fit-subject to restrictions</label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" style="width: 25px; height: 20px;" name="fit_restrictions" value="2" {{ $smCertificate->fit_restrictions == '2' ? 'checked' : '' }}><label style="font-size: 20px;">Unfit</label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-label" for="doe">10.Date of examination/Issue(DD/MM/YYY)</label>
                    <input class="form-control" type="date" name="doe" id="doe" value="{{ old('doe', $smCertificate->doe ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="doexpiry">11.Date of expiry(DD/MM/YYY)</label>
                    <input class="form-control" type="date" name="doexpiry" id="doexpiry" value="{{ old('doexpiry', $smCertificate->doexpiry ?? '') }}">
                    <small>No more than 2 years from the date of examination.</small>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="mb-3">
                <input type="submit" class="btn btn-primary" name="submit" value="Save">
            </div>
        </div>
    </form>
@endsection
