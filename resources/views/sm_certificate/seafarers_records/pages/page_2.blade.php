@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    <div class="row justify-content-center">
        @include('includes.flash-message')
        <div class="col-lg-6 offset-lg-1" style="font-weight: bold; font-size: 20px;">Additional Questions</div>
    </div></br>
    <form method="POST" action="{{ route('smcseafarer_recrord.page2_store', $form->id) }}">
        @csrf
    <div class="row">
        <p style="font-size: 20px;">Seafarer's Declarations (please tick)</p>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="signed_off_sick">35.Have you ever been signed off as sick or repatriated from a ship?</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="signed_off_sick" name="signed_off_sick" value="1" {{ $form->signed_off_sick == 1 ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="signed_off_sick">Yes</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="signed_off_sick" name="signed_off_sick" value="0" {{ $form->signed_off_sick == '0' ? 'checked' : '' }}>
                    <label for="signed_off_sick" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="hospitalized">36.Have you ever been hospitalized?</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="hospitalized" name="hospitalized" value="1" {{ $form->hospitalized == 1 ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="hospitalized">Yes</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="hospitalized" name="hospitalized" value="0" {{ $form->hospitalized == '0' ? 'checked' : '' }}>
                    <label for="2.High_blood_pressure" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="declared_for_sea_duty">37.Have you ever been declared unfit for sea duty?</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="declared_for_sea_duty" name="declared_for_sea_duty" value="1" {{ $form->declared_for_sea_duty == 1 ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="heart_disease">Yes</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="declared_for_sea_duty" name="declared_for_sea_duty" value="0" {{ $form->declared_for_sea_duty == '0' ? 'checked' : '' }}>
                    <label for="declared_for_sea_duty" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="medical_certificate_revoked">38.Has your medical certificate even been restricted or revoked?</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="medical_certificate_revoked" name="medical_certificate_revoked" value="1" {{ $form->medical_certificate_revoked == 1 ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="medical_certificate_revoked">Yes</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="heart_surgery" name="medical_certificate_revoked" value="0" {{ $form->medical_certificate_revoked == '0' ? 'checked' : '' }}>
                    <label for="medical_certificate_revoked" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="medical_problems">39.Are you aware that you have any medical problems, diseases or illnesses?</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="medical_problems" name="medical_problems" value="1" {{ $form->medical_problems == 1 ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="medical_problems">Yes</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="medical_problems" name="medical_problems" value="0" {{ $form->medical_problems == '0' ? 'checked' : '' }}>
                    <label for="medical_problems" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="feel_healthy">40.Do you feel healthy and fit to perform the duties of your designated position/occupation?</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="feel_healthy" name="feel_healthy" value="1" {{ $form->feel_healthy == 1 ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="feel_healthy">Yes</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="feel_healthy" name="feel_healthy" value="0" {{ $form->feel_healthy =='0'? 'checked' : '' }}>
                    <label for="feel_healthy" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="allergic_medication">41.Are you allergic to any medication?</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="allergic_medication" name="allergic_medication" value="1" {{ $form->allergic_medication == 1 ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="allergic_medication">Yes</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="allergic_medication" name="allergic_medication" value="0" {{ $form->allergic_medication == '0'? 'checked' : '' }}>
                    <label for="allergic_medication" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="medical_condition">42.Is the seafarer suffering from any medical condition likely to be aggravated by service at sea or to render the seafarer unfit or to endanger the health of other person on board?</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="medical_condition" name="medical_condition" value="1" {{ $form->medical_condition == 1 ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="medical_condition">Yes</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="medical_condition" name="medical_condition" value="0" {{ $form->medical_condition == '0'? 'checked' : '' }}>
                    <label for="medical_condition" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label">Comments:</label>
                    <textarea name="comments" class="form-control"> {{ $form->comments }}</textarea>
                </div>
                <div class="form-group">
                    <label class="form-label" for="prescription_medication">43.Are you using any non-prescription or prescription medication?</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="prescription_medication" name="prescription_medication" value="1" {{ $form->prescription_medication == 1 ? 'checked' : '' }}>
                    <label style="font-size: 20px;" for="prescription_medication">Yes</label>
                    <input type="checkbox" style="width: 25px;
height: 20px;" id="prescription_medication" name="prescription_medication" value="0" {{ $form->prescription_medication == '0' ? 'checked' : '' }}>
                    <label for="prescription_medication" style="font-size: 20px;">No</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="form-label" for="provide_details">If you answer "yes" to any of the above questions, please provide details:</label>
                    <textarea class="form-control" name="provide_details" id="provide_details">{{ $form->provide_details ?? '' }}</textarea>
                </div>
                <div class="form-group">
                    <label class="form-label">
                        Date and contact details for previous medical examination (if known)
                    </label>
                    <br>
                    <label class="form-label" for="line1">Line1</label>
                    <input class="form-control" type="text" name="line1" id="line1" value="{{ old('line1', $form->line1 ?? '') }}">
                    <label class="form-label" for="line2">Line2</label>
                    <input class="form-control" type="text" name="line2" id="line2" value="{{ old('line2', $form->line2 ?? '') }}">
                </div>
            </div>
            <div class="d-flex justify-content-between">
                <div class="mb-3">
                    <a href="{{ route('smcseafarer_recrord.page1_edit', $form->id) }}" class="btn btn-primary" >Previous</a>
                </div>

                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save & Next">
                </div>
            </div>
        </div>
    </form>
@endsection
