<!DOCTYPE html>
<html>
<head>
    <title>Download</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{{ asset('public/js/style.js') }}"></script>

</head>
<body>
<div class="all">
    <div class="all_in annex_all">
        <div style="height: 100px;">
            <div class="visible-print text-center" style="padding: 2px;">
                @php
                    use SimpleSoftwareIO\QrCode\Facades\QrCode;
                    $path  = 'qrcodes/_'.now().$form->id.'.svg';
                    QrCode::generate(route('smcseafarer_recrord.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
                @endphp
                <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
                <p>Scan me to return to the original page.</p>
            </div>
        </div>
        <div class="row_one">
            <div class="dr_logo s_float">
                <img src="img/d_logo.png" height="70px">
            </div>
            <div class="sea_medical s_float">
                <h1>SEAFARER MEDICAL CERTIFICATE</h1>
            </div>
        </div>
        <div class="row_two dr_details clear"   style=" line-height:10%; height: 100px;">
            <div class="sea_dr_name s_float" style=" font-size: 12px; width: 25%">
                <h4 class="dr_title" style="font-size: 18px;">Dr. Sabrina Mostafa</h4>
                <p>MBBS(DU)</p>
                <p>Diploma in Radilogy and Imaging</p>
                <p>Reg NO. A98203 </p>
                <p>BMDC, Dhaka, Bangladesh</p>
                <p>Seafarer's Medical Practtitioner</p>
                <p>Arrroved byy D.G. Shipping</p>
                <p>Dhaka, Bangladesh</p>
            </div>
            <div class="chember s_float" style=" height: 100px; padding-top: 18px; font-size: 12px; margin-left: 15px;">
                <p><b>Chamber-1</b></p>
                <p><b>IDEAL PATHOLOGY</b></p>
                <p>162, Sk. Mujib Road, Mostafa Plaza (2nd floor)</p>
                <p> Badamtoly, Mazar Gate, Agrabad, Chittagong.</p>
                <p> Contact No :031-713056, 01711-014513</p>
                <div style="margin-top: 5px;">
                    <p><b>Chamber-2</b></p>
                    <p><b>NUR & SONS PHARMACY</b></p>
                    <p>126, Sk. Mujib Road, Chowmuhoni Moor
                    <p>Chittagong.</p>
                    <p>Contact No :031-713056, 01711-014513</p>
                </div>
            </div>
        </div>
        <div class="row_three cconsultant_details">
            <p class="consult_text" style="font-size: 12px;">Consultant Doctor: Shipping & Crew Manning Agencies: </p>
            <div class="cont_details_text" style="font-size:1.24em; word-spacing:0; padding: 0; margin: 0;">
                <p style="padding: 0; margin: 0">
                    <b>This Certificate is issued by the undersigned authorized Medical
                        practitioner By the Director General, Department of Shipping Dhaka Bangladesh,
                        to the named seafarer in compliance with requirements of regulation 1/9,
                        secction A-1/9 and section B-1/9, of the STCW 95 convention as amended in 2010,
                        Guideline B.I.2.1. of the MLC 2006 and Guidelines
                        on the medical examination of seafarer's 2013 Publised by ILO.
                    </b>
                </p>
            </div>

        </div>
        <div class="row_four clear">
            <h4 class="sea_format_title" style="font-weight: bold; width: 100%;">FORMAT FOR RECORDING MEDICAL EXAMINATIONS OF SEAFARERS</h4>
            <div class="sea_format_field" style="font-size: 12px;">
                <div class="sea_format_field_row">
                    <div class="sea_name s_float" style="font-family:sans-serif;
                     width: 20%; font-size: 12px;">
                        Name (last,  first, middle):
                    </div>
                    <div class="sea_dr_name_value s_float" style="width: 80%; padding: 0; margin: 0;">
                        <p style="padding: 0; margin: 0;">{{ $form->seafarers_name }}</p>
                    </div>
                </div>
                <div class="sea_format_field_row">
                    <div class="sea_date_of_birth s_float">
                        <div class="sea_date_of_birth_text s_float" style="width: 40%; font-size: 12px;">
                            Date of birth (day/month/year):
                        </div>
                        <div class="sea_date_of_birth_value s_float" style="padding: 0; margin: 0; width: 50%;">
                            <p style="padding: 0; margin: 0;">{{ date('d/m/Y', strtotime($form->date_of_birth)) }}</p>
                        </div>
                    </div>

                    <div class="sea_gender s_float">
                        <div class="sea_gender_text s_float">
                            Sex:
                        </div>
                        <div class="sea_gender_text_value s_float">
                            @if($form->gender == 'Male')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                                Male
                            @if($form->gender == 'Female')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                                Female
                        </div>
                    </div>
                </div>
                <div class="sea_format_field_row">
                    <div class="sea_home_address s_float" style="width:12%; font-size: 12px;">
                        Home Address:
                    </div>
                    <div class="sea_home_address_value s_float"  style="padding: 0; margin: 0; width: 88%;">
                        <p  style="padding: 0; margin: 0;">{{ $form->home_address}}</p>
                    </div>
                </div>
                <div class="sea_format_field_row">
                    <div class="sea_home_passport s_float" style="width:25%; font-size: 12px;">
                        Passport No /seafarer's book No:
                    </div>
                    <div class="sea_passport_value s_float"  style="padding: 0; margin: 0; width: 75%;" >
                        <p style="padding: 0; margin: 0;">{{ $form->passport_no }}</p>
                    </div>
                </div>
                <div class="sea_format_field_row" style="padding: 0;margin: 0;">
                    <div class="sea_home_dep s_float" style="width:41%; font-size: 12px;">
                        Department:(decck/engine/radio/food handling/other):
                    </div>
                    <div class="sea_dep_value s_float" style="padding: 0; margin: 0; width: 59%;">
                        <p style="padding: 0; margin: 0;">{{ $form->dept}}</p>
                    </div>
                </div>
                <div class="sea_format_field_row">
                    <div class="sea_rank s_float" style="width:4.5%; font-size: 12px;" >
                        Rank:
                    </div>
                    <div class="sea_rank_value s_float"  style="width:95.5%; font-size: 12px;">
                        <p  style="padding: 0; margin: 0;">{{ $form->rank }}</p>
                    </div>
                </div>
                <div class="sea_format_field_row">
                    <div class="sea_routine s_float" style="width:33%; font-size: 12px;">
                        Routine and emergenccy, duties (if known):
                    </div>
                    <div class="sea_rank_routine s_float p-0 m-0" style="padding: 0; margin: 0; width: 67%;">
                        <p style="padding: 0; margin: 0;">{{ $form->routine_duties }}</p>
                    </div>
                </div>
                <div class="sea_format_field_row">
                    <div class="sea_type s_float" style="width:37%; font-size: 12px;">
                        Type of ship (e.g container, Tanker, passenger):
                    </div>
                    <div class="sea_type_value s_float" style="padding: 0; margin: 0; width: 63%;">
                        <p style="padding: 0; margin: 0;">{{ $form->type_of_ship }}</p>
                    </div>
                </div>
                <div class="sea_format_field_row">
                    <div class="sea_trade s_float" style="width:35%; font-size: 12px;">
                        Trade area (e.g coastal, tropical, worldwide):
                    </div>
                    <div class="sea_trade_value s_float"  style="padding: 0; margin: 0; width: 65%;">
                        <p style="padding: 0; margin: 0;">{{ $form->trading_area }}</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- row_four end -->
        <div class="row_five" style="font-size: 12px; font-family: 'sans-serif';">
           <div style="margin: 0; padding: 0;  line-height: 3px;">
               <h5 class="exam_personal" style="font-weight: bold;">EXAMINATION PERSONAL DECLARATION</h5>
               <p style="font-size: 12px; font-weight: bold;">Assistances should Be Offerd By Medical Staff</p>
               <p>Have you ever had any of the following conditions?
               </p>
           </div>
            <div class="sea_med_table_one">
                <div class="sea_table_one_row">
                    <div class="sea_table_one_p_1 s_float" style="width: 49.77%;">
                        <div class="sea_table_2_row" style="padding: 0; margin: 0;">
                            <div class="sea_tab_2_num s_float" style="width: 10%">

                            </div>
                            <div class="sea_tab_2_con s_float" style="width: 60%">
                                Condition
                            </div>
                            <div class="sea_tab_2_yes s_float" style="width: 10%">
                                Yes
                            </div>
                            <div class="sea_tab_2_no s_float" style="width: 10%">
                                No
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float" style="width: 10%">
                                1
                            </div>
                            <div class="sea_tab_2_con s_float" style="width: 60%">
                                Eye / vision problem
                            </div>
                            <div class="sea_tab_2_yes s_float" style="width: 10%">
                                @if($form->eye_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float" style="width: 10%">
                                @if($form->eye_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float" style="width: 10%;">
                                2
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                High blood pressure
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->high_blood_pressure == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->high_blood_pressure == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                3
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Heart/Vascular disease
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->heart_disease == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->heart_disease == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                4
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Heart surgery
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->heart_surgery == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->heart_surgery == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                5
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Varicose veins/piles
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->varicose_veins == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->varicose_veins == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                6
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Asthma/bronchitis
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->asthma_bronchitis == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->asthma_bronchitis == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                7
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Blood disorder
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->blood_disorder == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->blood_disorder == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                8
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Diabetes
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->diabetes == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->diabetes == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                9
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Thyroid problems
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->thyroid_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->thyroid_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="sea_table_one_p_2 s_float" style="width: 49.99%; padding: 0; margin: 0;">
                        <div class="sea_table_2_row" style="width: 100%;">
                            <div class="sea_tab_2_num s_float" style="width: 10%;">

                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Condition
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                Yes
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                No
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                10
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Digestive disorder
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->digestive_disorder == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->digestive_disorder == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                11
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Kidney problems
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->kidney_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->kidney_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                12
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Skin problems
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->skin_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->skin_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                13
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Allergies
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->allergies == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->allergies == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                14
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Infectious/contagius diseases
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->infectious_diseases == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->infectious_diseases == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                15
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Hernia
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->hernia == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->hernia == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                16
                            </div>
                            <div class="sea_tab_2_num s_float"  style="width: 60%;">
                                Genital disorders
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->genital_disorder == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->genital_disorder == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                17
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%;">
                                Pregnancy
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->pregnancy == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->pregnancy == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float"  style="width: 10%;">
                                18
                            </div>
                            <div class="sea_tab_2_con s_float"  style="width: 60%; font-size: 10px;">
                                Do you smoke,use alcohol or drugs?
                            </div>
                            <div class="sea_tab_2_yes s_float"  style="width: 10%;">
                                @if($form->smoke_alcohol_drug == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float"  style="width: 10%;">
                                @if($form->smoke_alcohol_drug == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="all">
    <div class="all_in annex_all">

        <!-- row_four end -->
        <div class="row_one">

            <div class="sea_med_table_one_2">
                <div class="sea_table_one_row">
                    <div class="sea_table_one_p_1 s_float">
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                19
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Operation/surgery
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->operation == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->operation == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                20
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Epilepsy/ seizures
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->epilesy_seizures == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->epilesy_seizures == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row" style="height: 35px;">
                            <div class="sea_tab_2_num s_float" style="height: 35px;">
                                21
                            </div>
                            <div class="sea_tab_2_con s_float" style="height: 35px;">
                                Dizziness/fainting
                            </div>
                            <div class="sea_tab_2_yes s_float" style="height: 35px;">
                                @if($form->dizziness == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float" style="height: 35px;">
                                @if($form->dizziness == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                22
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Loss of consciousness
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->loss_of_consciousness == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->loss_of_consciousness == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                23
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Psychiatric problems
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->psychiatric_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->psychiatric_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                24
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Attempted suicide
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->attempted_suicide == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->attempted_suicide == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                25
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Loss of memory
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->loss_of_memory == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->loss_of_memory == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                26
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Balance problem
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->balance_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->balance_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="sea_table_one_p_2 s_float">
                        <div class="sea_table_2_row" style="height: 35px;">
                            <div class="sea_tab_2_num s_float" style="height: 35px;">
                                27
                            </div>
                            <div class="sea_tab_2_con s_float" style="height: 35px;">
                                Severe headaches
                            </div>
                            <div class="sea_tab_2_yes s_float" style="height: 35px;">
                                @if($form->severe_headache == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float" style="height: 35px;">
                                @if($form->severe_headache == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                28
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Ear
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->hearing_throat_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->hearing_throat_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                29
                            </div>
                            <div class="sea_tab_2_con s_float">
                                 Depression
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->depression == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->depression == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                30
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Restricted mobility
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->restricted_mobility == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->restricted_mobility == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                31
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Back or joint problems
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->joint_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->joint_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                32
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Amputation
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->amputation == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->amputation == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                33
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Fractures/dislocation
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->fracture == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->fracture == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">
                            <div class="sea_tab_2_num s_float">
                                34
                            </div>
                            <div class="sea_tab_2_con s_float">
                                Sleep problem
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->sleep_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->sleep_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row_two">
            <p class="sea_row_if">if any of the above questions were answered "yes", Please gice details
            </p>
            <p class="sea_row_if">
                {{ $form->provide_details_1 }}
            </p>
        </div>

        <div class="row_three clear">
            <div class="sea_med_table_two">
                <div class="sea_med_table_two_row">
                    <div class="sea_med_2_sr s_float">

                    </div>
                    <div class="sea_med_2_add s_float">
                        Additional question
                    </div>
                    <div class="sea_med_2_yes s_float">
                        Yes
                    </div>
                    <div class="sea_med_2_no s_float">
                        NO
                    </div>

                </div>
                <div class="sea_med_table_two_row">
                    <div class="sea_med_2_sr s_float">
                        35
                    </div>
                    <div class="sea_med_2_add s_float">
                        Have anyy ever been signed of as sick or repartiated form a ship?
                    </div>
                    <div class="sea_med_2_yes s_float">
                        @if($form->signed_off_sick == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="sea_med_2_no s_float">
                        @if($form->signed_off_sick == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="sea_med_table_two_row">
                    <div class="sea_med_2_sr s_float">
                        36
                    </div>
                    <div class="sea_med_2_add s_float">
                        Have you ever been hospitalized?
                    </div>
                    <div class="sea_med_2_yes s_float">
                        @if($form->hospitalized == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="sea_med_2_no s_float">
                        @if($form->hospitalized == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="sea_med_table_two_row">
                    <div class="sea_med_2_sr s_float">
                        37
                    </div>
                    <div class="sea_med_2_add s_float">
                        Have you ever been declared unfit for sea duty?
                    </div>
                    <div class="sea_med_2_yes s_float">
                        @if($form->declared_for_sea_duty == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="sea_med_2_no s_float">
                        @if($form->declared_for_sea_duty == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="sea_med_table_two_row">
                    <div class="sea_med_2_sr s_float">
                        38
                    </div>
                    <div class="sea_med_2_add s_float">
                        Has your medical certificate ever been restricted or revoked?
                    </div>
                    <div class="sea_med_2_yes s_float">
                        @if($form->medical_certificate_revoked == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="sea_med_2_no s_float">
                        @if($form->medical_certificate_revoked == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="sea_med_table_two_row">
                    <div class="sea_med_2_sr s_float">
                        39
                    </div>
                    <div class="sea_med_2_add s_float">
                        Are you aware that you have any medical problems, diseases or illness?
                    </div>
                    <div class="sea_med_2_yes s_float">
                        @if($form->medical_problems == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="sea_med_2_no s_float">
                        @if($form->medical_problems == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="sea_med_table_two_row">
                    <div class="sea_med_2_sr s_float">
                        40
                    </div>
                    <div class="sea_med_2_add s_float">
                        Do you feel healthy and fit to perform the duties of your designed position/occupation?
                    </div>
                    <div class="sea_med_2_yes s_float">
                        @if($form->feel_healthy == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="sea_med_2_no s_float">
                        @if($form->feel_healthy == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="sea_med_table_two_row">
                    <div class="sea_med_2_sr s_float">
                        41
                    </div>
                    <div class="sea_med_2_add s_float">
                        Are you allergic to any medications?
                    </div>
                    <div class="sea_med_2_yes s_float">
                        @if($form->allergic_medication == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="sea_med_2_no s_float">
                        @if($form->allergic_medication == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="sea_med_table_two_row" style="height: 35px;">
                    <div class="sea_med_2_sr s_float" style="height: 35px;">
                        42
                    </div>
                    <div class="sea_med_2_add s_float" style="height: 35px;">
                        Is the seafarer suffering from any medical condition likely to be aggravated by service at sea or to render the seafarer unfit or to endanger the health of other person on board?
                    </div>
                    <div class="sea_med_2_yes s_float" style="height: 35px;">
                        @if($form->medical_condition == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="sea_med_2_no s_float" style="height: 35px;">
                        @if($form->medical_condition == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
            </div>
        </div>

        <div class="row_four">
            <p class="sea_comment">Comments: {{ $form->comments }} </p>
        </div>

        <div class="row_five">
            <div class="sea_med_table_two_2 clear">
                <div class="sea_med_table_two_row">
                    <div class="sea_med_2_sr s_float">

                    </div>
                    <div class="sea_med_2_add s_float">
                        Additional question
                    </div>
                    <div class="sea_med_2_yes s_float">
                        Yes
                    </div>
                    <div class="sea_med_2_no s_float">
                        NO
                    </div>

                </div>
                <div class="sea_med_table_two_row" style="border: none;">
                    <div class="sea_med_2_sr s_float">
                        43
                    </div>
                    <div class="sea_med_2_add s_float">
                        Are you taking any non-prescription or prescription medications?
                    </div>
                    <div class="sea_med_2_yes s_float">
                        @if($form->prescription_medication == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="sea_med_2_no s_float">
                        @if($form->prescription_medication == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
            </div>
        </div>
        <div class="row_six clear">
            <p class="sea_med_s">if yes, please list hte medicaltions taken and the purpose (s) and dosage (s):: {{ $form->provide_details }}</p>
        </div>

    </div>
</div>

<div class="all">
    <div class="all_in annex_all">
        <div class="row_one">
            <div class="sea_p_3_fields">
                <p class="sea_field_text">I hereby certify that the personal declaration above is a true statement to the best of my knowledge.</p>
                <div class="sea_p_f_row">
                    <div class="sea_p_sign s_float">
                        Singniture of examine:
                    </div>
                    <div class="sea_p_sign_value s_float">
                        <p></p>
                    </div>
                </div>

                <div class="sea_p_f_row_2 clear">
                    <div class="sea_p_date s_float">
                        Date (day/month/year:
                    </div>
                    <div class="sea_p_date_value s_float">
                        <p></p>
                    </div>
                </div>

                <div class="sea_p_f_row_3 clear">
                    <div class="sea_p_witness s_float">
                        Witnessed by:(Signaure)
                    </div>
                    <div class="sea_p_witeness_value s_float">
                        <p></p>
                    </div>
                </div>
                <div class="sea_p_f_row_4 clear">
                    <div class="sea_p_name_print s_float">
                        Name (Typed or printed):
                    </div>
                    <div class="sea_p__name_print_value s_float">
                        <p></p>
                    </div>
                </div>
                <div class="sea_p_f_row_5 clear">
                    <p class="sea_dr_text">I hereby authorize the release of all my previous medical records from any health professionals, health, institutions and public authorities to <span class="sea_dr_det">Dr.  sabrina Mostafa MBBS(DU)</span> (the approved medical practitioner).</p>
                </div>
                <div class="sea_p_f_row">
                    <div class="sea_p_sign s_float">
                        Singniture of examine:
                    </div>
                    <div class="sea_p_sign_value s_float">
                        <p></p>
                    </div>
                </div>

                <div class="sea_p_f_row_2 clear">
                    <div class="sea_p_date s_float">
                        Date (day/month/year:
                    </div>
                    <div class="sea_p_date_value s_float">
                        <p></p>
                    </div>
                </div>

                <div class="sea_p_f_row_3 clear">
                    <div class="sea_p_witness s_float">
                        Witnessed by:(Signaure)
                    </div>
                    <div class="sea_p_witeness_value s_float">
                        <p></p>
                    </div>
                </div>
                <div class="sea_p_f_row_4 clear">
                    <div class="sea_p_name_print s_float">
                        Name (Typed or printed):
                    </div>
                    <div class="sea_p__name_print_value s_float">
                        <p></p>
                    </div>
                </div>
                <div class="sea_p_f_row_5 clear">
                    <div class="sea_p_name_print s_float">
                       Date and contact details for prvious medical examination(if known):
                    </div>
                    <div class="sea_p__name_print_value s_float">
                        <p>{{ $form->line1 }}</p>
                    </div>
                    <div class="sea_p__name_print_value s_float">
                        <p> {{ $form->line2 }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row_two sea_3_two clear">
            <p class="sea_medial_exam">MEDICAL EXAMINATION</p>
            <p><b>SIGHT</b></p>
            <p>Use of glasses or contact lenses: Yes/No (if yes, specify which type and for what purpose)
                @if($form->contact_lenses == '1')
                    lense type:: {{ $form->lense_type }} <br> purpose :: {{ $form->purpose }}
                @endif
            </p>

            <div class="sea_table_three">
                <div class="visual_acu_table s_float">
                    <div class="visual_acu_row">
                        <p style="font-weight: bold">Visual acuity</p>
                    </div>
                    <div class="visual_acu_row">
                        <div class="unaid_null s_float">

                        </div>
                        <div class="unaid s_float">
                            Unaided
                        </div>
                        <div class="aid s_float">
                            Aided
                        </div>
                    </div>

                    <div class="visual_acu_row_3">
                        <div class="unaid_null_2 s_float">

                        </div>
                        <div class="unaid s_float unain_ex">
                            <div class="r_eye s_float">
                                Right Eye
                            </div>
                            <div class="r_eye s_float">
                                Left  Eye
                            </div>
                            <div class="r_bin s_float">
                                Binocular
                            </div>
                        </div>
                        <div class="aid s_float">
                            <div class="r_eye s_float">
                                Right Eye
                            </div>
                            <div class="r_eye s_float">
                                Left  Eye
                            </div>
                            <div class="r_bin s_float">
                                Binocular
                            </div>
                        </div>
                    </div>
                    <div class="visual_acu_row_4">
                        <div class="unaid_null_2 s_float">
                            Distant
                        </div>
                        <div class="unaid s_float unain_ex">
                            <div class="r_eye s_float">
                                {{ $form->right_eye_distant }}
                            </div>
                            <div class="r_eye s_float">
                                {{ $form->left_eye_distant }}
                            </div>
                            <div class="r_bin s_float">
                                {{ $form->binocular_eye_distant }}
                            </div>
                        </div>
                        <div class="aid s_float">
                            <div class="r_eye s_float">
                                {{ $form->aided_right_eye_distant }}
                            </div>
                            <div class="r_eye s_float">
                                {{ $form->aided_left_eye_distant }}
                            </div>
                            <div class="r_bin s_float">
                                {{ $form->aided_binocular_eye_distant }}
                            </div>
                        </div>
                    </div>

                    <div class="visual_acu_row_4">
                        <div class="unaid_null_2 s_float">
                            Near
                        </div>
                        <div class="unaid s_float unain_ex">
                            <div class="r_eye s_float">
                                {{ $form->right_eye_near }}
                            </div>
                            <div class="r_eye s_float">
                                {{ $form->left_eye_near }}
                            </div>
                            <div class="r_bin s_float">
                                {{ $form->binocular_eye_near }}
                            </div>
                        </div>
                        <div class="aid s_float">
                            <div class="r_eye s_float">
                                {{ $form->aided_right_eye_near }}
                            </div>
                            <div class="r_eye s_float">
                                {{ $form->aided_left_eye_near }}
                            </div>
                            <div class="r_bin s_float">
                                {{ $form->aided_binocular_eye_near }}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="visual_field_table">
                    <div class="vis_field_table">
                        <div class="vis_field_row">
                            <b>Visual Field</b>
                        </div>
                        <div class="vis_field_row">
                            <div class="vis_f_col s_float">
                                Eye
                            </div>
                            <div class="vis_f_col s_float">
                                Normal
                            </div>
                            <div class="vis_f_col s_float" style="border: none;">
                                Defective
                            </div>
                        </div>
                        <div class="vis_field_row">
                            <div class="vis_f_col s_float">
                                Right
                            </div>
                            <div class="vis_f_col s_float">
                                @if($form->visual_normal == '1')
                                    <i class="fa fa-check" aria-hidden="true">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="vis_f_col s_float" style="border: none;">
                                @if($form->visual_normal == '0')
                                    <i class="fa fa-check" aria-hidden="true">&#xf00c;</i>
                                @endif
                            </div>
                        </div>
                        <div class="vis_field_row">
                            <div class="vis_f_col s_float">
                                Left
                            </div>
                            <div class="vis_f_col s_float">
                                @if($form->visual_defective == '1')
                                    <i class="fa fa-check" aria-hidden="true">&#xf00c;</i>
                                @endif
                            </div>
                            <div class="vis_f_col s_float" style="border: none;">
                                @if($form->visual_defective == '0')
                                    <i class="fa fa-check" aria-hidden="true">&#xf00c;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- row two end -->
        <div class="row_three clear">
            <p><b>color Vision</b></p>
            <div class="sea_table">
                <div class="sea_color_col s_float">

                </div>
                <div class="sea_color_col s_float">
                    @if($form->color_vision == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                     Not Tested
                </div>
                <div class="sea_color_col s_float">
                    @if($form->color_vision == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        Normal
                </div>
                <div class="sea_color_col s_float">
                    @if($form->color_vision == '2')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        Doubtful
                </div>
                <div class="sea_color_col s_float" style="border: none;">
                    @if($form->color_vision == '3')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        Defctive
                </div>

            </div>
        </div>
        <!-- three end -->

        <div class="row_four clear">
            <div class="sea_hearing">
                <p><b>Hearing</b></p>
                <div class="sea_hear_left s_float">
                    <div class="sea_hear_table_one">
                        <div class="sea_hear_table_row">
                            Pure tone and audiometry (threshold values in dB)
                        </div>
                        <div class="sea_hear_table_row_2">
                            <div class="sea_hear_col_2 s_float">
                                Ear
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                500Hz
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                1000Hz
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                2000Hz
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                3000Hz
                            </div>
                            <div class="sea_hear_col_2 s_float">

                            </div>
                            <div class="sea_hear_col_2 s_float" style="border: none;">

                            </div>
                        </div>

                        <div class="sea_hear_table_row_2">
                            <div class="sea_hear_col_2 s_float">
                                Right
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                {{ $form->right_ear_500_hz }}
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                {{ $form->right_ear_1000_hz }}
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                {{ $form->right_ear_2000_hz }}
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                {{ $form->right_ear_3000_hz }}
                            </div>
                            <div class="sea_hear_col_2 s_float">
                            </div>
                            <div class="sea_hear_col_2 s_float" style="border: none;">
                            </div>
                        </div>

                        <div class="sea_hear_table_row_2">
                            <div class="sea_hear_col_2 s_float">
                                Left
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                {{$form->left_ear_500_hz}}
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                {{$form->left_ear_1000_hz}}
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                {{$form->left_ear_2000_hz}}
                            </div>
                            <div class="sea_hear_col_2 s_float">
                                {{$form->left_ear_3000_hz}}
                            </div>
                            <div class="sea_hear_col_2 s_float">
                            </div>
                            <div class="sea_hear_col_2 s_float" style="border: none;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sea_heear_left s_float">
                    <p>Speech and whisper test (meters)</p>
                    <div class="Sea_speech_table">
                        <div class="speech_table_row">
                            <div class="speech_table_col s_float">
                                Ear
                            </div>
                            <div class="speech_table_col s_float">
                                Normal
                            </div>
                            <div class="speech_table_col s_float" style="border: none;">
                                Whisper
                            </div>
                        </div>
                        <div class="speech_table_row">
                            <div class="speech_table_col s_float">
                                Right
                            </div>
                            <div class="speech_table_col s_float">
                                {{ $form->right_ear_normal }}
                            </div>
                            <div class="speech_table_col s_float" style="border: none;">
                                {{ $form->right_ear_whisper }}
                            </div>
                        </div>
                        <div class="speech_table_row">
                            <div class="speech_table_col s_float">
                                Left
                            </div>
                            <div class="speech_table_col s_float">
                                {{ $form->left_ear_normal }}
                            </div>
                            <div class="speech_table_col s_float" style="border: none;">
                                {{ $form->left_ear_whisper }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- row _four end -->
        <div class="row_five clear">
            <div class="sea_client_find">
                <div class="sea_client_find_row">
                    <p><b>Clinial Findings</b></p>
                    <div class="sea_client_height s_float">
                        Height:<span class="sea_h_value">{{ $form->clinical_height }}</span>(cm)
                    </div>
                    <div class="sea_client_height s_float">
                        Weight:<span class="sea_h_value">{{ $form->clinical_weight }}</span>(Kg)
                    </div>
                </div>
                <div class="sea_client_find_row clear">
                    <div class="sea_client_height s_float">
                        Pulse rate:<span class="sea_h_value">{{ $form->clinical_pulse_rate }}</span>(/Minute)
                    </div>
                    <div class="sea_client_height s_float">
                        Rhythm:<span class="sea_h_value">{{ $form->clinical_rhythm }}</span>(Kg)
                    </div>
                </div>
                <div class="sea_client_find_row_3 clear">
                    <div class="sea_client_blood s_float">
                        Blood Pressure:stolic: <span class="sea_h_value">{{ $form->clinical_b_pressure }}</span> (mmHg) Diastolic:  <span class="sea_h_value">{{ $form->clinical_diastolic }}</span> mmHg
                    </div>

                </div>
                <div class="sea_client_find_row_3 clear">
                    <div class="sea_client_blood s_float">
                        Urinalysis: Glucose: <span class="sea_h_value">{{ $form->urinalysis_glucose }}</span> Protein(Albumin):  <span class="sea_h_value">{{ $form->urinalysis_protin}}</span> Blood:<span class="sea_h_value">{{ $form->urinalysis_blood }}</span>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>

<div class="all">
    <div class="all_in annex_all">
        <div class="row_one">
            <div class="sea_med_4_table_one">
                <div class="sea_table_one_row">
                    <div class="sea_table_one_p_1 s_float">
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">

                            </div>
                            <div class="sea_tab_2_yes s_float">
                                Normal
                            </div>
                            <div class="sea_tab_2_no s_float">
                                Abnormal
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Head
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->head == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->head == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Sinuses, nose, throat
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->sinus_nose_throat == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->sinus_nose_throat == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Mouth/teeth
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->mouth_teeth == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->mouth_teeth == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Ears (general)
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->ears == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->ears == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Tympanic membrane
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->tympanic_member == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->tympanic_member == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Eyes
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->eyes == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->eyes == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Ophthalmoscopy
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->ophthalmoscopy == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->ophthalmoscopy == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Pupils
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->pupils == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->pupils == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Eye movement
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->eye_environment == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->eye_environment == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Lungs and chest
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->lungs_chest == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->lungs_chest == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Breast examination
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->breast_examination == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->breast_examination == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Heart
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->heart == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->heart == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="chest_xray s_float">
                                chest X-ray
                            </div>
                            <div class="chest_xray_value s_float">
                                @if($form->chest_xray == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                                 Not Performed
                            </div>

                        </div>
                    </div>

                    <div class="sea_table_one_p_2 s_float">
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">

                            </div>
                            <div class="sea_tab_2_yes s_float">
                                Normal
                            </div>
                            <div class="sea_tab_2_no s_float">
                                Abnormal
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Skin
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->skin == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->skin == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Varicose venis
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->varicose_vein == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->varicose_vein == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Vascular (inc. Pedal pulses)
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->vascular_pulse == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->vascular_pulse == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Abdomen and viscera
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->abdomen_viscera == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->abdomen_viscera == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Hernia
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->hernia_2 == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->hernia_2 == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Anus (not rectal exam.)
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->anus == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->anus == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                G-U system
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->g_u_system == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->g_u_system == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Upper and lower extremities
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->upper_lower_ext == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->upper_lower_ext == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Spine (C/S, T/S and L/S)
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->spine == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->spine == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Neurologic (full brief)
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->neurologic == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->neurologic == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                Psychiatric
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->psychiatric == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->psychiatric == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="sea_tab_2_con s_float">
                                General appearance
                            </div>
                            <div class="sea_tab_2_yes s_float">
                                @if($form->general_appearance == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="sea_tab_2_no s_float">
                                @if($form->general_appearance == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="sea_table_2_row">

                            <div class="performed_date s_float">
                                @if($form->chest_xray == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                                 Performed (day/month/year)
                            </div>
                            <div class="performed_date_vale s_float">
                                <p>{{ date('d/m/Y', strtotime($form->date_of_performed_on)) }}</p>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- row_one end -->

        <div class="row_two2 clear">
            <div class="sea_med_cer_4_resul_row">
                <div class="sea_med_result s_float">
                    Result:
                </div>
                <div class="sea_med_value s_float">
                    <p>{{ date('d/m/Y', strtotime($form->date_of_results_on)) }}</p>
                </div>
            </div>
        </div>
        <!-- row_two end -->

        <div class="row_three">
            <h4 class="sea_other_test">Other Dagonostic Test (S) and Result + (S)</h4>

            <div class="sea_drug_test_row">
                <div class="sea_drug_test_col_1 s_float">
                    <div class="sea_drug_test_text s_float">
                        Test:
                    </div>
                    <div class="sea_drug_test_value s_float">
                        <p>{{ $form->d_test }}</p>
                    </div>
                </div>
                <div class="sea_drug_result_col_1 s_float">
                    <div class="sea_drug_result_text s_float">
                        Result:
                    </div>
                    <div class="sea_drug_result_value s_float">
                        <p>{{ $form->d_result }}</p>
                    </div>
                </div>
            </div>
            <div class="sea_med_4_comment">
                <p style="margin-left: 20px;">Medical practitioner’s comments and assessment of fitness, with reasons for any limitations:</p>
            </div>
        </div>
        <!-- row three end -->

        <div class="row_three clear">
            <h3 class="sea_ass">Assesment of fitness for service at sea</h3>
            <p>On the basis of the examinee’s personal declaration, my clinical examination and the diagnostic
                test results recorded above, I declare the examinee medically:</p>

            <div class="sea_fit_not_fit">
                <div class="sea_fit_not_fit_col_1 s_float">
                    @if($form->fit_for_lookout_duty == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        fit for look-out
                </div>
                <div class="sea_fit_not_fit_col_2 s_float">
                    @if($form->fit_for_lookout_duty == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    Not fit for look-out
                </div>
            </div>
            <div class="sea_deck_service">
                <div class="sea_deck_row">
                    <div class="deck_ser_col s_float">

                    </div>
                    <div class="deck_ser_col s_float">
                        Deck Service
                    </div>
                    <div class="deck_ser_col s_float">
                        Engine Service
                    </div>
                    <div class="deck_ser_col s_float">
                        Catering Service
                    </div>
                    <div class="deck_ser_col s_float" style="border: none;">
                        Other Service
                    </div>
                </div>
                <div class="sea_deck_row">
                    <div class="deck_ser_col s_float">
                        Fit
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_desk == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_engine == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_catering == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float" style="border: none;">
                        @if($form->fit_other == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="sea_deck_row">
                    <div class="deck_ser_col s_float">
                        Unfit
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_desk == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_engine == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_catering == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float" style="border: none;">
                        @if($form->fit_other == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class=sea_rstrict>
                    <div class="without_ris s_float">
                        @if($form->with_restrictions == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Without restrictions
                    </div>
                    <div class="with_ris s_float">
                        @if($form->with_restrictions == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            With restrictions
                    </div>
                    <div class="with_ris_visual s_float">
                        visual aid required
                    </div>
                    <div class="without_ris_yes s_float">
                        @if($form->visual_aid_required == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Yes
                    </div>
                    <div class="without_ris_no s_float">
                        @if($form->visual_aid_required == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            No
                    </div>
                </div>

            </div>
        </div>
        <!-- row three end -->
        <div class="row_four clear">
            <div class="sea_med_4_comment" style="margin-top: 55px">
                <p style="margin-left: 20px;">one text of the printing and typesetting industry.</p>
            </div>
            <div class="med_cer_others">
                <div class="med_cer_others_row">
                    <div class="sea_m_cer_date s_float">
                        Medical certificate'x date of Experatio (day/month/year):
                    </div>
                    <div class="sea_m_cer_date_value s_float">
                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="sea_cerIissue_date s_float">
                        date of Medical Certificate issued (day/month/year):
                    </div>
                    <div class="sea_cerIissue_date_value s_float">
                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="sea_num_cer s_float">
                        Number of medical certificate:
                    </div>
                    <div class="sea_num_cer_value s_float">
                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="sea_name_med s_float">
                        Name of medical pracitioner (type of printed):
                    </div>
                    <div class="sea_name_med_value s_float">
                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="sea_licanse s_float">
                        Licanse number of medical parctitioner:
                    </div>
                    <div class="sea_licanse_value s_float">
                        Registration
                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="sea_address s_float">
                        Address of medica parcitioner:
                    </div>
                    <div class="sea_address_value s_float">
                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="sea_sign s_float">
                        Signature of medical practitioner:
                    </div>
                    <div class="sea_sign_value s_float">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
