<!DOCTYPE html>
<html>
<head>
    <title>one</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous">

</head>
<body>
<div class="ph_all">
    <div class="all ph_full">
        <div class="visible-print text-center" style="padding: 2px;">
            @php
                use SimpleSoftwareIO\QrCode\Facades\QrCode;
                $path  = 'qrcodes/_'.now().$form->id.'.svg';
                QrCode::generate(route('smcseafarer_recrord.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
            @endphp
            <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
            <p>Scan me to return to the original page.</p>
        </div>
        <div>
            <h3 class="m_c_heading">PHYSICAL EXAMINATION REPORT/CERTIFICATE</h3>
            <h3 class="m_c_heading">DEPUTY COMMISSIONER OF MARITIME AFFAIRS</h3>
            <h3 class="ph_h_an">ANNEX 2</h3>
            <h3 class="ph_h_lib">THE REPUBLIC OF LIBERIA</h3>
        </div>
        <div class=" phycsical_exam_page">

            <div class="row_one">
                <div class="m_c_table_1" style="border-top: none;">
                    <div class="m_c_tab_row">
                        <div class="m_c_col_1 s_float">
                            <p>LAST NAME OF APPLICANT: {{ $form->last_name }}</p>
                        </div>
                        <div class="m_c_col_2 s_float">
                            <p>SURNAME: {{ $form->first_name }}</p>

                        </div>
                        <div class="m_c_col_3 s_float">
                            <p>MIDDLE INITIAL: {{ $form->midle_initial }}</p>
                        </div>

                    </div>
                    <div class="m_c_tab_row_2">
                        <div class="m_c_col_two_1 s_float">
                            <p>DATE OF BIRTH:</p>
                            <div class="m_c_df_value">
                                <div class="m_c_day s_float">
                                    @php
                                       $date = \Carbon\Carbon::parse($form->dob);
                                    @endphp
                                    Day: {{ $date->day }}
                                </div>
                                <div class="m_c_month s_float">
                                    Month: {{ $date->month }}
                                </div>
                                <div class="m_c_year s_float">
                                    Year: {{ $date->year }}
                                </div>
                            </div>
                        </div>
                        <div class="m_c_col_two_2 s_float">
                            <div class="m_c_place_birth s_float">
                                <p>PLACE OF BIRTH</p>
                                <div class="m_c_dity_country">
                                    <div class="mc_city s_float">
                                        City: {{ $form->city }}
                                    </div>
                                    <div class="mc_city s_float">
                                        Country: {{ $form->country }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m_c_col_two_3 s_float">
                            <p>SEX</p>
                            <div class="m_c_gender s_float">
                                Male
                                @if($form->gender == 'Male' )
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="m_c_gender s_float">
                                Female
                                @if($form->gender == 'Female' )
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="m_c_tab_row_3">
                        <div class="m_col_three_1 s_float">
                            <p>EXAMINATION FOR DUTY AS:</p>
                            <div class="ph_sea_ex_du_p1 s_float">
                                <div class="ph_sea_ex_du_row">
                                    <div class="ph_sea_ex_du_text s_float">
                                       MASTER
                                    </div>
                                    <div class="ph_sea_ex_du_value s_float_right">
                                        @if($form->examination_as_duty == '0' )
                                            <i class="fa fa-check">&#xf046;</i>
                                        @else
                                            <i class="fa fa-check">&#xf096;</i>
                                        @endif
                                    </div>
                                </div>
                                <div class="ph_sea_ex_du_row">
                                    <div class="ph_sea_ex_du_text s_float">
                                        MATE
                                    </div>
                                    <div class="ph_sea_ex_du_value s_float_right">
                                        @if($form->examination_as_duty == '1' )
                                            <i class="fa fa-check">&#xf046;</i>
                                        @else
                                            <i class="fa fa-check">&#xf096;</i>
                                        @endif
                                    </div>
                                </div>
                                <div class="ph_sea_ex_du_row">
                                    <div class="ph_sea_ex_du_text s_float">
                                        ENGINEER
                                    </div>
                                    <div class="ph_sea_ex_du_value s_float_right">
                                        @if($form->examination_as_duty == '2' )
                                            <i class="fa fa-check">&#xf046;</i>
                                        @else
                                            <i class="fa fa-check">&#xf096;</i>
                                        @endif
                                    </div>
                                </div>
                                <div class="ph_sea_ex_du_row">
                                    <div class="ph_sea_ex_du_text s_float">
                                        RADIO OFF
                                    </div>
                                    <div class="ph_sea_ex_du_value s_float_right">
                                        @if($form->examination_as_duty == '3' )
                                            <i class="fa fa-check">&#xf046;</i>
                                        @else
                                            <i class="fa fa-check">&#xf096;</i>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="ph_sea_ex_du_p2 s_float">
                                <div class="ph_sea_ex_du_row_p_2">
                                    <div class="ph_sea_ex_du_text s_float">
                                        RATING
                                    </div>
                                    <div class="ph_sea_ex_du_value s_float_right">
                                        @if($form->examination_as_duty == '4' )
                                            <i class="fa fa-check">&#xf046;</i>
                                        @else
                                            <i class="fa fa-check">&#xf096;</i>
                                        @endif
                                    </div>
                                </div>
                                <div class="ph_sea_ex_du_row_p_2">
                                    <div class="ph_sea_ex_du_text s_float">
                                        MOU DECCK
                                    </div>
                                    <div class="ph_sea_ex_du_value s_float_right">
                                        @if($form->examination_as_duty == '5' )
                                            <i class="fa fa-check">&#xf046;</i>
                                        @else
                                            <i class="fa fa-check">&#xf096;</i>
                                        @endif
                                    </div>
                                </div>
                                <div class="ph_sea_ex_du_row_p_2">
                                    <div class="ph_sea_ex_du_text s_float">
                                        MOU ENGINE
                                    </div>
                                    <div class="ph_sea_ex_du_value s_float_right">
                                        @if($form->examination_as_duty == '6' )
                                            <i class="fa fa-check">&#xf046;</i>
                                        @else
                                            <i class="fa fa-check">&#xf096;</i>
                                        @endif
                                    </div>
                                </div>
                                <div class="ph_sea_ex_du_row_p_2">
                                    <div class="ph_sea_ex_du_text s_float">
                                        SUPERNUMERARY
                                    </div>
                                    <div class="ph_sea_ex_du_value s_float_right">
                                        @if($form->examination_as_duty == '7' )
                                            <i class="fa fa-check">&#xf046;</i>
                                        @else
                                            <i class="fa fa-check">&#xf096;</i>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="m_col_three_2 s_float">
                            <p>MAILING ADDRESS OF APPLICANT: {{ $form->mailing_address }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- row_one end -->
            <div class="ph_ex_row clear">
                <p>MEDICAL EXAMINATION (SEE REVERSE SIDE FOR MEDICAL REQUIREMENTS) STATE DETAILS ON REVERSE SIDE </p>
            </div>
            <div class="ph_ex_row_1 clear">
                <div class="ph_height s_float">
                    <p>HEIGHT {{ $form->height }}</p>
                </div>
                <div class="ph_weight s_float">
                    <p>WEIGHT  {{ $form->weight }}</p>
                </div>
                <div class="ph_bloor s_float">
                    <p>BLOOD PRESSURE  {{ $form->blood_pressure }}</p>
                </div>
                <div class="ph_pulse s_float">
                    <p>PULSE  {{ $form->pulse }}</p>
                </div>
                <div class="ph_respi s_float">
                    <p>RESPIRATION  {{ $form->respiration }}</p>
                </div>
                <div class="ph_general s_float">
                    <p>GENERAL APPEARANCE  {{ $form->general_appearance }}</p>
                </div>
            </div>

            <div class="ph_ex_row_2 clear">
                <div class="ph_vision_row">
                    <div class="ph_vison s_float">
                        Vision
                    </div>
                    <div class="ph_ritht_eye s_float">
                        RIGHT EYE
                    </div>
                    <div class="ph_left_eye s_float">
                        LEFT EYE
                    </div>
                </div>
                <div class="ph_vision_row">
                    <div class="ph_vison s_float">
                        <div class="vision_without_glass">
                            WITHOUT GLASSES
                        </div>
                    </div>
                    <div class="ph_ritht_eye s_float">
                        <div class="vision_without_glass_right">
                            <p>{{ $form->right_eye }}</p>
                        </div>
                    </div>
                    <div class="ph_left_eye s_float">
                        <div class="vision_without_glass_left">
                            <p>{{ $form->left_eye }}</p>
                        </div>
                    </div>
                </div>
                <div class="ph_vision_row">
                    <div class="ph_vison s_float">
                        <div class="vision_without_glass">
                            WITH GLASSES
                        </div>
                    </div>
                    <div class="ph_ritht_eye s_float">
                        <div class="vision_without_glass_right">
                            <p>{{ $form->with_g_right_eye }}</p>
                        </div>
                    </div>
                    <div class="ph_left_eye s_float">
                        <div class="vision_without_glass_left">
                            <p>{{ $form->with_g_left_eye }}</p>
                        </div>
                    </div>
                </div>

                <div class="ph_vision_row clear">
                    <p class="ph_last_color_te">DATE OF LAST COLOR VISION TEST (Month/Day/Year) <span class="ph_xtlast_color_date">{{ date('d/m/y', strtotime($form->date_of_vision_test)) }}</span>Testing Required every 6 years</p>
                </div>
                <div class="ph_vision_row">
                    <div class="ph_color_vision_meet s_float">
                        <p>COLOR VISION MEETS STANDARDS IN STCW CODE, TABLE A-I/9?</p>
                    </div>
                    <div class="ph_color_vision_meet_yes s_float">
                        YES
                        @if($form->color_vision == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="ph_color_vision_meet_no s_float">
                        NO
                        @if($form->color_vision == '0' )
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
            </div>
            <div class="ph_ex_row_3">
                <p class="color_t_type">COLOR TEST TYPE:
                    @if($form->color_test_type == '1' )
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    BOOK
                    @if($form->examination_as_duty == '0' )
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    LANTERN
                    CHECK IF COLOR TEST IS NORMAL YELLOW
                    <span class="cif_icon_bor">
                          @if($form->test_normal == '0' )
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </span> RED
                    <span class="cif_icon_bor">
                          @if($form->test_normal == '1' )
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </span> GREEN
                    <span class="cif_icon_bor">
                          @if($form->test_normal == '2' )
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </span> BLUE
                    <span class="cif_icon_bor">
                          @if($form->test_normal == '3' )
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </span></p>
            </div>
            <div class="ph_ex_row_4">
                <div class="ph_hearing s_float">
                    HEARING
                </div>
                <div class="ph_hearing_right s_float">
                    RT. EAR <span class="ph_ear_result">{{ $form->hearing_right_ear }}</span>
                </div>
                <div class="ph_hearing_left s_float">
                    LEFT EAR <span class="ph_ear_result">{{ $form->hearing_left_ear }}</span>
                </div>
            </div>

            <div class="ph_ex_row_5">
                <div class="ph_head_nick s_float">
                    HEAD AND NECK <span class="ph_hn_value">{{ $form->head_neck }}</span>
                </div>
                <div class="ph_head_nick_2 s_float">
                    HEART (CARDIOVASCULAR) <span class="ph_hn_value">{{ $form->heart }}</span>
                </div>
            </div>
            <div class="ph_ex_row_6">
                <div class="ph_head_nick s_float">
                    LUNGS <span class="ph_hn_value">{{ $form->lungs }}</span>
                </div>
                <div class="ph_head_nick_2 s_float">
                    SPEECH (DECK/NAVIGATIONAL OFFICER AND RADIO OFFICER)
                    IS SPEECH UNIMPAIRED FOR NORMAL VOICE COMMUNICATION?
                    <span class="ph_hn_value">
                       {{ $form->speech }}
                    </span>
                </div>
            </div>
            <div class="ph_ex_row_7">
                <h3 class="ph_extream_heading">EXTREMITIES:</h3>
                <div class="ph_extream_upper s_float">
                    <p>UPPER<span class="ph_extream_result">{{ $form->extremities_upper }}</span></p>
                </div>
                <div class="ph_extream_lower s_float">
                    <p>LOWER<span class="ph_extream_result">{{ $form->extremities_lower }}</span></p>
                </div>
            </div>

            <div class="ph_ex_row_8">
                <p>IS APPLICANT SUFFERING FROM ANY DISEASE LIKELY TO BE AGGRAVATED BY, OR TO RENDER HIM UNFIT FOR SERVICE AT SEA OR LIKELY TO ENDANGER THE HEALTH OF OTHER PERSONS ON BOARD? IF YES, EXPLAIN IN DETAILS OF MEDICAL EXAMINATION ON PAGE 2
                </p>
                <div class="ph_color_vision_meet_yes s_float">
                    YES
                    @if($form->is_applicant_suffering == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
                <div class="ph_color_vision_meet_yes s_float">
                    NO
                    @if($form->is_applicant_suffering == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </div>
            </div>

            <div class="ph_ex_9">
                <div class="ph_exapir_date_row">
                    <div class="ph_ex_date_col s_float">
                        <p>SIGNATURE OF APPLICANT </p>
                    </div>
                    <div class="ph_ex_date_col s_float" style="margin-left: 20px;">
                        <p>DATE OF EXAM </p>
                    </div>
                    <div class="ph_ex_date_col s_float" style="margin-left: 27px;">
                        <p>EXPIRY DATE </p>
                    </div>
                </div>
                <P class="ph_ex_text clear">THIS SIGNATURE SHOULD BE AFFIXED IN THE PRESENCE OF THE EXAMINING PHYSICIAN.</P>
                <div class="ph_name_of_app clear">
                    <div class="ph_cert s_float">
                        <p>THIS IS TO CERTIFY THAT A PHYSICAL EXAMINATION WAS GIVEN TO:</p>
                    </div>
                    <div class="ph_cert_value">
                        <p>MD. BAKHTIAR HOSSAIN</p>
                        <p class="ph_name_of_app_border">(NAME OF APPLICANT)</p>
                    </div>
                </div>
                <p class="ph_app_fit_unfit">(HE
                   @if($form->gender == 'Male')
                       <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    )

                    (SHE
                    @if($form->gender == 'Female')
                       <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    )
                    IS FOUND TO BE (
                    @if($form->fit == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    FIT) (
                    @if($form->fit == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    NOT FIT) FOR DUTY AS A: (
                    @if($form->examination_as_duty == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    MASTER,
                    @if($form->examination_as_duty == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    MATE,
                    @if($form->examination_as_duty == '2')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    ENGINEER,
                    @if($form->examination_as_duty == '3')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    RADIO OFFICER,
                    @if($form->examination_as_duty == '4')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    RATING,
                    @if($form->examination_as_duty == '5')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    MOU DECK,
                    @if($form->examination_as_duty == '6')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    MOU ENGINE or
                    @if($form->examination_as_duty == '7')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    SUPERNUMERARY).
                </p>
            </div>

            <div class="ph_ex_10 clear">
                <div class="ph_name_degree_row">
                    NAME AND DEGREE OF PHYSICIAN<span class="name_digree_value">DR. SABRINA MOSTAFA, M.B.B.S (D.U)</span>
                </div>
                <div class="ph_name_address_row">
                    ADDRESS <span class="ph_address_value">126, SK. MUJIB ROAD, CHOWMUHONI, CHITTAGONG, BANGLADESH.</span>
                </div>
                <div class="ph_cer_author">
                    NAME OF PHYSICIAN'S CERTIFICATING AUTHORITY<span class="ph_cer_author_value">REGISTRATION NO.: A-68208, B.M.D.C, DHAKA, BANGLADESH</span>
                </div>
                <div class="ph_issue_date_row">
                    DATE OF ISSUE OF PHYSICIAN'S CERTIFICATE <span class="ph_issue_date_value"></span>
                </div>
                <div class="ph_phy_sigin_exame_date">
                    SIGNATURE OF PHYSICIAN <span class="ph_phy_sign_value"></span> DATE OF EXAMINATION: <span class="ph_phy_exam_date_value"></span>
                </div>
            </div>

        </div>
        <div class="ph_footer">
            <div class="ph_foot_row_1">
                <p>This certificate is issued by authority of the Deputy Commissioner of Maritime Affairs, R.L. and in compliance with the requirements of the Maritime Labour Convention, 2006 for the Medical Examination of Seafarers.</p>
            </div>
            <div class="ph_foot_row_2">
                <p>The Medical Certificate shall be valid for no more than two (2) years from the date of the Ex amination for those over 18 years of age and
                    for no more than one (1) year for those under 18 years of age</p>
            </div>
        </div>
        <div class="ph_page_no">
            <div class="ph_rlm s_float">
                RLM-l05M (REV. 06/16)
            </div>
            <div class="ph_p_n s_float">
                1
            </div>
        </div>
    </div>
</div>

</body>
</html>
