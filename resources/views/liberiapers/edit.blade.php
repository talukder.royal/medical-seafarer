@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <div class="row">
        <div class="col-lg-12 text-center">
            <div>
                SEAFARER'S PHYSICAL EXAMINATION REPORT/CERTIFICATE
            </div>
        </div>
    </div>
    <form class="form-control" action="{{ route('liberiapers.update', $form->id) }}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="last_name"><span class="text-danger">*</span>LAST NAME OF APPLICANT</label>
                    <input type="text" class="form-control" name="last_name" id="last_name" value="{{ old('last_name', $form->last_name ?? '') }}">
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="first_name"><span class="text-danger">*</span>FIRST NAME</label>
                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{ old('first_name', $form->first_name ?? '') }}">
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="midle_initial"><span class="text-danger">*</span>MIDDLE INITIAL</label>
                    <input type="text" class="form-control" name="midle_initial" id="midle_initial" value="{{ old('midle_initial', $form->midle_initial ?? '') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="dob">Date of birth:</label>
                    <input type="date" class="form-control" name="dob" id="dob" value="{{ old('dob', $form->dob ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="d-flex justify-content-between">
                        <div class="">
                            <label class="form-label" for="city">Place of birth: City</label>
                            <input type="text" class="form-control" name="city" id="city" value="{{ old('city', $form->city ?? '') }}">
                        </div>
                        <div class="ml-2">
                            <label class="form-label" for="country">Country</label>
                            <input type="text" class="form-control" name="country" id="country" value="{{ old('country', $form->country ?? '') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="document_no"><span class="text-danger">*</span>Sex:</label>
                    <br>
                    <label class="form-label text-capitalize" for="male">Male:</label>
                    <input style="width: 25px; height: 20px;" type="checkbox"  name="gender" id="male" value="Male" {{ $form->gender == 'Male' ? 'checked' : ''}}>
                    <label class="form-label text-capitalize" for="female">Female:</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="gender" id="female" value="Female"  {{ $form->gender == 'Female' ? 'checked' : ''}}>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-class">Examination for duty as:</label>
                </div>
                <div class="form-group" style="padding-left: 50px;">
                    <div class="d-flex justify-content-between">
                        <div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="master">Master</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="master" value="0" {{ $form->examination_as_duty == '0' ? 'checked' : '' }}>
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="mate">Mate</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="mate" value="1" {{ $form->examination_as_duty == '1' ? 'checked' : '' }}>
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="engineer">Engineer</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="engineer" value="2" {{ $form->examination_as_duty == '2' ? 'checked' : '' }}>
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="radio_off">Radio Off</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="radio_off" value="3" {{ $form->examination_as_duty == '3' ? 'checked' : '' }}>
                            </div>
                        </div>
                        <div class="mr-2">
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="rating">Rating</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="rating" value="4" {{ $form->examination_as_duty == '4' ? 'checked' : '' }}>
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="mou_deck">Mou Deck</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="mou_deck" value="5" {{ $form->examination_as_duty == '5' ? 'checked' : '' }}>
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="mou_engine">Mou Engine</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="mou_engine" value="6" {{ $form->examination_as_duty == '6' ? 'checked' : '' }}>
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="super_numer">Supernumerary</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="super_numer" value="7" {{ $form->examination_as_duty == '7' ? 'checked' : '' }}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="mailing_address">Mailing address of applicant:</label>
                    <textarea  class="form-control" name="mailing_address" id="mailing_address">{{ old('mailing_address', $form->mailing_address ?? '') }}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <hr>
                <h6 class="text-center" style="color: #95a2af; text-transform: uppercase">Medical examination (see last page for medical requirements) state details on reverse side</h6>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="height">Height</label>
                    <input  class="form-control" type="text" name="height" id="height" value="{{ old('height', $form->height ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="weight">Weight</label>
                    <input  class="form-control" type="text" name="weight" id="weight" value="{{ old('weight', $form->weight ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="blood_pressure">Blood pressure</label>
                    <input  class="form-control" type="text" name="blood_pressure" id="blood_pressure" value="{{ old('blood_pressure', $form->blood_pressure ?? '') }}">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="pulse">Pulse</label>
                    <input  class="form-control" type="text" name="pulse" id="pulse" value="{{ old('pulse', $form->pulse ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="respiration">Respiration</label>
                    <input  class="form-control" type="text" name="respiration" id="respiration" value="{{ old('respiration', $form->respiration ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="general_appearance">General appearance</label>
                    <input  class="form-control" type="text" name="general_appearance" id="general_appearance" value="{{ old('general_appearance', $form->general_appearance ?? '') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="">Vision(Without glasses):</label>
                    <br>
                    <label class="form-label" for="right_eye">Right eye:</label>
                    <input class="form-control" type="text" name="right_eye" id="right_eye" value="{{ old('right_eye', $form->right_eye ?? '') }}">
                    <label class="form-label" for="left_eye">Left eye:</label>
                    <input class="form-control" type="text" name="left_eye" id="left_eye" value="{{ old('left_eye', $form->left_eye ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="">Vision(With glasses):</label>
                    <br>
                    <label class="form-label" for="with_g_right_eye">Right eye:</label>
                    <input class="form-control" type="text" name="with_g_right_eye" id="with_g_right_eye" value="{{ old('with_g_right_eye', $form->with_g_right_eye ?? '') }}">
                    <label class="form-label" for="with_g_left_eye">Left eye:</label>
                    <input class="form-control" type="text" name="with_g_left_eye" id="with_g_left_eye" value="{{ old('with_g_left_eye', $form->with_g_left_eye ?? '') }}">
                </div>
            </div>
        </div>

        <div  class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="date_of_vision_test">Date of last color vision test:</label>
                    <input class="form-control"  type="date" name="date_of_vision_test" id="date_of_vision_test" value="{{ old('date_of_vision_test', $form->date_of_vision_test ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="date_of_vision_test">Testing Required every 6 years</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="clolor_vision_label">COLOR VISION MEETS STANDARDS IN STCW, TABLE A-I/9?</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="color_vision_yes">Yes</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_vision"  id="color_vision_yes"  value="1" {{ $form->color_vision == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="color_vision_no">No</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_vision"  id="color_vision_no"  value="0" {{ $form->color_vision == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>
        <div  class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="color_test_type">Color test type:</label>
                    <label class="form-label" for="book">Book</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_test_type" id="book" value="1"  {{ $form->color_test_type == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="lanter">Lantern</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_test_type" id="lanter" value="0"  {{ $form->color_test_type == '0' ? 'checked' : '' }}>
                    <label class="form-label" for="test_normal">Check if color test is normal:</label>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label d-block" for="color_test_type"></label>
                    <br>
                    <label class="form-label" for="yellow">Yellow</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="yellow" value="0"  {{ $form->test_normal == '0' ? 'checked' : '' }}>
                    <label class="form-label" for="red">Red</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="red" value="1"  {{ $form->test_normal == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="green">Green</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="green" value="2"  {{ $form->test_normal == '2' ? 'checked' : '' }}>
                    <label class="form-label" for="blue">Blue</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="blue" value="3"  {{ $form->test_normal == '3' ? 'checked' : '' }}>
                </div>
            </div>
        </div>

        <div  class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label d-block" for="">Hearing:</label>
                    <label class="form-label" for="hearing_right_ear">Right ear:</label>
                    <input class="form-control" type="text" name="hearing_right_ear" id="hearing_right_ear" value="{{ old('first_name', $form->first_name ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label d-block" for=""></label>
                    <br>
                    <label class="form-label" for="hearing_left_ear">Left ear:</label>
                    <input class="form-control" type="text" name="hearing_left_ear" id="hearing_left_ear" value="{{ old('hearing_left_ear', $form->hearing_left_ear ?? '') }}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="head_neck">Head and neck</label>
                    <input class="form-control" type="text" name="head_neck"  id="head_neck"  value="{{ old('head_neck', $form->head_neck ?? '') }}" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="lungs">Lungs</label>
                    <input class="form-control" type="text" name="lungs" id="lungs"  value="{{ old('lungs', $form->lungs ?? '') }}" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="extremities_upper">Extremities(upper):</label>
                    <input class="form-control" type="text" name="extremities_upper" id="extremities_upper" value="{{ old('extremities_upper', $form->extremities_upper ?? '') }}" >
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" id="heart">Heart(cardiovascular)</label>
                    <input class="form-control" type="text" name="heart" id="heart" value="{{ old('heart', $form->heart ?? '') }}" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="speech">Speech(Deck/navigational officer and radio officer)Is speech unimpaired for normal voice communication?</label>
                    <input class="form-control" type="text" name="speech" id="speech" value="{{ old('speech', $form->speech ?? '') }}" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="extremities_lower">Extremities(Lower):</label>
                    <input class="form-control" type="text" name="extremities_lower" id="extremities_lower" value="{{ old('extremities_lower', $form->extremities_lower ?? '') }}" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="is_applicant_suffering">Is applicant suffering from any disease likely to
                        be aggravated by working aboard a vessel, or to render him/
                        her unfit for service at sea or likely to endanger the health
                        of of other persons on board?
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="is_applicant_suffering_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="is_applicant_suffering" id="is_applicant_suffering_yes" value="1" {{ $form->is_applicant_suffering == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="is_applicant_suffering_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="is_applicant_suffering" id="is_applicant_suffering_no" value="0" {{ $form->is_applicant_suffering == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                <label>
                    (He <input style="width: 25px; height: 20px;" type="checkbox" {{ $form->gender == 'Male' ? 'checked' : '' }}>/She <input style="width: 25px; height: 20px;" type="checkbox" value="1" {{ $form->gender == 'Female' ? 'checked' : '' }}>) is found to be (Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="1" {{ $form->fit == '1' ? 'checked' : '' }}>/ not Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="0" {{ $form->fit == '0' ? 'checked' : '' }}>) for duty as a (without any<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="0" {{ $form->with == '0' ? 'checked' : '' }}> / with the following<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="1"  {{ $form->with == '1' ? 'checked' : '' }}>) restrictions:
                </label>
                <br>
                <label for="line1">Line1</label>
                <input type="text" name="line1" class="form-control" id="line1" value="{{ old('line1', $form->line1 ?? '') }}">
                <label for="line2">Line2</label>
                <input type="text" name="line2" class="form-control" id="line2"  value="{{ old('line2', $form->line2 ?? '') }}">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="date_of_exam">Date of Examination:</label>
                    <input class="form-control" type="date" name="date_of_exam" id="date_of_exam" value="{{ old('date_of_exam', $form->date_of_exam ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="date_of_expiration">Date of Expiration:</label>
                    <input class="form-control" type="date" name="date_of_expiration" id="date_of_expiration" value="{{ old('date_of_expiration', $form->date_of_expiration ?? '') }}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>
    </form>
@endsection
