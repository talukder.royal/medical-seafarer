@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <div class="row">
        <div class="col-lg-12 text-center">
            <div>
                SEAFARER'S PHYSICAL EXAMINATION REPORT/CERTIFICATE
            </div>
        </div>
    </div>
    <form class="form-control" action="{{ route('liberiapers.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="last_name"><span class="text-danger">*</span>LAST NAME OF APPLICANT</label>
                    <input type="text" class="form-control" name="last_name" id="last_name">
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="first_name"><span class="text-danger">*</span>FIRST NAME</label>
                    <input type="text" class="form-control" name="first_name" id="first_name" value="">
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="midle_initial"><span class="text-danger">*</span>MIDDLE INITIAL</label>
                    <input type="text" class="form-control" name="midle_initial" id="midle_initial" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="dob">Date of birth:</label>
                    <input type="date" class="form-control" name="dob" id="dob" value="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="d-flex justify-content-between">
                        <div class="">
                            <label class="form-label" for="city">Place of birth: City</label>
                            <input type="text" class="form-control" name="city" id="city" value="">
                        </div>
                        <div class="ml-2">
                            <label class="form-label" for="country">Country</label>
                            <input type="text" class="form-control" name="country" id="country" value="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="document_no"><span class="text-danger">*</span>Sex:</label>
                    <br>
                    <label class="form-label text-capitalize" for="male">Male:</label>
                    <input style="width: 25px; height: 20px;" type="checkbox"  name="gender" id="male" value="Male">
                    <label class="form-label text-capitalize" for="female">Female:</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="gender" id="female" value="Female">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-class">Examination for duty as:</label>
                </div>
                <div class="form-group" style="padding-left: 50px;">
                    <div class="d-flex justify-content-between">
                        <div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="master">Master</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="master" value="0">
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="mate">Mate</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="mate" value="1">
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="engineer">Engineer</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="engineer" value="2">
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="radio_off">Radio Off</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="radio_off" value="3">
                            </div>
                        </div>
                        <div class="mr-2">
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="rating">Rating</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="rating" value="4">
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="mou_deck">Mou Deck</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="mou_deck" value="5">
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="mou_engine">Mou Engine</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="mou_engine" value="6">
                            </div>
                            <div class="form-group d-flex justify-content-between" style="width: 200px;">
                                <label class="form-label d-block" for="super_numer">Supernumerary</label>
                                <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="super_numer" value="7">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="mailing_address">Mailing address of applicant:</label>
                    <textarea  class="form-control" name="mailing_address" id="mailing_address"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <hr>
                <h6 class="text-center" style="color: #95a2af; text-transform: uppercase">Medical examination (see last page for medical requirements) state details on reverse side</h6>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="height">Height</label>
                    <input  class="form-control" type="text" name="height" id="height" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="weight">Weight</label>
                    <input  class="form-control" type="text" name="weight" id="weight" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="blood_pressure">Blood pressure</label>
                    <input  class="form-control" type="text" name="blood_pressure" id="blood_pressure" value="">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="pulse">Pulse</label>
                    <input  class="form-control" type="text" name="pulse" id="pulse" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="respiration">Respiration</label>
                    <input  class="form-control" type="text" name="respiration" id="respiration" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="general_appearance">General appearance</label>
                    <input  class="form-control" type="text" name="general_appearance" id="general_appearance" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="">Vision(Without glasses):</label>
                    <br>
                    <label class="form-label" for="right_eye">Right eye:</label>
                    <input class="form-control" type="text" name="right_eye" id="right_eye" value="">
                    <label class="form-label" for="left_eye">Left eye:</label>
                    <input class="form-control" type="text" name="left_eye" id="left_eye" value="">
                    <br>
                    <label class="form-label" for="date_of_vision_test">Date of last color vision test:</label>
                    <input class="form-control"  type="date" name="date_of_vision_test" id="date_of_vision_test" value="{{ old('date_of_vision_test', $form->date_of_vision_test ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="">Vision(With glasses):</label>
                    <br>
                    <label class="form-label" for="with_g_right_eye">Right eye:</label>
                    <input class="form-control" type="text" name="with_g_right_eye" id="with_g_right_eye" value="">
                    <label class="form-label" for="with_g_left_eye">Left eye:</label>
                    <input class="form-control" type="text" name="with_g_left_eye" id="with_g_left_eye" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="form-label" for="clolor_vision_label">COLOR VISION MEETS STANDARDS IN STCW, TABLE A-I/9?</label>
                    <label class="form-label" for="color_vision_yes">Yes</label>
                    <input style="width:25px; height: 20px;" type="checkbox" name="color_vision"  id="color_vision_yes"  value="1">
                    <label class="form-label" for="color_vision_no">No</label>
                    <input style="width:25px; height: 20px;" type="checkbox" name="color_vision"  id="color_vision_no"  value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="color_test_type">Color test type:</label>
                    <label class="form-label" for="book">Book</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_test_type" id="book" value="1">
                    <label class="form-label" for="lanter">Lantern</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_test_type" id="lanter" value="0">
                    <label class="form-label" for="test_normal">Check if color test is normal:</label>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <br>
                    <br>
                    <label class="form-label" for="yellow">Yellow</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="yellow" value="0">
                    <label class="form-label" for="red">Red</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="red" value="1">
                    <label class="form-label" for="green">Green</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="green" value="2">
                    <label class="form-label" for="blue">Blue</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="blue" value="3">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label d-block" for="">Hearing:</label>
                    <label class="form-label" for="hearing_right_ear">Right ear:</label>
                    <input class="form-control" type="text" name="hearing_right_ear" id="hearing_right_ear" value="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label d-block" for=""></label>
                    <br>
                    <label class="form-label" for="hearing_left_ear">Left ear:</label>
                    <input class="form-control" type="text" name="hearing_left_ear" id="hearing_left_ear" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="head_neck">Head and neck</label>
                    <input class="form-control" type="text" name="head_neck"  id="head_neck"  value="" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="lungs">Lungs</label>
                    <input class="form-control" type="text" name="lungs" id="lungs"  value="" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="extremities_upper">Extremities(upper):</label>
                    <input class="form-control" type="text" name="extremities_upper" id="extremities_upper" value="" >
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" id="heart">Heart(cardiovascular)</label>
                    <input class="form-control" type="text" name="heart" id="heart" value="" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="speech">Speech(Deck/navigational officer and radio officer)Is speech unimpaired for normal voice communication?</label>
                    <input class="form-control" type="text" name="speech" id="speech" value="" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="extremities_lower">Extremities(Lower):</label>
                    <input class="form-control" type="text" name="extremities_lower" id="extremities_lower" value="" >
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="is_applicant_suffering">Is applicant suffering from any disease likely to
                        be aggravated by working aboard a vessel, or to render him/
                        her unfit for service at sea or likely to endanger the health
                        of of other persons on board?
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="is_applicant_suffering_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="is_applicant_suffering" id="is_applicant_suffering_yes" value="1">
                    <label class="form-label" for="is_applicant_suffering_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="is_applicant_suffering" id="is_applicant_suffering_no" value="0">
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                <label>
                   (He <input style="width: 25px; height: 20px;" type="checkbox" name="" >/She  <input style="width: 25px; height: 20px;" type="checkbox" name="" value="">) is found to be (Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="1">/ not Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="0">) for duty as a (without any<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="1"> / with the following<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="0">) restrictions:
                </label>
                <br>
                <label for="line1">Line1</label>
                <input type="text" name="line1" class="form-control" id="line1">
                <label for="line2">Line2</label>
                <input type="text" name="line2" class="form-control" id="line2">
            </div>
        </div>
        <div class="row">
              <div class="col-lg-6">
                  <div class="form-group">
                      <label class="form-label" for="date_of_exam">Date of Examination:</label>
                      <input class="form-control" type="date" name="date_of_exam" id="date_of_exam" value="">
                  </div>
              </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="date_of_expiration">Date of Expiration:</label>
                    <input class="form-control" type="date" name="date_of_expiration" id="date_of_expiration" value="">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>
    </form>
@endsection
