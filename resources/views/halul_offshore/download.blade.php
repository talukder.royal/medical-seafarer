<!DOCTYPE html>
<html>
<head>
    <title>one</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
    <style>
        .p-0-m-0 {
            padding: 0;
            margin: 0;
        }
        .p-4{
            padding: 4px;
        }
        .p-left-6 {
            padding-left: 6px;
        }
        .p-top-4 {
            padding-top: 4px;
        }
        .p-right-4 {
            padding-right: 4px;
        }
        .p-right-0 {
            padding-right: 0;
        }
        .line-height-1-6 {
            line-height:1.6;
        }
        .text-center-h {
            text-align: center;
        }

        .width-60-per {
            width: 58%;
        }

        .width-50-per {
            width: 49.5%;
        }

        .width-40-per {
            width: 38.75%;
        }

        .width-30-per {
            width: 29.75%;
        }
        .border-right {
            border-right: 1px solid black;
        }

        .border-left {
            border-left: 1px solid black;
        }
        .float-right {
            float: right;
        }


    </style>
</head>
<body >
{{--Start of page 1--}}
<div class="hal_body" style="font-size: 12px;">

<div class="hul_head">
{{--    start of QR--}}
{{--    <div class="visible-print text-center" style="padding: 2px;">--}}
{{--        @php--}}
{{--            use SimpleSoftwareIO\QrCode\Facades\QrCode;--}}
{{--            $path  = 'qrcodes/_'.now().$form->id.'.svg';--}}
{{--            QrCode::generate(route('smcseafarer_recrord.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));--}}
{{--        @endphp--}}
{{--        <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">--}}
{{--        <p>Scan me to return to the original page.</p>--}}
{{--    </div>--}}
{{--    End of QR--}}
    <div>
        <div class="hul_head_sec_1 s_float" style="width: 40%; line-height: 2px;">
            <p class="p-0-m-0 line-height-1-6">HALUL OFFSHORE SERVICES COMPANY W.L.L.</p>
            <p class="p-0-m-0 line-height-1-6">REVISION: 00</p>
        </div>
        <div class="hul_head_sec_2 s_float line-height-" style="width: 25%; float: right;">
            <p class="p-0-m-0 line-height-1-6">WF/CREW/431</p>
            <p class="p-0-m-0 line-height-1-6">Date: 31/07/2013</p>
        </div>
    </div>
</div>
<div style=" padding: 10px; font-family: sans-serif; font-style:normal;">
    <div class="hal_full clear" style="margin-left: 10px; margin-top: 30px; border: 1px solid black;">
        <div class="hul_row_1">
            <div class="p-0-m-0 text-center-h line-height-1-6">
                <h4 class="hul_title_1 text-center-h p-0-m-0">SEAFARER’S MEDICAL EXAMINATION REPORT/CERTIFICATE</h4>
                <h4 class="hul_title_2 text-center-h p-0-m-0" style="color: #95a2af;">CONFIDENTIAL DOCUMENT</h4>
            </div>
            <p class="hul_cer_issue_details" style="padding-left: 6px;">This certificate is issued by authority of the Maritime Administrator and in compliance with the requirements of the Medical Examination (Seafarers) Convention 1946 (ILO No. 73), as amended, STCW Convention, 1978 as amended and the Maritime Labour Convention, 2006.</p>
        </div>
        <div class="hal_row_2">
            <div class="hol_s_name s_float width-40-per border-right p-left-6 p-right-4 p-top-4" style="height:3.5em;">
                SURENAME  {{ $form->surname }}
            </div>
            <div class="hol_g_name s_float width-60-per p-left-6 p-right-4 p-top-4" style="height: 3.5em;">
                GIVEN NAMES(S)  {{ $form->given_name }}
            </div>
        </div>
        <div class="hal_row_3" style="">
            <div class="hol_nat s_float  width-40-per border-right p-left-6 p-right-4 p-top-4 " style="height: 3.5em;">
               NATIONALITY :
               {{ $form->nationality }}
            </div>
            <div class="hol_id_doc s_float  width-60-per  p-left-6 p-right-4 p-top-4" style="height: 3.5em;">
                ID DOCUMENT NO:
                {{ $form->document_no }}
            </div>
        </div>
        <div class="hol_row_4 p-0-m-0" style="">
            <div class="hol_date_of_birth s_float  width-40-per border-right p-left-6 p-right-4 p-top-4" style="height: 4.5em;">
                @php
                   $date_of_birth = \Carbon\Carbon::parse($form->dob);
                @endphp
                DATE OF BIRTH
                <div class="hot_date_d_m" style="margin-left: 4em; margin-top: 1.5em;"><pre class="p-0-m-0">MONTH<span class="hol_m_value">  {{ $date_of_birth->month }}</span>  Day<span class="hol_d_value">   {{ $date_of_birth->day }}</span>   YEAR<span class="hol_year_value">  {{ $date_of_birth->year }}</span></pre></div>
            </div>
            <div class="hol_place_of_birth s_float  p-0-m-0" style="padding: 0; margin: 0; width: 59.5%; height: 4.5em;">
                <div class="hot_place_of_birth s_float p-left-6 p-right-0 p-top-4">
                    <p class="p-0-m-0" style="">Place OF BIRTH</p>
                    <div style="background: #0e5a2e; width: 100%; padding-top: .20em;" class="p-0-m-0">
                      <pre>CITY {{ $form->city }}      <span style="text-transform: uppercase;">country {{ $form->country }}</span></pre>
                    </div>
                </div>
                <div class="hol_sex  p-top-4" style="width: 38.5%; float: right; height: 2em;">
                    <p class="p-0-m-0">SEX</p>
                    <div class="hol_m_f p-0-m-0" style="margin-top: 1em;">
                        <div style="width: 60%; float: left;">
                            @if($form->gender == 'Male')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        MALE
                        </div>
                        <div style="float: right; width: 40%;" class="p-0-m-0">
                        @if($form->gender == 'Female')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        FEMALE
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hol_row_5">
            <div class="hol_exam_duty s_float width-40-per">
                <p class="hol_exm_duty_title">EXAMINATION FOR DUTY AS:</p>
                <div class="p-0-m-0" style="margin-left: 4em; background: #39aeb5;">
                    <div class="m_c_col_three_row">
                        <div class="ms_position_hol s_float width-50-per">
                            MASTER
                        </div>
                        <div class="ms_position_hol_value float-right width-30-per">
                            @if($form->examination_as_duty == '0')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                    <div class="m_c_col_three_row">
                        <div class="ms_position_hol s_float width-50-per">
                            DECK OFFICER
                        </div>
                        <div class="ms_position_hol_value float-right width-30-per">
                            @if($form->examination_as_duty == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                    <div class="m_c_col_three_row">
                        <div class="ms_position_hol s_float width-60-per">
                            ENGINEERING OFFICER
                        </div>
                        <div class="ms_position_hol_value float-right width-30-per">
                            @if($form->examination_as_duty == '2')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                    <div class="m_c_col_three_row">
                        <div class="ms_position_hol s_float width-60-per">
                            RADIO OPERATOR
                        </div>
                        <div class="ms_position_hol_value float-right width-30-per">
                            @if($form->examination_as_duty == '3')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                    <div class="m_c_col_three_row">
                        <div class="ms_position_hol s_float width-60-per">
                            RATING
                        </div>
                        <div class="ms_position_hol_value float-right width-30-per">
                            @if($form->examination_as_duty == '4')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="hol_mail s_float width-60-per">
                <p>MAILING ADDRESS OF APPLICANT:</p>
                <p>{{ $form->mailing_address }}</p>
            </div>
        </div>
        <div class="hol_row_6 clear">
            <p>DECLARATION OF APPROVED MEDICAL PRACTIONER:<span></span></p>
            <p class="hol_confir_check">I CONFIRM THAT IDENTIFICATION DOCUMENTS WERE CHECKED:<span>
                    @if($form->identi_documents == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    Yes/
                     @if($form->identi_documents == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    No</span></p>
        </div>
        <div class="hol_row_7">
            <p><span style="font-size: 12px; font-weight: bold;">MEDICAL EXAMINATION</span> (SEE LAST PAGE FOR MEDICAL REQUIREMENTS) STATE DETAILS ON REVERSE SIDE</p>
        </div>

        <div class="hol_row_8 clear">
            <div class="hol_height s_float">
                <p>HEIGHT <span>{{ $form->height }}</span></p>
            </div>
            <div class="hol_weight s_float">
                <p>WEIGHT <span>{{ $form->weight }}</span></p>
            </div>
            <div class="hol_bloor s_float">
                <p>BLOOD PRESSURE <span>{{ $form->blood_pressure }}</span></p>
            </div>
            <div class="hol_pulse s_float">
                <p>PULSE <span>{{ $form->pulse }}</span></p>
            </div>
            <div class="hol_respi s_float">
                <p>RESPIRATION <span>{{ $form->respiration }}</span></p>
            </div>
            <div class="hol_general s_float">
                <p>GENERAL APPEARANCE <span>{{ $form->general_appearance }}</span></p>
            </div>
        </div>
        <div class="hol_row_9">
            <div class="hol_eye s_float">
                <div class="hol_vision_row">
                    <div class="hol_vison s_float">
                        VISION
                    </div>
                    <div class="hol_ritht_eye s_float">
                        RIGHT EYE
                    </div>
                    <div class="hol_left_eye s_float">
                        LEFT EYE
                    </div>
                </div>
                <div class="hol_vision_row">
                    <div class="hol_vison s_float">
                        <div class="vision_without_glass">
                            WITHOUT GLASSES
                        </div>
                    </div>
                    <div class="hol_ritht_eye s_float">
                        <div class="vision_without_glass_right">
                            <p>{{ $form->right_eye }}</p>
                        </div>
                    </div>
                    <div class="hol_left_eye s_float">
                        <div class="vision_without_glass_left">
                            <p>{{ $form->left_eye }}</p>
                        </div>
                    </div>
                </div>
                <div class="hol_vision_row">
                    <div class="hol_vison s_float">
                        <div class="vision_without_glass">
                            WITH GLASSES
                        </div>
                    </div>
                    <div class="hol_ritht_eye s_float">
                        <div class="vision_without_glass_right">
                            <p>{{ $form->with_g_right_eye }}</p>
                        </div>
                    </div>
                    <div class="hol_left_eye s_float">
                        <div class="vision_without_glass_left">
                            <p>{{ $form->with_g_left_eye }}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="hol_hearing s_float">
                <p class="hol_hear_title">HEARING</p>
                <p class="hol_hear_value">RT. EAR <span class="hol_r_l_ear_value">{{ $form->hering_right_ear }}</span> LEFT EAR <span class="hol_r_l_ear_value">{{ $form->hering_left_ear }}</span></p>
            </div>
        </div>

        <div class="hol_row_10">
            <p class="color_t_type">
                <b style="font-size: 11px;">
                    COLOR TEST TYPE:
                    @if($form->color_test_type == 1)
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    BOOK
                    @if($form->color_test_type == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    LANTERN
                </b>
                CHECK IF COLOR TEST IS NORMAL
                <b style="font-size: 11px;">
                    YELLOW
                    <span class="hol_col_icon_bor">
                        @if($form->test_normal == 1)
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </span>
                    RED
                    <span class="hol_col_icon_bor">
                        @if($form->test_normal == 1)
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </span>
                    GREEN
                    <span class="hol_col_icon_bor">
                        @if($form->test_normal == 1)
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </span>
                    BLUE
                    <span class="hol_col_icon_bor">
                        @if($form->test_normal == 1)
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </span>
                </b>
            </p>

            <p class="hol_date_last_col_t">DATE OF LAST COLOR VISION TEST: <span class="hol_c_t_value"></span></p>
        </div>
        <div class="hol_row_11">
            <div class="hol_head_neck s_float">
                <p>HEAD AND NECK: {{ $form->head_neck }}]</p>
            </div>
            <div class="hol_heart s_float">
                <p>HEART (CARDIOVASCULAR) : {{ $form->heart }}</p>
            </div>

        </div>
        <div class="hol_row_12 clear">
            <div class="hol_head_neck s_float">
                <p>LUNGS  {{ $form->lungs }}</p>
            </div>
            <div class="hol_heart s_float">
                <p style="font-size: 12px; font-weight: bold;">SPEECH : {{ $form->speech }} <span style="font-size: 9px;">(DECK/NAVIGATIONAL OFFICER AND RADIO OFFICER)</span></p>
                <p style="font-size: 8px; font-weight: normal;">IS SPEECH UNIMPAIRED FOR NORMAL VOICE COMMUNICATION?</p>
            </div>

        </div>

        <div class="hol_row_13">
            <p class="hol_ext">EXTREMITIES:</p>
            <p class="hol_ext_value">UPPERt: <span class="hol_ext_u_value">{{ $form->extremities_upper }}</span> LOWER<span class="hol_ext_u_value">{{ $form->extremities_lower }}</span></p>
        </div>

        <div class="hol_row_14">
            IS APPLICANT VACCINATED IN ACCORDANCE WITH WHO REQUIREMENTS?
            <span class="hol_app_y_n">
                @if($form->vaccinated_requirement == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                YES
            </span>
            <span class="hol_app_y_n">
                @if($form->vaccinated_requirement == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NO
            </span>
        </div>
        <div class="hol_row_15">
            <p>S APPLICANT SUFFERING FROM ANY DISEASE LIKELY TO BE AGGRAVATED BY WORKING ABOARD A VESSEL, OR TO RENDER HIM/HER UNFIT FOR SERVICE AT SEA OR LIKELY TO ENDANGER THE HEALTH OF OTHER PERSONS ON BOARD?</p>
        </div>
        <div class="hol_row_14">
            IS APPLICANT TAKING ANY NON-PRESCRIPTION OR PRESCRIPTION MEDICATIONS?
            <span class="hol_app_y_n">
                @if($form->is_prescirption_medication == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif>
                YES
            </span>
            <span class="hol_app_y_n">
                @if($form->is_prescirption_medication == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NO
            </span>
        </div>
        <div class="hol_row_16">
            <div class="hol_sign s_float">
                <p></p>
                <p class="hol_sign_text">SIGNATURE OF APPLICANT</p>
            </div>
            <div class="hol_sign_date s_float">
                <p></p>
                <p class="hol_date_text">DATE</p>
            </div>
        </div>
        <div class="hol_foot">
            <p class="hol_foot_text">Page 1 of 3</p>
        </div>

    </div>
</div>
</div>
{{-- End of page 1 --}}

<div class="hal_body">
<div class="hul_head">
    <div class="hul_head_sec_1 s_float">
        <p>HALUL OFFSHORE SERVICES COMPANY W.L.L.</p>
        <p>REVISION: 00</p>
    </div>
    <div class="hul_head_sec_2 s_float">
        <p>WF/CREW/431</p>
        <p>Date: 31/07/2013</p>
    </div>
</div>
<div class="hal_full_2 clear">
    <div class="hol_row_17">
        <p>THIS SIGNATURE SHOULD BE AFFIXED IN THE PRESENCE OF THE EXAMINING PHYSICIAN.</p>
    </div>
    <div class="hol_row_18">
        <div class="hol_p_2_cer s_float">
            <p>THIS IS TO CERTIFY THAT A PHYSICAL EXAMINATION WAS GIVEN TO:</p>
        </div>
        <div class="hol_name_of_apliant s_float">
            <p></p>
            <p class="hol_name_of_apliant_text">NAME OF APPLICANT</p>
        </div>
    </div>
    <div class="hol_row_19">
        <p>THIS APPLICANT IS CERTIFIED FREE OF COMMUNICABLE DISEASE <span class="hol_app_yes">YES <i class="far fa-square"></i></span> <span class="hol_app_no">YES <i class="far fa-square"></i></span></p>
    </div>
    <div class="hol_row_20">
        <p>IRCLE APPROPRIATE CHOICE: (
            @if($form->gender == 'Male')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            HE /
            @if($form->gender == 'Female')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            SHE) IS FOUND TO BE (
            @if($form->fit == '1')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            FIT /
            @if($form->fit == '0')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            NOT FIT) FOR DUTY AS A (
            @if($form->examination_as_duty == '0')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            MASTER /
            @if($form->examination_as_duty == '1')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            DECK OFFICER /
            @if($form->examination_as_duty == '2')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            ENGINEERING OFFICER
            /
            @if($form->examination_as_duty == '3')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            RADIO OFFICER /
            @if($form->test_normal == '4')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            RATING) (
            @if($form->with == '0')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            WITHOUT ANY /
            @if($form->with == '1')
                <i class="fa fa-check">&#xf046;</i>
            @else
                <i class="fa fa-check">&#xf096;</i>
            @endif
            WITH THE FOLLOWING) RESTRICTIONS:{{ $form->line1 }}</p>

    </div>
    <div class="hol_row_21">
        <div class="hol_row_same">
            <p>NAME AND DEGREE OF PHYSICIAN <span class="hol_p_2_n_d"></span></p>
        </div>
        <div class="hol_row_same">
            <p>ADDRESS <span class="hol_p_2_address"></span></p>
        </div>
        <div class="hol_row_same">
            <p>NAME OF PHYSICIAN’S CERTIFICATING AUTHORITY <span class="hol_p_2_phy"></span></p>
        </div>
        <div class="hol_row_same">
            <p>DATE OF ISSUE OF PHYSICIAN’S CERTIFICATE <span class="hol_p_2_date_iss"></span></p>
        </div>
        <div class="hol_row_same">
            <p>SIGNATURE / STAMP OF PHYSICIAN : <span class="hol_p_2_stamp"></span></p>
        </div>
        <div class="hol_row_same">
            <p>DATE OF EXAMINATION: <span class="hol_p_2_d_exam"></span></p>
        </div>
        <div class="hol_row_same">
            <p>EXPIRY DATE OF CERTIFICATE : <span class="hol_p_2_d_exp"></span></p>
        </div>
    </div>

<div class="hol_row_22">
    <p class="hol_ack">SEAFARER ACKNOWLEDGMENT</p>
    <p class="hol_ack_details">I <span class="hol_ack_name"></span>(NAME OF SEAFARER), CONFIRM THAT I HAVE BEEN INFORMED OF
        THE CONTENT OF CERTIFICATE AND THE RIGHT TO GET A REVIEW</p>
</div>


</div>
<div class="hol_foot_2">
    <p class="hol_foot_text">Page 2 of 3</p>
</div>
<div class="hal_body">
    <div class="hul_head_3">
        <div class="hul_head_sec_1_p3 s_float">
            <p>HALUL OFFSHORE SERVICES COMPANY W.L.L.</p>
            <p>REVISION: 00</p>
        </div>
        <div class="hul_head_sec_2_p3 s_float">
            <p>WF/CREW/431</p>
            <p>Date: 31/07/2013</p>
        </div>
    </div>
    <div class="hal_full_p3 clear">
        <div class="hal_p_sec_1">
            <div class="hol_p_3_med_req">
                <h6 class="hol_med_req_title">MEDICAL REQUIREMENTS</h6>
                <p class="hol_med_req_details">All applicants for an officer certificate, Seafarer's Identification and Record Book or certification of special qualifications shall be required to have a physical examination reported on this Medical Form completed by a certificated physician. The completed medical form must accompany the application for officer certificate, application for seafarer's identity document, or application for certification of special qualifications. This physical examination must be carried out not more than 12 months prior to the date of making application for an officercertificate, certification of special qualifications or a seafarer's book. The examination shall be conducted in accordance with theInternational Labor Organization World Health Organization,<i> Guidelines for Conducting Pre-sea and Periodic Medical FitnessExaminations for Seafarers (ILO/WHO/D.2/1997.</i> Such proof of examination must establish that the applicant is in satisfactoryphysical and mental condition for the specific duty assignment undertaken and is generally in possession of all body faculties necessary in fulfilling the requirements of the seafaring profession.</p>

                <div class="hol_p_3_row_2">
                    <p>In conducting the examination, the certified physician should, where appropriate, examine the seafarer’s previous medical records (including vaccinations) and information on occupational history, noting any diseases, including alcohol or drug-related problems and/or injuries. In addition, the following minimum requirements shall apply:</p>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(a)</span> Hearing</p>
                    <ul class="hol_p_3_3_ul">
                        <li>All applicants must have hearing unimpaired for normal sounds and be capable of hearing a whispered voice in better ear at 15 feet (4.57 m) and in poorer ear at 5 feet (1.52 m).</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(b)</span> Eyesight</p>
                    <ul class="hol_p_3_3_ul">
                        <li>Deck officer applicants must have (either with or without glasses) at least 20/20(1.00) vision in one eye and at least 20/40 (0.50)in the other. If the applicant wears glasses, he must have vision without glasses of at least 20/160 (0.13) in both eyes. Deck officer applicants must also have normal color perception and be capable of distinguishing the colors red, green, blue and yellow.</li>
                        <li>Engineer and radio officer applicants must have (either with or without glasses) at least 20/30 (0.63) vision in one eye and at least 20/50 (0.40) in the other. If the applicant wears glasses, he must have vision without glasses of at least 20/200 (0.10) in both eyes. Engineer and radio officer applicants must also be able to perceive the colors red, yellow and green.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(c)</span> Dental</p>
                    <ul class="hol_p_3_3_ul">
                        <li>Seafarers must be free from infections of the mouth cavity or gums.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(d)</span> Blood Pressure</p>
                    <ul class="hol_p_3_3_ul">
                        <li>An applicant's blood pressure must fall within an average range, taking age into consideration.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(e)</span> Voice</p>
                    <ul class="hol_p_3_3_ul">
                        <li>Deck/Navigational officer applicants and Radio officer applicants must have speech which is unimpaired for normal voice communication.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(f)</span> Vaccinations</p>
                    <ul class="hol_p_3_3_ul">
                        <li>All applicants shall be vaccinated according to the requirements indicated in the WHO publication, International Travel and Health, Vaccination Requirements and Health Advice, and shall be given advice by the certified physician on immunizations. If new vaccinations are given, these shall be recorded.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(g)</span> Hearing</p>
                    <ul class="hol_p_3_3_ul">
                        <li>Applicants afflicted with any of the following diseases or conditions shall be disqualified: epilepsy, insanity, senility, alcoholism, tuberculosis, acenereal disease or neurosyphilis, AIDS, and/or the use of narcotics. Applicants diagnosed with, suspected of, or exposed to any communicable disease transmittable by food shall be restricted from working with food or in food –related areas until symptom-free for at least 48 hours.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(h)</span> Physical Requirements</p>
                    <ul class="hol_p_3_3_ul">
                        <li>Applicants for able seaman, bosun, GP-1, ordinary seaman and junior ordinary seaman must meet the physical requirements for a deck/navigational officer's certificate.</li>
                        <li>Applicants for fireman/watertender, oiler/motorman, pumpman, electrician, wiper, tankerman and survival craft/rescue boat crewman must meet the physical requirements for an engineer officer's certificate.</li>
                    </ul>
                </div>


            </div>

        </div>
        <div class="hal_p_sec_2">
            <h6 class="hol_p2_not_title">IMPORTANT NOTE:</h6>
            <p>The seafarer must retain the original of the ‘Medical Examination Report/Certificate’ as evidence of physical qualification while serving on board a vessel.</p>
            <p>An applicant who has been refused a medical certificate or has had a limitation imposed on his/her ability to work, shall be given the opportunity to have an additional examination by another medical practitioner or medical referee who is independent of the shipowner or of any organization of shipowners or seafarers.</p>
            <p>Medical examination reports shall be marked as and remain confidential with the applicant having the fight of a copy to his/report. The medical examination report shall be used only for determining the fitness of the seafarer for work and enhancing health care.</p>
        </div>
        <div class="hal_p_sec_3">
            <h6 class="hal_p_3_title_2">DETAILS OF MEDICAL EXAMINATION:</h6>
            <p>(To be completed by examining physician; alternatively the examining physician may attach a form similar or identical to the model provided – Medical Exam Form).</p>
        </div>
        <div class="hol_foot">
            <p class="hol_foot_text">Page 1 of 3</p>
        </div>
    </div>
</div>
</div>

</body>
</html>
