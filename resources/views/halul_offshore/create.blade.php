@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <div class="row">
        <div class="col-lg-12 text-center">
            <div>
                SEAFARER'S  MEDICAL EXAMINATION REPORT/CERTIFICATE
            </div>
            <div>
                CONFIDENTIAL DOCUMENT
            </div>
        </div>
    </div>
    <form class="form-control" action="{{ route('halul-offshore.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="surname"><span class="text-danger">*</span>SURNAME</label>
                    <input type="text" class="form-control" name="surname" id="surname" value="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="given_name"><span class="text-danger">*</span>GIVEN NAME(s)</label>
                    <input type="text" class="form-control" name="given_name" id="given_name" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for=""><span class="text-danger">*</span>nationality:</label>
                    <input type="text" class="form-control" name="nationality" id="" value="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="document_no"><span class="text-danger">*</span>ID document no:</label>
                    <input type="text" class="form-control" name="document_no" id="document_no" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="dob">Date of birth:</label>
                    <input type="date" class="form-control" name="dob" id="dob" value="{{ old('date_of_birth', $form->date_of_birth ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label text-capitalize" for="gender">Sex</label>
                    <select type="date" class="form-control" name="gender" id="gender">
                        <option value="">Select</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="d-flex justify-content-between">
                        <div class="">
                            <label class="form-label" for="city">Place of birth: City</label>
                            <input type="text" class="form-control" name="city" id="city" value="{{ old('city', $form->documents->city ?? '') }}">
                        </div>
                        <div class="ml-2">
                            <label class="form-label" for="">Country</label>
                            <input type="text" class="form-control" name="country" id="country" value="{{ old('nationality', $form->documents->country ?? '') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-class">Examination for duty as:</label>
                </div>
                <div class="form-group" style="padding-left: 100px;">
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="master">Master</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="master" value="0">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="deck_officer">Deck officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="deck_officer" value="1">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="engineering_officer">Engineering officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="engineering_officer" value="2">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="radio_officer">Radio officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="radio_officer" value="3">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="rating">Rating</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="rating" value="4">
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="mailing_address">Mailing address of applicant:</label>
                    <textarea  class="form-control" name="mailing_address" id="mailing_address">{{ old('mailing_address', $form->documents->mailing_address ?? '') }}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <label class="from-group" style="text-transform: uppercase">Declaration of approved medical practioner:</label>
                <br>
                <label class="form-label" style="text-transform: uppercase" for=""> i confirm that identification documents were checked:</label>
                <input style="width: 25px; height: 20px;" type="checkbox" name="identi_documents" value="1" id="identi_yes">
                <label class="form-label" for="identi_yes">Yes</label>
                <input style="width: 25px; height: 20px;" type="checkbox" name="identi_documents" value="0" id="identi_yes">
                <label class="form-label" for="identi_yes">No</label>
            </div>
            <div class="col-lg-12">
                <hr>
                <h6 class="text-center" style="color: #95a2af; text-transform: uppercase">Medical examination (see last page for medical requirements) state details on reverse side</h6>
                <hr>
            </div>
        </div>
        <div class="row" style="border: 1px solid #a7abb1;">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="height">Height</label>
                    <input  class="form-control" type="text" name="height" id="height" value="{{ old('height', $form->documents->height ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="blood_pressure">Blood pressure</label>
                    <input  class="form-control" type="text" name="blood_pressure" id="blood_pressure" value="{{ old('height', $form->documents->blood_pressure ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="respiration">Respiration</label>
                    <input  class="form-control" type="text" name="respiration" id="respiration" value="{{ old('pulse', $form->documents->respiration ?? '') }}">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="weight">Weight</label>
                    <input  class="form-control" type="text" name="weight" id="weight" value="{{ old('height', $form->documents->weight ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="pulse">Pulse</label>
                    <input  class="form-control" type="text" name="pulse" id="pulse" value="{{ old('pulse', $form->documents->pulse ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="general_appearance">General appearance</label>
                    <input  class="form-control" type="text" name="general_appearance" id="general_appearance" value="{{ old('pulse', $form->documents->general_appearance ?? '') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="">Vision(Without glasses):</label>
                    <br>
                    <label class="form-label" for="right_eye">Right eye:</label>
                    <input class="form-control" type="text" name="right_eye" id="right_eye" value="{{ old('pulse', $form->documents->right_eye ?? '') }}">
                    <label class="form-label" for="left_eye">Left eye:</label>
                    <input class="form-control" type="text" name="left_eye" id="left_eye" value="{{ old('pulse', $form->documents->left_eye ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="">Hearing:</label>
                    <br>
                    <label class="form-label" for="hering_right_ear">Right ear:</label>
                    <input class="form-control" type="text" name="hering_right_ear" id="hering_right_ear" value="{{ old('pulse', $form->documents->hering_right_ear ?? '') }}">
                    <label class="form-label" for="hering_left_ear">Left ear:</label>
                    <input class="form-control" type="text" name="hering_left_ear" id="hering_left_ear" value="{{ old('pulse', $form->documents->hering_left_ear ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="">Vision(With glasses):</label>
                    <br>
                    <label class="form-label" for="with_g_right_eye">Right eye:</label>
                    <input class="form-control" type="text" name="with_g_right_eye" id="with_g_right_eye" value="{{ old('pulse', $form->documents->with_g_right_eye ?? '') }}">
                    <label class="form-label" for="with_g_left_eye">Left eye:</label>
                    <input class="form-control" type="text" name="with_g_left_eye" id="with_g_left_eye" value="{{ old('pulse', $form->documents->with_g_left_eye ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_test_type">Color test type:</label>
                    <label class="form-label" for="book">Book</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_test_type" id="book" value="1">
                    <label class="form-label" for="lanter">Lantern</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_test_type" id="lanter" value="0">
                    <label class="form-label" for="test_normal">Check if color test is normal:</label>
                    <br>
                    <label class="form-label" for="yellow">Yellow</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="yellow" value="0">
                    <label class="form-label" for="red">Red</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="red" value="1">
                    <label class="form-label" for="green">Green</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="green" value="2">
                    <label class="form-label" for="blue">Blue</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="blue" value="3">
                    <label class="form-label" for="date_of_vision_test">Date of last color vision test:</label>
                    <input class="form-control"  type="date" name="date_of_vision_test" id="date_of_vision_test" value="{{ old('date', $form->documents->date_of_vision_test ?? '') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <label class="form-label" for="vision_standard">Are glasses or contact lenses necessary to meet the required vision standard?</label>
            </div>
            <div class="col-lg-4">
                <label class="form-label" for="vision_standard_yes">Yes</label>
                <input type="checkbox" style="width: 25px; height: 20px; margin-right:40px; " name="vision_standard" id="vision_standard_yes"  value="1">
                <label class="form-label" for="vision_standard_no">No</label>
                <input type="checkbox" style="width: 25px; height: 20px;" name="vision_standard" value="0">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="head_neck">Head and neck</label>
                    <input class="form-control" type="text" name="head_neck"  id="head_neck"  value="{{ old('pulse', $form->documents->head_neck ?? '') }}" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="lungs">Lungs</label>
                    <input class="form-control" type="text" name="lungs" id="lungs"  value="{{ old('pulse', $form->documents->lungs ?? '') }}" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="extremities_upper">Extremities(upper):</label>
                    <input class="form-control" type="text" name="extremities_upper" id="extremities_upper" value="{{ old('pulse', $form->documents->extremities_upper ?? '') }}" >
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" id="heart">Heart(cardiovascular)</label>
                    <input class="form-control" type="text" name="heart" id="heart" value="{{ old('pulse', $form->documents->heart ?? '') }}" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="speech">Speech(Deck/navigational officer and radio officer)Is speech unimpaired for normal voice communication?</label>
                    <input class="form-control" type="text" name="speech" id="speech" value="{{ old('pulse', $form->documents->speech ?? '') }}" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="extremities_lower">Extremities(Lower):</label>
                    <input class="form-control" type="text" name="extremities_lower" id="extremities_lower" value="{{ old('pulse', $form->documents->extremities_lower ?? '') }}" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="vaccinated_requirement">Is applicant vaccinated in accordance with who requirements?</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="vaccinated_requirement_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="vaccinated_requirement" id="vaccinated_requirement_yes" value="1">
                    <label class="form-label" for="vaccinated_requirement_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="vaccinated_requirement" id="vaccinated_requirement_no" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="is_applicant_suffering">Is applicant suffering from any disease likely to
                        be aggravated by working aboard a vessel, or to render him/
                        her unfit for service at sea or likely to endanger the health
                        of of other persons on board?
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="is_applicant_suffering_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="is_applicant_suffering" id="is_applicant_suffering_yes" value="1">
                    <label class="form-label" for="is_applicant_suffering_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="is_applicant_suffering" id="is_applicant_suffering_no" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="is_prescirption_medication">Is applicant taking any non-prescription or prescription medications?</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="is_prescirption_medication_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="is_prescirption_medication" id="is_prescirption_medication_yes" value="1">
                    <label class="form-label" for="is_prescirption_medication_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="is_prescirption_medication" id="is_prescirption_medication_yes" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="communicable_disease">This applicant is certified free of communicable disease?</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="communicable_disease_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="communicable_disease" id="communicable_disease_yes" value="1">
                    <label class="form-label" for="communicable_disease_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="communicable_disease" id="communicable_disease_no" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="restriction" style="text-transform: uppercase">
                        Circle appropiate choice: (he/she) is found to be (Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="1">/ not Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="0">) for duty as a (master / deck officer / engineering officer/ radio operator / rating) (without any<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="0"> / with the following<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="1">) restrictions:
                    </label>
                    <input type="text" name="line1" class="form-control" id="line1">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="doe">Date of examination</label>
                    <input  class="form-control" type="date" name="doe" id="doe" value="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="doexpiry">Date of expiry</label>
                    <input  class="form-control" type="date" name="doexpiry" id="doexpiry" value="">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>
    </form>
@endsection
