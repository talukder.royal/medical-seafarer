@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block d-flex justify-content-between">
        <strong>{{ $message }}</strong>
        <button type="button" class="close" data-dismiss="alert">×</button>
    </div>
@endif


@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block d-flex justify-content-between">
        <strong>{{ $message }}</strong>
        <button type="button" class="close" data-dismiss="alert">×</button>
    </div>
@endif


@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block d-flex justify-content-between">
        <strong>{{ $message }}</strong>
        <button type="button" class="close" data-dismiss="alert">×</button>
    </div>
@endif


@if ($message = Session::get('info'))
    <div class="alert alert-info alert-block">
        <strong>{{ $message }}</strong>
        <button type="button" class="close" data-dismiss="alert">×</button>
    </div>
@endif


@if ($errors->any())
    <div class="alert alert-danger d-flex justify-content-between">
        Please check the form below for errors
        <button type="button" class="close" data-dismiss="alert">×</button>
    </div>
@endif
