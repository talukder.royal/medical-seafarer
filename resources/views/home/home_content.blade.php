
<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>@yield('title')</title>

      <!-- Favicon -->
      <link rel="shortcut icon" href="{{asset('public/assets/images/favicon.ico')}}" />

      <!-- Library / Plugin Css Build -->
      <link rel="stylesheet" href="{{asset('public/assets/css/core/libs.min.css')}}" />

      <!-- Aos Animation Css -->
      <link rel="stylesheet" href="{{asset('public/assets/vendor/aos/dist/aos.css')}}" />

      <!-- Hope Ui Design System Css -->
      <link rel="stylesheet" href="{{asset('public/assets/css/hope-ui.min.css?v=1.1.2')}}" />

      <!-- Custom Css -->
      <link rel="stylesheet" href="{{asset('public/assets/css/custom.min.css?v=1.1.2')}}" />

      <!-- Customizer Css -->
      <link rel="stylesheet" href="{{asset('public/assets/css/customizer.min.css')}}" />

      <!-- Dark Css -->
      <link rel="stylesheet" href="{{asset('public/assets/css/dark.min.css')}}"/>

      <!-- RTL Css -->
      <link rel="stylesheet" href="{{asset('public/assets/css/rtl.min.css')}}"/>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <!link rel="stylesheet" href="{{asset('public/style.css') }}"/>
      <!link rel="stylesheet" href="{{asset('public/css/all.css') }}"/>

  </head>
  <body class="  ">
    <!-- loader Start -->

    <!-- loader END -->

    <aside class="sidebar sidebar-default navs-rounded-all ">
        <div class="sidebar-header d-flex align-items-center justify-content-start">
            <a href="{{url('/dashboard')}}" class="navbar-brand">
                <!--Logo start-->
                <svg width="30" class="" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="-0.757324" y="19.2427" width="28" height="4" rx="2" transform="rotate(-45 -0.757324 19.2427)" fill="currentColor"/>
                    <rect x="7.72803" y="27.728" width="28" height="4" rx="2" transform="rotate(-45 7.72803 27.728)" fill="currentColor"/>
                    <rect x="10.5366" y="16.3945" width="16" height="4" rx="2" transform="rotate(45 10.5366 16.3945)" fill="currentColor"/>
                    <rect x="10.5562" y="-0.556152" width="28" height="4" rx="2" transform="rotate(45 10.5562 -0.556152)" fill="currentColor"/>
                </svg>
                <!--logo End-->            <h4 class="logo-title">Seafarer</h4>
            </a>
            <div class="sidebar-toggle" data-toggle="sidebar" data-active="true">
                <i class="icon">
                    <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M4.25 12.2744L19.25 12.2744" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path d="M10.2998 18.2988L4.2498 12.2748L10.2998 6.24976" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                    </svg>
                </i>
            </div>
        </div>
        <div class="sidebar-body pt-0 data-scrollbar">
          <!-- Sidebar start -->
          @include('includes.sidebar')
          <!-- Sidebar end -->
        </div>
        <div class="sidebar-footer"></div>
    </aside>
    <main class="main-content">
      <div class="position-relative">
        <!--Nav Start-->
        @include('includes.topbar')
        <!-- Nav Header Component Start -->        <!-- Nav Header Component End -->
        <!--Nav End-->
      </div>
      <div class="conatiner-fluid content-inner mt-n5 py-0">
       @yield('main_content')
      </div>
      <!-- Footer Section Start -->
      <footer class="footer">
          <div class="footer-body">
              <ul class="left-panel list-inline mb-0 p-0"></ul>
              <div class="right-panel">
                  ©<script>document.write(new Date().getFullYear())</script> CGIT
              </div>
          </div>
      </footer>
      <!-- Footer Section End -->    </main>

    <!-- Wrapper End-->
    <!-- Library Bundle Script -->
    <script src="{{asset('public/assets/js/core/libs.min.js')}}"></script>

    <!-- External Library Bundle Script -->
    <script src="{{asset('public/assets/js/core/external.min.js')}}"></script>

    <!-- Widgetchart Script -->
    <script src="{{asset('public/assets/js/charts/widgetcharts.js')}}"></script>

    <!-- mapchart Script -->
    <script src="{{asset('public/assets/js/charts/vectore-chart.js')}}"></script>
    <script src="{{asset('public/assets/js/charts/dashboard.js')}}" ></script>

    <!-- fslightbox Script -->
    <script src="{{asset('public/assets/js/plugins/fslightbox.js')}}"></script>

    <!-- Settings Script -->
    <script src="{{asset('public/assets/js/plugins/setting.js')}}"></script>

    <!-- Form Wizard Script -->
    <script src="{{asset('public/assets/js/plugins/form-wizard.js')}}"></script>

    <!-- AOS Animation Plugin-->
    <script src="{{asset('public/assets/vendor/aos/dist/aos.js')}}"></script>

    <!-- App Script -->
    <script src="{{asset('public/assets/js/hope-ui.js')}}" defer></script>

    <script>
        $("document").ready(function(){
            setTimeout(function(){
                $("div.alert").remove();
            }, 5000 ); // 5 secs
        });
    </script>
{{--    <script>--}}
{{--        $("document").ready(function () {--}}
{{--            $("#sidebar-menu").on('click', 'a', function (){--}}
{{--                $("#sidebar-menu a.active").removeClass('active');--}}
{{--                $(this).addClass('active');--}}
{{--            })--}}
{{--        })--}}
{{--    </script>--}}

    <script>
        $('document').ready(function () {
            let location = window.location.href;
            $('#sidebar-menu  a').each(function(){
                if(location.indexOf(this.href)>-2) {
                    $(this).parent().addClass('active');
                }
            });
        });
    </script>
  </body>
</html>
