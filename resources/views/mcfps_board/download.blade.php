<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
</head>
<body>
<div class="all mc_ex_bor">
    <div class="all_in  m_c_page">
{{--        start of the QR code--}}
        <div class="visible-print text-center" style="padding: 2px;">
            @php
                use SimpleSoftwareIO\QrCode\Facades\QrCode;
                $path  = 'qrcodes/_'.now().$form->id.'.svg';
                QrCode::generate(route('smcseafarer_recrord.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
            @endphp
            <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
            <p>Scan me to return to the original page.</p>
        </div>
 {{--        End of the QR code--}}
        <div class="m_c_heading">
            <div class="mc_logo s_float">
                <img src="img/m.PNG">
            </div>
            <div class="mc_meddical s_float">
                <h3 class="m_c_heading">MEDICAL CERTIFICATE FOR PERSONNEL SERVICE ON BOARD</h3>
            </div>
        </div>

        <div class="row_one clear">
            <div class="m_c_table_1">
                <div class="m_c_tab_row">
                    <div class="m_c_col_1 s_float">
                        <p>SURNAME: {{ $form->surname }}</p>
                    </div>
                    <div class="m_c_col_2 s_float">
                        <p>GIVEN NAME (S):{{ $form->given_name }}</p>
                    </div>

                </div>
                <div class="m_c_tab_row_2">
                    <div class="m_c_col_two_1 s_float">
                        <p>DATE OF BIRTH:</p>
                        @php
                            $date_of_birth = \Carbon\Carbon::parse($form->dob);
                        @endphp
                        <div class="m_c_df_value">
                            <div class="m_c_day s_float">
                                Day: {{ $date_of_birth->day }}
                            </div>
                            <div class="m_c_month s_float">
                                Month: {{ $date_of_birth->month }}
                            </div>
                            <div class="m_c_year s_float">
                                Year: {{ $date_of_birth->year }}
                            </div>
                        </div>
                    </div>
                    <div class="m_c_col_two_2 s_float">
                        <div class="m_c_place_birth s_float">
                            <p>PLACE OF BIRTH</p>
                            <div class="m_c_dity_country">
                                <div class="mc_city s_float">
                                    City: {{ $form->city }}
                                </div>
                                <div class="mc_city s_float">
                                    Country: {{ $form->country }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m_c_col_two_3 s_float">
                        <div class="m_c_gender s_float">
                            Male
                            @if($form->gender == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class="m_c_gender s_float">
                            Female
                            @if($form->gender == '1')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="m_c_tab_row_3">
                    <div class="m_col_three_1 s_float">
                        <p>POSITION ON BOARD:</p>
                        <div class="m_c_col_three_row">
                            <div class="ms_position s_float">
                                MASTER
                            </div>
                            <div class="ms_position_value s_float">
                                @if($form->examination_as_duty == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="m_c_col_three_row">
                            <div class="ms_position s_float">
                                DECK OFFICER
                            </div>
                            <div class="ms_position_value s_float">
                                @if($form->examination_as_duty == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="m_c_col_three_row">
                            <div class="ms_position s_float">
                                ENGINEERING OFFICER
                            </div>
                            <div class="ms_position_value s_float">
                                @if($form->examination_as_duty == '2')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="m_c_col_three_row">
                            <div class="ms_position s_float">
                                RADIO OPERATOR
                            </div>
                            <div class="ms_position_value s_float">
                                @if($form->examination_as_duty == '3')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="m_c_col_three_row">
                            <div class="ms_position s_float">
                                RATING
                            </div>
                            <div class="ms_position_value s_float">
                                @if($form->examination_as_duty == '4')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="m_col_three_2 s_float">
                        <p>MAILING ADDRESS OF APPLICANT: {{ $form->mailing_address }}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- row_one end -->

        <div class="mc_row clear">
            <p>DECLARATION OF THE AUTHORIZED PHYSICIAN</p>
        </div>
        <div class="row_two clear">
            <div class="mc_table_2">
                <div class="mc_table_2_p_1 s_float">
                    <div  class="mc_table_2_p_1_row_1">
                        <p>VISION</p>
                    </div>
                    <div  class="mc_table_2_p_1_row_2" style="border-bottom: 1px solid black;">
                        <div class="mc_table_2_col1 s_float">

                        </div>
                        <div class="mc_table_2_col2 s_float">
                            WITHOUT GLASSES
                        </div>
                        <div class="mc_table_2_col2 s_float"  style="border: none;">
                            WITH GLASSES
                        </div>
                    </div>
                    <div  class="mc_table_2_p_1_row_2">
                        <div class="mc_table_2_col1 s_float">
                            <p  class="r_bors">RIGHT EYE </p>
                        </div>
                        <div class="mc_table_2_col2 s_float">
                            <p class="mcg_e_value">{{ $form->right_eye }}</p>
                        </div>
                        <div class="mc_table_2_col2 s_float"  style="border: none;">
                            <p class="mcg_e_value">{{ $form->with_g_right_eye }}</p>
                        </div>
                    </div>
                    <div  class="mc_table_2_p_1_row_2">
                        <div class="mc_table_2_col1 s_float">

                        </div>
                        <div class="mc_table_2_col2 s_float">

                        </div>
                        <div class="mc_table_2_col2 s_float" style="border: none;">

                        </div>
                    </div>
                    <div  class="mc_table_2_p_1_row_3" style="border: none;">
                        <div class="mc_table_2_col1 s_float">
                            <p  class="l_bors">LEFT EYE</p>
                        </div>
                        <div class="mc_table_2_col2 s_float">
                            <p class="mcg_e_value">{{$form->left_eye }}</p>
                        </div>
                        <div class="mc_table_2_col2 s_float"  style="border: none;">
                            <p class="mcg_e_value">{{ $form->with_g_left_eye }}</p>
                        </div>
                    </div>
                </div>
                <div class="mc_table_2_p_2 s_float">
                    <div  class="mc_table_2_p_2_row_1">
                        <div class="mc_table_2_p_2_col s_float">
                            COLOR TEST TYPE
                        </div>
                        <div class="mc_table_2_p_2_col s_float" style="border-right: none;">
                            HEARING
                        </div>
                    </div>
                    <div  class="mc_table_2_p_2_row_2">
                        <div class="mc_table_2_p_2_col_2 s_float">
                            <p>
                                @if($form->color_test_type == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                                BOOK
                            </p>
                            <p>
                                @if($form->color_test_type == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                                LANTERN
                            </p>
                            <p>YELLOW <span class="mc_color_t_value">{{ $form->test_yellow }}</span> RED <span class="mc_color_t_value">{{ $form->test_red }}</span></p>
                            <p>GREEN <span class="mc_color_t_value">{{ $form->test_green }}]</span> BLUE <span class="mc_color_t_value">{{ $form->test_blue }}</span></p>
                        </div>
                        <div class="mc_table_2_p_2_col_2 s_float" style="border: none;">
                            <p>RIGHT EAR <span class="mc_ear_value">{{ $form->hering_right_ear }}</span></p><br>
                            <p>LEFT EAR <span class="mc_ear_value">{{ $form->hering_left_ear }}</span></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="mc_row">
            <p>Confirmation that identification documents were checked at the point of examination:
                YES
                @if($form->confirmation == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NO
                @if($form->confirmation == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row">
            <p>Hearing meets the standards in STCW Code, Section A-1/9?
                YES
                @if($form->hearing_meets == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NO
                @if($form->hearing_meets == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NOT APLICABLE
                @if($form->not_applicable == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row">
            <p>Unaided hearing satisfactory? YES
                @if($form->hearing_sat == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NO
                @if($form->hearing_sat == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row">
            <p>Visual acuity meets standards in STCW Code, Section A-1/9?
                YES
                @if($form->visual_meets == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif &nbsp; &nbsp;
                NO
                @if($form->visual_meets == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif

            </p>
        </div>
        <div class="mc_row_2">
            <p>Color vision meets standards in STCW Code, Section A-1/9?
                YES
                @if($form->color_vision == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif &nbsp; &nbsp;
                NO
                @if($form->color_vision == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif

            </p>
            <p>(the visual test it is required every six years)</p>
            <p>Date of the last colour vision test: (Day/Month/Year)<span class="md_date_value">{{ date('d/m/Y', strtotime($form->date_of_vision_test ?? '')) }}</span></p>
        </div>
        <div class="mc_row">
            <p>Are glasses or contact lenses necessary to meet the required vision standards?
                YES
                @if($form->vision_standard == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif &nbsp; &nbsp;
                NO
                @if($form->vision_standard == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif

            </p>
        </div>
        <div class="mc_row">
            <p>Able for watchkeeping?
                YES
                @if($form->watch_keep == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif &nbsp; &nbsp;
                NO
                @if($form->watch_keep == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif

            </p>
        </div>
        <div class="mc_row">
            <p>Is applicant taking any non-prescription or prescription medications?
                YES
                @if($form->medications == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif &nbsp; &nbsp;
                NO
                @if($form->medications == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif

            </p>
        </div>
        <div class="mc_row_3">
            <p>Is the seafarer free from any medical condition likely to be aggravated by service at sea or to render the seafarers unfit for such service or to endanger the health of other persons on board?
                YES
                @if($form->is_applicant_suffering == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif &nbsp; &nbsp;
                NO
                @if($form->is_applicant_suffering == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif

            </p>
        </div>
        <div class="mc_row_4">
            <p style="padding-left: 4px;">Hereby I declare that I am in Knowledge of the contents of the Physical Examination.
            </p>
            <div class="mc_sign_sec">
                <div class="mc_sign_date">
                    <div class="mc_sign_col s_float">
                        <p class="sg_value"></p>
                        <p class="sg_text">Signature of Applicant</p>
                    </div>
                    <div class="mc_sign_col s_float">
                        <p class="sg_value"></p>
                        <p class="sg_text">Name of Applicant </p>
                    </div>
                    <div class="mc_sign_col s_float">
                        <p class="sg_value"></p>
                        <p class="sg_text">Date</p>
                    </div>
                </div>
                <p class="mc_circle_text">CIRCLE APPROPIATE CHOICE: (
                    @if($form->gender == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    HE /
                    @if($form->gender == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    SHE) IS FOUND TO BE (
                    @if($form->fit == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    FIT/
                    @if($form->fit == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    NOT FIT) FOR DUTY AS A (
                    @if($form->examination_as_duty == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    MASTER/
                    @if($form->examination_as_duty == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    DECK OFFCIER/
                    @if($form->examination_as_duty == '2')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    ENGINEERING OFFICER/
                    @if($form->examination_as_duty == '3')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    RADIO OPERATOR /
                    @if($form->examination_as_duty == '4')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    RATING) (
                    @if($form->with == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    WITHOUT ANY /
                    @if($form->with == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif &nbsp; &nbsp;
                    WITH THE FOLLOWING) RESTRICTIONS:</p>
                <p>{{ $form->line1 }}</p>
                <p>{{ $form->line2 }}</p>
            </div>

        </div>
        <div class="mc_row_5 clear">
            <div class="mc_date_degree_row">
                <div class="mc_name_degree_text s_float">
                    NAME AND DEGREE OF PHYSICIAN:
                </div>
                <div class="mc_name_degree_value s_float">
                    <b>DR. SABRINA MOSTAFA, M.B.B.S (D.U)</b>
                </div>
            </div>
            <div class="mc_date_degree_row clear">
                <div class="mc_address_text s_float">
                    ADDRESS:
                </div>
                <div class="mc_address_value s_float">
                    <b>126, SK. MUJIB ROAD, CHOWMUHONI, CHITTAGONG, BANGLADESH.</b>
                </div>
            </div>
            <div class="mc_date_degree_row clear">
                <div class="mc_psysician_text s_float">
                    NAME OF PHYSICIAN'S CERTIFICATING AUTHORITY:
                </div>
                <div class="mc_psysician_value s_float">
                    <b>REGISTRATION NO.: A-68208, B.M.D.C, DHAKA, BANGLADESH</b>
                </div>
            </div>
            <div class="mc_date_degree_row clear">
                <div class="mc_date_issue_text s_float">
                    DATE OF ISSUE PHYSICIAN'S CERTIFICATE:
                </div>
                <div class="mc_date_issue_value s_float">
                    <b>08-Jun-14</b>
                </div>
            </div>

        </div>
        <div class="mc_row_6 clear">
            <div class="sign_phn s_float">
                <div class="mc_sign_phn_text s_float">
                    SIGNATURE OF PHYSICIAN:
                </div>
                <div class="mc_sign_phn_value s_float">

                </div>
            </div>
            <div class="mc_stamp s_float">
                <div class="mc_stamp_text s_float">
                    STAMP OF PHYSICIAN:
                </div>
                <div class="mc_stamp_value s_float">

                </div>
            </div>
            <div class="mc_sig_phn_date s_float">
                <div class="mc_date_phn_text s_float">
                    DATE
                </div>
                <div class="mc_date_phn_value s_float">

                </div>
            </div>
        </div>
        <div class="mc_row">
            <p>EXPIRY DATE OF CERTIFICATE:</p>
        </div>
        <div class="mc_row_7">
            <p style="text-align: center; font-size: 12px;"><i>This certificate is issued by the Panama Maritime Authority in compliance with the requirements<br>
                    of the STCW Convention, 1978, as amended and the Maritime Labour Convention, 2006.</i></p>
        </div>
        <div class="mc_row_8">

            <div class="m_rev">
                <p>F-ALM-012</p>
                <p>Rev.05</p>
                <p>Page 1 de 1</p>
                <p>Date: 13/03/2013</p>
            </div>

        </div>
    </div>
</div>
</body>
</html>
