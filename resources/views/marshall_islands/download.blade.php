<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css')  }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
</head>
<body>
<div class="mar_is_body_1 ">
    <div class="mar_is_full clear">
{{--        start of QR--}}

        <div class="visible-print text-center" style="padding: 2px;">
            @php
                use SimpleSoftwareIO\QrCode\Facades\QrCode;
                $path  = 'qrcodes/_'.now().$form->id.'.svg';
                QrCode::generate(route('smcseafarer_recrord.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
            @endphp
            <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
            <p>Scan me to return to the original page.</p>
        </div>
 {{--        End of QR--}}
        <div class="mar_is_row_1">
            <h1 class="mar_is_t1">MEDICAL EXAMINATION REPORT/CERTIFICATE</h1>
            <h2 class="mar_is_t2">MARITIME ADMINISTRATOR</h2>
            <h5 class="mar_is_t3">CONFIDENTIAL DOCUMENT</h5>
            <h2 class="mar_is_t4">REPUBLIC OF THE MARSHALL ISLANDS</h2>
        </div>
        <div class="hal_row_2">
            <div class="hol_s_name s_float">
                SURENAME : {{ $form->surname }}
            </div>
            <div class="hol_g_name s_float" style="border: none;">
                GIVEN NAMES(S) :  {{ $form->given_name }}
            </div>
        </div>

        <div class="hol_row_4">
            <div class="hol_date_of_birth s_float">
                <p>DATE OF BIRTH</p>
                @php
                   $date = \Carbon\Carbon::parse($form->dob);
                @endphp
                <div class="hot_date_d_m">MONTH<span class="hol_m_value">{{ $date->month }}</span> Day<span class="hol_d_value">{{ $date->day }}</span> YEAR<span class="hol_year_value">{{ $date->year }}</span></div>
            </div>
            <div class="hol_place_of_birth s_float">
                <p>Place OF BIRTH</p>
                <div class="hot_place_of_birth">CITY<span class="hol_c_value">{{ $form->city }}</span> COUNTRY<span class="hol_d_value">{{ $form->country }}</span></div>
            </div>
            <div class="hol_sex s_float">
                <p>SEX</p>
                <p class="hol_m_f">
                    @if($form->gender == 'Male')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    MALE
                    <span class="">
                        @if($form->gender == 'Female')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                       FEMALE
                    </span></p>
            </div>

        </div>
        <div class="hol_row_5">
            <div class="hol_exam_duty s_float">
                <p class="hol_exm_duty_title">EXAMINATION FOR DUTY AS:</p>
                <div class="m_c_col_three_row">
                    <div class="ms_position_hol s_float">
                        MASTER
                    </div>
                    <div class="ms_position_hol_value s_float">
                        @if($form->examination_as_duty == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="m_c_col_three_row">
                    <div class="ms_position_hol s_float">
                        DECK OFFICER
                    </div>
                    <div class="ms_position_hol_value s_float">
                        @if($form->examination_as_duty == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="m_c_col_three_row">
                    <div class="ms_position_hol s_float">
                        ENGINEERING OFFICER
                    </div>
                    <div class="ms_position_hol_value s_float">
                        @if($form->examination_as_duty == '2')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="m_c_col_three_row">
                    <div class="ms_position_hol s_float">
                        RADIO OPERATOR
                    </div>
                    <div class="ms_position_hol_value s_float">
                        @if($form->examination_as_duty == '3')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="m_c_col_three_row">
                    <div class="ms_position_hol s_float">
                        RATING
                    </div>
                    <div class="ms_position_hol_value s_float">
                        @if($form->examination_as_duty == '4')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
            </div>

            <div class="hol_mail s_float">
                <p>MAILING ADDRESS OF APPLICANT:</p>
                <p>{{ $form->mailing_address }}</p>
            </div>
        </div>

        <div class="hol_row_7">
            <p><span style="font-size: 12px; font-weight: bold;">MEDICAL EXAMINATION</span> (SEE LAST PAGE FOR MEDICAL REQUIREMENTS) STATE DETAILS ON REVERSE SIDE</p>
        </div>

        <div class="hol_row_8 clear">
            <div class="hol_height s_float">
                <p>HEIGHT</p>
            </div>
            <div class="hol_weight s_float">
                <p>WEIGHT</p>
            </div>
            <div class="hol_bloor s_float">
                <p>BLOOD PRESSURE</p>
            </div>
            <div class="hol_pulse s_float">
                <p>PULSE</p>
            </div>
            <div class="hol_respi s_float">
                <p>RESPIRATION</p>
            </div>
            <div class="hol_general s_float">
                <p>GENERAL APPEARANCE</p>
            </div>
        </div>
        <div class="hol_row_9">
            <div class="hol_eye s_float">
                <div class="hol_vision_row">
                    <div class="hol_vison s_float">
                        VISION
                    </div>
                    <div class="hol_ritht_eye s_float">
                        RIGHT EYE
                    </div>
                    <div class="hol_left_eye s_float">
                        LEFT EYE
                    </div>
                </div>
                <div class="hol_vision_row">
                    <div class="hol_vison s_float">
                        <div class="vision_without_glass">
                            WITHOUT GLASSES
                        </div>
                    </div>
                    <div class="hol_ritht_eye s_float">
                        <div class="vision_without_glass_right">
                            <p>{{ $form->right_eye }}</p>
                        </div>
                    </div>
                    <div class="hol_left_eye s_float">
                        <div class="vision_without_glass_left">
                            <p>{{ $form->left_eye }}</p>
                        </div>
                    </div>
                </div>
                <div class="hol_vision_row">
                    <div class="hol_vison s_float">
                        <div class="vision_without_glass">
                            WITH GLASSES
                        </div>
                    </div>
                    <div class="hol_ritht_eye s_float">
                        <div class="vision_without_glass_right">
                            <p>{{ $form->with_g_right_eye }}</p>
                        </div>
                    </div>
                    <div class="hol_left_eye s_float">
                        <div class="vision_without_glass_left">
                            <p>{{ $form->with_g_left_eye }}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="hol_hearing s_float">
                <p class="hol_hear_title">HEARING</p>
                <p class="hol_hear_value">RT. EAR <span class="hol_r_l_ear_value">{{ $form->hering_right_ear }}</span> LEFT EAR <span class="hol_r_l_ear_value">{{ $form->hering_left_ear }}</span></p>
            </div>
        </div>

        <div class="hol_row_10 MAR_IS_ROW_10">
            <p class="color_t_type"><b style="font-size: 11px;">COLOR TEST TYPE:  BOOK
                    @if($form->color_test_type == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    LANTERN
                    @if($form->color_test_type == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </b> IS COLOR TEST NORMAL?
                @if($form->test_normal == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                YES
                @if($form->test_normal == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                 NO  (IF "NO" EXPLAIN ON PAGE 2)  </p>
        </div>
        <div class="mar_is_ex_1">
            <p>ARE GLASSES OR CONTACT LENSES NECESSARY TO MEET THE REQUIRED VISION STANDARD?
                <span class="mar_is_glass">
                    @if($form->vision_standard == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    YES
                </span>
                <span class="mar_is_glass">
                    @if($form->vision_standard == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    NO
                </span>
            </p>
        </div>
        <div class="hol_row_11">
            <div class="hol_head_neck s_float">
                <p>HEAD AND NECK</p>
            </div>
            <div class="hol_heart s_float">
                <p>HEART (CARDIOVASCULAR)</p>
            </div>

        </div>

        <div class="hol_row_12 clear">
            <div class="hol_head_neck s_float">
                <p>LUNGS</p>
            </div>
            <div class="hol_heart s_float">
                <p style="font-size: 12px; font-weight: bold;">SPEECH <span style="font-size: 9px;">(DECK/NAVIGATIONAL OFFICER AND RADIO OFFICER)</span></p>
                <p style="font-size: 8px; font-weight: normal;">IS SPEECH UNIMPAIRED FOR NORMAL VOICE COMMUNICATION?</p>
            </div>

        </div>

        <div class="hol_row_13">
            <p class="hol_ext">EXTREMITIES:</p>
            <p class="hol_ext_value">UPPERt: <span class="hol_ext_u_value">{{ $form->extremities_upper }}</span> LOWER<span class="hol_ext_u_value">{{ $form->extremities_lower }}</span></p>
        </div>

        <div class="hol_row_14">
            IS APPLICANT VACCINATED IN ACCORDANCE WITH WHO REQUIREMENTS?
            <span class="hol_app_y_n">
                @if($form->vaccinated_requirement == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                YES
            </span>
            <span class="hol_app_y_n">
                @if($form->vaccinated_requirement == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NO
            </span>
        </div>
        <div class="hol_row_15">
            <p>IS APPLICANT SUFFERING FROM ANY DISEASE LIKELY TO BE AGGRAVATED BY WORKING ABOARD A VESSEL, OR TO RENDER HIM/HER UNFIT FOR SERVICE AT SEA OR LIKELY TO ENDANGER THE HEALTH OF OTHER PERSONS ON BOARD?
                <span class="mar_is_glass">
                    YES
                    @if($form->is_applicant_suffering == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </span>
                <span class="mar_is_glass">
                    NO
                    @if($form->is_applicant_suffering == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </span>
            </p>
            <p>IF YES, PLEASE ENTER EXPLANATION IN THE SECTION AT THE BOTTOM OF ON PAGE 2</p>
        </div>
        <div class="hol_row_14">
            IS APPLICANT TAKING ANY NON-PRESCRIPTION OR PRESCRIPTION MEDICATIONS?
            <span class="hol_app_y_n">
                @if($form->is_prescirption_medication == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                YES
            </span>
            <span class="hol_app_y_n">
                @if($form->is_prescirption_medication == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NO
            </span>
        </div>
        <div class="mar_is_ph_ex_9">
            <div class="ph_exapir_date_row">
                <div class="ph_ex_date_col s_float">
                    <p>SIGNATURE OF APPLICANT </p>
                </div>
{{--                need set the plot to put value of date--}}
                <div class="ph_ex_date_col s_float" style="margin-left: 20px;">
                    <p>DATE OF EXAM : {{ date('d/m/Y', strtotime($form->doe ?? '')) }} </p>
                </div>
                {{--                need set the plot to put value of date--}}
                <div class="ph_ex_date_col s_float" style="margin-left: 27px;">
                    <p>EXPIRY DATE : {{ date('d/m/Y', strtotime($form->doex ?? '')) }}</p>
                </div>
            </div>
            <div class="ph_name_of_app clear">
                <div class="ph_cert s_float">
                    <p>THIS IS TO CERTIFY THAT A PHYSICAL EXAMINATION WAS GIVEN TO:</p>
                </div>
                <div class="ph_cert_value">
                    <p style="height: 10px;"></p>
                    <p class="ph_name_of_app_border">(NAME OF APPLICANT)</p>
                </div>
            </div>
            <p class="ph_app_fit_unfit mar_is_fit_unfit" style="font-size: 12px;">SEAFARER IS FOUND TO BE
                @if($form->fit == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                FIT /
                @if($form->fit == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NOT FIT FOR DUTY AS A
                @if($form->examination_as_duty == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                MASTER /
                @if($form->examination_as_duty == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                DECK OFFICER /
                @if($form->examination_as_duty == '2')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                ENGINEERING OFFICER /
                @if($form->examination_as_duty == '2')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                RADIO OFFICER /
                @if($form->examination_as_duty == '3')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                RATING /
                @if($form->cook == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                CHIEF COOK /
                @if($form->cook == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                COOK
                @if($form->with == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                WITHOUT ANY RESTRICTIONS /
                @if($form->with == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                WITH THE FOLLOWING
                RESTRICTIONS: {{ $form->restriction }}

            <div class="ph_ex_10 clear">
                <div class="ph_name_degree_row mar_is_name_degree">
                    NAME AND DEGREE OF PHYSICIAN <span class="name_digree_value">DR. SABRINA MOSTAFA, M.B.B.S (D.U)</span>
                </div>
                <div class="ph_name_address_row mar_is_address">
                    ADDRESS <span class="ph_address_value">126, SK. MUJIB ROAD, CHOWMUHONI, CHITTAGONG, BANGLADESH.</span>
                </div>
                <div class="ph_cer_author mar_is_author">
                    NAME OF PHYSICIAN'S CERTIFICATING AUTHORITY<span class="ph_cer_author_value">REGISTRATION NO.: A-68208, B.M.D.C, DHAKA, BANGLADESH</span>
                </div>
                <div class="ph_issue_date_row mar_is_issue_date">
                    DATE OF ISSUE OF PHYSICIAN'S CERTIFICATE <span class="ph_issue_date_value">08-Jun-14</span>
                </div>
                <div class="ph_phy_sigin_exame_date mar_is_exam_date">
                    SIGNATURE OF PHYSICIAN <span class="ph_phy_sign_value mar_is_sign_value"></span>  <span class="ph_phy_exam_date_value mar_is_exam_date_value"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=" mar_is_body">
    <div class="hal_full_p3 mar_is_full_p3 clear">
        <div class="hal_p_sec_1">
            <div class="hol_p_3_med_req">
                <h6 class="hol_med_req_title">MEDICAL REQUIREMENTS</h6>
                <p class="hol_med_req_details">All applicants for an officer certificate, Seafarer's Identification and Record Book or certification of special qualifications shall be required to havea medical examination reported on this Medical Form completed by a certificated physician. The completed medical form must accompany the application for officer’s certificate, application for Seafarer's Identification and Record Book, or application for certification  o fspecial qualifications. This medical examination must be carried out within the 24 months immediately preceding application for an officer certificate,certification of special qualifications or a Seafarer’s Identification and Record Book. The examination shall be conducted in accordance withRMI MG-7-47-1. Such proof of examination must establish that the applicant is in satisfactory physical and mentalcondition for the specific dutyassignment undertaken and is generally in possession of all body faculties necessary in fulfilling the requirements of the seafaring profession.
                </p>

                <div class="hol_p_3_row_2">
                    <p>In conducting the examination, the certified physician should, where appropriate, examine the seafarer’s previous medical records (including vaccinations) and information on occupational history, noting any diseases, including alcohol or drug-related problems and/or injuries. In addition, the following minimum requirements shall apply:</p>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(a)</span> Hearing</p>
                    <ul class="hol_p_3_3_ul">
                        <li>All applicants must have hearing unimpaired for normal sounds and be capable of hearing a whispered voice in better ear at 15 feet (4.57 m) and in poorer ear at 5 feet (1.52 m).</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(b)</span> Eyesight</p>
                    <ul class="hol_p_3_3_ul">
                        <li>Deck officer applicants must have (either with or without glasses) at least 20/20(1.00) vision in one eye and at least 20/40 (0.50) in the other. Applicants for deck officer and deck ratings who will serve on vessels of 500 gross tons or more must have normal color perception that complies with C.I.E. Standard 1; those serving on vessels less than 500 gross tons must comply with C.I.E. Standards 1 or 2.</li>
                        <li>Engineer and radio officer applicants must have (either with or without glasses) at least 20/30 (0.63) vision in one eye and at least 20/50 (0.40) in the other. Applicants for engineering officer or rating and for radio operator must comply with C.I.E. Standards 1, 2, or 3. Engineer and radio officer applicants must also be able to perceive the colors red, yellow and green.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(c)</span> Dental</p>
                    <ul class="hol_p_3_3_ul">
                        <li>Seafarers must be free from infections of the mouth cavity or gums.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(d)</span> Blood Pressure</p>
                    <ul class="hol_p_3_3_ul">
                        <li>An applicant's blood pressure must fall within an average range, taking age into consideration.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(e)</span> Voice</p>
                    <ul class="hol_p_3_3_ul">
                        <li>Deck/Navigational officer applicants and Radio officer applicants must have speech which is unimpaired for normal voice communication.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(f)</span> Vaccinations</p>
                    <ul class="hol_p_3_3_ul">
                        <li>All applicants should be vaccinated according to the recommendations provided in the WHO publication, International Travel and Health, Vaccination Requirements and Health Advice, and should be given advice by the certified physician on immunizations. If new vaccinations are given, these should be recorded.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(g)</span> Diseases or Conditions</p>
                    <ul class="hol_p_3_3_ul">
                        <li>Applicants afflicted with any of the following diseases or conditions shall be disqualified: epilepsy, insanity, senility, alcoholism, tuberculosis, acute venereal disease or neurosyphilis, AIDS, and/or the use of narcotics.</li>
                    </ul>
                </div>

                <div class="hol_p_3_no">
                    <p><span class="no_a">(h)</span> Physical Requirements</p>
                    <ul class="hol_p_3_3_ul">
                        <li>Applicants for able seafarer, bosun, GP-1, ordinary seafarer and junior ordinary seafarer must meet the physical requirements for a deck/navigational officer's certificate.</li>
                        <li>Applicants for fire/watertender, oiler/motor, pump technician, electrician, wiper, tanker rating and survival craft/rescue boat crewmember must meet the physical requirements for an engineer officer's certific</li>
                    </ul>
                </div>


            </div>

        </div>
    </div>
    <div class="hal_p_sec_2 mar_is_im_note">
        <h6 class="hol_p2_not_title">IMPORTANT NOTE:</h6>
        <p>A copy of the MI-105M must accompany the application. The applicant must retain the original of the MI-105M as evidence of physical qualification while serving on board a vessel.</p>
        <p>An applicant who has been refused a medical certificate or has had a limitation imposed on his/her ability to work, shall be given the opportunity to have an additional examination by another medical practitioner or medical referee who is independent of the shipowner or of any organization of shipowners or seafarers.</p>
        <p>Medical examination reports shall be marked as and remain confidential with the applicant having the right of a copy to his/her report. The medical examination report shall be used only for determining the fitness of the seafarer for work and enhancing health care.</p>
    </div>
    <div class="hal_p_sec_3 mar_is_be_compleate mar_is_extra">
        <h6 class="hal_p_3_title_2">DETAILS OF MEDICAL EXAMINATION:</h6>
        <p>(To be completed by examining physician; alternatively, the examining physician may attach a form similar or identical to the model provided in Appendix 1 of RMI MG-7-47-1).)</p>

        <div class="mar_is_phy_exam_row">
            <div class="mar_is_phy_exam_col s_float">
                <div class="mar_is_exam_col_row"><span class="mar_is_col_text">Complete Physical Examination</span> <span class="mar_is_colon">:</span> <span class="mar_is_exam_value"></span></div>
                <div class="mar_is_exam_col_row"><span class="mar_is_col_text">Blood For Routine Examination</span> <span class="mar_is_colon">:</span> <span class="mar_is_exam_value"></span></div>
                <div class="mar_is_exam_col_row"><span class="mar_is_col_text">Electro Cardiogram Test</span> <span class="mar_is_colon">:</span> <span class="mar_is_exam_value"></span></div>
            </div>
            <div class="mar_is_phy_exam_col s_float">
                <div class="mar_is_exam_col_row"><span class="mar_is_col_text">Urine For Routine Examination </span> <span class="mar_is_colon">:</span> <span class="mar_is_exam_value"></span></div>
                <div class="mar_is_exam_col_row"><span class="mar_is_col_text">X-Ray Chest PA View</span> <span class="mar_is_colon">:</span> <span class="mar_is_exam_value"></span></div>
                <div class="mar_is_exam_col_row"><span class="mar_is_col_text">Eye Examination For V/A & C/V</span> <span class="mar_is_colon">:</span> <span class="mar_is_exam_value"></span></div>
            </div>
        </div>
    </div>
    <div class="mar_is_phy_exam_row_l">
        <div class="mar_is_phy_exam_col s_float">
            <p style="text-align: left;">Rev. Jul/2017 </p>
        </div>
        <div class="mar_is_phy_exam_col s_float">
            <p style="text-align: right;">MI-105M</p>
        </div>
    </div>
</div>
</body>
</html>
