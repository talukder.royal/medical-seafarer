@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <div class="row">
        <div class="col-lg-12 text-center">
            <div>
                SEAFARER'S  MEDICAL EXAMINATION REPORT/CERTIFICATE
            </div>
            <div>
                CONFIDENTIAL DOCUMENT
            </div>
        </div>
    </div>
    <form class="form-control" action="{{ route('marshall-island.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="surname"><span class="text-danger">*</span>SURNAME</label>
                    <input type="text" class="form-control" name="surname" id="surname" value="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="given_name"><span class="text-danger">*</span>GIVEN NAME(s)</label>
                    <input type="text" class="form-control" name="given_name" id="given_name" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="dob"><span class="text-danger">*</span>Date of birth:</label>
                    <input type="date" class="form-control" name="dob" id="dob" value="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="d-flex justify-content-between">
                        <div class="">
                            <label class="form-label" for="city"><span class="text-danger">*</span>Place of birth: City</label>
                            <input type="text" class="form-control" name="city" id="city" value="">
                        </div>
                        <div class="ml-2">
                            <label class="form-label" for=""><span class="text-danger">*</span>Country</label>
                            <input type="text" class="form-control" name="country" id="country" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="gender"><span class="text-danger">*</span>Sex:</label>
                    <select class="form-control" name="gender" id="gender">
                        <option>Select</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Others">Others</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-class">Examination for duty as:</label>
                </div>
                <div class="form-group" style="padding-left: 100px;">
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="master">Master</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="master" value="0">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="deck_officer">Deck officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="deck_officer" value="1">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="engineering_officer">Engineering officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="engineering_officer" value="2">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="radio_officer">Radio officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="radio_officer" value="3">
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="rating">Rating</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="rating" value="4">
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="mailing_address">Mailing address of applicant:</label>
                    <textarea  class="form-control" name="mailing_address" id="mailing_address"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <hr>
                <h6 class="text-center" style="color: #95a2af; text-transform: uppercase">Medical examination (see last page for medical requirements) state details on reverse side</h6>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="height">Height</label>
                    <input  class="form-control" type="text" name="height" id="height" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="weight">Weight</label>
                    <input  class="form-control" type="text" name="weight" id="weight" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="blood_pressure">Blood pressure</label>
                    <input  class="form-control" type="text" name="blood_pressure" id="blood_pressure" value="">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="pulse">Pulse</label>
                    <input  class="form-control" type="text" name="pulse" id="pulse" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="respiration">Respiration</label>
                    <input  class="form-control" type="text" name="respiration" id="respiration" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="general_appearance">General appearance</label>
                    <input  class="form-control" type="text" name="general_appearance" id="general_appearance" value="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="">Vision(Without glasses):</label>
                    <br>
                    <label class="form-label" for="right_eye">Right eye:</label>
                    <input class="form-control" type="text" name="right_eye" id="right_eye" value="">
                    <label class="form-label" for="left_eye">Left eye:</label>
                    <input class="form-control" type="text" name="left_eye" id="left_eye" value="">
                </div>

                <div class="form-group">
                    <label class="form-label" for="">Vision(With glasses):</label>
                    <br>
                    <label class="form-label" for="with_g_right_eye">Right eye:</label>
                    <input class="form-control" type="text" name="with_g_right_eye" id="with_g_right_eye" value="">
                    <label class="form-label" for="with_g_left_eye">Left eye:</label>
                    <input class="form-control" type="text" name="with_g_left_eye" id="with_g_left_eye" value="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="">Hearing:</label>
                    <br>
                    <label class="form-label" for="hering_right_ear">Right ear:</label>
                    <input class="form-control" type="text" name="hering_right_ear" id="hering_right_ear" value="">
                    <label class="form-label" for="hering_left_ear">Left ear:</label>
                    <input class="form-control" type="text" name="hering_left_ear" id="hering_left_ear" value="">
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_test_type">Color test type:</label>
                    <label class="form-label" for="book">Book</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_test_type" id="book" value="1">
                    <label class="form-label" for="lanter">Lantern</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_test_type" id="lanter" value="0">
                    <label class="form-label" for="test_normal">Check if color test is normal:</label>
                    <label class="form-label" for="test_normal_yes">Yes</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="test_normal_yes" value="1">
                    <label class="form-label" for="test_normal_no">No</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="test_normal" id="test_normal_no" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <label class="form-label" for="vision_standard">Are glasses or contact lenses necessary to meet the required vision standard?</label>
            </div>
            <div class="col-lg-4">
                <label class="form-label" for="vision_standard_yes">Yes</label>
                <input type="checkbox" style="width: 25px; height: 20px; margin-right:40px; " name="vision_standard" id="vision_standard_yes"  value="1">
                <label class="form-label" for="vision_standard_no">No</label>
                <input type="checkbox" style="width: 25px; height: 20px;" name="vision_standard" value="0">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="head_neck">Head and neck</label>
                    <input class="form-control" type="text" name="head_neck"  id="head_neck"  value="" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="lungs">Lungs</label>
                    <input class="form-control" type="text" name="lungs" id="lungs"  value="" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="extremities_upper">Extremities(upper):</label>
                    <input class="form-control" type="text" name="extremities_upper" id="extremities_upper" value="" >
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" id="heart">Heart(cardiovascular)</label>
                    <input class="form-control" type="text" name="heart" id="heart" value="" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="speech">Speech(Deck/navigational officer and radio officer)Is speech unimpaired for normal voice communication?</label>
                    <input class="form-control" type="text" name="speech" id="speech" value="" >
                </div>
                <div class="form-group">
                    <label class="form-label" for="extremities_lower">Extremities(Lower):</label>
                    <input class="form-control" type="text" name="extremities_lower" id="extremities_lower" value="" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="vaccinated_requirement">Is applicant vaccinated in accordance with who requirements?</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="vaccinated_requirement_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="vaccinated_requirement" id="vaccinated_requirement_yes" value="1">
                    <label class="form-label" for="vaccinated_requirement_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="vaccinated_requirement" id="vaccinated_requirement_no" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="is_applicant_suffering">Is applicant suffering from any disease likely to
                        be aggravated by working aboard a vessel, or to render him/
                        her unfit for service at sea or likely to endanger the health
                        of of other persons on board?
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="is_applicant_suffering_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="is_applicant_suffering" id="is_applicant_suffering_yes" value="1">
                    <label class="form-label" for="is_applicant_suffering_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="is_applicant_suffering" id="is_applicant_suffering_no" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="is_prescirption_medication">Is applicant taking any non-prescription or prescription medications?</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="is_prescirption_medication_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="is_prescirption_medication" id="is_prescirption_medication_yes" value="1">
                    <label class="form-label" for="is_prescirption_medication_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="is_prescirption_medication" id="is_prescirption_medication_yes" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="communicable_disease">This applicant is certified free of communicable disease?</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="communicable_disease_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="communicable_disease" id="communicable_disease_yes" value="1">
                    <label class="form-label" for="communicable_disease_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="communicable_disease" id="communicable_disease_no" value="0">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="restriction">
                        Seafarer is found to be (Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="1">/ not Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="0">) for duty as a <input style="width: 25px; height: 20px;" type="checkbox" name="cook" value="1"> Chief Cook/ <input style="width: 25px; height: 20px;" type="checkbox" name="cook" value="0"> Cook (without any<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="0"> / with the following<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="1">) restrictions:
                    </label>
                    <input type="text" name="restriction" class="form-control" id="restriction">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="doe">Date of examination:</label>
                    <input class="form-control" type="date" name="doe" id="doe">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="doex">Date of expiry:</label>
                    <input class="form-control" type="date" name="doex" id="doex">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>
    </form>
@endsection
