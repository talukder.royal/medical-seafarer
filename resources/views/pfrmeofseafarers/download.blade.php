<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/milaha.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{{ asset('public/js/style.js') }}"></script>
</head>
<body>
<div class="format_rec_all " style="border: none;">
    <div class="all_in format_rec_all_in">
        <div class="visible-print text-center" style="padding: 2px;">
            @php
                use SimpleSoftwareIO\QrCode\Facades\QrCode;
                $path  = 'qrcodes/_'.now().$form->id.'.svg';
                QrCode::generate(route('pfrmeofseafarer.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
            @endphp
            <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
            <p>Scan me to return to the original page.</p>
        </div>
        <div class="row_four_rec format_rec  clear">
            <img class="for_rec_img" src="img/m.PNG">
            <p class="format_rec sea_format_title">FORMAT FOR RECORDING MEDICAL EXAMINATIONS<br> OF SEAFARERS</p>
            <div class="format_rec sea_format_field">
                <div class="format_rec sea_format_field_row">
                    <div class="format_rec sea_name s_float">
                        Name (last,  first, middle):
                    </div>
                    <div class="format_rec sea_dr_name_value s_float">
                        <p>{{ $form->seafarers_name }}</p>
                    </div>
                </div>
                <div class="format_rec sea_format_field_row">
                    <div class="format_rec sea_date_of_birth s_float">
                        <div class="format_rec sea_date_of_birth_text s_float">
                            Date of birth (day/month/year):
                        </div>
                        <div class="format_rec sea_date_of_birth_value s_float">
                            <p>{{ date('d/m/Y', strtotime($form->date_of_birth ?? '')) }}</p>
                        </div>
                    </div>

                    <div class="format_rec sea_gender s_float">
                        <div class="format_rec sea_gender_text s_float">
                            Sex:
                        </div>
                        <div class="format_rec sea_gender_text_value s_float">
                            @if($form->gender == 'Male')
                                 <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                            Male
                            @if($form->gender == 'Female')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                            Female
                        </div>
                    </div>
                </div>
                <div class="format_rec sea_format_field_row">
                    <div class="format_rec sea_home_address s_float">
                        Home Address:
                    </div>
                    <div class="format_rec sea_home_address_value s_float">
                        <p>{{ $form->home_address }}</p>
                    </div>
                </div>
                <div class="format_rec sea_format_field_row">
                    <div class="format_rec sea_home_passport s_float">
                        Passport No /seafarer's book No:
                    </div>
                    <div class="format_rec sea_passport_value s_float">
                        <p>{{ $form->nationality }}</p>
                    </div>
                </div>
                <div class="format_rec sea_format_field_row">
                    <div class="format_rec sea_home_dep s_float">
                        Department:(decck/engine/radio/food handling/other):
                    </div>
                    <div class="format_rec sea_dep_value s_float">
                        <p>{{ $form->dept }}</p>
                    </div>
                </div>

                <div class="format_rec sea_format_field_row">
                    <div class="format_rec sea_routine s_float">
                        Routine and emergenccy, duties (if known):
                    </div>
                    <div class="format_rec sea_rank_routine s_float">
                        <p>{{ $form->routine_duties }}</p>
                    </div>
                </div>
                <div class="format_rec sea_format_field_row">
                    <div class="format_rec sea_type s_float">
                        Type of ship (e.g container, Tanker, passenger,fishing):
                    </div>
                    <div class="format_rec sea_type_value s_float">
                        <p>{{$form->type_of_ship}}</p>
                    </div>
                </div>
                <div class="format_rec sea_format_field_row">
                    <div class="format_rec sea_trade s_float">
                        Trade area (e.g coastal, tropical, worldwide):
                    </div>
                    <div class="format_rec sea_trade_value s_float text-capitalize p-0 m-0">
                        <p class="p-0 m-0">
                            @if($form->trading_area == 'coastal')
                                coastal
                            @elseif($form->trading_area == 'worldwide')
                                 worldwide
                            @else
                                tropical
                            @endif
                        </p>
                    </div>
                </div>


            </div>

        </div>
        <!-- row_four end -->
        <div class="row_five">
            <p class="exam_personal format_rec_exam_personal ">EXAMINEE ́S PERSONAL DECLARATION (ASSISTANCE SHOULD BE OFFERED BY MEDICAL STAFF)</p>
            <p style="margin-top: 10px;">Have you ever had any of the following conditions?</p>
            <div class="format_rec sea_med_table_one">
                <div class="format_rec sea_table_one_row">
                    <div class="format_rec sea_table_one_p_1 s_float">
                        <div class=" format_rec sea_table_2_row ">
                            <div class="format_rec sea_tab_2_num s_float">

                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Condition
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                Yes
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                No
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row for_rec_ex_19">
                            <div class="format_rec sea_tab_2_num s_float for_rec_ex_19">
                                1
                            </div>
                            <div class="format_rec sea_tab_2_con s_float for_rec_ex_19">
                                Eye / vision problem
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float for_rec_ex_19">
                                @if($form->eye_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float for_rec_ex_19">
                                @if($form->eye_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                2
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                High blood pressure
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->high_blood_pressure == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->high_blood_pressure == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                3
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Heart/vascular disease
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->heart_disease == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->heart_disease == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                4
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Heart surgery
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->heart_surgery == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->heart_surgery == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                5
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Varicose veins/piles
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->varicose_veins == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->varicose_veins == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                6
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Asthma/bronchitis
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->asthma_bronchitis == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->asthma_bronchitis == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                7
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Blood disorder
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->blood_disorder == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->blood_disorder == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                8
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Diabetes Mellitsus
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->diabetes == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->diabetes == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                9
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Thyroid problems
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->thyroid_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->thyroid_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                10
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Digestive disorder
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->digestive_disorder == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->digestive_disorder == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                11
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Kidney problems
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->kidney_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->kidney_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row for_rec_ex_19">
                            <div class="for_rec_ex_19 format_rec sea_tab_2_num s_float">
                                12
                            </div>
                            <div class="format_rec sea_tab_2_con s_float for_rec_ex_19">
                                Skin problems
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float for_rec_ex_19">
                                @if($form->skin_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float for_rec_ex_19">
                                @if($form->skin_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                13
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Allergies
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->allergies == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->allergies == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                14
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Infectious/contagius diseases
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->infectious_diseases == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->infectious_diseases == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                15
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Hernia
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->hernia == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->hernia == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                16
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Genital disorders
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->genital_disorder == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->genital_disorder == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                17
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Pregnancy
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->pregnancy == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->pregnancy == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                18
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Sleep problem
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->sleep_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->sleep_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="format_rec sea_table_one_p_2 s_float">
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">

                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Condition
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                Yes
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                No
                            </div>
                        </div>
                        <div class=" for_rec_ex_19 format_rec sea_table_2_row ">
                            <div class="for_red_ex_19 format_rec sea_tab_2_num s_float">
                                19
                            </div>
                            <div class=" for_rec_ex_19 format_rec sea_tab_2_con s_float">
                                Do you smoke,use alcohol or drugs?
                            </div>
                            <div class=" for_rec_ex_19 format_rec sea_tab_2_yes s_float">
                                @if($form->smoke_alcohol_drug == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec_ex_19format_rec sea_tab_2_no s_float">
                                @if($form->smoke_alcohol_drug == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                20
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Operation/surgery
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->operation == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->operation == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                21
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Epilepsy/ seizures
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->epilesy_seizures == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->epilesy_seizures == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                22
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Dizziness/fainting
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->dizziness == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->dizziness == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                23
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Loss of consciousness
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->loss_of_consciousness == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->loss_of_consciousness == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                24
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Psychiatric problems
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->psychiatric_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->psychiatric_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                25
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Loss of consciousness
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->loss_of_consciousness == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->loss_of_consciousness == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                26
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Attempted suicide
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->attempted_suicide == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->attempted_suicide == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                27
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Loss of memory
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->loss_of_memory == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->loss_of_memory == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                28
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Balance problems
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->balance_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->balance_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                29
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Severe headaches
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->severe_headache == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->severe_headache == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row for_rec_ex_19">
                            <div class="format_rec sea_tab_2_num s_float for_rec_ex_19">
                                30
                            </div>
                            <div class="format_rec sea_tab_2_con s_float for_rec_ex_19">
                                Ear (hearing/ tinnitus) nose/throat problems
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float for_rec_ex_19">
                                @if($form->hearing_throat_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float for_rec_ex_19">
                                @if($form->hearing_throat_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                31
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Restricted mobility
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->restricted_mobility == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->restricted_mobility == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                32
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Back or joint problems
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->joint_problem == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->joint_problem == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                33
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Amputation
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->amputation == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->amputation == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">
                                34
                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                                Fractures/dislocation
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                @if($form->fracture == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                @if($form->fracture == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">

                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                <i class="far fa-square"></i>
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                <i class="far fa-check-square"></i>
                            </div>
                        </div>
                        <div class="format_rec sea_table_2_row">
                            <div class="format_rec sea_tab_2_num s_float">

                            </div>
                            <div class="format_rec sea_tab_2_con s_float">
                            </div>
                            <div class="format_rec sea_tab_2_yes s_float">
                                <i class="far fa-square"></i>
                            </div>
                            <div class="format_rec sea_tab_2_no s_float">
                                <i class="far fa-check-square"></i>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
        <div class="mar_rec-row_six">
            <p>If any of the above questions were answered “yes”, please give details</p>
            <p>{{ $form->provide_details_1 }}</p>
        </div>
        <div class="mar_rec_foot">
            <p>F-ALM-011</p>
            <p>Rev. 03</p>
            <p>Page 1 de 4</p>
            <p>Date: 13/03/2013.</p>
        </div>
    </div>
</div>

<div class="format_rec_all " style="border: none;">
    <div class="all_in format_rec_all_in">
        <div class="row_three clear">
            <div class="format_rec sea_med_table_two">
                <div class="format_rec sea_med_table_two_row">
                    <div class="format_rec sea_med_2_sr s_float">

                    </div>
                    <div class="format_rec sea_med_2_add s_float">
                        <b>Additional question</b>
                    </div>
                    <div class="format_rec sea_med_2_yes s_float">
                        Yes
                    </div>
                    <div class="format_rec sea_med_2_no s_float">
                        NO
                    </div>

                </div>
                <div class="format_rec sea_med_table_two_row">
                    <div class="format_rec sea_med_2_sr s_float">
                        35
                    </div>
                    <div class="format_rec sea_med_2_add s_float">
                        Have anyy ever been signed of as sick or repartiated form a ship?
                    </div>
                    <div class="format_rec sea_med_2_yes s_float">
                        @if($form->signed_off_sick == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="format_rec sea_med_2_no s_float">
                        @if($form->signed_off_sick == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="format_rec sea_med_table_two_row">
                    <div class="format_rec sea_med_2_sr s_float">
                        36
                    </div>
                    <div class="format_rec sea_med_2_add s_float">
                        Have you ever been hospitalized?
                    </div>
                    <div class="format_rec sea_med_2_yes s_float">
                        @if($form->hospitalized == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="format_rec sea_med_2_no s_float">
                        @if($form->hospitalized == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="format_rec sea_med_table_two_row">
                    <div class="format_rec sea_med_2_sr s_float">
                        37
                    </div>
                    <div class="format_rec sea_med_2_add s_float">
                        Have you ever been declared unfit for sea duty?
                    </div>
                    <div class="format_rec sea_med_2_yes s_float">
                        @if($form->declared_for_sea_duty == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="format_rec sea_med_2_no s_float">
                        @if($form->declared_for_sea_duty == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="format_rec sea_med_table_two_row">
                    <div class="format_rec sea_med_2_sr s_float">
                        38
                    </div>
                    <div class="format_rec sea_med_2_add s_float">
                        Has your medical certificate ever been restricted or revoked?
                    </div>
                    <div class="format_rec sea_med_2_yes s_float">
                        @if($form->medical_certificate_revoked == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="format_rec sea_med_2_no s_float">
                        @if($form->medical_certificate_revoked == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="format_rec sea_med_table_two_row">
                    <div class="format_rec sea_med_2_sr s_float">
                        39
                    </div>
                    <div class="format_rec sea_med_2_add s_float">
                        Are you aware that you have any medical problems, diseases or illness?
                    </div>
                    <div class="format_rec sea_med_2_yes s_float">
                        @if($form->medical_problems == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="format_rec sea_med_2_no s_float">
                        @if($form->medical_problems == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="format_rec sea_med_table_two_row">
                    <div class="format_rec sea_med_2_sr s_float">
                        40
                    </div>
                    <div class="format_rec sea_med_2_add s_float">
                        Do you feel healthy and fit to perform the duties of your designed position/occupation?
                    </div>
                    <div class="format_rec sea_med_2_yes s_float">
                        @if($form->feel_healthy == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="format_rec sea_med_2_no s_float">
                        @if($form->feel_healthy == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
                <div class="format_rec sea_med_table_two_row">
                    <div class="format_rec sea_med_2_sr s_float">
                        41
                    </div>
                    <div class="format_rec sea_med_2_add s_float">
                        Are you allergic to any medications?
                    </div>
                    <div class="format_rec sea_med_2_yes s_float">
                        @if($form->allergic_medication == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="format_rec sea_med_2_no s_float">
                        @if($form->allergic_medication == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>

            </div>
        </div>

        <div class="row_four">
            <p class="format_rec sea_comment"><b>Comments:</b> <br>
                {{ $form->comments }}
            </p>
        </div>

        <div class="row_five">
            <div class="format_rec sea_med_table_two_2 clear">
                <div class="format_rec sea_med_table_two_row">
                    <div class="format_rec sea_med_2_sr s_float">

                    </div>
                    <div class="format_rec sea_med_2_add s_float">
                        <b>Additional question</b>
                    </div>
                    <div class="format_rec sea_med_2_yes s_float">
                        Yes
                    </div>
                    <div class="format_rec sea_med_2_no s_float">
                        NO
                    </div>

                </div>
                <div class="format_rec sea_med_table_two_row" style="border: none;">
                    <div class="format_rec sea_med_2_sr s_float">
                        42
                    </div>
                    <div class="format_rec sea_med_2_add s_float">
                        Are you taking any non-prescription or prescription medications?
                    </div>
                    <div class="format_rec sea_med_2_yes s_float">
                        @if($form->prescription_medication == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="format_rec sea_med_2_no s_float">
                        @if($form->prescription_medication == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>

                </div>
            </div>
        </div>

        <div class="row_six clear">
            <p class="format_rec sea_med_s">if yes, please list hte medicaltions taken and the purpose (s) and dosage (s)</p>
            <p>{{ $form->provide_details }}</p>
        </div>
        <div class="row_one">
            <div class="format_rec sea_p_3_fields">
                <p class="format_rec sea_field_text">I hereby certify that the personal declaration above is a true statement to the best of my knowledge.</p>
                <div class="format_rec sea_p_f_row">
                    <div class="format_rec sea_p_sign s_float">
                        Singniture of examine:
                    </div>
                    <div class="format_rec sea_p_sign_value s_float">
                        <p></p>
                    </div>
                </div>

                <div class="format_rec sea_p_f_row_2 clear">
                    <div class="format_rec sea_p_date s_float">
                        Date (day/month/year:
                    </div>
                    <div class="format_rec sea_p_date_value s_float">
                        <p></p>
                    </div>
                </div>

                <div class="format_rec sea_p_f_row_3 clear">
                    <div class="format_rec sea_p_witness s_float">
                        Witnessed by:(Signaure)
                    </div>
                    <div class="format_rec sea_p_witeness_value s_float">
                        <p></p>
                    </div>
                </div>
                <div class="format_rec sea_p_f_row_4 clear">
                    <div class="format_rec sea_p_name_print s_float">
                        Name (Typed or printed):
                    </div>
                    <div class="format_rec sea_p__name_print_value s_float">
                        <p></p>
                    </div>
                </div>
                <div class="format_rec sea_p_f_row_5 clear">
                    <p class="format_rec sea_dr_text">I hereby authorize the release of all my previous medical records from any health professionals,<BR> health, institutions and public authorities to <span class="format_rec sea_dr_det"></span> (the approved medical practitioner).</p>
                </div>
                <div class="format_rec sea_p_f_row">
                    <div class="format_rec sea_p_sign s_float">
                        Singniture of examine:
                    </div>
                    <div class="format_rec sea_p_sign_value s_float">
                        <p></p>
                    </div>
                </div>

                <div class="format_rec sea_p_f_row_2 clear">
                    <div class="format_rec sea_p_date s_float">
                        Date (day/month/year:
                    </div>
                    <div class="format_rec sea_p_date_value s_float">
                        <p></p>
                    </div>
                </div>

                <div class="format_rec sea_p_f_row_3 clear">
                    <div class="format_rec sea_p_witness s_float">
                        Witnessed by:(Signaure)
                    </div>
                    <div class="format_rec sea_p_witeness_value s_float">
                        <p></p>
                    </div>
                </div>
                <div class="format_rec sea_p_f_row_4 clear">
                    <div class="format_rec sea_p_name_print s_float">
                        Name (Typed or printed):
                    </div>
                    <div class="format_rec sea_p__name_print_value s_float">
                        <p></p>
                    </div>
                </div>
                <div class="format_rec sea_date_contact">
                    <div class="format_rec sea_date_contact_text s_float">
                        Date and ccontact details for previous medial exainatin(if known):
                    </div>
                    <div class="format_rec sea_date_contact_value s_float">

                    </div>
                </div>
            </div>
        </div>
        <div class="mar_rec_foot">
            <p>F-ALM-011</p>
            <p>Rev. 03</p>
            <p>Page 1 de 4</p>
            <p>Date: 13/03/2013.</p>
        </div>
    </div>
</div>
<div class="for_rec format_rec_all " style="border: none;">
    <div class="for_rec all_in format_rec_all_in_3">
        <div class="for_rec row_two sea_3_two clear">
            <p class="for_rec format_rec sea_medial_exam">MEDICAL EXAMINATION</p>
            <p><b>Sight</b></p>
            <P>Use of glasses or contact lenses:
                @if($form->contact_lenses == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                Yes
                /
                @if($form->contact_lenses == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                No
                (if yes, specify which type and for what purpose)</P>
            <div class="for_rec for_rec sea_table_three sea_table_three_for">
                <div class="fr visual_acu_table s_float">
                    <div class="visual_acu_row">
                        <p style="font-weight: bold">Visual acuity</p>
                    </div>
                    <div class="visual_acu_row">
                        <div class="unaid_null s_float">

                        </div>
                        <div class="unaid s_float">
                            Unaided
                        </div>
                        <div class="aid s_float">
                            Aided
                        </div>
                    </div>

                    <div class="visual_acu_row_3">
                        <div class="unaid_null_2 s_float">

                        </div>
                        <div class="unaid s_float unain_ex">
                            <div class="r_eye s_float">
                                Right Eye
                            </div>
                            <div class="r_eye s_float">
                                Left  Eye
                            </div>
                            <div class="r_bin s_float">
                                Binocular
                            </div>
                        </div>
                        <div class="aid s_float">
                            <div class="r_eye s_float">
                                Right Eye
                            </div>
                            <div class="r_eye s_float">
                                Left  Eye
                            </div>
                            <div class="r_bin s_float">
                                Binocular
                            </div>
                        </div>
                    </div>
                    <div class="visual_acu_row_4">
                        <div class="unaid_null_2 s_float">
                            Distant
                        </div>
                        <div class="unaid s_float unain_ex">
                            <div class="r_eye s_float">
                                 {{ $form->right_eye_distant }}
                            </div>
                            <div class="r_eye s_float">
                                {{ $form->left_eye_distant }}
                            </div>
                            <div class="r_bin s_float">
                                {{ $form->binocular_eye_distant }}
                            </div>
                        </div>
                        <div class="aid s_float">
                            <div class="r_eye s_float">
                                {{ $form->aided_right_eye_distant }}
                            </div>
                            <div class="r_eye s_float">
                                {{ $form->aided_left_eye_distant }}
                            </div>
                            <div class="r_bin s_float">
                                {{ $form->aided_binocular_eye_distant }}
                            </div>
                        </div>
                    </div>



                </div>
                <div class="for_rec visual_field_table">
                    <div class="for_rec vis_field_table">
                        <div class="for_rec vis_field_row">
                            <b>Visual Field</b>
                        </div>
                        <div class="for_rec vis_field_row">
                            <div class="for_rec vis_f_col s_float">
                                Eye
                            </div>
                            <div class="for_rec vis_f_col s_float">
                                Normal
                            </div>
                            <div class="for_rec vis_f_col s_float" style="border: none;">
                                Defective
                            </div>
                        </div>
                        <div class="for_rec vis_field_row">
                            <div class="for_rec vis_f_col s_float">
                                Right
                            </div>
                            <div class="for_rec vis_f_col s_float">
                                @if($form->visual_normal == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec vis_f_col s_float" style="border: none;">
                                @if($form->visual_normal == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec vis_field_row">
                            <div class="for_rec vis_f_col s_float">
                                Left
                            </div>
                            <div class="for_rec vis_f_col s_float">
                                @if($form->visual_defective == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec vis_f_col s_float" style="border: none;">
                                @if($form->visual_defective == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- row two end -->
        <div class="for_rec row_three clear">
            <p><b>color Vision</b></p>
            <div class="for_rec format_rec sea_table">
                <div class="for_rec format_rec sea_color_col s_float">

                </div>
                <div class="for_rec format_rec sea_color_col s_float">
                    @if($form->color_vision == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        Not Tested
                </div>
                <div class="for_rec format_rec sea_color_col s_float">
                    @if($form->color_vision == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                        @endif
                        Normal
                </div>
                <div class="for_rec format_rec sea_color_col s_float">
                    @if($form->color_vision == '2')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        Doubtful
                </div>
                <div class="for_rec format_rec sea_color_col s_float" style="border: none;">
                    @if($form->color_vision == '3')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        Defctive
                </div>

            </div>
        </div>
        <!-- three end -->

        <div class="for_rec row_four clear">
            <div class="for_rec format_rec sea_hearing">
                <p><b>Hearing</b></p>
                <div class="for_rec format_rec sea_hear_left s_float">
                    <div class="for_rec format_rec sea_hear_table_one">
                        <div class="for_rec format_rec sea_hear_table_row">
                            Pure tone and audiometry (threshold values in dB)
                        </div>
                        <div class="for_rec format_rec sea_hear_table_row_2">
                            <div class="for_rec format_rec sea_hear_col_2 s_float">

                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                500Hz
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                1000Hz
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                2000Hz
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                3000Hz
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">

                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float" style="border: none;">

                            </div>
                        </div>

                        <div class="for_rec format_rec sea_hear_table_row_2">
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                Right Ear
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                  {{ $form->right_ear_500_hz }}
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                {{ $form->right_ear_1000_hz }}
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                {{ $form->right_ear_2000_hz }}
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                {{ $form->right_ear_3000_hz }}
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float" style="border: none;">
                            </div>
                        </div>

                        <div class="for_rec format_rec sea_hear_table_row_2">
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                Left Ear
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                {{ $form->left_ear_500_hz }}
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                {{ $form->left_ear_1000_hz }}
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                {{ $form->left_ear_2000_hz }}
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                                {{ $form->left_ear_3000_hz }}
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float">
                            </div>
                            <div class="for_rec format_rec sea_hear_col_2 s_float" style="border: none;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="for_rec format_rec sea_heear_left s_float">
                    <p>Speech and whisper test (meters)</p>
                    <div class="for_rec format_rec sea_speech_table">
                        <div class="for_rec speech_table_row">
                            <div class="for_rec speech_table_col s_float">

                            </div>
                            <div class="for_rec speech_table_col s_float">
                                Normal
                            </div>
                            <div class="for_rec speech_table_col s_float" style="border: none;">
                                Whisper
                            </div>
                        </div>
                        <div class="for_rec speech_table_row">
                            <div class="for_rec speech_table_col s_float">
                                Right Ear
                            </div>
                            <div class="for_rec speech_table_col s_float">
                                {{ $form->right_ear_normal }}
                            </div>
                            <div class="for_rec speech_table_col s_float" style="border: none;">
                                {{ $form->right_ear_whisper }}
                            </div>
                        </div>
                        <div class="for_rec speech_table_row">
                            <div class="for_rec speech_table_col s_float">
                                Left Ear
                            </div>
                            <div class="for_rec speech_table_col s_float">
                                {{ $form->left_ear_normal }}
                            </div>
                            <div class="for_rec speech_table_col s_float" style="border: none;">
                                {{ $form->left_ear_whisper }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- row _four end -->
        <div class="for_rec row_five clear">
            <div class="for_rec format_rec sea_client_find">
                <div class="for_rec format_rec sea_client_find_row">
                    <p><b>Clinial Findings</b></p>
                    <div class="for_rec format_rec sea_client_height s_float">
                        Height:<span class="for_rec format_rec sea_h_value">{{ $form->clinical_height }}</span>(cm)
                    </div>
                    <div class="for_rec format_rec sea_client_height s_float">
                        Weight:<span class="for_rec format_rec sea_h_value">{{ $form->clinical_weight }}</span>(Kg)
                    </div>
                </div>
                <div class="for_rec format_rec sea_client_find_row clear">
                    <div class="for_rec format_rec sea_client_height s_float">
                        Pulse rate:<span class="for_rec format_rec sea_h_value">{{ $form->clinical_pulse_rate }}</span>(/Minute)
                    </div>
                    <div class="for_rec format_rec sea_client_height s_float">
                        Rhythm:<span class="for_rec format_rec sea_h_value">{{ $form->clinical_rhythm }}</span>(Kg)
                    </div>
                </div>
                <div class="for_rec format_rec sea_client_find_row_3 clear">
                    <div class="for_rec format_rec sea_client_blood s_float">
                        Blood Pressure:stolic: <span class="for_rec format_rec sea_h_value">{{ $form->clinical_b_pressure }}</span> (mmHg) Diastolic:  <span class="for_rec format_rec sea_h_value"></span> mmHg
                    </div>

                </div>
                <div class="for_rec format_rec sea_client_find_row_3 clear">
                    <div class="for_rec format_rec sea_client_blood s_float">
                        Urinalysis: Glucose: <span class="for_rec format_rec sea_h_value">{{ $form->urinalysis_glucose }}</span> Protein(Albumin):  <span class="for_rec format_rec sea_h_value">{{$form->urinalysis_protin}}</span> Blood:<span class="for_rec format_rec sea_h_value">{{$form->urinalysis_blood}}</span>
                    </div>

                </div>


            </div>
        </div>

        <div class="for_rec row_one">
            <div class="for_rec sea_med_4_table_one">
                <div class="for_rec sea_table_one_row">
                    <div class="for_rec sea_table_one_p_1 s_float">
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">

                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                Normal
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                Abnormal
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Head
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->head == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->head == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Sinuses, nose, throat
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->sinus_nose_throat == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->sinus_nose_throat == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Mouth/teeth
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->mouth_teeth == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->mouth_teeth == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Ears (general)
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->ears == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->ears == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Tympanic membrane
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->tympanic_member == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->tympanic_member == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Eyes
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->eyes == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->eyes == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Ophthalmoscopy
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->ophthalmoscopy == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->ophthalmoscopy == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Pupils
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->pupils == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->pupils == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif>
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Eye movement
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->eye_environment == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->eye_environment == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Lungs and chest
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->lungs_chest == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->lungs_chest == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Breast examination
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->breast_examination == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->breast_examination == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Heart
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->heart == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->heart == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec chest_xray s_float">
                                chest X-ray
                            </div>
                            <div class="for_rec chest_xray_value s_float">
                                @if($form->chest_xray == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                                Not performed
                            </div>

                        </div>
                    </div>

                    <div class="for_rec sea_table_one_p_2 s_float">
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">

                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                Normal
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                Abnormal
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Skin
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->skin == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->skin == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Varicose venis
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->varicose_vein == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->varicose_vein == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Vascular (inc. Pedal pulses)
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->vascular_pulse == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->vascular_pulse == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Abdomen and viscera
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->abdomen_viscera == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->abdomen_viscera == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Hernias
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->hernia == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->hernia == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Anus (not rectal exam.)
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->anus == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->anus == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                G-U system
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->g_u_system == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->g_u_system == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Upper and lower extremities
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->upper_lower_ext == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->upper_lower_ext == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Spine (C/S, T/S and L/S)
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->spine == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->spine == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Neurologic (full brief)
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->neurologic == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->neurologic == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                Psychiatric
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->psychiatric == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->psychiatric == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec sea_tab_2_con s_float">
                                General appearance
                            </div>
                            <div class="for_rec sea_tab_2_yes s_float">
                                @if($form->general_appearance == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="for_rec sea_tab_2_no s_float">
                                @if($form->general_appearance == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="for_rec sea_table_2_row">

                            <div class="for_rec performed_date s_float" style="border: none;">
                                @if($form->chest_xray == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                                    Performed (day/month/year)
                            </div>
                            <div class="for_rec performed_date_vale s_float">
                                <p>11 AUG 2021</p>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- row_one end -->

        <div class="for_rec row_two2 clear">
            <div class="for_rec sea_med_cer_4_resul_row">
                <div class="for_rec sea_med_result s_float">
                    Result:
                </div>
                <div class="for_rec sea_med_value s_float">
                    <p>{{ $form->date_of_results_on }}</p>
                </div>
            </div>
        </div>
        <div class="mar_rec_foot">
            <p>F-ALM-011</p>
            <p>Rev. 03</p>
            <p>Page 1 de 4</p>
            <p>Date: 13/03/2013.</p>
        </div>
    </div>
</div>
<div class="format_rec_all " style="border: none;">
    <div class="all_in format_rec_all_in_4">
        <div class="row_three">
            <h4 class="format_rec sea_other_test">Other Dagonostic Test (S) and Result + (S)</h4>

            <div class="format_rec sea_drug_test_row">
                <div class="format_rec sea_drug_test_col_1 s_float">
                    <div class="format_rec sea_drug_test_text s_float">
                        Test:
                    </div>
                    <div class="format_rec sea_drug_test_value s_float">
                        <p>{{ $form->d_test}}</p>
                    </div>
                </div>
                <div class="format_rec sea_drug_result_col_1 s_float">
                    <div class="format_rec sea_drug_result_text s_float">
                        Result:
                    </div>
                    <div class="format_rec sea_drug_result_value s_float">
                        <p>{{ $form->d_result }}</p>
                    </div>
                </div>
            </div>
            <div class="format_rec sea_med_4_comment">
                <p style="margin-left: 20px;">Medical practitioner’s comments and assessment of fitness, with reasons for any limitations:</p>
            </div>
        </div>
        <!-- row three end -->

        <div class="row_three clear">
            <h3 class="format_rec sea_ass">Assesment of fitness for service at sea</h3>
            <p>On the basis of the examinee’s personal declaration, my clinical examination and the diagnostic<br>
                test results recorded above, I declare the examinee medically:</p>

            <div class="format_rec sea_fit_not_fit">
                <div class="format_rec sea_fit_not_fit_col_1 s_float">
                    @if($form->fit_for_lookout_duty == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        fit for look-out
                </div>
                <div class="format_rec sea_fit_not_fit_col_2 s_float">
                    @if($form->fit_for_lookout_duty == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        Not fit for look-out
                </div>
            </div>
            <div class="format_rec sea_deck_service">
                <div class="format_rec sea_deck_row">
                    <div class="deck_ser_col s_float">

                    </div>
                    <div class="deck_ser_col s_float">
                        Deck Service
                    </div>
                    <div class="deck_ser_col s_float">
                        Engine Service
                    </div>
                    <div class="deck_ser_col s_float">
                        Catering Service
                    </div>
                    <div class="deck_ser_col s_float" style="border: none;">
                        Other Service
                    </div>
                </div>
                <div class="format_rec sea_deck_row">
                    <div class="deck_ser_col s_float">
                        Fit
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_desk == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_engine == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_catering == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float" style="border: none;">
                        @if($form->fit_other == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="format_rec sea_deck_row">
                    <div class="deck_ser_col s_float">
                        Unfit
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_desk == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_engine == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float">
                        @if($form->fit_catering == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                    <div class="deck_ser_col s_float" style="border: none;">
                        @if($form->fit_other == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
                <div class="for_rec sea_rstrict">
                    <div class="without_ris s_float">
                        @if($form->with_restrictions == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Without restrictions
                    </div>
                    <div class="with_ris s_float">
                        @if($form->with_restrictions == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                            @endif
                            With restrictions
                    </div>
                    <div class="with_ris_visual s_float">
                        visual aid required
                    </div>
                    <div class="without_ris_yes s_float">
                        @if($form->with_restrictions == '1')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Yes
                    </div>
                    <div class="without_ris_no s_float">
                        @if($form->with_restrictions == '0')
                            <i class="fa fa-check">&#xf046;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            No
                    </div>
                </div>

            </div>
        </div>
        <!-- row three end -->
        <div class="row_four clear">
            <div class="format_rec sea_med_4_comment for_rec_rec" style="margin-top: 55px">
                <p style="margin-left: 20px;">Describe restrictions (e.g. specific positions, type of ship, trade area</p>
            </div>
            <div class="med_cer_others">
                <div class="med_cer_others_row">
                    <div class="format_rec sea_m_cer_date s_float">
                        Medical certificate'x date of Experatio (day/month/year):
                    </div>
                    <div class="format_rec sea_m_cer_date_value s_float">
                        {{ $form->dofex }}
                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="format_rec sea_cerIissue_date s_float">
                        date of Medical Certificate issued (day/month/year):
                    </div>
                    <div class="format_rec sea_cerIissue_date_value s_float">
                        {{ $form->dofi }}
                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="format_rec sea_num_cer s_float">
                        Number of medical certificate:
                    </div>
                    <div class="format_rec sea_num_cer_value s_float">
                        {{ $form->nofmc }}
                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="format_rec sea_name_med s_float">
                        Name of medical pracitioner (type of printed):
                    </div>
                    <div class="format_rec sea_name_med_value s_float">
                        <b></b>
                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="format_rec sea_licanse s_float">
                        Licanse number of medical parctitioner:
                    </div>
                    <div class="format_rec sea_licanse_value s_float">

                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="format_rec sea_address s_float">
                        Address of medica parcitioner:
                    </div>
                    <div class="format_rec sea_address_value s_float">

                    </div>
                </div>
                <div class="med_cer_others_row clear">
                    <div class="format_rec sea_sign s_float">
                        Signature of medical practitioner:
                    </div>
                    <div class="format_rec sea_sign_value s_float">
                    </div>
                </div>
            </div>
        </div>
        <div class="for_rec_foot_1">
            <div class="for_rec_seal s_float">
                <p>seal</p>
            </div>
            <div class="for_rec_seal_value s_float">

            </div>
        </div>
        <div class="mar_rec_foot mar_rec_foot_p4 clear">
            <p>F-ALM-011</p>
            <p>Rev. 03</p>
            <p>Page 1 de 4</p>
            <p>Date: 13/03/2013.</p>
        </div>
    </div>
</div>

<div class="all mc_ex_bor">
    <div class="all_in  m_c_page">
        <div class="m_c_heading">
            <div class="mc_logo s_float">
                <img src="img/m.PNG">
            </div>
            <div class="mc_meddical s_float">
                <h3 class="m_c_heading">MEDICAL CERTIFICATE FOR PERSONNEL SERVICE ON BOARD REPUBLIC OF PANAMA</h3>
            </div>
        </div>

        <div class="row_one clear">
            <div class="m_c_table_1">
                <div class="m_c_tab_row">
                    <div class="m_c_col_1 s_float">
                        <p>SURNAME: {{ $form->certificate->surname }}</p>
                    </div>
                    <div class="m_c_col_2 s_float">
                        <p>GIVEN NAME (S): {{ $form->certificate->given_name }}</p>
                    </div>

                </div>
                <div class="m_c_tab_row_2">
                    <div class="m_c_col_two_1 s_float">
                        <p>DATE OF BIRTH:</p>
                            @php
                                $date = \Carbon\Carbon::parse($form->date_of_birth);
                            @endphp
                        <div class="m_c_df_value">
                            <div class="m_c_day s_float">
                                Day: {{ $date->day }}
                            </div>
                            <div class="m_c_month s_float">
                                Month: {{ $date->month }}
                            </div>
                            <div class="m_c_year s_float">
                                Year: {{ $date->year }}
                            </div>
                        </div>
                    </div>
                    <div class="m_c_col_two_2 s_float">
                        <div class="m_c_place_birth s_float">
                            <p>PLACE OF BIRTH</p>
                            <div class="m_c_dity_country">
                                <div class="mc_city s_float">
                                    City: {{ $form->city }}
                                </div>
                                <div class="mc_city s_float">
                                    Country:  {{ $form->country }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m_c_col_two_3 s_float">
                        <div class="m_c_gender s_float">
                            Male
                            @if($form->gender == 'Male')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class="m_c_gender s_float">
                            @if($form->gender == 'Female')
                                <i class="fa fa-check">&#xf046;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                                Female
                        </div>
                    </div>
                </div>
                <div class="m_c_tab_row_3">
                    <div class="m_col_three_1 s_float">
                        <p>POSITION ON BOARD:</p>
                        <div class="m_c_col_three_row">
                            <div class="ms_position s_float">
                                MASTER
                            </div>
                            <div class="ms_position_value s_float">
                                @if($form->certificate->examination_as_duty == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="m_c_col_three_row">
                            <div class="ms_position s_float">
                                DECK OFFICER
                            </div>
                            <div class="ms_position_value s_float">
                                @if($form->certificate->examination_as_duty == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="m_c_col_three_row">
                            <div class="ms_position s_float">
                                ENGINEERING OFFICER
                            </div>
                            <div class="ms_position_value s_float">
                                @if($form->certificate->examination_as_duty == '2')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="m_c_col_three_row">
                            <div class="ms_position s_float">
                                RADIO OPERATOR
                            </div>
                            <div class="ms_position_value s_float">
                                @if($form->certificate->examination_as_duty == '3')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="m_c_col_three_row">
                            <div class="ms_position s_float">
                                RATING
                            </div>
                            <div class="ms_position_value s_float">
                                @if($form->certificate->examination_as_duty == '4')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="m_col_three_2 s_float">
                        <p>MAILING ADDRESS OF APPLICANT: {{ $form->certificate->mailing_address }}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- row_one end -->

        <div class="mc_row clear">
            <p>DECLARATION OF THE AUTHORIZED PHYSICIAN</p>
        </div>
        <div class="row_two clear">
            <div class="mc_table_2">
                <div class="mc_table_2_p_1 s_float">
                    <div  class="mc_table_2_p_1_row_1">
                        <p>VISION</p>
                    </div>
                    <div  class="mc_table_2_p_1_row_2" style="border-bottom: 1px solid black;">
                        <div class="mc_table_2_col1 s_float">
                            one
                        </div>
                        <div class="mc_table_2_col2 s_float">
                            WITHOUT GLASSES
                        </div>
                        <div class="mc_table_2_col2 s_float"  style="border: none;">
                            WITH GLASSES
                        </div>
                    </div>
                    <div  class="mc_table_2_p_1_row_2">
                        <div class="mc_table_2_col1 s_float">
                            <p  class="r_bors">RIGHT EYE </p>
                        </div>
                        <div class="mc_table_2_col2 s_float">
                            <p class="mcg_e_value">{{ $form->certificate->right_eye }}</p>
                        </div>
                        <div class="mc_table_2_col2 s_float"  style="border: none;">
                            <p class="mcg_e_value">{{ $form->certificate->with_g_right_eye }}</p>
                        </div>
                    </div>
                    <div  class="mc_table_2_p_1_row_2">
                        <div class="mc_table_2_col1 s_float">

                        </div>
                        <div class="mc_table_2_col2 s_float">

                        </div>
                        <div class="mc_table_2_col2 s_float" style="border: none;">

                        </div>
                    </div>
                    <div  class="mc_table_2_p_1_row_3" style="border: none;">
                        <div class="mc_table_2_col1 s_float">
                            <p  class="l_bors">LEFT EYE</p>
                        </div>
                        <div class="mc_table_2_col2 s_float">
                            <p class="mcg_e_value">{{$form->certificate->left_eye}}</p>
                        </div>
                        <div class="mc_table_2_col2 s_float"  style="border: none;">
                            <p class="mcg_e_value">{{$form->certificate->with_g_left_eye}}</p>
                        </div>
                    </div>
                </div>
                <div class="mc_table_2_p_2 s_float">
                    <div  class="mc_table_2_p_2_row_1">
                        <div class="mc_table_2_p_2_col s_float">
                            COLOR TEST TYPE
                        </div>
                        <div class="mc_table_2_p_2_col s_float" style="border-right: none;">
                            HEARING
                        </div>
                    </div>
                    <div  class="mc_table_2_p_2_row_2">
                        <div class="mc_table_2_p_2_col_2 s_float">
                            <p>
                                @if($form->certificate->color_test_type == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                                BOOK
                            </p>
                            <p>
                                @if($form->certificate->color_test_type == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                                LANTERN</p>
                            <p>YELLOW <span class="mc_color_t_value">{{ $form->certificate->test_yellow }}</span> Red <span class="mc_color_t_value">{{ $form->certificate->test_red }}</span></p>
                            <p>GREEN <span class="mc_color_t_value">{{ $form->certificate->test_green }}</span> Blue<span class="mc_color_t_value">{{ $form->certificate->test_blue }}</span></p>
                        </div>
                        <div class="mc_table_2_p_2_col_2 s_float" style="border: none;">
                            <p>RIGHT EAR <span class="mc_ear_value">{{ $form->certificate->hering_right_ear }}</span></p><br>
                            <p>LEFT EAR <span class="mc_ear_value">{{ $form->certificate->hering_right_ear }}</span></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="mc_row">
            <p>Confirmation that identification documents were checked at the point of examination:
                YES
                @if($form->certificate->confirmation == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NO
                @if($form->certificate->confirmation == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row">
            <p>Hearing meets the standards in STCW Code, Section A-1/9?
                YES
                @if($form->certificate->hearing_meets == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                &nbsp;  NO
                @if($form->certificate->hearing_meets == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                NOT APLICABLE
                @if($form->certificate->not_applicable == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row">
            <p>Unaided hearing satisfactory?
                YES
                @if($form->certificate->hearing_sat == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                &nbsp; NO
                @if($form->certificate->hearing_sat == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row">
            <p>Visual acuity meets standards in STCW Code, Section A-1/9?
                YES
                @if($form->certificate->visual_meets == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                &nbsp; NO
                @if($form->certificate->visual_meets == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row_2">
            <p>Colour vision meets standards in STCW Code, Section A-1/9?
                YES
                @if($form->certificate->color_vision == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                &nbsp; &nbsp; NO
                @if($form->certificate->color_vision == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
            <p>(the visual test it is required every six years)</p>
            <p>Date of the last colour vision test: (Day/Month/Year)
                <span class="md_date_value">
                    {{ date('d/m/Y', strtotime($form->date_of_vision_test ?? '')) }}
                </span></p>
        </div>
        <div class="mc_row">
            <p>Are glasses or contact lenses necessary to meet the required vision standards?
                YES
                @if($form->certificate->vision_standard == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
                &nbsp; NO
                @if($form->certificate->vision_standard == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row">
            <p>Able for watchkeeping?
                YES
                @if($form->certificate->watch_keep == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif &nbsp;
                NO
                @if($form->certificate->watch_keep == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row">
            <p>Is applicant taking any non-prescription or prescription medications?
                YES
                @if($form->certificate->medications == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif&nbsp; &nbsp;
                NO
                @if($form->certificate->medications == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row_3">
            <p>Is the seafarer free from any medical condition likely to be aggravated by service at sea or to render the seafarers unfit for such service or to endanger the health of other persons on board?
                YES
                @if($form->certificate->is_applicant_suffering == '1')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif&nbsp;
                &nbsp; NO
                @if($form->certificate->is_applicant_suffering == '0')
                    <i class="fa fa-check">&#xf046;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
            </p>
        </div>
        <div class="mc_row_4">
            <p style="padding-left: 4px;">
                Hereby I declare that I am in knowledge of the contents of the Physical Examination.
            </p>
            <div class="mc_sign_sec">
                <div class="mc_sign_date">
                    <div class="mc_sign_col s_float">
                        <p class="sg_value"></p>
                        <p class="sg_text">Signature of Applicant</p>
                    </div>
                    <div class="mc_sign_col s_float">
                        <p class="sg_value"></p>
                        <p class="sg_text">Name of Applicant </p>
                    </div>
                    <div class="mc_sign_col s_float">
                        <p class="sg_value"></p>
                        <p class="sg_text">Date</p>
                    </div>
                </div>
                <p class="mc_circle_text">CIRCLE APPROPIATE CHOICE: (
                    @if($form->gender == 'Male')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    HE /
                    @if($form->gender == 'Female')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    SHE) IS FOUND TO BE (
                    @if($form->certificate->fit == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    FIT/
                    @if($form->certificate->fit == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    NOT FIT) FOR DUTY AS A (
                    @if($form->certificate->examination_as_duty == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    MASTER/
                    @if($form->certificate->examination_as_duty == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    DECK OFFCIER/
                    @if($form->certificate->examination_as_duty == '2')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    ENGINEERING OFFICER/
                    @if($form->certificate->examination_as_duty == '3')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    RADIO OPERATOR /
                    @if($form->certificate->examination_as_duty == '4')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    RATING) (
                    @if($form->certificate->with == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    WITHOUT ANY /
                    @if($form->certificate->with == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    WITH THE FOLLOWING) RESTRICTIONS:
                </p>
                <p>{{ $form->certificate->line1 }}</p>
                <p>{{ $form->certificate->line2 }}</p>
            </div>

        </div>
        <div class="mc_row_5 clear">
            <div class="mc_date_degree_row">
                <div class="mc_name_degree_text s_float">
                    NAME AND DEGREE OF PHYSICIAN:
                </div>
                <div class="mc_name_degree_value s_float">
                    <b>DR. SABRINA MOSTAFA, M.B.B.S (D.U)</b>
                </div>
            </div>
            <div class="mc_date_degree_row clear">
                <div class="mc_address_text s_float">
                    ADDRESS:
                </div>
                <div class="mc_address_value s_float">
                    <b>126, SK. MUJIB ROAD, CHOWMUHONI, CHITTAGONG, BANGLADESH.</b>
                </div>
            </div>
            <div class="mc_date_degree_row clear">
                <div class="mc_psysician_text s_float">
                    NAME OF PHYSICIAN'S CERTIFICATING AUTHORITY:
                </div>
                <div class="mc_psysician_value s_float">
                    <b>REGISTRATION NO.: A-68208, B.M.D.C, DHAKA, BANGLADESH</b>
                </div>
            </div>
            <div class="mc_date_degree_row clear">
                <div class="mc_date_issue_text s_float">
                    DATE OF ISSUE PHYSICIAN'S CERTIFICATE:
                </div>
                <div class="mc_date_issue_value s_float">
                    <b>08-Jun-14</b>
                </div>
            </div>

        </div>
        <div class="mc_row_6 clear">
            <div class="sign_phn s_float">
                <div class="mc_sign_phn_text s_float">
                    SIGNATURE OF PHYSICIAN:
                </div>
                <div class="mc_sign_phn_value s_float">

                </div>
            </div>
            <div class="mc_stamp s_float">
                <div class="mc_stamp_text s_float">
                    STAMP OF PHYSICIAN:
                </div>
                <div class="mc_stamp_value s_float">

                </div>
            </div>
            <div class="mc_sig_phn_date s_float">
                <div class="mc_date_phn_text s_float">
                    DATE
                </div>
                <div class="mc_date_phn_value s_float">

                </div>
            </div>
        </div>
        <div class="mc_row">
            <p>EXPIRY DATE OF CERTIFICATE:</p>
        </div>
        <div class="mc_row_7">
            <p style="text-align: center; font-size: 12px;"><i>This certificate is issued by the Panama Maritime Authority in compliance with the requirements<br>
                    of the STCW Convention, 1978, as amended and the Maritime Labour Convention, 2006.</i></p>
        </div>
        <div class="mc_row_8">

            <div class="m_rev">
                <p>F-ALM-012</p>
                <p>Rev.05</p>
                <p>Page 1 de 1</p>
                <p>Date: 13/03/2013</p>
            </div>

        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js" integrity="sha512-YSdqvJoZr83hj76AIVdOcvLWYMWzy6sJyIMic2aQz5kh2bPTd9dzY3NtdeEAzPp/PhgZqr4aJObB3ym/vsItMg==" crossorigin="anonymous"></script>
</body>
</html>
