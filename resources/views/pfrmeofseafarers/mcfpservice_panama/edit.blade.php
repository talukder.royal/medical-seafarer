@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <div class="row">
        <div class="col-lg-12 text-center">
            <div style="text-transform: uppercase">
                Medical certificate for personnel service on board republic of panama
            </div>
        </div>
    </div>
    <form class="form-control" action="{{ route('mcfpservice_panama.update', $form->id) }}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="surname"><span class="text-danger">*</span>SURNAME</label>
                    <input type="text" class="form-control" name="surname" id="surname" value="{{ old('surname' , $form->certificate->surname ?? '') }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="given_name"><span class="text-danger">*</span>GIVEN NAME(s)</label>
                    <input type="text" class="form-control" name="given_name" id="given_name" value="{{ old('given_name', $form->certificate->given_name ?? '') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label text-capitalize" for="dob">Date of birth:</label>
                    <input type="date" class="form-control" name="dob" id="dob" value="{{ old('date_of_birth', $form->date_of_birth ?? '') }}">
                </div>
            </div>
            <div class="col-lg-5">
                <div class="form-group">
                    <div class="d-flex justify-content-between">
                        <div class="">
                            <label class="form-label" for="city">Place of birth: City</label>
                            <input type="text" class="form-control" name="city" id="city" value="{{ old('city', $form->certificate->city ?? '') }}">
                        </div>
                        <div class="ml-2">
                            <label class="form-label" for="country">Country</label>
                            <input type="text" class="form-control" name="country" id="country" value="{{ old('country', $form->certificate->country ?? '') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="form-label">Sex</label>
                    <br>
                    <label class="form-label" for="male">Male</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="gender" id="male" value="1" {{ $form->gender == 'Male' ? 'checked' : '' }} >
                    <label class="form-label" for="female">Female</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="gender" id="female" value="0" {{ $form->gender == 'Female' ? 'checked' : '' }}>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-class">POSITION ON BOARD:</label>
                </div>
                <div class="form-group" style="padding-left: 100px;">
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="master">Master</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="master" value="0" {{ $form->certificate->examination_as_duty == '0' ? 'checked' : '' }}>
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="deck_officer">Deck officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="deck_officer" value="1" {{ $form->certificate->examination_as_duty == '1' ? 'checked' : '' }}>
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="engineering_officer">Engineering officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="engineering_officer" value="2" {{ $form->certificate->examination_as_duty == '2' ? 'checked' : '' }}>
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="radio_officer">Radio officer</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="radio_officer" value="3" {{ $form->certificate->examination_as_duty == '3' ? 'checked' : '' }}>
                    </div>
                    <div class="form-group d-flex justify-content-between" style="width: 200px;">
                        <label class="form-label d-block" for="rating">Rating</label>
                        <input type="checkbox" style="width: 25px; height: 20px;" name="examination_as_duty" id="rating" value="4" {{ $form->certificate->examination_as_duty == '4' ? 'checked' : '' }}>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="mailing_address">Mailing address of applicant:</label>
                    <textarea  class="form-control" name="mailing_address" id="mailing_address">{{ old('mailing_address', $form->certificate->mailing_address ?? '') }}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <label class="from-group" style="text-transform: uppercase">Declaration of approved medical practioner:</label>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="">Vision(Without glasses):</label>
                    <br>
                    <label class="form-label" for="right_eye">Right eye:</label>
                    <input class="form-control" type="text" name="right_eye" id="right_eye" value="{{ old('right_eye', $form->certificate->right_eye ?? '') }}">
                    <label class="form-label" for="left_eye">Left eye:</label>
                    <input class="form-control" type="text" name="left_eye" id="left_eye" value="{{ old('left_eye', $form->certificate->left_eye ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_test_type">Color test type:</label>
                    <label class="form-label" for="book">Book</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_test_type" id="book" value="1" {{ $form->certificate->color_test_type == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="lanter">Lantern</label>
                    <input style="width: 25px; height: 20px;" type="checkbox" name="color_test_type" id="lanter" value="0" {{ $form->certificate->color_test_type == '1' ? 'checked' : '' }}>
                    <br>
                    <div class="m-0 p-0 d-flex justify-content-between">
                        <div  class="m-0 p-0">
                            <label class="form-label" for="yellow">Yellow</label>
                            <input class="form-control" type="text" name="test_yellow" id="yellow" value="{{ old('test_yellow', $form->certificate->test_yellow ?? '') }}">
                            <label class="form-label" for="red">Red</label>
                            <input class="form-control" type="text" name="test_red" id="red" value="{{ old('test_red', $form->certificate->test_yellow ?? '') }}">
                        </div>

                        <div  class="m-0 p-0">
                            <label class="form-label" for="green">Green</label>
                            <input class="form-control" type="text" name="test_green" id="green" value="{{ old('test_green', $form->certificate->test_yellow ?? '') }}">
                            <label class="form-label" for="blue">Blue</label>
                            <input class="form-control" type="text" name="test_blue" id="blue" value="{{ old('test_blue', $form->certificate->test_yellow ?? '') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="">Vision(With glasses):</label>
                    <br>
                    <label class="form-label" for="with_g_right_eye">Right eye:</label>
                    <input class="form-control" type="text" name="with_g_right_eye" id="with_g_right_eye" value="{{ old('with_g_right_eye', $form->certificate->with_g_right_eye ?? '') }}">
                    <label class="form-label" for="with_g_left_eye">Left eye:</label>
                    <input class="form-control" type="text" name="with_g_left_eye" id="with_g_left_eye" value="{{ old('with_g_left_eye', $form->certificate->with_g_left_eye ?? '') }}">
                </div>
                <div class="form-group">
                    <label class="form-label" for="">Hearing:</label>
                    <br>
                    <label class="form-label" for="hering_right_ear">Right ear:</label>
                    <input class="form-control" type="text" name="hering_right_ear" id="hering_right_ear" value="{{ old('hering_right_ear', $form->certificate->hering_right_ear ?? '') }}">
                    <label class="form-label" for="hering_left_ear">Left ear:</label>
                    <input class="form-control" type="text" name="hering_left_ear" id="hering_left_ear" value="{{ old('hering_left_ear', $form->certificate->hering_left_ear ?? '') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="confirmation">Confirmation that identification documents were checked at the point of examination:</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="confirmation_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="confirmation" id="confirmation_yes" value="1" {{ $form->certificate->confirmation == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="confirmation_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="confirmation" id="confirmation_no" value="0" {{ $form->certificate->confirmation == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="hearing_meets">Hearing meets the standards in STCW code , Section A-1/9?:</label>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="form-group">
                    <label class="form-label" for="hearing_meets_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="hearing_meets" id="hearing_meets_yes" value="1" {{ $form->certificate->hearing_meets == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="hearing_meets_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="hearing_meets" id="hearing_meets_no" value="0" {{ $form->certificate->hearing_meets == '0' ? 'checked' : '' }}>
                    <label class="form-label" for="not_applicable">Not Applicable</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="not_applicable" id="not_applicable" value="0" {{ $form->certificate->not_applicable == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="hearing_sat">Unaided hearing satisfactory?</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="hearing_sat">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="hearing_sat" id="hearing_sat" value="1" {{ $form->certificate->hearing_sat == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="hearing_sat">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="hearing_sat" id="hearing_sat" value="0" {{ $form->certificate->hearing_sat == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="visual_meets">Visual acuity meets standards in STCW Code, Section A-1/9?</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="visual_meets">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="visual_meets" id="visual_meets" value="1" {{ $form->certificate->visual_meets == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="visual_meets">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="visual_meets" id="visual_meets" value="0" {{ $form->certificate->visual_meets == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="color_vision">Colour vision meets standards in STCW code, Section A-1/9?</label>
                    <small>(the visual test it is required every six years)</small>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="color_vision">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="color_vision" id="color_vision" value="1" {{ $form->certificate->color_vision == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="color_vision">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="color_vision" id="color_vision" value="0" {{ $form->certificate->color_vision == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>  <label class="form-label" for="date_of_vision_test"></label>

        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="date_of_vision_test">Date of last color vision test:</label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <input class="form-control" style="width: 70%" type="date" name="date_of_vision_test" id="date_of_vision_test" value="{{ old('date_of_vision_test', $form->certificate->date_of_vision_test ?? '') }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <label class="form-label" for="watch_keep">Able for watchkeeping??</label>
            </div>
            <div class="col-lg-4">
                <label class="form-label" for="watch_keep">Yes</label>
                <input type="checkbox" style="width: 25px; height: 20px; margin-right:40px; " name="watch_keep" id="watch_keep"  value="1" {{ $form->certificate->watch_keep == '1' ? 'checked' : '' }}>
                <label class="form-label" for="watch_keep">No</label>
                <input type="checkbox" style="width: 25px; height: 20px;" name="watch_keep" id="watch_keep" value="0" {{ $form->certificate->watch_keep == '0' ? 'checked' : '' }}>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <label class="form-label" for="vision_standard">Are glasses or contact lenses necessary to meet the required vision standard?</label>
            </div>
            <div class="col-lg-4">
                <label class="form-label" for="vision_standard_yes">Yes</label>
                <input type="checkbox" style="width: 25px; height: 20px; margin-right:40px; " name="vision_standard" id="vision_standard_yes"  value="1" {{ $form->certificate->vision_standard == '1' ? 'checked' : '' }}>
                <label class="form-label" for="vision_standard_no">No</label>
                <input type="checkbox" style="width: 25px; height: 20px;" name="vision_standard" id="vision_standard_no" value="0" {{ $form->certificate->vision_standard == '0' ? 'checked' : '' }}>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="medications">
                        Is applicant taking any non-prescription or prescription medications?
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="medications">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="medications" id="medications" value="1" {{ $form->certificate->medications == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="medications">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="medications" id="medications" value="0" {{ $form->certificate->medications == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group">
                    <label class="form-label" for="is_applicant_suffering">Is the seafarer free from any disease likely to
                        be aggravated by working aboard a vessel, or to render him/
                        her unfit for service at sea or likely to endanger the health
                        of of other persons on board?
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-label" for="is_applicant_suffering_yes">Yes</label>
                    <input type="checkbox" style="width: 25px; height: 20px; margin-right: 30px;" name="is_applicant_suffering" id="is_applicant_suffering_yes" value="1" {{ $form->certificate->is_applicant_suffering == '1' ? 'checked' : '' }}>
                    <label class="form-label" for="is_applicant_suffering_no">No</label>
                    <input type="checkbox" style="width: 25px; height: 20px;" name="is_applicant_suffering" id="is_applicant_suffering_no" value="0" {{ $form->certificate->is_applicant_suffering == '0' ? 'checked' : '' }}>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="restriction" style="text-transform: uppercase">
                        Circle appropiate choice: (he/she) is found to be (Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="1" {{ $form->certificate->fit == '1' ? 'checked' : '' }}>/ not Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="0"  {{ $form->certificate->fit == '0' ? 'checked' : '' }}>) for duty as a (master / deck officer / engineering officer/ radio operator / rating) (without any<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="1"  {{ $form->certificate->with == '0' ? 'checked' : '' }}> / with <input style="width: 25px; height: 20px;" type="checkbox" name="with" value="1" {{ $form->certificate->with == '1' ? 'checked' : '' }}> the following) restrictions:
                    </label>
                    <br>
                    <label for="line1">Line1</label>
                    <input type="text" name="line1" class="form-control" id="line1" value="{{ old('line1', $form->certificate->line1 ?? '') }}">
                    <label for="line2">Line2</label>
                    <input type="text" name="line2" class="form-control" id="line2" value="{{ old('line2', $form->certificate->line2 ?? '') }}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>
    </form>
@endsection
