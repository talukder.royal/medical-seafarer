@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <form action="" method="get">
        <div class="row">
            <div class="input-group mb-2">
                <input type="text" class="form-control" name="search"
                       value="{{ request('search') }}"
                       placeholder="{{ $placeholder ?? 'Search' }}"
                       data-toggle="tooltip"
                       data-placement="bottom"
                       title="{{ isset($searchable) ? 'Searchable parameters: '. $searchable : 'Search' }}"
                       autocomplete="off">
                <div class="input-group-append">
                    <button style="margin: 0" type="submit" class="btn btn-primary" title="Search">
                        <i class="fa fa-search"></i>
                    </button>
                    @if(request()->query())
                        <a href="{{ route($route ?? request()->route()->getName(), $param ?? []) }}"
                           class="btn btn-danger"
                           title="Clear search">
                            <i class="fa fa-times"></i>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </form>
    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="bg-soft-dark p-2">
                <h3 style="color:#0c112e">All Records</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(auth()->user())
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" colspan="2">Name </th>
                    <th scope="col">Passport No</th>
                    <th scope="col">Nationality</th>
                    <th scope="col" style="text-align: center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($forms as $key => $form)
                    <tr>
                        <th style="text-align: left" scope="row">{{ (($forms->currentPage() - 1) * $forms->perPage() + $key+1)}}</th>
                        <td style="text-align: left" colspan="2">{{ $form->seafarers_name }} </td>
                        <td style="text-align: left">{{ $form->passport_no }}</td>
                        <td style="text-align: left">{{ $form->nationality }}</td>
                        <td style="text-align: left">
                            <a class="btn btn-primary" href="{{ route('pfrmeofseafarer.page1_edit', $form->id) }}" type="button">Edit</a>
                            @if(empty($form->certificate))
                                <a class="btn btn-soft-success" href="{{ route('mcfpservice_panama.create', $form->id) }}" type="button"><i class="fa fa-edit"></i>See Document</a>
                            @endif
                            @if(!empty($form->certificate))
                                <a class="btn btn-soft-success" href="{{ route('mcfpservice_panama.edit', $form->id) }}" type="button"><i class="fa fa-edit"></i>See Document</a>
                                <a class="btn btn-soft-success" href="{{ route('pfrmeofseafarer.download', $form->id) }}" type="button"><i class="fa fa-download"></i>Download</a>
                            @endif
                            <form action="{{ route('pfrmeofseafarer.delete', $form->id) }}" method="post" class="d-inline-block">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr><td class="text-warning" colspan="5">Form not found!</td></tr>
                @endforelse
                </tbody>
            </table>
            @elseif(request('search') !== null && !auth()->user())
                @forelse($forms as $form)
                    @if($form->dofex >= \Carbon\Carbon::today()->toDateString())
                        <div class="form-group">
                            <label class="form-label" for="name">Sefarer's Name</label>
                            <input type="text" class="form-control" id="name" value="{{ $form->seafarers_name }}" disabled>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="dob">Date of birth</label>
                            <input type="text" class="form-control" id="dob" value="{{ $form->date_of_birth }}" disabled>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="gender">Gender/Sex</label>
                            <input type="text" class="form-control" id="gender" value="{{ $form->gender }}" disabled>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="nationality">Nationality</label>
                            <input type="text" class="form-control" id="nationality" value="{{ $form->nationality ?? '' }}" disabled>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="passport_no">Passport No</label>
                            <input type="text" class="form-control" id="passport_no" value="{{ $form->passport_no ?? '' }}" disabled>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="cdc_no">CDC /Seafarer's book no</label>
                            <input type="text" class="form-control" id="cdc_no" value="{{ $form->cdc_no ?? ''}}" disabled>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="seaman_id">Seaman ID no</label>
                            <input type="text" class="form-control" id="seaman_id" value="{{ $form->seaman_id ?? ''}}" disabled>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="father_name">Father's/ Husband's Name</label>
                            <input type="text" class="form-control" id="father_name" value="{{ $form->father_husband_name ?? '' }}" disabled>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="mother_name">Mother's Name</label>
                            <input type="text" class="form-control" id="mother_name" value="{{ $form->mother_name ?? '' }}" disabled>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="doe">Date of issue</label>
                            <input type="text" class="form-control" id="doe" value="{{ $form->dofi}}" disabled>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="doexpiry">Date of expiry</label>
                            <input type="text" class="form-control" id="doexpiry" value="{{ $form->dofex }}" disabled>
                        </div>
                    @else
                        <p class="form-control text-warning">Form was expired at {{ $form->dofex }} </p>
                    @endif
                @empty
                    <div class="form-group">
                        <p class="form-control text-warning">Form is  not found!</p>
                    </div>
                @endforelse
            @endif
            <div class="pagination pagination-sm justify-content-center">
                {{$forms->appends(request()->query())->links()}}
            </div>
        </div>
    </div>
@endsection
