<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
</head>
<body>
<div class="bel_body">
    <div class="bel_full">
        <div class="bel_head">
            <div class="visible-print text-center" style="padding: 2px;">
                @php
                    use SimpleSoftwareIO\QrCode\Facades\QrCode;
                    $path  = 'qrcodes/_'.now().$form->id.'.svg';
                    QrCode::generate(route('smcseafarer_recrord.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
                @endphp
                <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
                <p>Scan me to return to the original page.</p>
            </div>
            <div class="bel_logo_row">
                <div class="bel_logo s_float">
                    <img src="img/b.PNG">
                </div>
                <div class="bel_sfc s_float">
                    SFC-005
                </div>
            </div>
            <div class="clear bel_stcc">
                <i>STCW 1978 as amended</i>
            </div>
        </div>
        <div class="bel_inner">

            <div class="bel_sec_one">
                <h1 class="bel_med_fit">MEDICAL FITNESS CERTIFICATE FOR PERSONNEL SERVICE 0N BOARD</h1>
                <h1 class="bel_med_fit">BELIZEAN REGISTERED SHIPS</h1>
                <div class="bel_sec_row_1 clear">
                    <div class="bel_row_1_last_name s_float">
                        <p>Last name:<span>{{ $form->last_name }}</span></p>
                    </div>
                    <div class="bel_row_1_given_name s_float">
                        <p>Given names(s):<span>{{ $form->given_name }}</span></p>
                    </div>
                </div>
                <div class="bel_sec_row_2 clear">
                    <p>Position Onboard: <span>{{ $form->position_board }}</span></p>
                </div>
                <div class="bel_sec_row_3">
                    <div class="bel_place_birth s_float">
                        <p>Placce of birth (City, Country) <span>{{ $form->place_of_birth }}</span></p>
                    </div>
                    <div class="bel_sec_mailing s_float">
                        <p>Mailing address of Appliccant (Street, city, Country</p>
                    </div>
                    <div class="bel_date_of_birth s_float">
                        <p>Date of Birth(dd/mm/yy)</p>
                        <p class="date_gray">{{ date('d/m/Y', strtotime( $form->dob ?? ''))  }}</p>
                    </div>
                    <div class="bel_sex s_float">
                        <p>Sex</p>
                        M
                        @if($form->gender == 'Male')
                            <i class="far fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                        F
                        @if($form->gender == 'Female')
                            <i class="far fa-check">&#xf046;</i>
                        @else
                            <i class="far fa-check">&#xf096;</i>
                        @endif
                    </div>
                </div>
            </div>

            <div class="sec_two">
                <h1 class="bel_dec">DECLARATION 0F THE AUTHORIZED PHYSICIAN</h1>
                <div class="bel_dec_table">
                    <div class="bel_dec_row_1">
                        <div class="bel_dec_col_1 s_float">
                            Confirmation that identification documents were checked at the point of examination
                        </div>
                        <div class="bel_dec_col_2 s_float">
                            <span class="bel_tab_yes">Yes:
                                @if($form->confirmation == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                            <span class="bel_tab_no"> No:
                                @if($form->confirmation == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                        </div>
                    </div>
                    <div class="bel_dec_row_s">
                        <div class="bel_dec_col_s_1 s_float">
                            Hearing meets the standards in STCW Code, Section A-1/9?
                        </div>
                        <div class="bel_dec_col_s_2 s_float">
                            <span class="bel_tab_yes">Yes:
                                 @if($form->hearing_standard == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span> <span class="bel_tab_no"> No:
                                 @if($form->hearing_standard == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="bel_dec_row_s">
                        <div class="bel_dec_col_s_1 s_float">
                            Unaided hearing satisfactory?
                        </div>
                        <div class="bel_dec_col_s_2 s_float">
                            <span class="bel_tab_yes">
                                Yes:
                                @if($form->unaided_statisfactory == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                            <span class="bel_tab_no"> No:
                               @if($form->unaided_statisfactory == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="bel_dec_row_s">
                        <div class="bel_dec_col_s_1 s_float">
                            Visual acuity meets standards in STCW Code, Section A-1/9?
                        </div>
                        <div class="bel_dec_col_s_2 s_float">
                            <span class="bel_tab_yes">Yes:
                                @if($form->visual_acutity == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                            <span class="bel_tab_no"> No:
                                @if($form->visual_acutity == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="bel_dec_row_s">
                        <div class="bel_dec_col_s_1 s_float">
                            Colour vision meets standards in STCW Code, Section A-I/9?
                        </div>
                        <div class="bel_dec_col_s_2 s_float">
                            <span class="bel_tab_yes">Yes:
                              @if($form->color_vision_standard == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                            <span class="bel_tab_no"> No:
                                @if($form->color_vision_standard == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="bel_dec_row_1">
                        <div class="bel_dec_col_1 s_float">
                            <p>The visual test is required every six years</p>
                            <p>Date of the last colour vision test</p>
                        </div>
                        <div class="bel_dec_col_2 s_float">
                            {{ date('d/m/Y', strtotime($form->color_vision_test ?? '')) }}
                        </div>
                    </div>

                    <div class="bel_dec_row_s">
                        <div class="bel_dec_col_s_1 s_float">
                            Are glasses or contact lenses necessary to meet the required vision standards?
                        </div>
                        <div class="bel_dec_col_s_2 s_float">
                            <span class="bel_tab_yes">Yes:
                                @if($form->vision_standard == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                            <span class="bel_tab_no"> No:
                                @if($form->vision_standard == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="bel_dec_row_s">
                        <div class="bel_dec_col_s_1 s_float">
                            Able for watchkeeping?
                        </div>
                        <div class="bel_dec_col_s_2 s_float">
                            <span class="bel_tab_yes">Yes:
                                @if($form->able_watch == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                            <span class="bel_tab_no"> No:
                                @if($form->able_watch == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="bel_dec_row_s">
                        <div class="bel_dec_col_s_1 s_float">
                            ls the applicant taking any non-prescription or prescription medication?
                        </div>
                        <div class="bel_dec_col_s_2 s_float">
                            <span class="bel_tab_yes">Yes:
                                @if($form->prescript_medication == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                            <span class="bel_tab_no"> No:
                                @if($form->prescript_medication == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                        </div>
                    </div>
                    <div class="bel_dec_row_m">
                        <div class="bel_dec_col_m_1 s_float">
                            <p>ls the seafarer free from any medical condition likely to be aggravated by service at sea or to render the seafarer unfit for such service or to endanger the health of other persons on board?</p>
                        </div>
                        <div class="bel_dec_col_m_2 s_float">
                            <span class="bel_tab_yes">Yes:
                                @if($form->unfit_for_service == '1')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                            <span class="bel_tab_no"> No:
                                @if($form->unfit_for_service == '0')
                                    <i class="fa fa-check">&#xf046;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </span>
                        </div>
                    </div>
                    <div class="bel_dec_row_f">
                        <p>hereby declare that I am in knowledge of the contents of the Physical Examination.</p>
                        <div class="bel_nam_sign">
                            <div class="bel_nam_sign_col s_float">
                                <p></p>
                                <p class="bel_nam_sign_text">Name of Applicant</p>
                            </div>
                            <div class="bel_nam_sign_col s_float">
                                <p></p>
                                <p class="bel_nam_sign_text">Signature of Applicant</p>
                            </div>
                            <div class="bel_nam_sign_col s_float">
                                <p></p>
                                <p class="bel_nam_sign_text">Date</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bel_sec_three">
                <p class="bel_c-r">Circle the appropriate choice:</p>
                <div class="bel_duty">
                    HE   @if($form->gender == 'Male')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    / SHE
                    @if($form->gender == 'Female')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                        is found t be (
                    @if($form->fit == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    FIT/ @if($form->fit == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    NOT FIT) for duty as a (WITHOUT ANY
                    @if($form->with == '0')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    /WHIT  @if($form->with == '1')
                        <i class="fa fa-check">&#xf046;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    THE FOLLOWING) restriction:<br>
                    <span class="del_duty_text"></span>
                    <div class="bel_bor_1">{{ $form->line1 }}</div>
                    <div class="bel_bor_2">{{ $form->line1 }}</div>
                </div>
                <p class="bel_front">Front</p>


            </div>


        </div>
        <p class="del_page">1</p>
    </div>
</div>

<div class="bel_body">
    <div class="bel_full">
        <div class="bel_head">
            <div class="bel_logo_row">
                <div class="bel_logo s_float">
                    <img src="img/b.PNG">
                </div>
                <div class="bel_sfc s_float">
                    SFC-005
                </div>
            </div>
            <div class="clear bel_stcc">
                <i>STCW 1978 as amended</i>
            </div>
        </div>
        <div class="bel_inner_2">
            <div class="des_p_2_sec_1">
                <div class="del_p_2_row">
                    <p>Name and Degree of Physician: <span class="del_p2_name_deg_value"></span></p>
                </div>
                <div class="del_p_2_row">
                    <p>Full address: <span class="del_p2_address_value"></span></p>
                </div>
                <div class="del_p_2_row">
                    <p>Name of Physician's certificating authority: <span class="del_p2_nam_ph_value"></span></p>
                </div>
                <div class="del_p_2_row">
                    <p>Issuance date of Certificate: <span class="del_p2_date_value">{{ date('d/m/Y', strtotime($form->doe ?? '')) }}</span></p>
                </div>

            </div>

            <div class="des_p_2_sec_2">
                <div class="del_stamp_table">
                    <div class="del_stamp_table_col s_float">
                        <p class="del_segin_phy_value"></p>
                        <p class="del_segin_phy_text"><b>Signature of Physician</b></p>
                    </div>
                    <div class="del_stamp_table_col s_float">
                        <p class="del_segin_phy_stamp">Stamp of Physician:</p>
                    </div>
                    <div class="del_stamp_table_col s_float">
                        <p class="del_segin_phy_date_value">Date: <span class="del_sec_2_p_date_value"></span></p>
                        <p class="del_segin_phy_text" style="margin-left: 12px;"><i><b>(day/Month/Year)</b></i></p>
                    </div>
                </div>
            </div>
            <div class="des_p_2_sec_3">
                <p class="del_P_2_expair">Expiration Date of Certificate: <span class="del_expair_value">{{ date('d/m/Y', strtotime($form->doexpiry ?? '')) }}</span></p>
                <div class="del_validity">
                    <p class="del_cer_title">(This certificate cannot be valid for more than 2 years)</p>
                    <p class="del_cer_details">If the per.Iod of validity of this cert.Ificate expires in the course of the voyage, then the med.Ical certif.Icate shall continue in force until the next port of call where a medical practitioner recogn.Ized by IMMARBE is available, provided that this period does not exceed 3 months.</p>
                </div>
            </div>
        </div>
        <div class="del_note_sec">
            <p class="del_note_title">IMPORTANT NOTE</p>
            <p class="del_note_details">The original or a certified copy of this certificate must be carried on board in accordance with regulation
                I/2, paragraph 11 of the revised STCW Convention by the seafarer while serving on board of any Belize
                Flag vessel in order to prove that he/she is medically fit to serve in the aforementioned capacity</p>

            <p class="del_foot_title_1">DETAILS OF MEDICAL EXAMINATION</p>
            <p class="del_foot_title_2"><i>(To be completed by examining physician)</i></p>
        </div>
        <p class="del_page">2</p>
    </div>
</div>
</body>
</html>
