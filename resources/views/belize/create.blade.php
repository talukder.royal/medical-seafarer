@extends('home.home_content')
@section('main_content')
    <div class="row" style="height: 100px;">
    </div>
    @include('includes.flash-message')
    <div style="height: 100px;" class="row justify-content-center">
        <div class="col-lg-6">
            <div style="font-weight: bold; font-size: 1.5rem; text-transform: capitalize" class="text-center">Seafarer Medical Certificate</div>
        </div>
    </div>
    <form method="post" action="{{ route('belize.store') }}">
        @csrf
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="last_name"><span class="text-danger">*</span>Last Name:</label>
                    <input type="text" name="last_name" class="form-control" id="last_name">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="given_name"><span class="text-danger">*</span>Given Name:</label>
                    <input type="text" name="given_name" class="form-control" id="given_name">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="form-label" for="position_board">Position On Board:</label>
                    <input class="form-control" type="text" name="position_board" id="position_board">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="place_of_birth">Place of birth(city, country)</label>
                    <input class="form-control" type="text" name="place_of_birth" id="place_of_birth">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="mailing_add">Mailing Address of Applicant(street, city, country):</label>
                    <textarea class="form-control" type="text" name="mailing_add" id="mailing_add"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="dob" ><span class="text-danger">*</span>Date of Birth:(DD/MM/YYYY):</label>
                    <input type="date" name="dob" class="form-control" id="dob">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="gender" ><span class="text-danger">*</span>Sex:(Male/Female):</label>
                    <select class="form-control" name="gender" id="gender">
                        <option>Select option</option>
                        <option value="Male">Male</option>
                        <option value="Female" selected>Female</option>
                        <option value="Others">Others</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="text-center">
                DECLAPATION 0F THE RECOGNIZED MEDICAL PRACTITIONER :
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="confirmation">1. Confirmation that identification documents were checked at the point of examination:</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="confirmation" value="1">
                    <label style="font-size: 20px;" for="confirmation">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="confirmation" value="0">
                    <label for="pregnancy" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="hearing_standard">2.Hearing meets the standards in section A-1 / 9 ?</label>
                    <br>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="hearing_standard" name="hearing_standard" value="1">
                    <label style="font-size: 20px;" for="hearing_standard">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="hearing_standard" name="hearing_standard" value="0">
                    <label for="hearing_standard" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="unaided_statisfactory">3.Unaided hearing stisfactory?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="unaided_statisfactory" name="unaided_statisfactory" value="1">
                    <label style="font-size: 20px;" for="unaided_statisfactory">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="unaided_statisfactory" name="unaided_statisfactory" value="0">
                    <label for="unaided_statisfactory" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="visual_acutity">4.Visual acutity meets standards in section A-1 / 9 ?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="visual_acutity" name="visual_acutity" value="1">
                    <label style="font-size: 20px;" for="visual_acutity">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="visual_acutity" value="0">
                    <label for="visual_acutity" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_vision_standard">5.Colour vision meets standards in section A-1 / 9 ?</label>
                    <br>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="color_vision_standard" name="color_vision_standard" value="1">
                    <label style="font-size: 20px;" for="color_vision_standard">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="color_vision_standard" name="color_vision_standard" value="0">
                    <label for="color_vision_standard" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="color_vision_test">Date of last colour vision test</label>
                    <input type="date" class="form-control" id="color_vision_test" name="color_vision_test">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="vision_standard">6.Are glasses or contact lenses necessary to meet the required vision standards?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="vision_standard_yes" name="vision_standard" value="1">
                    <label style="font-size: 20px;" for="vision_standard_yes">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="vision_standard_no" name="vision_standard" value="0">
                    <label for="vision_standard_no" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="able_watch">7. Able for watchkeeping?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="able_watch_yes" name="able_watch" value="1">
                    <label style="font-size: 20px;" for="able_watch_yes">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="able_watch_no" name="able_watch" value="0">
                    <label for="able_watch_no" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="prescript_medication">8. Is applicant taking any non prescription or prescription medication?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="prescript_medication_yes" name="prescript_medication" value="1">
                    <label style="font-size: 20px;" for="prescript_medication_yes">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="prescript_medication_no" name="prescript_medication" value="0">
                    <label for="prescript_medication_no" style="font-size: 20px;">No</label>
                </div>
                <div class="form-group">
                    <label class="form-label" for="unfit_for_service">9.Is the seafarer free from any medical condition likely to be aggravated by service at sea or the render the seafarer unfit for service or the render the health of any other persons on board ?</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="visual_acutity" name="unfit_for_service" value="1">
                    <label style="font-size: 20px;" for="unfit_for_service">Yes</label>
                    <input type="checkbox" style="width: 25px;
    height: 20px;" id="confirmation" name="unfit_for_service" value="0">
                    <label for="unfit_for_service" style="font-size: 20px;">No</label>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label>
                        10. (He/She) is found to be (Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="1">/ not Fit <input style="width: 25px; height: 20px;" type="checkbox" name="fit" value="0">) for duty as a (without any<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="1"> / with the following<input style="width: 25px; height: 20px;" type="checkbox" name="with" value="0">) restrictions:
                    </label>
                    <br>
                    <label for="line1">Line1</label>
                    <input type="text" name="line1" class="form-control" id="line1">
                    <label for="line2">Line2</label>
                    <input type="text" name="line2" class="form-control" id="line2">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="form-label" for="doe">11.Date of examination/Issue(DD/MM/YYY)</label>
                    <input class="form-control" type="date" name="doe" id="doe">
                </div>
                <div class="form-group">
                    <label class="form-label" for="doexpiry">12.Date of expiry(DD/MM/YYY)</label>
                    <input class="form-control" type="date" name="doexpiry" id="doexpiry">
                    <small>No more than 2 years from the date of examination.</small>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="mb-3">
                <input type="submit" class="btn btn-primary" name="submit" value="Save">
            </div>
        </div>
    </form>
@endsection
