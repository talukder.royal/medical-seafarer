<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/all.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{{ asset('public/js/style.js') }}"></script>
    <title>Download</title>

</head>
<body>
<div class="ms format_rec_all " style="border: none;">
    <div class="all_in ms format_rec_all_in">
        <div class="row_four_rec ms format_rec  clear">
            <img class="ms_rec_img" src="{{asset('public/img/mi.PNG')}}">
            <div class="mi_exam_from">
                <div class="mi_exam_from_left s_float">
                    <p>MEDICAL EXAM FORM</p>
                </div>
{{--                Start of qr code--}}
                <div class="visible-print text-center">
                    @php
                        use SimpleSoftwareIO\QrCode\Facades\QrCode;
                        $path = 'qrcodes/_'.now().$form->id.'.svg';
                        QrCode::generate(route('milashaship_management.index.guest') . '?search=' . $form->id, public_path('qrcodes/_'.now().$form->id.'.svg'));
                    @endphp
                    <img src="{{public_path($path)  }}" width="100" height="100" alt="qrcode">
                    <p>Scan me to return to the original page.</p>
                </div>
{{--                End of qr code--}}
                <div class="mi_exam_from_ritht s_float">
                    <p>MSM – OOV – SM - 161</p>
                </div>
            </div>
            <p class="ms format_rec sea_format_title clear ms_con">CONFIDENTIALFORM</p>
            <p class="ms format_rec sea_format_title clear ms_con_2">Pre-sea Exam
                @if($form->pre_sea_exam == '1')
                <i class="fa fa-check">&#xf14a;</i>
                @else
                <i class="fa fa-check">&#xf096;</i>
                @endif
                Periodic Exam
                @if($form->pre_sea_exam == '0')
                    <i class="fa fa-check">&#xf14a;</i>
                @else
                    <i class="fa fa-check">&#xf096;</i>
                @endif
             </p>
            <div class="ms format_rec sea_format_field">
                <div class="ms format_rec sea_format_field_row">
                    <div class="ms format_rec sea_name s_float">
                        Name (last,  first, middle):
                    </div>
                    <div class="ms format_rec sea_dr_name_value s_float">
                        <p>{{ $form->seafarers_name}}</p>
                    </div>
                </div>
                <div class="ms format_rec sea_format_field_row">
                    <div class="ms format_rec sea_date_of_birth s_float">
                        <div class="ms format_rec sea_date_of_birth_text s_float">
                            Date of birth (day/month/year):
                        </div>
                        <div class="ms format_rec sea_date_of_birth_value s_float">
                            <p>{{ date('d/m/Y', strtotime($form->date_of_birth ?? '' )) }}</p>
                        </div>
                    </div>

                    <div class="ms format_rec sea_gender s_float">
                        <div class="ms format_rec sea_gender_text s_float">
                            Sex:
                        </div>
                        <div class="ms format_rec sea_gender_text_value s_float">
                            @if($form->gender == 'Male')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                                Male
                            @if($form->gender == 'Female')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif <i class="far fa-square"></i>
                                Female
                        </div>
                    </div>
                </div>
                <div class="ms format_rec sea_format_field_row">
                    <div class="ms format_rec sea_home_address s_float">
                        Home Address:
                    </div>
                    <div class="ms format_rec sea_home_address_value s_float">
                        <p>{{ $form->home_address }}</p>
                    </div>
                </div>
                <div class="ms format_rec sea_format_field_row">
                    <div class="ms format_rec sea_home_passport s_float">
                        Passport No /Discharge book No:
                    </div>
                    <div class="ms format_rec sea_passport_value s_float">
                        <p>{{ $form->passport_no }}</p>
                    </div>
                </div>
                <div class="ms format_rec sea_format_field_row">
                    <div class="ms format_rec sea_home_dep s_float">
                        Department:(decck/engine/radio/food handling/other):
                    </div>
                    <div class="ms format_rec sea_dep_value s_float">
                        <p>{{ $form->dept }}</p>
                    </div>
                </div>

                <div class="ms format_rec sea_format_field_row">
                    <div class="ms format_rec sea_routine s_float">
                        Routine and emergenccy, duties(if known):
                    </div>
                    <div class="ms format_rec sea_rank_routine s_float">
                        <p>{{ $form->routine_duties }}</p>
                    </div>
                </div>
                <div class="ms format_rec sea_format_field_row">
                    <div class="ms format_rec sea_type s_float">
                        Type of ship (eg. Bulk carrier, other cargo ships, offshore vessels)
                    </div>
                    <div class="ms format_rec sea_type_value s_float">
                        <p>{{ $form->type_of_ship }}</p>
                    </div>
                </div>
                <div class="ms format_rec sea_format_field_row">
                    <div class="ms format_rec sea_trade s_float">
                        Trade area (e.g coastal, tropical, worldwide):
                    </div>
                    <div class="ms format_rec sea_trade_value s_float">
                        <p>{{ $form->trading_area }}</p>
                    </div>
                </div>


            </div>

        </div>
        <!-- row_four end -->
        <div class="row_five">
            <p class="exam_personal ms format_rec_exam_personal ms_exam ">EXAMINEE's PERSONAL DECLARATION <br>(ASSISTANCE SHOULD BE OFFERED BY MEDICAL STAFF)</p>
            <p style="margin-top: 10px;">Have you ever had any of the following conditions?</p>
            <div class="ms format_rec sea_med_table_one">
                <div class="ms format_rec sea_table_one_row">
                    <div class="ms format_rec sea_table_one_p_1 s_float">
                        <div class=" ms format_rec sea_table_2_row ">
                            <div class="ms format_rec sea_tab_2_num s_float">

                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Condition
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                Yes
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                No
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float ">
                                1
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float  ">
                                Eye / vision problem
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float  ">
                                @if($form->eye_problem == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float  ">
                                @if($form->eye_problem == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row  ">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                2
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                High blood pressure
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->high_blood_pressure == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->high_blood_pressure == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                3
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Heart/vascular disease
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->heart_disease == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->heart_disease == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                4
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Heart surgery
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->heart_surgery == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->heart_surgery == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                5
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Varicose veins
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->varicose_veins == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->varicose_veins == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                6
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Asthma/bronchitis
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->asthma_bronchitis == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->asthma_bronchitis == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                7
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Blood disorder
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->blood_disorder == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->blood_disorder == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                8
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Diabetes
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->diabetes == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->diabetes == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                9
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Thyroid problems
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->thyroid_problem == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->thyroid_problem == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                10
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Digestive disorder
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->digestive_disorder == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->digestive_disorder == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                11
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Kidney problem
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->kidney_problem == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->kidney_problem == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row  ">
                            <div class="  ms format_rec sea_tab_2_num s_float">
                                12
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float  ">
                                Skin problem
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float  ">
                                @if($form->skin_problem == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float  ">
                                @if($form->skin_problem == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                13
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Allergies
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->allergies == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->allergies == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                14
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Infectious/contagious diseases
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->infectious_diseases == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->infectious_diseases == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                15
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Hernia
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->hernia == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->hernia == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                16
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Genital disorders
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->genital_disorder == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->genital_disorder == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                17
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Pregnancy
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->pregnancy == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->pregnancy == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="ms format_rec sea_table_one_p_2 s_float">
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">

                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Condition
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                Yes
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                No
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                18
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Sleeping problem
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->sleep_problem == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->sleep_problem == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="   ms format_rec sea_table_2_row ">
                            <div class="for_red_ex_19 ms format_rec sea_tab_2_num s_float">
                                19
                            </div>
                            <div class="   ms format_rec sea_tab_2_con s_float">
                                Do you smoke?
                            </div>
                            <div class="   ms format_rec sea_tab_2_yes s_float">
                                @if($form->smoke_alcohol_drug == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class=" ms format_rec sea_tab_2_no s_float">
                                @if($form->smoke_alcohol_drug == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                20
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Operation/surgery
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->operation == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->operation == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                21
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Epilepsy/ seizures
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->epilesy_seizures == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->epilesy_seizures == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                22
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Dizziness/fainting
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->dizziness == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->dizziness == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                23
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Loss of consciousness
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->loss_of_consciousness == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->loss_of_consciousness == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                24
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Psychiatric problems
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->psychiatric_problem == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->psychiatric_problem == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                25
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Depression
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->depression == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->depression == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                26
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Attempted suicide
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->attempted_suicide == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->attempted_suicide == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                27
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Loss of memory
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->loss_of_memory == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->loss_of_memory == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                28
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Balance problems
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->balance_problem == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->balance_problem == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                29
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Severe headaches
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->severe_headache == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->severe_headache == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row  ">
                            <div class="ms format_rec sea_tab_2_num s_float ">
                                30
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float  ">
                                Ear nose/throat problems
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float  ">
                                @if($form->hearing_throat_problem == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float  ">
                                @if($form->hearing_throat_problem == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                31
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Restricted mobility
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->restricted_mobility == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->restricted_mobility == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                32
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Back problems
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->joint_problem == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->joint_problem == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                33
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Amputation
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->amputation == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->amputation == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                        <div class="ms format_rec sea_table_2_row">
                            <div class="ms format_rec sea_tab_2_num s_float">
                                34
                            </div>
                            <div class="ms format_rec sea_tab_2_con s_float">
                                Fractures/dislocation
                            </div>
                            <div class="ms format_rec sea_tab_2_yes s_float">
                                @if($form->fracture == '1')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                            <div class="ms format_rec sea_tab_2_no s_float">
                                @if($form->fracture == '0')
                                    <i class="fa fa-check">&#xf14a;</i>
                                @else
                                    <i class="fa fa-check">&#xf096;</i>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mar_rec-row_six">
            <p>If any of the above questions were answered “yes”, please give details</p>
            <p>{{ $form->provide_details_1 ?? '' }}</p>
        </div>
        <div class="ms_foot">
            <div class="ms_foot_row_1">
                <div class="ms_foot_1 s_float">
                    Rev 00
                </div>
                <div class="ms_foot_1 s_float">
                    Date: 01-Apr-2018
                </div>
                <div class="ms_foot_1 s_float">
                    Page 1 of 9
                </div>
            </div>
            <div class="ms_foot_row_2 clear">
                Master copy available with DPA. Controlled copies are in distribution as per distribution list
            </div>
        </div>

    </div>
</div>

<div class="ms  ms format_rec_all " style="border: none;">
    <div class="ms all_in   ms format_rec_all_in">
        <div class="row_four_rec ms  ms format_rec  clear">
            <img class="ms_rec_img" src="{{ asset('public/img/mi.PNG') }}">
            <div class="mi_exam_from">
                <div class="mi_exam_from_left s_float">
                    <p>MEDICAL EXAM FORM</p>
                </div>
                <div class="mi_exam_from_right s_float">
                    <p>MSM – OOV – SM - 161</p>
                </div>
            </div>
            <div class="row_three clear">
                <div class=" ms format_rec sea_med_table_two">
                    <div class=" ms format_rec sea_med_table_two_row">
                        <div class=" ms format_rec sea_med_2_sr s_float">

                        </div>
                        <div class=" ms format_rec sea_med_2_add s_float">
                            <b>Additional question</b>
                        </div>
                        <div class=" ms format_rec sea_med_2_yes s_float">
                            Yes
                        </div>
                        <div class=" ms format_rec sea_med_2_no s_float">
                            NO
                        </div>

                    </div>
                    <div class=" ms format_rec sea_med_table_two_row">
                        <div class=" ms format_rec sea_med_2_sr s_float">
                            35
                        </div>
                        <div class=" ms format_rec sea_med_2_add s_float">
                            Have anyy ever been signed of as sick or repartiated form a ship?
                        </div>
                        <div class=" ms format_rec sea_med_2_yes s_float">
                            @if($form->signed_off_sick == '1')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class=" ms format_rec sea_med_2_no s_float">
                            @if($form->signed_off_sick == '0')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>

                    </div>
                    <div class=" ms format_rec sea_med_table_two_row">
                        <div class=" ms format_rec sea_med_2_sr s_float">
                            36
                        </div>
                        <div class=" ms format_rec sea_med_2_add s_float">
                            Have you ever been hospitalized?
                        </div>
                        <div class=" ms format_rec sea_med_2_yes s_float">
                            @if($form->hospitalized == '1')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class=" ms format_rec sea_med_2_no s_float">
                            @if($form->hospitalized == '0')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>

                    </div>
                    <div class=" ms format_rec sea_med_table_two_row">
                        <div class=" ms format_rec sea_med_2_sr s_float">
                            37
                        </div>
                        <div class=" ms format_rec sea_med_2_add s_float">
                            Have you ever been declared unfit for sea duty?
                        </div>
                        <div class=" ms format_rec sea_med_2_yes s_float">
                            @if($form->declared_for_sea_duty == '1')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class=" ms format_rec sea_med_2_no s_float">
                            @if($form->declared_for_sea_duty == '0')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>

                    </div>
                    <div class=" ms format_rec sea_med_table_two_row">
                        <div class=" ms format_rec sea_med_2_sr s_float">
                            38
                        </div>
                        <div class=" ms format_rec sea_med_2_add s_float">
                            Has your medical certificate ever been restricted or revoked?
                        </div>
                        <div class=" ms format_rec sea_med_2_yes s_float">
                            @if($form->medical_certificate_revoked == '1')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class=" ms format_rec sea_med_2_no s_float">
                            @if($form->medical_certificate_revoked == '0')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>

                    </div>
                    <div class=" ms format_rec sea_med_table_two_row">
                        <div class=" ms format_rec sea_med_2_sr s_float">
                            39
                        </div>
                        <div class=" ms format_rec sea_med_2_add s_float">
                            Are you aware that you have any medical problems, diseases or illness?
                        </div>
                        <div class=" ms format_rec sea_med_2_yes s_float">
                            @if($form->medical_problems == '1')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class=" ms format_rec sea_med_2_no s_float">
                            @if($form->medical_problems == '0')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>

                    </div>
                    <div class=" ms format_rec sea_med_table_two_row">
                        <div class=" ms format_rec sea_med_2_sr s_float">
                            40
                        </div>
                        <div class=" ms format_rec sea_med_2_add s_float">
                            Do you feel healthy and fit to perform the duties of your designed position/occupation?
                        </div>
                        <div class=" ms format_rec sea_med_2_yes s_float">
                            @if($form->feel_healthy == '1')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class=" ms format_rec sea_med_2_no s_float">
                            @if($form->feel_healthy == '0')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>

                    </div>
                    <div class=" ms format_rec sea_med_table_two_row">
                        <div class=" ms format_rec sea_med_2_sr s_float">
                            41
                        </div>
                        <div class=" ms format_rec sea_med_2_add s_float">
                            Are you allergic to any medications?
                        </div>
                        <div class=" ms format_rec sea_med_2_yes s_float">
                            @if($form->allergic_medication == '1')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class=" ms format_rec sea_med_2_no s_float">
                            @if($form->allergic_medication == '0')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>

                    </div>

                </div>
            </div>

            <div class="row_four">
                <p class=" ms format_rec sea_comment"><b>Comments:</b> {{ $form->comments }} </p>
            </div>

            <div class="row_five">
                <div class=" ms format_rec sea_med_table_two_2 clear">

                    <div class=" ms format_rec sea_med_table_two_row" style="border: none;">
                        <div class=" ms format_rec sea_med_2_sr s_float">
                            42
                        </div>
                        <div class=" ms format_rec sea_med_2_add s_float">
                            Are you taking any non-prescription or prescription medications?
                        </div>
                        <div class=" ms format_rec sea_med_2_yes s_float">
                            @if($form->prescription_medication == '1')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                        <div class=" ms format_rec sea_med_2_no s_float">
                            @if($form->prescription_medication == '0')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>

                    </div>
                </div>
            </div>

            <div class="ms row_six clear">
                <p class=" ms format_rec sea_med_s">if yes, please list hte medicaltions taken and the purpose (s) and dosage (s)</p>
                <p class=" ms format_rec sea_med_s">{{ $form->provide_details }}</p>
            </div>
            <div class="row_one">
                <div class=" ms format_rec sea_p_3_fields">
                    <p class=" ms format_rec sea_field_text">I hereby certify that the personal declaration above is a true statement to the best of my knowledge.</p>
                    <div class=" ms format_rec sea_p_f_row">
                        <div class=" ms format_rec sea_p_sign s_float">
                            Singniture of examine:
                        </div>
                        <div class=" ms format_rec sea_p_sign_value s_float">
                            <p></p>
                        </div>
                    </div>

                    <div class=" ms format_rec sea_p_f_row_2 clear">
                        <div class=" ms format_rec sea_p_date s_float">
                            Date (day/month/year):
                        </div>
                        <div class=" ms format_rec sea_p_date_value s_float">
                            <p></p>
                        </div>
                    </div>

                    <div class=" ms format_rec sea_p_f_row_3 clear">
                        <div class=" ms format_rec sea_p_witness s_float">
                            Witnessed by:(Signaure)
                        </div>
                        <div class=" ms format_rec sea_p_witeness_value s_float">
                            <p></p>
                        </div>
                    </div>
                    <div class=" ms format_rec sea_p_f_row_4 clear">
                        <div class=" ms format_rec sea_p_name_print s_float">
                            Name (Typed or printed):
                        </div>
                        <div class=" ms format_rec sea_p__name_print_value s_float">
                            <p></p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="ms_foot ms_foot_2">
                <div class="ms_foot_row_1">
                    <div class="ms_foot_1 s_float">
                        Rev 00
                    </div>
                    <div class="ms_foot_1 s_float">
                        Date: 01-Apr-2018
                    </div>
                    <div class="ms_foot_1 s_float">
                        Page 1 of 9
                    </div>
                </div>
                <div class="ms_foot_row_2 clear">
                    Master copy available with DPA. Controlled copies are in distribution as per distribution list
                </div>
            </div>

        </div>
    </div>

    <div class="ms  ms format_rec_all " style="border: none;">
        <div class="ms all_in   ms format_rec_all_in">
            <div class="row_four_rec ms  ms format_rec  clear">
                <img class="ms_rec_img" src="{{ asset('public/img/mi.PNG') }}">
                <div class="mi_exam_from">
                    <div class="mi_exam_from_left s_float">
                        <p>MEDICAL EXAM FORM</p>
                    </div>
                    <div class="mi_exam_from_right s_float">
                        <p>MSM – OOV – SM - 161</p>
                    </div>
                </div>

            </div>

            <div class="format_rec sea_p_f_row_5 clear">
                <p class="format_rec sea_dr_text">I hereby authorize the release of all my previous medical records from any health professionals,<BR> health, institutions and public authorities to <span class="format_rec sea_dr_det"></span> (the approved medical practitioner).</p>
            </div>
            <div class="format_rec sea_p_f_row">
                <div class="format_rec sea_p_sign s_float">
                    Singniture of examine:
                </div>
                <div class="format_rec sea_p_sign_value s_float">
                    <p></p>
                </div>
            </div>

            <div class="format_rec sea_p_f_row_2 clear">
                <div class="format_rec sea_p_date s_float">
                    Date (day/month/year:
                </div>
                <div class="format_rec sea_p_date_value s_float">
                    <p></p>
                </div>
            </div>

            <div class="format_rec sea_p_f_row_3 clear">
                <div class="format_rec sea_p_witness s_float">
                    Witnessed by:(Signaure)
                </div>
                <div class="format_rec sea_p_witeness_value s_float">
                    <p></p>
                </div>
            </div>
            <div class="format_rec sea_p_f_row_4 clear">
                <div class="format_rec sea_p_name_print s_float">
                    Name (Typed or printed):
                </div>
                <div class="format_rec sea_p__name_print_value s_float">
                    <p></p>
                </div>
            </div>
            <div class="format_rec sea_date_contact">
                <div class="format_rec sea_date_contact_text s_float">
                    Date and ccontact details for previous medial exainatin(if known):
                </div>
                <div class="format_rec sea_date_contact_value s_float">

                </div>
            </div>


            <div class="ms_foot ms_foot_3">
                <div class="ms_foot_row_1">
                    <div class="ms_foot_1 s_float">
                        Rev 00
                    </div>
                    <div class="ms_foot_1 s_float">
                        Date: 01-Apr-2018
                    </div>
                    <div class="ms_foot_1 s_float">
                        Page 1 of 9
                    </div>
                </div>
                <div class="ms_foot_row_2 clear">
                    Master copy available with DPA. Controlled copies are in distribution as per distribution list
                </div>
            </div>
        </div>
    </div>

    <div class="ms  ms format_rec_all " style="border: none;">
        <div class="ms all_in   ms format_rec_all_in" style="width: 17.7cm;">
            <div class="row_four_rec ms  ms format_rec  clear">
                <img class="ms_rec_img" src="{{ asset('public/img/mi.PNG') }}">
                <div class="mi_exam_from">
                    <div class="mi_exam_from_left s_float">
                        <p>MEDICAL EXAM FORM</p>
                    </div>
                    <div class="mi_exam_from_right s_float">
                        <p>MSM – OOV – SM - 161</p>
                    </div>
                </div>

            </div>

            <div class=" ms for_rec row_two sea_3_two clear">
                <p class=" ms for_rec format_rec sea_medial_exam">MEDICAL EXAMINATION</p>
                <p class="ms_sight"><b>Sight</b></p>
                <P class="ms_use">Use of glasses or contact lenses:
                    @if($form->contact_lenses == '1')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    Yes
                    @if($form->contact_lenses == '0')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    No (if yes, specify which type and for what purpose)</P>

                <div class=" ms for_rec  ms for_rec sea_table_three sea_table_three_for">
                    <div class="fr visual_acu_table s_float">
                        <div class="visual_acu_row">
                            <p style="font-weight: bold">Visual acuity</p>
                        </div>
                        <div class="visual_acu_row">
                            <div class="unaid_null s_float">

                            </div>
                            <div class="unaid s_float">
                                Unaided
                            </div>
                            <div class="aid s_float">
                                Aided
                            </div>
                        </div>

                        <div class="visual_acu_row_3">
                            <div class="unaid_null_2 s_float">

                            </div>
                            <div class="unaid s_float unain_ex">
                                <div class="r_eye s_float">
                                    Right Eye
                                </div>
                                <div class="r_eye s_float">
                                    Left  Eye
                                </div>
                                <div class="r_bin s_float">
                                    Binocular
                                </div>
                            </div>
                            <div class="aid s_float">
                                <div class="r_eye s_float">
                                    Right Eye
                                </div>
                                <div class="r_eye s_float">
                                    Left  Eye
                                </div>
                                <div class="r_bin s_float">
                                    Binocular
                                </div>
                            </div>
                        </div>
                        <div class="visual_acu_row_4">
                            <div class="unaid_null_2 s_float">
                                Distant
                            </div>
                            <div class="unaid s_float unain_ex">
                                <div class="r_eye s_float">
                                     {{ $form->right_eye_distant }}
                                </div>
                                <div class="r_eye s_float">
                                    {{ $form->left_eye_distant }}
                                </div>
                                <div class="r_bin s_float">
                                    {{ $form->binocular_eye_distant }}
                                </div>
                            </div>
                            <div class="aid s_float">
                                <div class="r_eye s_float">
                                    {{ $form->right_eye_near }}
                                </div>
                                <div class="r_eye s_float">
                                    {{ $form->right_eye_near }}
                                </div>
                                <div class="r_bin s_float">
                                    {{ $form->right_eye_near }}
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class=" ms for_rec visual_field_table">
                        <div class=" ms for_rec vis_field_table">
                            <div class=" ms for_rec vis_field_row">
                                <b>Visual Field</b>
                            </div>
                            <div class=" ms for_rec vis_field_row">
                                <div class=" ms for_rec vis_f_col s_float">
                                    Eye
                                </div>
                                <div class=" ms for_rec vis_f_col s_float">
                                    Normal
                                </div>
                                <div class=" ms for_rec vis_f_col s_float" style="border: none;">
                                    Defective
                                </div>
                            </div>
                            <div class=" ms for_rec vis_field_row">
                                <div class=" ms for_rec vis_f_col s_float">
                                    Right
                                </div>
                                <div class=" ms for_rec vis_f_col s_float">
                                    @if($form->visual_normal == '1')
                                        <i class=" ms for_rec fa fa-check" aria-hidden="true">&#xf00c;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec vis_f_col s_float" style="border: none;">
                                    @if($form->visual_normal == '0')
                                        <i class=" ms for_rec fa fa-check" aria-hidden="true">&#xf00c;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec vis_field_row">
                                <div class=" ms for_rec vis_f_col s_float">
                                    Left
                                </div>
                                <div class=" ms for_rec vis_f_col s_float">
                                    @if($form->visual_defective == '1')
                                        <i class=" ms for_rec fa fa-check" aria-hidden="true">&#xf00c;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec vis_f_col s_float" style="border: none;">
                                    @if($form->visual_defective == '0')
                                        <i class=" ms for_rec fa fa-check" aria-hidden="true">&#xf00c;</i>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- row two end -->
            <div class=" ms for_rec row_three clear">
                <div class=" ms for_rec format_rec sea_table">
                    <div class=" ms for_rec format_rec sea_color_col s_float">
                        color Vision
                    </div>
                    <div class=" ms for_rec format_rec sea_color_col s_float">
                        @if($form->color_vision == '0')
                            <i class="fa fa-check">&#xf14a;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                         Not Tested
                    </div>
                    <div class=" ms for_rec format_rec sea_color_col s_float">
                        @if($form->color_vision == '1')
                            <i class="fa fa-check">&#xf14a;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Normal
                    </div>
                    <div class=" ms for_rec format_rec sea_color_col s_float">
                        @if($form->color_vision == '2')
                            <i class="fa fa-check">&#xf14a;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Doubtful
                    </div>
                    <div class=" ms for_rec format_rec sea_color_col s_float" style="border: none;">
                        @if($form->color_vision == '3')
                            <i class="fa fa-check">&#xf14a;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                            Defctive
                    </div>

                </div>
            </div>
            <!-- three end -->

            <div class=" ms for_rec row_four clear">
                <div class=" ms for_rec format_rec sea_hearing">
                    <p class="ms_hearing"><b>Hearing</b></p>
                    <div class=" ms for_rec format_rec sea_hear_left s_float">
                        <div class=" ms for_rec format_rec sea_hear_table_one">
                            <div class=" ms for_rec format_rec sea_hear_table_row">
                                Pure tone and audiometry (threshold values in dB)
                            </div>
                            <div class=" ms for_rec format_rec sea_hear_table_row_2">
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">

                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    500Hz
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    1000Hz
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    2000Hz
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    3000Hz
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">

                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float" style="border: none;">

                                </div>
                            </div>

                            <div class=" ms for_rec format_rec sea_hear_table_row_2">
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    Right Ear
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    {{ $form->right_ear_500_hz }}
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    {{ $form->right_ear_1000_hz }}
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    {{ $form->right_ear_2000_hz }}
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    {{ $form->right_ear_3000_hz }}
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float" style="border: none;">

                                </div>
                            </div>

                            <div class=" ms for_rec format_rec sea_hear_table_row_2">
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    Left Ear
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    {{ $form->left_ear_500_hz }}
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    {{ $form->left_ear_1000_hz }}
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    {{ $form->left_ear_2000_hz }}
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                    {{ $form->left_ear_3000_hz }}
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float">
                                </div>
                                <div class=" ms for_rec format_rec sea_hear_col_2 s_float" style="border: none;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" ms for_rec format_rec sea_heear_left s_float">
                        <p>Speech and whisper test (meters)</p>
                        <div class=" ms for_rec format_rec sea_speech_table">
                            <div class=" ms for_rec speech_table_row">
                                <div class=" ms for_rec speech_table_col s_float">

                                </div>
                                <div class=" ms for_rec speech_table_col s_float">
                                    Normal
                                </div>
                                <div class=" ms for_rec speech_table_col s_float" style="border: none;">
                                    Whisper
                                </div>
                            </div>
                            <div class=" ms for_rec speech_table_row">
                                <div class=" ms for_rec speech_table_col s_float">
                                    Right Ear
                                </div>
                                <div class=" ms for_rec speech_table_col s_float">
                                    {{ $form->right_ear_normal }}
                                </div>
                                <div class=" ms for_rec speech_table_col s_float" style="border: none;">
                                    {{ $form->right_ear_whisper }}
                                </div>
                            </div>
                            <div class=" ms for_rec speech_table_row">
                                <div class=" ms for_rec speech_table_col s_float">
                                    Left Ear
                                </div>
                                <div class=" ms for_rec speech_table_col s_float">
                                    {{ $form->left_ear_normal }}
                                </div>
                                <div class=" ms for_rec speech_table_col s_float" style="border: none;">
                                    {{ $form->left_ear_whisper }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- row _four end -->
            <div class=" ms for_rec row_five clear">
                <div class=" ms for_rec format_rec sea_client_find">
                    <div class=" ms for_rec format_rec sea_client_find_row">
                        <div class=" ms for_rec format_rec sea_client_height s_float ms_c_1">
                            Height:<span class=" ms for_rec format_rec sea_h_value">{{ $form->clinical_height }}</span>(cm)
                        </div>
                        <div class=" ms for_rec format_rec sea_client_height s_float ms_c_2">
                            Weight:<span class=" ms for_rec format_rec sea_h_value">{{$form->clinical_weight }}</span>(Kg)
                        </div>
                        <div class=" ms for_rec format_rec sea_client_height s_float ms_c_3">
                            Pulse rate:<span class=" ms for_rec format_rec sea_h_value">{{$form->clinical_pulse_rate }}</span>(/Minute)
                        </div>
                        <div class=" ms for_rec format_rec sea_client_height s_float ms_c_4">
                            Rhythm:<span class=" ms for_rec format_rec sea_h_value">{{$form->clinical_rhythm }}</span>(Kg)
                        </div>
                    </div>

                    <div class=" ms for_rec format_rec sea_client_find_row_3 clear">
                        <div class=" ms for_rec format_rec sea_client_blood s_float ms_c_5">
                            Blood Pressure:  systolic: <span class=" ms for_rec format_rec sea_h_value ms_b_value_1">{{$form->clinical_b_pressure }}</span> (mmHg) Diastolic:  <span class=" ms for_rec format_rec sea_h_value ms_b_value_2">{{ $form->clinical_diastolic }}</span> mmHg
                        </div>
                    </div>
                </div>
            </div>

            <div class=" ms for_rec row_one">
                <div class=" ms for_rec sea_med_4_table_one">
                    <div class=" ms for_rec sea_table_one_row">
                        <div class=" ms for_rec sea_table_one_p_1 s_float">
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">

                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    Normal
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    Abnormal
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Head
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->head == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->head == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Sinuses, nose, throat
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->sinus_nose_throat == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->sinus_nose_throat == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Mouth/teeth
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->mouth_teeth == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->mouth_teeth == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Ears (general)
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->ears == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->ears == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Tympanic membrane
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->tympanic_member == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->tympanic_member == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Eyes
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->eyes == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->eyes == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Ophthalmoscopy
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->ophthalmoscopy == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->ophthalmoscopy == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Pupils
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->pupils == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->pupils == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Eye movement
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->eye_environment == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->eye_environment == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Lungs and chest
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->lungs_chest == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->lungs_chest == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Breast examination
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->breast_examination == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->breast_examination == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Heart
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->heart == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->heart == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec chest_xray s_float">
                                    chest X-ray
                                </div>
                                <div class=" ms for_rec chest_xray_value s_float">
                                    @if($form->chest_xray == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                        @endif
                                        Not Performed
                                </div>
                            </div>
                        </div>

                        <div class=" ms for_rec sea_table_one_p_2 s_float">
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">

                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    Normal
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    Abnormal
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Skin
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->skin == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->skin == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Varicose venis
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->varicose_vein == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->varicose_vein == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Vascular (inc. Pedal pulses)
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->vascular_pulse == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->vascular_pulse == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Abdomen and viscera
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->abdomen_viscera == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->abdomen_viscera == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Hernias
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->hernia == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->hernia == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Anus (not rectal exam.)
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->anus == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->anus == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    G-U system
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->g_u_system == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->g_u_system == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Upper and lower extremities
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->upper_lower_ext == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->upper_lower_ext == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Spine (C/S, T/S and L/S)
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->spine == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->spine == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Neurologic (full brief)
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->neurologic == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->neurologic == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    Psychiatric
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->psychiatric == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->psychiatric == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec sea_tab_2_con s_float">
                                    General appearance
                                </div>
                                <div class=" ms for_rec sea_tab_2_yes s_float">
                                    @if($form->general_appearance == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                                <div class=" ms for_rec sea_tab_2_no s_float">
                                    @if($form->general_appearance == '0')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                </div>
                            </div>
                            <div class=" ms for_rec sea_table_2_row">

                                <div class=" ms for_rec performed_date s_float" style="border: none;">
                                    @if($form->chest_xray == '1')
                                        <i class="fa fa-check">&#xf14a;</i>
                                    @else
                                        <i class="fa fa-check">&#xf096;</i>
                                    @endif
                                        Performed (day/month/year)
                                </div>
                                <div class=" ms for_rec performed_date_vale s_float">
                                    <p>{{ date('d/m/y', strtotime($form->date_of_performed_on ?? '')) }}</p>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>


            <div class="ms_foot ms_foot_4">
                <div class="ms_foot_row_1">
                    <div class="ms_foot_1 s_float">
                        Rev 00
                    </div>
                    <div class="ms_foot_1 s_float">
                        Date: 01-Apr-2018
                    </div>
                    <div class="ms_foot_1 s_float">
                        Page 1 of 9
                    </div>
                </div>
                <div class="ms_foot_row_2 clear">
                    Master copy available with DPA. Controlled copies are in distribution as per distribution list
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ms_6 ms  ms ms ms format_rec_all " style="border: none;">
    <div class="ms_6 ms all_in   ms ms ms format_rec_all_in">
        <div class="ms_6 row_four_rec ms  ms ms ms format_rec  clear">
            <img class="ms_6 ms_rec_img" src="{{ asset('public/img/mi.PNG') }}">
            <div class="ms_6 mi_exam_from">
                <div class="ms_6 mi_exam_from_left s_float">
                    <p>MEDICAL EXAM FORM</p>
                </div>
                <div class="ms_6 mi_exam_from_right s_float">
                    <p>MSM – OOV – SM - 161</p>
                </div>
            </div>

        </div>

        <div class="ms_6 hal_full clear">
            <div class="ms_6 hul_row_1">
                <h4 class="ms_6 hul_title_1">SEAFARER’S MEDICAL EXAMINATION REPORT/CERTIFICATE</h4>
                <h4 class="ms_6 hul_title_2">CONFIDENTIAL DOCUMENT</h4>
                <p class="ms_6 hul_cer_issue_details">This certificate is issued by authority of the Maritime Administrator and in compliance with the requirements of the Medical Examination (Seafarers) Convention 1946 (ILO No. 73), as amended, STCW Convention, 1978 as amended and the Maritime Labour Convention, 2006.</p>
            </div>
            <div class="ms_6 hal_row_2">
                <div class="ms_6 hol_s_name s_float">
                    SURNAME
                </div>
                <div class="ms_6 hol_g_name s_float" style="border: none;">
                    GIVEN NAMES(S)
                </div>
            </div>
            <div class="ms_6 hal_row_3">
                <div class="ms_6 hol_nat s_float ">
                    <p>NATIONALITY</p>
                    <p>{{ $form->nationality }}</p>
                </div>
                <div class="ms_6 hol_id_doc s_float" style="border: none;">
                    <p>ID DOCUMENT NO:</p>
                    <p class="ms_6 hol_id_doc_value">{{ $form->documents->document_no }}</p>
                </div>
            </div>
            <div class="ms_6 hol_row_4">
                <div class="ms_6 hol_date_of_birth s_float">
                    <p>DATE OF BIRTH</p>
                    @php
                        $date = \Carbon\Carbon::parse($form->date_of_birth);
                    @endphp
                    <div class="ms_6 hot_date_d_m">MONTH<span class="ms_6 hol_m_value">{{ $date->month }}</span> Day<span class="ms_6 hol_d_value">{{ $date->day }}</span> YEAR<span class="ms_6 hol_year_value">{{ $date->year }}</span></div>
                </div>
                <div class="ms_6 hol_place_of_birth s_float">
                    <p>PLACE OF BIRTH</p>
                    <div class="ms_6 hot_place_of_birth">CITY<span class="ms_6 hol_c_value">{{ $form->documents->city }}</span> COUNTRY <span class="ms_6 hol_c_value">{{ $form->documents->country }}</span></div>
                </div>
                <div class="ms_6 hol_sex s_float">
                    <p>SEX</p>
                    <p class="ms_6 hol_m_f">
                        @if($form->gender == 'Male')
                            <i class="fa fa-check">&#xf14a;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif MALE <span class="ms_6 ">
                        @if($form->gender == 'Female')
                            <i class="fa fa-check">&#xf14a;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        FEMALE</span></p>
                </div>

            </div>
            <div class="ms_6 hol_row_5">
                <div class="ms_6 hol_exam_duty s_float">
                    <p class="ms_6 hol_exm_duty_title">EXAMINATION FOR DUTY AS:</p>
                    <div class="ms_6 m_c_col_three_row">
                        <div class="ms_6 ms_position_hol s_float">
                            MASTER
                        </div>
                        <div class="ms_6 ms_position_hol_value s_float">
                            @if($form->documents->examination_as_duty == '0')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                    <div class="ms_6 m_c_col_three_row">
                        <div class="ms_6 ms_position_hol s_float">
                            DECK OFFICER
                        </div>
                        <div class="ms_6 ms_position_hol_value s_float">
                            @if($form->documents->examination_as_duty == '1')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                    <div class="ms_6 m_c_col_three_row">
                        <div class="ms_6 ms_position_hol s_float">
                            ENGINEERING OFFICER
                        </div>
                        <div class="ms_6 ms_position_hol_value s_float">
                            @if($form->documents->examination_as_duty == '2')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                    <div class="ms_6 m_c_col_three_row">
                        <div class="ms_6 ms_position_hol s_float">
                            RADIO OPERATOR
                        </div>
                        <div class="ms_6 ms_position_hol_value s_float">
                            @if($form->documents->examination_as_duty == '3')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                    <div class="ms_6 m_c_col_three_row">
                        <div class="ms_6 ms_position_hol s_float">
                            RATING
                        </div>
                        <div class="ms_6 ms_position_hol_value s_float">
                            @if($form->documents->examination_as_duty == '4')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="ms_6 hol_mail s_float">
                    <p>MAILING ADDRESS OF APPLICANT:</p>
                    <p>{{ $form->documents->mailing_address }}</p>
                </div>
            </div>
            <div class="ms_6 hol_row_6 clear">
                <p>DECLARATION OF APPROVED MEDICAL PRACTIONER:<span></span></p>
                <p class="ms_6 hol_confir_check">I CONFIRM THAT IDENTIFICATION DOCUMENTS WERE CHECKED:<span>
                        @if($form->documents->identi_documents == '1')
                            <i class="fa fa-check">&#xf14a;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        Yes
                         @if($form->documents->identi_documents == '0')
                            <i class="fa fa-check">&#xf14a;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                    No</span></p>
            </div>
            <div class="ms_6 hol_row_7">
                <p><span style="font-size: 12px; font-weight: bold;">MEDICAL EXAMINATION</span> (SEE LAST PAGE FOR MEDICAL REQUIREMENTS) STATE DETAILS ON REVERSE SIDE</p>
            </div>

            <div class="ms_6 hol_row_8 clear">
                <div class="ms_6 hol_height s_float">
                    <p>HEIGHT</p>
                </div>
                <div class="ms_6 hol_weight s_float">
                    <p>WEIGHT</p>
                </div>
                <div class="ms_6 hol_bloor s_float">
                    <p>BLOOD PRESSURE</p>
                </div>
                <div class="ms_6 hol_pulse s_float">
                    <p>PULSE</p>
                </div>
                <div class="ms_6 hol_respi s_float">
                    <p>RESPIRATION</p>
                </div>
                <div class="ms_6 hol_general s_float">
                    <p>GENERAL APPEARANCE</p>
                </div>
            </div>
            <div class="ms_6 hol_row_9">
                <div class="ms_6 hol_eye s_float">
                    <div class="ms_6 hol_vision_row">
                        <div class="ms_6 hol_vison s_float">
                            VISION
                        </div>
                        <div class="ms_6 hol_ritht_eye s_float">
                            RIGHT EYE
                        </div>
                        <div class="ms_6 hol_left_eye s_float">
                            LEFT EYE
                        </div>
                    </div>
                    <div class="ms_6 hol_vision_row">
                        <div class="ms_6 hol_vison s_float">
                            <div class="ms_6 vision_without_glass">
                                WITHOUT GLASSES
                            </div>
                        </div>
                        <div class="ms_6 hol_ritht_eye s_float">
                            <div class="ms_6 vision_without_glass_right">
                                <p>{{ $form->documents->right_eye }}</p>
                            </div>
                        </div>
                        <div class="ms_6 hol_left_eye s_float">
                            <div class="ms_6 vision_without_glass_left">
                                <p>{{ $form->documents->left_eye }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="ms_6 hol_vision_row">
                        <div class="ms_6 hol_vison s_float">
                            <div class="ms_6 vision_without_glass">
                                WITH GLASSES
                            </div>
                        </div>
                        <div class="ms_6 hol_ritht_eye s_float">
                            <div class="ms_6 vision_without_glass_right">
                                <p>{{ $form->documents->with_g_right_eye }}</p>
                            </div>
                        </div>
                        <div class="ms_6 hol_left_eye s_float">
                            <div class="ms_6 vision_without_glass_left">
                                <p>{{ $form->documents->with_g_left_eye }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ms_6 hol_hearing s_float">
                    <p class="ms_6 hol_hear_title">HEARING</p>
                    <p class="ms_6 hol_hear_value">RT. EAR <span class="ms_6 hol_r_l_ear_value">{{ $form->documents->hering_right_ear }}</span> LEFT EAR <span class="ms_6 hol_r_l_ear_value">{{ $form->documents->hering_left_ear }}</span></p>
                </div>
            </div>

            <div class="ms_6 hol_row_10">
                <p class="ms_6 color_t_type"><b style="font-size: 11px;">COLOR TEST TYPE:
                        @if($form->color_test_type == '1')
                            <i class="fa fa-check">&#xf14a;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif BOOK
                        @if($form->color_test_type == '0')
                            <i class="fa fa-check">&#xf14a;</i>
                        @else
                            <i class="fa fa-check">&#xf096;</i>
                        @endif
                        LANTERN</b> CHECK IF COLOR TEST IS NORMAL <b style="font-size: 11px;">
                            YELLOW
                        <span class="ms_6 hol_col_icon_bor">
                              @if($form->test_normal == '0')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </span>
                            RED
                        <span class="ms_6 hol_col_icon_bor">
                            @if($form->test_normal == '1')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </span>
                            GREEN
                        <span class="ms_6 hol_col_icon_bor">
                            @if($form->test_normal == '2')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </span>
                            BLUE
                        <span class="ms_6 hol_col_icon_bor">
                            @if($form->test_normal == '3')
                                <i class="fa fa-check">&#xf14a;</i>
                            @else
                                <i class="fa fa-check">&#xf096;</i>
                            @endif
                        </span>
                         </b></p>

                <p class="ms_6 hol_date_last_col_t">DATE OF LAST COLOR VISION TEST: <span class="ms_6 hol_c_t_value">{{ date('d/m/Y/', strtotime($form->documents->date_of_vision_test ?? '')) }}</span></p>
            </div>
            <div class="ms_6  ms_10">
                <p>ARE GLASSES OR CONTACT LENSES NECESSARY TO MEET THE REQUIRED VISION STANDARD?
                    YES
                    @if($form->vision_standard == '1')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    No
                    @if($form->vision_standard == '0')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                </p>
            </div>

            <div class="ms_6 hol_row_11">
                <div class="ms_6 hol_head_neck s_float">
                    <p>HEAD AND NECK</p>
                </div>
                <div class="ms_6 hol_heart s_float">
                    <p>HEART (CARDIOVASCULAR)</p>
                </div>

            </div>
            <div class="ms_6 hol_row_12 clear">
                <div class="ms_6 hol_head_neck s_float">
                    <p>LUNGS</p>
                </div>
                <div class="ms_6 hol_heart s_float">
                    <p style="font-size: 12px; font-weight: bold;">SPEECH <span style="font-size: 9px;">(DECK/NAVIGATIONAL OFFICER AND RADIO OFFICER)</span></p>
                    <p style="font-size: 8px; font-weight: normal;">IS SPEECH UNIMPAIRED FOR NORMAL VOICE COMMUNICATION?</p>
                </div>

            </div>

            <div class="ms_6 hol_row_13">
                <p class="ms_6 hol_ext">EXTREMITIES:</p>
                <p class="ms_6 hol_ext_value">UPPERt: <span class="ms_6 hol_ext_u_value">{{ $form->documents->extremities_upper }}</span> LOWER<span class="ms_6 hol_ext_u_value">{{ $form->documents->extremities_lower }}</span></p>
            </div>

            <div class="ms_6 hol_row_14">
                IS APPLICANT VACCINATED IN ACCORDANCE WITH WHO REQUIREMENTS?
                <span class="ms_6 hol_app_y_n">
                       @if($form->vaccinated_requirement == '1')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    YES
                </span>
                <span class="ms_6 hol_app_y_n">
                      @if($form->vaccinated_requirement == '0')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    NO
                </span>
            </div>
            <div class="ms_6 hol_row_15">
                <p>S APPLICANT SUFFERING FROM ANY DISEASE LIKELY TO BE AGGRAVATED BY WORKING ABOARD A VESSEL, OR TO RENDER HIM/HER UNFIT FOR SERVICE AT SEA OR LIKELY TO ENDANGER THE HEALTH OF OTHER PERSONS ON BOARD?</p>
            </div>
            <div class="ms_6 hol_row_14">
                IS APPLICANT TAKING ANY NON-PRESCRIPTION OR PRESCRIPTION MEDICATIONS?
                <span class="ms_6 hol_app_y_n">
                    @if($form->is_prescirption_medication == '1')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    YES
                </span>
                <span class="ms_6 hol_app_y_n">
                   @if($form->is_prescirption_medication == '0')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    NO
                </span>
            </div>
            <div class="ms_6 hol_row_16">
                <div class="ms_6 hol_sign s_float">
                    <p></p>
                    <p class="ms_6 hol_sign_text">SIGNATURE OF APPLICANT</p>
                </div>
                <div class="ms_6 hol_sign_date s_float">
                    <p></p>
                    <p class="ms_6 hol_date_text">DATE</p>
                </div>
            </div>

            <div class="ms_6 ms_foot ms_foot_6">
                <div class="ms_6 ms_foot_row_1">
                    <div class="ms_6 ms_foot_1 s_float">
                        Rev 00
                    </div>
                    <div class="ms_6 ms_foot_1 s_float">
                        Date: 01-Apr-2018
                    </div>
                    <div class="ms_6 ms_foot_1 s_float">
                        Page 1 of 9
                    </div>
                </div>
                <div class="ms_6 ms_foot_row_2 clear">
                    Master copy available with DPA. Controlled copies are in distribution as per distribution list
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ms_6 ms  ms format_rec_all " style="border: none;">
    <div class="ms_6 ms all_in   ms format_rec_all_in">
        <div class="ms_6 row_four_rec ms  ms format_rec  clear">
            <img class="ms_6 ms_rec_img" src="{{ asset('public/img/mi.PNG') }}">
            <div class="ms_6 mi_exam_from">
                <div class="ms_6 mi_exam_from_left s_float">
                    <p>MEDICAL EXAM FORM</p>
                </div>
                <div class="ms_6 mi_exam_from_right s_float">
                    <p>MSM – OOV – SM - 161</p>
                </div>
            </div>

        </div>

        <div class="ms_6 hal_full_2 clear">
            <div class="ms_6 hol_row_17">
                <p>THIS SIGNATURE SHOULD BE AFFIXED IN THE PRESENCE OF THE EXAMINING PHYSICIAN.</p>
            </div>
            <div class="ms_6 hol_row_18">
                <div class="ms_6 hol_p_2_cer s_float">
                    <p>THIS IS TO CERTIFY THAT A PHYSICAL EXAMINATION WAS GIVEN TO:</p>
                </div>
                <div class="ms_6 hol_name_of_apliant s_float">
                    <p></p>
                    <p class="ms_6 hol_name_of_apliant_text">NAME OF APPLICANT</p>
                </div>
            </div>
            <div class="ms_6 hol_row_19">
                <p>THIS APPLICANT IS CERTIFIED FREE OF COMMUNICABLE DISEASE <span class="ms_6 hol_app_yes">YES <i class="ms_6 far fa-square"></i></span> <span class="ms_6 hol_app_no">YES <i class="ms_6 far fa-square"></i></span></p>
            </div>
            <div class="ms_6 hol_row_20">
                <p>IRCLE APPROPRIATE CHOICE: (
                    @if($form->gender == 'Male')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    HE /
                    @if($form->gender == 'Female')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    SHE) IS FOUND TO BE (
                    @if($form->documents->fit == '1')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    FIT /
                    @if($form->documents->fit == '0')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    NOT FIT) FOR DUTY AS A (
                    @if($form->documents->examination_as_duty == '0')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    MASTER /
                    @if($form->documents->examination_as_duty == '1')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    DECK OFFICER /
                    @if($form->documents->examination_as_duty == '2')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    ENGINEERING OFFICER /
                    @if($form->documents->examination_as_duty == '3')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    RADIO OFFICER /
                    @if($form->documents->examination_as_duty == '4')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    RATING) (
                    @if($form->documents->with == '0')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    WITHOUT ANY /
                    @if($form->documents->with == '1')
                        <i class="fa fa-check">&#xf14a;</i>
                    @else
                        <i class="fa fa-check">&#xf096;</i>
                    @endif
                    WITH THE FOLLOWING) RESTRICTIONS: {{ $form->documents->restriction }}</p>
            </div>
            <div class="ms_6 hol_row_21">
                <div class="ms_6 hol_row_same">
                    <p>NAME AND DEGREE OF PHYSICIAN <span class="ms_6 hol_p_2_n_d"><b>DR. SABRINA MOSTAFA. MBBS. (D.U),</b></span></p>
                </div>
                <div class="ms_6 hol_row_same">
                    <p>ADDRESS <span class="ms_6 hol_p_2_address"><b>126, SHEIKH MUJIB ROAD, CHOWMUHANI, CHATTOGRAM.</b></span></p>
                </div>
                <div class="ms_6 hol_row_same">
                    <p>NAME OF PHYSICIAN’S CERTIFICATING AUTHORITY <span class="ms_6 hol_p_2_phy"><b> BMDC, DHAKA. REG NO A-68208</b></span></p>
                </div>
                <div class="ms_6 hol_row_same">
                    <p>DATE OF ISSUE OF PHYSICIAN’S CERTIFICATE <span class="ms_6 hol_p_2_date_iss">08. JUNE. 2014</span></p>
                </div>
                <div class="ms_6 hol_row_same">
                    <p>SIGNATURE / STAMP OF PHYSICIAN : <span class="ms_6 hol_p_2_stamp"></span></p>
                </div>
                <div class="ms_6 hol_row_same">
                    <p>DATE OF EXAMINATION: <span class="ms_6 hol_p_2_d_exam"></span></p>
                </div>
                <div class="ms_6 hol_row_same">
                    <p>EXPIRY DATE OF CERTIFICATE : <span class="ms_6 hol_p_2_d_exp"></span></p>
                </div>
            </div>

            <div class="ms_6 hol_row_22">
                <p class="ms_6 hol_ack">SEAFARER ACKNOWLEDGMENT</p>
                <p class="ms_6 hol_ack_details">I <span class="ms_6 hol_ack_name"></span>(NAME OF SEAFARER), CONFIRM THAT I HAVE BEEN INFORMED OF
                    THE CONTENT OF CERTIFICATE AND THE RIGHT TO GET A REVIEW</p>
            </div>

            <div class="ms_6 ms_foot ms_foot_7">
                <div class="ms_6 ms_foot_row_1">
                    <div class="ms_6 ms_foot_1 s_float">
                        Rev 00
                    </div>
                    <div class="ms_6 ms_foot_1 s_float">
                        Date: 01-Apr-2018
                    </div>
                    <div class="ms_6 ms_foot_1 s_float">
                        Page 1 of 9
                    </div>
                </div>
                <div class="ms_6 ms_foot_row_2 clear">
                    Master copy available with DPA. Controlled copies are in distribution as per distribution list
                </div>
            </div>
        </div>
    </div>
</div>
{{--last--}}
<div class="ms_6 ms_6 ms  ms ms ms format_rec_all " style="border: none;">
    <div class="ms_6 ms_6 ms all_in   ms ms ms format_rec_all_in">
        <div class="ms_6 ms_6 row_four_rec ms  ms ms ms format_rec  clear">
            <img class="ms_6 ms_6 ms_rec_img" src="{{ asset('public/img/mi.PNG') }}">
            <div class="ms_6 ms_6 mi_exam_from">
                <div class="ms_6 ms_6 mi_exam_from_left s_float">
                    <p>MEDICAL EXAM FORM</p>
                </div>
                <div class="ms_6 ms_6 mi_exam_from_right s_float">
                    <p>MSM – OOV – SM - 161</p>
                </div>
            </div>

        </div>
        <div class="ms_6 hal_full_p3 clear">
            <div class="ms_6 hal_p_sec_1">
                <div class="ms_6 hol_p_3_med_req">
                    <h6 class="ms_6 hol_med_req_title">MEDICAL REQUIREMENTS</h6>
                    <p class="ms_6 hol_med_req_details">All applicants for an officer certificate, Seafarer's Identification and Record Book or certification of special qualifications shall be required to have a physical examination reported on this Medical Form completed by a certificated physician. The completed medical form must accompany the application for officer certificate, application for seafarer's identity document, or application for certification of special qualifications. This physical examination must be carried out not more than 12 months prior to the date of making application for an officercertificate, certification of special qualifications or a seafarer's book. The examination shall be conducted in accordance with theInternational Labor Organization World Health Organization,<i> Guidelines for Conducting Pre-sea and Periodic Medical FitnessExaminations for Seafarers (ILO/WHO/D.2/1997.</i> Such proof of examination must establish that the applicant is in satisfactoryphysical and mental condition for the specific duty assignment undertaken and is generally in possession of all body faculties necessary in fulfilling the requirements of the seafaring profession.</p>

                    <div class="ms_6 hol_p_3_row_2">
                        <p>In conducting the examination, the certified physician should, where appropriate, examine the seafarer’s previous medical records (including vaccinations) and information on occupational history, noting any diseases, including alcohol or drug-related problems and/or injuries. In addition, the following minimum requirements shall apply:</p>
                    </div>

                    <div class="ms_6 hol_p_3_no">
                        <p><span class="ms_6 no_a">(a)</span> Hearing</p>
                        <div style="margin: 0; padding: 0;">
                            {{--                            <span style="bottom:10px;"> <i class="fa fa-check" style="font-size: 5px;">&#xf111;</i></span>--}}
                            <div style="margin-top: 0;">
                                <img style="margin-left: 60px;" src="{{ asset('public/img/dot.svg') }}">
                                <p style="margin-top: -20px; margin-left: 80px;">
                                    All applicants must have hearing unimpaired for normal sounds and
                                    be capable of hearing a whispered voice in better ear at
                                    15 feet (4.57 m) and in poorer ear at 5 feet (1.52 m).
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="ms_6 hol_p_3_no">
                        <p><span class="ms_6 no_a">(b)</span> Eyesight</p>
                        <div style="margin: 0; padding: 0;">
                            {{--                            <span style="bottom:10px;"> <i class="fa fa-check" style="font-size: 5px;">&#xf111;</i></span>--}}
                            <div style="margin-top: 0;">
                                <img style="margin-left: 60px;" src="{{ asset('public/img/dot.svg') }}">
                                <p style="margin-top: -20px; margin-left: 80px;">
                                    Deck officer applicants must have (either with or without glasses)
                                    at least 20/20(1.00) vision in one eye and at least 20/40 (0.50)in the other.
                                    If the applicant wears glasses, he must have vision without glasses of at
                                    least 20/160 (0.13) in both eyes. Deck officer applicants must
                                    also have normal color perception and be capable of
                                    distinguishing the colors red, green, blue and yellow.
                                </p>
                            </div>
                        </div>
                        <div style="margin: 0; padding: 0;">
                            {{--                            <span style="bottom:10px;"> <i class="fa fa-check" style="font-size: 5px;">&#xf111;</i></span>--}}
                            <div style="margin-top: 0;">
                                <img style="margin-left: 60px;" src="{{ asset('public/img/dot.svg') }}">
                                <p style="margin-top: -20px; margin-left: 80px;">
                                    Engineer and radio officer applicants must have
                                    (either with or without glasses) at least 20/30 (0.63)
                                    vision in one eye and at least 20/50 (0.40) in the other.
                                    If the applicant wears glasses, he must have vision without glasses of
                                    at least 20/200 (0.10) in both eyes. Engineer and radio
                                    officer applicants must also be able to perceive the colors red,
                                    yellow and green.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="ms_6 hol_p_3_no">
                        <p><span class="ms_6 no_a">(c)</span> Dental</p>
                        <div style="margin: 0; padding: 0;">
                            {{--                            <span style="bottom:10px;"> <i class="fa fa-check" style="font-size: 5px;">&#xf111;</i></span>--}}
                            <div style="margin-top: 0;">
                                <img style="margin-left: 60px;" src="{{ asset('public/img/dot.svg') }}">
                                <p style="margin-top: -20px; margin-left: 80px;">
                                    Seafarers must be free from infections of the mouth cavity or gums.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="ms_6 hol_p_3_no">
                        <p><span class="ms_6 no_a">(d)</span> Blood Pressure</p>
                        <div style="margin: 0; padding: 0;">
                            {{--                            <span style="bottom:10px;"> <i class="fa fa-check" style="font-size: 5px;">&#xf111;</i></span>--}}
                            <div style="margin-top: 0;">
                                <img style="margin-left: 60px;" src="{{ asset('public/img/dot.svg') }}">
                                <p style="margin-top: -20px; margin-left: 80px;">
                                    An applicant's blood pressure must fall within an average range,
                                    taking age into consideration.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="ms_6 hol_p_3_no">
                        <p><span class="ms_6 no_a">(e)</span> Voice</p>
                        <div style="margin: 0; padding: 0;">
                            {{--                            <span style="bottom:10px;"> <i class="fa fa-check" style="font-size: 5px;">&#xf111;</i></span>--}}
                            <div style="margin-top: 0;">
                                <img style="margin-left: 60px;" src="{{ asset('public/img/dot.svg') }}">
                                <p style="margin-top: -20px; margin-left: 80px;">
                                    Deck/Navigational officer applicants and Radio officer applicants must have
                                    speech which is unimpaired for normal voice communication.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="ms_6 hol_p_3_no">
                        <p><span class="ms_6 no_a">(f)</span> Vaccinations</p>
                        <div style="margin: 0; padding: 0;">
                            {{--                            <span style="bottom:10px;"> <i class="fa fa-check" style="font-size: 5px;">&#xf111;</i></span>--}}
                            <div style="margin-top: 0;">
                                <img style="margin-left: 60px;" src="{{ asset('public/img/dot.svg') }}">
                                <p style="margin-top: -20px; margin-left: 80px;">
                                    All applicants shall be vaccinated according to the requirements indicated in the WHO publication, International Travel and Health, Vaccination Requirements and Health Advice, and shall be given advice by the certified physician on immunizations. If new vaccinations are given, these shall be recorded.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="ms_6 hol_p_3_no">
                        <p><span class="ms_6 no_a">(g)</span> Diseases or Conditions</p>
                        <div style="margin: 0; padding: 0;">
                            {{--                            <span style="bottom:10px;"> <i class="fa fa-check" style="font-size: 5px;">&#xf111;</i></span>--}}
                            <div style="margin-top: 0;">
                                <img style="margin-left: 60px;" src="{{ asset('public/img/dot.svg') }}">
                                <p style="margin-top: -20px; margin-left: 80px;">
                                    Applicants afflicted with any of the following diseases or conditions shall be disqualified: epilepsy, insanity, senility, alcoholism, tuberculosis, acenereal disease or neurosyphilis, AIDS, and/or the use of narcotics. Applicants diagnosed with, suspected of, or exposed to any communicable disease transmittable by food shall be restricted from working with food or in food –related areas until symptom-free for at least 48 hours.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="ms_6 hol_p_3_no">
                        <p><span class="ms_6 no_a">(h)</span> Physical Requirements</p>
                        <div style="margin: 0; padding: 0;">
{{--                            <span style="bottom:10px;"> <i class="fa fa-check" style="font-size: 5px;">&#xf111;</i></span>--}}
                            <div style="margin-top: 0;">
                                <img style="margin-left: 60px;" src="{{ asset('public/img/dot.svg') }}">
                                <p style="margin-top: -20px; margin-left: 80px;">
                                    Applicants for able seaman, bosun, GP-1, ordinary seaman and junior ordinary seaman must meet the physical requirements for a deck/navigational officer's certificate.
                                </p>
                            </div>
                        </div>
                        <div style="margin: 0; padding: 0;">
                            {{--                            <span style="bottom:10px;"> <i class="fa fa-check" style="font-size: 5px;">&#xf111;</i></span>--}}
                            <div style="margin-top: 0;">
                                <img style="margin-left: 60px;" src="{{ asset('public/img/dot.svg') }}">
                                <p style="margin-top: -20px; margin-left: 80px;">
                                    Applicants for fireman/watertender, oiler/motorman, pumpman, electrician, wiper, tankerman and survival craft/rescue boat crewman must meet the physical requirements for an engineer officer's certificate.
                                </p>
                            </div>
                        </div>

{{--                        <ul class="ms_6 hol_p_3_3_ul">--}}
{{--                            <li>Applicants for able seaman, bosun, GP-1, ordinary seaman and junior ordinary seaman must meet the physical requirements for a deck/navigational officer's certificate.</li>--}}
{{--                            <li>Applicants for fireman/watertender, oiler/motorman, pumpman, electrician, wiper, tankerman and survival craft/rescue boat crewman must meet the physical requirements for an engineer officer's certificate.</li>--}}
{{--                        </ul>--}}
                    </div>


                </div>

            </div>
            <div class="ms_6 hal_p_sec_2">
                <h6 class="ms_6 hol_p2_not_title">IMPORTANT NOTE:</h6>
                <p>The seafarer must retain the original of the ‘Medical Examination Report/Certificate’ as evidence of physical qualification while serving on board a vessel.</p>
                <p>An applicant who has been refused a medical certificate or has had a limitation imposed on his/her ability to work, shall be given the opportunity to have an additional examination by another medical practitioner or medical referee who is independent of the shipowner or of any organization of shipowners or seafarers.</p>
                <p>Medical examination reports shall be marked as and remain confidential with the applicant having the fight of a copy to his/report. The medical examination report shall be used only for determining the fitness of the seafarer for work and enhancing health care.</p>
            </div>
            <div class="ms hal_p_sec_3">
                <h6 class="ms hal_p_3_title_2">DETAILS OF MEDICAL EXAMINATION:</h6>
                <p>(To be completed by examining physician; alternatively the examining physician may attach a form similar or identical to the model provided – Medical Exam Form).</p>
            </div>


            <div class="ms_6 ms_6 ms_foot ms_foot_8">
                <div class="ms_6 ms_6 ms_foot_row_1">
                    <div class="ms_6 ms_6 ms_foot_1 s_float">
                        Rev 00
                    </div>
                    <div class="ms_6 ms_6 ms_foot_1 s_float">
                        Date: 01-Apr-2018
                    </div>
                    <div class="ms_6 ms_6 ms_foot_1 s_float">
                        Page 1 of 9
                    </div>
                </div>
                <div class="ms_6 ms_6 ms_foot_row_2 clear">
                    Master copy available with DPA. Controlled copies are in distribution as per distribution list
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js" integrity="sha512-YSdqvJoZr83hj76AIVdOcvLWYMWzy6sJyIMic2aQz5kh2bPTd9dzY3NtdeEAzPp/PhgZqr4aJObB3ym/vsItMg==" crossorigin="anonymous"></script>
</body>
</html>
