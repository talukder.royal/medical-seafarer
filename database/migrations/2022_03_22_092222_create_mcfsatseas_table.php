<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMcfsatseasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mcfsatseas', function (Blueprint $table) {
            $table->id();
            $table->string('surname')->nullable();
            $table->string('given_name')->nullable();
            $table->date('dob')->nullable();
            $table->string('passport_no')->nullable();
            $table->tinyInteger('po_applied_for')->nullable();
            $table->string('gender')->nullable();
            $table->string('cdc_no')->nullable();
            $table->string('avatar')->nullable();
            $table->boolean('hearing_sight_color')->nullable();
            $table->boolean('seafarer_is_suffering')->nullable();
            $table->boolean('visual_aid')->nullable();
            $table->boolean('regular_medication')->nullable();
            $table->boolean('fit_for_duty')->nullable();
            $table->boolean('fit_for_duty_restrictions')->nullable();
            $table->tinyInteger('performed_duty')->nullable();
            $table->date('issued_date')->nullable();
            $table->date('valid_till')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mcfsatseas');
    }
}
