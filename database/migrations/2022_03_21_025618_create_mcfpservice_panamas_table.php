<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMcfpservicePanamasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mcfpservice_panamas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pfrmeofseafarers_id')
                ->constrained('pfrmeofseafarers')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('surname')->nullable();
            $table->string('given_name')->nullable();
            $table->tinyInteger('examination_as_duty')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('mailing_address')->nullable();
            $table->string('right_eye')->nullable();
            $table->string('left_eye')->nullable();
            $table->string('with_g_right_eye')->nullable();
            $table->string('with_g_left_eye')->nullable();
            $table->string('hering_right_ear')->nullable();
            $table->string('hering_left_ear')->nullable();
            $table->boolean('color_test_type')->nullable();
            $table->string('test_yellow')->nullable();
            $table->string('test_red')->nullable();
            $table->string('test_green')->nullable();
            $table->string('test_blue')->nullable();
            $table->boolean('confirmation')->nullable();
            $table->boolean('hearing_meets')->nullable();
            $table->boolean('not_applicable')->nullable();
            $table->boolean('hearing_sat')->nullable();
            $table->boolean('visual_meets')->nullable();
            $table->boolean('color_vision')->nullable();
            $table->date('date_of_vision_test')->nullable();
            $table->boolean('watch_keep')->nullable();
            $table->boolean('vision_standard')->nullable();
            $table->boolean('is_applicant_suffering')->nullable();
            $table->boolean('medications')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mcfpservice_panamas');
    }
}
