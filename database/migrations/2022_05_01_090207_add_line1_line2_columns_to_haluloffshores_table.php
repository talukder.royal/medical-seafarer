<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLine1Line2ColumnsToHaluloffshoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('haluloffshores', function (Blueprint $table) {
            $table->boolean('fit')->after('communicable_disease')->nullable();
            $table->boolean('with')->after('fit')->nullable();
            $table->string('line1')->after('with')->nullable();
            $table->string('line2')->after('line1')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('haluloffshores', function (Blueprint $table) {
            //
        });
    }
}
