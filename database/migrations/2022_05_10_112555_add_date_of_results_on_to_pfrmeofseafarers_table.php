<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateOfResultsOnToPfrmeofseafarersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pfrmeofseafarers', function (Blueprint $table) {
            $table->string('date_of_results_on')->after('date_of_performed_on')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pfrmeofseafarers', function (Blueprint $table) {
            //
        });
    }
}
