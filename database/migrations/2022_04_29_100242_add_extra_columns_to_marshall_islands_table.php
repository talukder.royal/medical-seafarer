<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraColumnsToMarshallIslandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marshall_islands', function (Blueprint $table) {
           $table->boolean('fit')->after('communicable_disease')->nullable();
            $table->boolean('with')->after('fit')->nullable();
            $table->boolean('cook')->after('with')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marshall_islands', function (Blueprint $table) {
            //
        });
    }
}
