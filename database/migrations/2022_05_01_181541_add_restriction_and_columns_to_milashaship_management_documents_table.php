<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRestrictionAndColumnsToMilashashipManagementDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('milashaship_management_documents', function (Blueprint $table) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('milashaship_management_documents', function (Blueprint $table) {
            $table->boolean('fit')->after('communicable_disease')->nullable();
            $table->boolean('with')->after('fit')->nullable();
            $table->string('restriction')->after('with')->nullable();
        });
    }
}
