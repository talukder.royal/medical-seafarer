<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBelgiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('belgia', function (Blueprint $table) {
            $table->id();
            $table->string('last_name');
            $table->string('first_name');
            $table->date('dob');
            $table->string('place_of_birth');
            $table->string('gender')->nullable();
            $table->string('function_on_board')->nullable();
            $table->string('nationality')->nullable();
            $table->string('identification_number')->nullable();
            $table->string('passport_number')->nullable();
            $table->string('number_of_seaman_book')->nullable();
            $table->boolean('documents_checked')->nullable();

            $table->string('service_watch_b')->nullable();
            $table->string('service_watch_m')->nullable();
            $table->string('seafarer_w_watch_services')->nullable();
            $table->string('remainig_seafarers')->nullable();
            $table->string('sight_s_watch')->nullable();
            $table->string('sight_s_watch_mach')->nullable();
            $table->string('sight_remaing_sea')->nullable();
            $table->boolean('required_namely')->nullable();
            $table->boolean('color_blindness')->nullable();
            $table->date('color_v_test')->nullable();
            $table->string('validity_18_plus')->nullable();
            $table->string('validity_18_under')->nullable();
            $table->string('validity_other')->nullable();
            $table->boolean('restrictive_condition')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('person_suffering')->nullable();
            $table->date('date_of_exam')->nullable();
            $table->date('date_of_validity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('belgia');
    }
}
