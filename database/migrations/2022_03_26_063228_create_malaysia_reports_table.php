<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMalaysiaReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('malaysia_reports', function (Blueprint $table) {
            $table->id();
            $table->foreignId('malaysia_id')
                ->constrained('malaysias')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->string('hearing_right_ear')->nullable();
            $table->string('hearing_left_ear')->nullable();
            $table->string('eye_sight_right')->nullable();
            $table->string('eye_sight_left')->nullable();
            $table->string('eye_right_with_aid')->nullable();
            $table->string('eye_left_with_aid')->nullable();
            $table->string('color_vision')->nullable();
            $table->string('blood_pressure')->nullable();
            $table->string('urinalysis')->nullable();
            $table->string('sugar')->nullable();
            $table->string('pulse')->nullable();
            $table->string('chest_x_ray')->nullable();
            $table->string('x_ray_number')->nullable();
            $table->string('ecg')->nullable();
            $table->boolean('infectios')->nullable();
            $table->string('infectios_remarks')->nullable();
            $table->boolean('malignant')->nullable();
            $table->string('malignant_remarks')->nullable();
            $table->boolean('endocrine')->nullable();
            $table->string('endocrine_remarks')->nullable();
            $table->boolean('disease_blood')->nullable();
            $table->string('disease_blood_remarks')->nullable();
            $table->boolean('mental_disorder')->nullable();
            $table->string('mental_disorder_remarks')->nullable();
            $table->boolean('central_nervious')->nullable();
            $table->string('central_nervious_rm')->nullable();
            $table->boolean('cardio_vascular')->nullable();
            $table->string('cardio_vascular_remarks')->nullable();
            $table->boolean('rs_system')->nullable();
            $table->string('rs_system_remarks')->nullable();
            $table->boolean('digestiv_sm')->nullable();
            $table->string('digestiv_sm_remarks')->nullable();
            $table->boolean('genito_sm')->nullable();
            $table->string('genito_sm_remarks')->nullable();
            $table->boolean('pregnancy')->nullable();
            $table->string('pregnancy_rm')->nullable();
            $table->boolean('skin')->nullable();
            $table->string('skin_rm')->nullable();
            $table->boolean('musculo')->nullable();
            $table->string('musculo_rm')->nullable();
            $table->boolean('speech_defects')->nullable();
            $table->string('speech_defects_rm')->nullable();
            $table->boolean('nose_throat')->nullable();
            $table->string('nose_throat_rm')->nullable();
            $table->boolean('eyes')->nullable();
            $table->string('eyes_rm')->nullable();

            $table->boolean('eye_disorders')->nullable();
            $table->string('eye_disorders_rm')->nullable();
            $table->boolean('cataract')->nullable();
            $table->string('cataract_rm')->nullable();
            $table->boolean('monocular_sight')->nullable();
            $table->string('monocular_sight_rm')->nullable();
            $table->boolean('hinder_vision')->nullable();
            $table->string('hinder_vision_rm')->nullable();
            $table->boolean('color_bind')->nullable();
            $table->string('color_bind_rm')->nullable();
            $table->boolean('night_blind')->nullable();
            $table->string('night_blind_rm')->nullable();
            $table->boolean('convulsion')->nullable();
            $table->string('convulsion_rm')->nullable();
            $table->boolean('head_injuris')->nullable();
            $table->string('head_injuris_rm')->nullable();
            $table->boolean('dizziness')->nullable();
            $table->string('dizziness_rm')->nullable();
            $table->boolean('headache_migrane')->nullable();
            $table->string('headache_migrane_rm')->nullable();
            $table->boolean('brain_operation')->nullable();
            $table->string('brain_operation_rm')->nullable();
            $table->boolean('diabetis_trt')->nullable();
            $table->string('diabetis_trt_rm')->nullable();
            $table->boolean('mental_dis')->nullable();
            $table->string('mental_dis_rm')->nullable();
            $table->boolean('alcohol_drugs')->nullable();
            $table->string('alcohol_drugs_rm')->nullable();
            $table->boolean('spinal')->nullable();
            $table->string('spinal_rm')->nullable();
            $table->boolean('heart_pal')->nullable();
            $table->string('heart_pal_rm')->nullable();
            $table->boolean('breathing')->nullable();
            $table->string('breathing_rm')->nullable();
            $table->boolean('deafiness')->nullable();
            $table->string('deafiness_rm')->nullable();
            $table->boolean('kidney_dis')->nullable();
            $table->string('kidney_dis_rm')->nullable();
            $table->boolean('medical_treat')->nullable();
            $table->string('medical_treat_rm')->nullable();
            $table->boolean('disease_stated')->nullable();
            $table->string('disease_stated_rm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('malaysia_reports');
    }
}
