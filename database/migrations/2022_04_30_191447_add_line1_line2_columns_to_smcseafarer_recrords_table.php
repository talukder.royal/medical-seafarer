<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLine1Line2ColumnsToSmcseafarerRecrordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('smcseafarer_recrords', function (Blueprint $table) {
            $table->string('line1')->after('comments')->nullable();
            $table->string('line2')->after('line1')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('smcseafarer_recrords', function (Blueprint $table) {
            //
        });
    }
}
