<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeafarerMadicalCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seafarer_madical_certificates', function (Blueprint $table) {
            $table->id();
            $table->string('firstName');
            $table->string('middleName')->nullable();
            $table->string('lastName');
            $table->date('dob');
            $table->string('gender');
            $table->string('nationality')->nullable();
            $table->string('passport_no')->nullable();
            $table->string('cdc_no')->nullable();
            $table->string('seaman_id')->nullable();
            $table->string('occupation')->nullable();
            $table->string('father_husband_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('mailing_address')->nullable();
            $table->string('house_no')->nullable();
            $table->string('street_no')->nullable();
            $table->string('village')->nullable();
            $table->string('p_o')->nullable();
            $table->string('p_s')->nullable();
            $table->string('district')->nullable();
            $table->boolean('confirmation')->nullable();
            $table->boolean('hearing_standard')->nullable();
            $table->boolean('unaided_statisfactory')->nullable();
            $table->boolean('visual_acutity')->nullable();
            $table->boolean('color_vision_standard')->nullable();
            $table->date('color_vision_test')->nullable();
            $table->boolean('fit_lookout')->nullable();
            $table->boolean('unfit_for_service')->nullable();
            $table->boolean('any_restrictions')->nullable();
            $table->string('duties')->nullable();
            $table->string('location')->nullable();
            $table->string('medical_other')->nullable();
            $table->tinyInteger('fit_restrictions')->nullable();
            $table->date('doe')->nullable();
            $table->date('doexpiry')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seafarer_madical_certificates');
    }
}
