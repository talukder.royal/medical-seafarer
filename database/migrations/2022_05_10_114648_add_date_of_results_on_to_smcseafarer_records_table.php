<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateOfResultsOnToSmcseafarerRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('smcseafarer_recrords', function (Blueprint $table) {
            $table->string('date_of_results_on')->after('date_of_performed_on')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('smcseafarer_records', function (Blueprint $table) {
            //
        });
    }
}
