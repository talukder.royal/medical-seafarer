<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormOnesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_ones', function (Blueprint $table) {
            $table->id();
            $table->string('seafarers_name');
            $table->string('gender');
            $table->date('date_of_birth');
            $table->string('place_of_birth');
            $table->string('nationality');
            $table->string('passport_no')->nullable();
            $table->string('dept');
            $table->string('rank')->nullable();
            $table->string('type_of_ship')->nullable();
            $table->string('home_address')->nullable();
            $table->string('routine_duties')->nullable();
            $table->string('trading_area')->nullable();
            $table->boolean('high_blood_pressure')->nullable();
            $table->boolean('eye_problem')->nullable();
            $table->boolean('heart_disease')->nullable();
            $table->boolean('heart_surgery')->nullable();
            $table->boolean('varicose_veins')->nullable();
            $table->boolean('asthma_bronchitis')->nullable();
            $table->boolean('blood_disorder')->nullable();
            $table->boolean('diabetes')->nullable();
            $table->boolean('thyroid_problem')->nullable();
            $table->boolean('digestive_disorder')->nullable();
            $table->boolean('kidney_problem')->nullable();
            $table->boolean('skin_problem')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_ones');
    }
}
