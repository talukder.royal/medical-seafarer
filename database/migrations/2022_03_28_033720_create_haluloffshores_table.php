<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHaluloffshoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('haluloffshores', function (Blueprint $table) {
            $table->id();
            $table->string('surname')->nullable();
            $table->string('given_name')->nullable();
            $table->string('nationality')->nullable();
            $table->string('document_no')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->tinyInteger('examination_as_duty')->nullable();
            $table->string('mailing_address')->nullable();
            $table->boolean('identi_documents')->nullable();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->string('blood_pressure')->nullable();
            $table->string('pulse')->nullable();
            $table->string('respiration')->nullable();
            $table->string('general_appearance')->nullable();
            $table->string('right_eye')->nullable();
            $table->string('left_eye')->nullable();
            $table->string('with_g_right_eye')->nullable();
            $table->string('with_g_left_eye')->nullable();
            $table->string('hering_right_ear')->nullable();
            $table->string('hering_left_ear')->nullable();
            $table->boolean('color_test_type')->nullable();
            $table->tinyInteger('test_normal')->nullable();
            $table->date('date_of_vision_test')->nullable();
            $table->boolean('vision_standard')->nullable();
            $table->string('head_neck')->nullable();
            $table->string('lungs')->nullable();
            $table->string('extremities_upper')->nullable();
            $table->string('heart')->nullable();
            $table->string('speech')->nullable();
            $table->string('extremities_lower')->nullable();
            $table->boolean('vaccinated_requirement')->nullable();
            $table->boolean('is_applicant_suffering')->nullable();
            $table->boolean('is_prescirption_medication')->nullable();
            $table->boolean('communicable_disease')->nullable();
            $table->date('doe')->nullable();
            $table->date('doexpiry')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('haluloffshores');
    }
}
