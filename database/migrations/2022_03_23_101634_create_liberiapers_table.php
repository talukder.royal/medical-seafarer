<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLiberiapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liberiapers', function (Blueprint $table) {
            $table->id();
            $table->string('last_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('midle_initial')->nullable();
            $table->string('gender')->nullable();
            $table->string('national_id')->nullable();
            $table->string('document_no')->nullable();
            $table->date('dob')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->tinyInteger('examination_as_duty')->nullable();
            $table->longText('mailing_address')->nullable();
            $table->boolean('identi_documents')->nullable();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->string('blood_pressure')->nullable();
            $table->string('pulse')->nullable();
            $table->string('respiration')->nullable();
            $table->string('general_appearance')->nullable();
            $table->string('right_eye')->nullable();
            $table->string('left_eye')->nullable();
            $table->string('with_g_right_eye')->nullable();
            $table->string('with_g_left_eye')->nullable();
            $table->boolean('color_vision')->nullable();
            $table->string('hearing_right_ear')->nullable();
            $table->string('hearing_left_ear')->nullable();
            $table->boolean('color_test_type')->nullable();
            $table->date('date_of_vision_test')->nullable();
            $table->tinyInteger('test_normal')->nullable();
            $table->string('head_neck')->nullable();
            $table->string('lungs')->nullable();
            $table->string('extremities_upper')->nullable();
            $table->string('heart')->nullable();
            $table->string('speech')->nullable();
            $table->string('extremities_lower')->nullable();
            $table->boolean('is_applicant_suffering')->nullable();
            $table->date('date_of_exam')->nullable();
            $table->date('date_of_expiration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liberiapers');
    }
}
