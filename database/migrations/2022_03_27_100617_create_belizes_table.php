<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBelizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('belizes', function (Blueprint $table) {
            $table->id();
            $table->string('last_name');
            $table->string('given_name');
            $table->date('dob');
            $table->string('place_of_birth')->nullable();
            $table->longText('mailing_add')->nullable();
            $table->string('gender')->nullable();
            $table->string('position_board')->nullable();
            $table->boolean('confirmation')->nullable();
            $table->boolean('hearing_standard')->nullable();
            $table->boolean('unaided_statisfactory')->nullable();
            $table->boolean('visual_acutity')->nullable();
            $table->boolean('color_vision_standard')->nullable();
            $table->date('color_vision_test')->nullable();
            $table->boolean('vision_standard')->nullable();
            $table->boolean('able_watch')->nullable();
            $table->boolean('prescript_medication')->nullable();
            $table->boolean('unfit_for_service')->nullable();
            $table->date('doe')->nullable();
            $table->date('doexpiry')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('belizes');
    }
}
