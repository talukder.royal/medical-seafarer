<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraColumnsToBelizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('belizes', function (Blueprint $table) {
            $table->boolean('fit')->after('unfit_for_service')->nullable();
            $table->boolean('with')->after('fit')->nullable();
            $table->string('line1')->after('with')->nullable();
            $table->string('line2')->after('line1')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('belize', function (Blueprint $table) {
            //
        });
    }
}
