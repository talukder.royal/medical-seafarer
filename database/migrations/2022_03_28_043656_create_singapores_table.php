<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSingaporesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('singapores', function (Blueprint $table) {
            $table->id();
            $table->foreignId('rmes_form_id')
                ->constrained('rmes_forms')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->boolean('confirmation')->nullable();
            $table->boolean('hearing_standard')->nullable();
            $table->boolean('unaided_statisfactory')->nullable();
            $table->boolean('visual_acutity')->nullable();
            $table->boolean('color_vision_standard')->nullable();
            $table->date('color_vision_test')->nullable();
            $table->boolean('fit_lookout')->nullable();
            $table->boolean('unfit_for_service')->nullable();
            $table->boolean('any_restrictions')->nullable();
            $table->longText('restrinctions_des')->nullable();
            $table->date('doe')->nullable();
            $table->date('doexpiry')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('singapores');
    }
}
