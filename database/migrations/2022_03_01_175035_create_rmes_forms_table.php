<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmesFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rmes_forms', function (Blueprint $table) {
            $table->id();
            //first page
            $table->string('seafarers_name');
            $table->string('gender');
            $table->date('date_of_birth');
            $table->string('place_of_birth');
            $table->string('nationality');
            $table->string('passport_no')->nullable();
            $table->string('dept');
            $table->string('rank')->nullable();
            $table->string('type_of_ship')->nullable();
            $table->string('home_address')->nullable();
            $table->string('routine_duties')->nullable();
            $table->string('trading_area')->nullable();
            $table->boolean('high_blood_pressure')->nullable();
            $table->boolean('eye_problem')->nullable();
            $table->boolean('heart_disease')->nullable();
            $table->boolean('heart_surgery')->nullable();
            $table->boolean('varicose_veins')->nullable();
            $table->boolean('asthma_bronchitis')->nullable();
            $table->boolean('blood_disorder')->nullable();
            $table->boolean('diabetes')->nullable();
            $table->boolean('thyroid_problem')->nullable();
            $table->boolean('digestive_disorder')->nullable();
            $table->boolean('kidney_problem')->nullable();
            $table->boolean('skin_problem')->nullable();
            $table->boolean('allergies')->nullable();
            $table->boolean('infectious_diseases')->nullable();
            $table->boolean('hernia')->nullable();
            $table->boolean('genital_disorder')->nullable();
            $table->boolean('pregnancy')->nullable();
            $table->boolean('sleep_problem')->nullable();
            $table->boolean('smoke_alcohol_drug')->nullable();
            $table->boolean('operation')->nullable();
            $table->boolean('epilesy_seizures')->nullable();
            $table->boolean('dizziness')->nullable();
            $table->boolean('loss_of_consciousness')->nullable();
            $table->boolean('psychiatric_problem')->nullable();
            $table->boolean('depression')->nullable();
            $table->boolean('attempted_suicide')->nullable();
            $table->boolean('loss_of_memory')->nullable();
            $table->boolean('balance_problem')->nullable();
            $table->boolean('severe_headache')->nullable();
            $table->boolean('hearing_throat_problem')->nullable();
            $table->boolean('restricted_mobility')->nullable();
            $table->boolean('joint_problem')->nullable();
            $table->boolean('amputation')->nullable();
            $table->boolean('fracture')->nullable();
            $table->longText('provide_details_1')->nullable();
            //page2

            $table->boolean('signed_off_sick')->nullable();
            $table->boolean('hospitalized')->nullable();
            $table->boolean('declared_for_sea_duty')->nullable();
            $table->boolean('medical_certificate_revoked')->nullable();
            $table->boolean('medical_problems')->nullable();
            $table->boolean('feel_healthy')->nullable();
            $table->boolean('allergic_medication')->nullable();
            $table->boolean('prescription_medication')->nullable();
            $table->longText('provide_details')->nullable();

            //page3
            $table->boolean('contact_lenses')->nullable();
            $table->string('lense_type')->nullable();
            $table->string('purpose')->nullable();
            $table->string('right_eye_distant')->nullable();
            $table->string('left_eye_distant')->nullable();
            $table->string('binocular_eye_distant')->nullable();
            $table->string('right_eye_near')->nullable();
            $table->string('left_eye_near')->nullable();
            $table->string('binocular_eye_near')->nullable();
            $table->string('aided_right_eye_distant')->nullable();
            $table->string('aided_left_eye_distant')->nullable();
            $table->string('aided_binocular_eye_distant')->nullable();
            $table->string('aided_right_eye_near')->nullable();
            $table->string('aided_left_eye_near')->nullable();
            $table->string('aided_binocular_eye_near')->nullable();
            $table->boolean('visual_normal')->nullable();
            $table->boolean('visual_defective')->nullable();
            $table->tinyInteger('color_vision')->nullable();
            $table->string('right_ear_500_hz')->nullable();
            $table->string('right_ear_1000_hz')->nullable();
            $table->string('right_ear_2000_hz')->nullable();
            $table->string('right_ear_3000_hz')->nullable();
            $table->string('left_ear_500_hz')->nullable();
            $table->string('left_ear_1000_hz')->nullable();
            $table->string('left_ear_2000_hz')->nullable();
            $table->string('left_ear_3000_hz')->nullable();
            $table->string('right_ear_normal')->nullable();
            $table->string('right_ear_whisper')->nullable();
            $table->string('left_ear_normal')->nullable();
            $table->string('left_ear_whisper')->nullable();
            $table->string('clinical_height')->nullable();
            $table->string('clinical_weight')->nullable();
            $table->string('clinical_pulse_rate')->nullable();
            $table->string('clinical_rhythm')->nullable();
            $table->string('clinical_b_pressure')->nullable();
            $table->string('clinical_diastolic')->nullable();
            $table->string('urinalysis_glucose')->nullable();
            $table->string('urinalysis_protin')->nullable();
            $table->string('urinalysis_blood')->nullable();

            //page4
            $table->boolean('head')->nullable();
            $table->boolean('sinus_nose_throat')->nullable();
            $table->boolean('mouth_teeth')->nullable();
            $table->boolean('ears')->nullable();
            $table->boolean('tympanic_member')->nullable();
            $table->boolean('eyes')->nullable();
            $table->boolean('ophthalmoscopy')->nullable();
            $table->boolean('pupils')->nullable();
            $table->boolean('eye_environment')->nullable();
            $table->boolean('lungs_chest')->nullable();
            $table->boolean('breast_examination')->nullable();
            $table->boolean('heart')->nullable();
            $table->boolean('skin')->nullable();
            $table->boolean('varicose_vein')->nullable();
            $table->boolean('vascular_pulse')->nullable();
            $table->boolean('abdomen_viscera')->nullable();
            $table->boolean('hernia_2')->nullable();
            $table->boolean('anus')->nullable();
            $table->boolean('g_u_system')->nullable();
            $table->boolean('upper_lower_ext')->nullable();
            $table->boolean('spine')->nullable();
            $table->boolean('neurologic')->nullable();
            $table->boolean('psychiatric')->nullable();
            $table->boolean('general_appearance')->nullable();
            $table->boolean('chest_xray')->nullable();
            $table->date('date_of_performed_on')->nullable();
            $table->date('date_of_results_on')->nullable();
            $table->string('d_test')->nullable();
            $table->string('d_result')->nullable();
            $table->boolean('fit_for_lookout_duty')->nullable();
            $table->boolean('visual_aid')->nullable();
            $table->boolean('fit_desk')->nullable();
            $table->boolean('fit_engine')->nullable();
            $table->boolean('fit_catering')->nullable();
            $table->boolean('fit_other')->nullable();
            $table->boolean('with_restrictions')->nullable();
            $table->longText('description_restrictions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rmes_forms');
    }
}
