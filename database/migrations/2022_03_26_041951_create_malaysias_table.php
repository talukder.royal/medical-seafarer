<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMalaysiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('malaysias', function (Blueprint $table) {
            $table->id();
            $table->string('family_name');
            $table->string('given_name');
            $table->date('dob')->nullable();
            $table->string('gender')->nullable();
            $table->string('nationality')->nullable();
            $table->string('passport_no')->nullable();
            $table->string('seaman_id')->nullable();
            $table->string('occupation')->nullable();
            $table->boolean('confirmation')->nullable();
            $table->boolean('hearing_standard')->nullable();
            $table->boolean('unaided_statisfactory')->nullable();
            $table->boolean('visual_acutity')->nullable();
            $table->boolean('color_vision_standard')->nullable();
            $table->date('color_vision_test')->nullable();
            $table->boolean('fit_lookout')->nullable();
            $table->boolean('unfit_for_service')->nullable();
            $table->boolean('any_restrictions')->nullable();
            $table->longText('limit_restriction')->nullable();
            $table->date('doe')->nullable();
            $table->date('doexpiry')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('malaysias');
    }
}
