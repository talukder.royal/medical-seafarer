<?php

namespace App\Http\Controllers;

use App\Models\MarshallIsland;
use App\Traits\MarshallIslandFormDataValidation;
use Illuminate\Http\Request;
use PDF;

class MarshallIslandController extends Controller
{
    use MarshallIslandFormDataValidation;

    /**
     * show forms
    */
    public function index()
    {
        $forms = MarshallIsland::filter()->latest()->paginate(15);
        return view('marshall_islands.index', compact('forms'));
    }

    /**
     * Create form
     */
    public function create()
    {
        return view('marshall_islands.create');
    }

    /**
     * store data of the form
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $data = $this->validatedData($request);
        $data['name'] = $request->given_name. ' ' .$request->surname;
        MarshallIsland::create($data);

        return redirect()->back()->with('success', 'The form has been saved successfully!');
    }

    /**
     * Edit form
     * @param MarshallIsland $form
     * @return mixed
     *
     */
    public function edit(MarshallIsland $form)
    {
        return view('marshall_islands.edit', compact('form'));
    }

    /**
     * update form
     * @param Request $request
     * @param MarshallIsland $form
     * @return mixed
     *
     */
    public function update(Request $request, MarshallIsland $form)
    {
        $data = $this->validatedData($request);
        $data['name'] = $request->given_name. ' ' .$request->surname;
        $form->update($data);

        return redirect()->back()->with('success', 'The form has been updated successfully!');
    }

    /**
     * Delete form
     * @param MarshallIsland $form
     * @return mixed
     *
     */
    public function delete(MarshallIsland $form)
    {
        $form->delete();
        return redirect()->back()->with('success', 'The form has been deleted successfully!');
    }

    /**
     * Download form
     * @param MarshallIsland $form
     * @return void
     *
     */
    public function download(MarshallIsland $form)
    {
        $pdf = PDF::chunkLoadView(
            '<html-separator/>',
            'marshall_islands.download',
            ['form' => $form],
            [],
            [
                'title'      => 'Marshall Island',
                'margin_top' => 10,
                'margin_right' => 5,
                'margin_left' => 5,
                'margin_bottom' => 10,
            ]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
