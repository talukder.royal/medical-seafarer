<?php

namespace App\Http\Controllers;

use App\Models\Belize;
use App\Traits\BelizeFormDataValidation;
use Illuminate\Http\Request;
use PDF;

class BelizeController extends Controller
{
    use BelizeFormDataValidation;

    /**
     *show forms
     */
    public function index()
    {
        $forms = Belize::filter()->latest()->paginate(15);
        return view('belize.index', compact('forms'));
    }

    /**
     *Create form
     */
    public function create()
    {
        return view('belize.create');
    }

    /**
     *Store date of Belize form
     * @param Request $request
     * @return mixed
     */

    public function store(Request $request)
    {
        $data = $this->validatedData($request);
        $data['name'] = $request->given_name. ' ' .$request->last_name;

        Belize::create($data);

        return redirect()->back()->with('success', 'This form has been saved successfully!');
    }

    /**
     *Edit form
     * @param Belize $form
     */
    public function edit(Belize $form)
    {
        return view('belize.edit', compact('form'));
    }

    /**
     *Update form
     * @param Request $request
     * @param Belize $form
     */
    public function update(Request $request, Belize $form)
    {
        $data = $this->validatedData($request);
        $data['name'] = $request->given_name. ' ' .$request->last_name;


        $form->update($data);

        return redirect()->back()->with('success', 'This form has been updated successfully!');
    }

    /**
     *Delete form
     * @param Belize $form
     */
    public function delete(Belize $form)
    {
        $form->delete();
        return redirect()->back()->with('success', 'The form has been deleted successfully!');
    }

    /**
     *Download form
     * @param Belize $form
     */
    public function download(Belize $form)
    {
        $pdf = PDF::chunkLoadView(
            '<html-separator/>',
            'belize.download',
            ['form' => $form],
            [],
            [
                'title'      => 'Belize',
                'margin_top' => 10,
                'margin_right' => 5,
                'margin_left' => 5,
                'margin_bottom' => 10,
            ]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
