<?php

namespace App\Http\Controllers;

use App\Models\SmcseafarerRecrord;
use App\Traits\SmcseafarerRecordFormDataValidation;
use Illuminate\Http\Request;
use PDF;

class SmcseafarerRecrordController extends Controller
{
    use SmcseafarerRecordFormDataValidation;

    /**
     * protects all methods
     * from unauthenticated
     *user
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     *  showing all
     *  SmecseafarerRecordForm
     *
     */
    public function index()
    {
        $forms = SmcseafarerRecrord::filter()->latest()->paginate(15);
        return view('sm_certificate.seafarers_records.index', compact('forms'));
    }

    /**
     * Creating
     * page1 of SmecseafarerRecordForm
     *
     */
    public function page1_create()
    {
       return view('sm_certificate.seafarers_records.pages.page_1');
    }

    /**
     * Creating
     * page2 of SmecseafarerRecordForm
     *
     */
    public function page2_create(SmcseafarerRecrord $form)
    {
        return view('sm_certificate.seafarers_records.pages.page_2', compact('form'));
    }

    /**
     * creating
     * page3 of SmecseafarerRecordForm
     *
     */
    public function page3_create(SmcseafarerRecrord $form)
    {
        return view('sm_certificate.seafarers_records.pages.page_3', compact('form'));
    }

    /**
     * Creating
     * page4 of SmecseafarerRecordForm
     *
     */
    public function page4_create(SmcseafarerRecrord $form)
    {
        return view('sm_certificate.seafarers_records.pages.page_4', compact('form'));
    }

    /**
     * Storing data for
     * page1 of SmecseafarerRecordForm
     * @param Request $request
     */

    public function page1_store(Request $request)
    {
        $form =  SmcseafarerRecrord::create($this->validatedDataForPage1($request));
        return redirect()->route('smcseafarer_recrord.page2_create', $form->id)->with('success', 'Successfully Saved!');
    }

    /**
     * Storing data for
     * page2 of SmecseafarerRecordForm
     * @param Request $request
     */

    public function page2_store(Request $request, SmcseafarerRecrord $form)
    {
        $form->update($this->validatedDataForPage2($request));
        return redirect()->route('smcseafarer_recrord.page3_create', $form->id)->with('success', 'Successfully saved');
    }

    /**
     * Storing data for
     * page3 of SmecseafarerRecordForm
     * @param Request $request
     */
    public function page3_store(Request $request, SmcseafarerRecrord  $form)
    {
        $form->update($this->validatedDataForPage3($request));
        return redirect()->route('smcseafarer_recrord.page4_create', $form->id)->with('success', 'Successfully Saved!');
    }

    /**
     * Storing data for
     * page4 of SmecseafarerRecordForm
     * @param Request $request
     */
    public function page4_store(Request $request, SmcseafarerRecrord $form)
    {
        $form->update($this->validatedDataForPage4($request));
        return view('sm_certificate.seafarers_records.edit.page1_edit', compact('form'))->with('success', 'All data successfully given');
    }

    /**
     * Editing
     * page1 of SmecseafarerRecordForm
     *
     */
    public function page1_edit(SmcseafarerRecrord $form)
    {
        return view('sm_certificate.seafarers_records.edit.page1_edit', compact('form'));
    }


    /**
     * Updating
     * page1 of SmecseafarerRecordForm
     *
     */
    public function page1_update(Request $request, SmcseafarerRecrord $form)
    {
        $form->update($this->validatedDataForPage1($request));
        return redirect()->route('smcseafarer_recrord.page2_create', $form->id)->with('success', 'Successfully saved!');
    }

    /**
     * Deleting
     * page1 of SmecseafarerRecordForm
     *
     */
    public function delete(SmcseafarerRecrord $form)
    {
        $form->delete();
        return redirect()->back()->with('success', 'Successfully deleted!');
    }

    /**
     * Downloading
     * page1 of SmecseafarerRecordForm
     *
     */
    public function download(SmcseafarerRecrord $form)
    {
        $pdf = PDF::chunkLoadView('<html-separator/>', 'sm_certificate.seafarers_records.download', ['form' => $form]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
