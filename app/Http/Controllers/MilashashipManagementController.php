<?php

namespace App\Http\Controllers;

use App\Models\MilashashipManagement;
use App\Traits\MilashashipManagementDataValidation;
use Illuminate\Http\Request;
use PDF;

class MilashashipManagementController extends Controller
{
    use MilashashipManagementDataValidation;

    /**
     * showing all forms
     */
    public function index()
    {
        $forms = MilashashipManagement::filter()->latest()->paginate(15);
        return view('milasha_ship_management.confidential_form.index', compact('forms'));
    }

    /**
     * Create page1 of the form
     */
    public function page1_create()
    {
        return view('milasha_ship_management.confidential_form.pages.page1');
    }

    /**
     * Create page2 of the form
     */
    public function page2_create(MilashashipManagement $form)
    {
        return view('milasha_ship_management.confidential_form.pages.page2', compact('form'));
    }

    /**
     * Create page3 of the form
     */
    public function page3_create(MilashashipManagement $form)
    {
        return view('milasha_ship_management.confidential_form.pages.page3', compact('form'));
    }

    /**
     * Create page3 of the form
     */
    public function page4_create(MilashashipManagement $form)
    {
        return view('milasha_ship_management.confidential_form.pages.page4', compact('form'));
    }

    /**
     * Storing data of page1
     * @param Request $request
     * @param  MilahshipManagement $form
     *
     */
    public function page1_store(Request $request)
    {
        $form = MilashashipManagement::create($this->dataValidationForPage1($request));
        return redirect()->route('milashaship_management.page2_create', $form->id)->with('success', 'Successfully Saved!');
    }

    /**
     * Storing data of page2
     * @param Request $request
     * @param  MilahshipManagement $form
     *
     */
    public function page2_store(Request $request, MilashashipManagement $form)
    {
        $form->update($this->dataValidationForPage2($request));
        return redirect()->route('milashaship_management.page3_create', $form->id)->with('success', 'Successfully saved');
    }

    /**
     * Storing data of page3
     * @param Request $request
     * @param  MilahshipManagement $form
     *
     */
    public function page3_store(Request $request, MilashashipManagement  $form)
    {
        $form->update($this->dataValidationForPage3($request));
        return redirect()->route('milashaship_management.page4_create', $form->id)->with('success', 'Successfully Saved!');
    }

    /**
     * Storing data of page4
     * @param Request $request
     * @param  MilahshipManagement $form
     *
     */
    public function page4_store(Request $request, MilashashipManagement $form)
    {
        $form->update($this->dataValidationForPage4($request));
        return redirect()->route('milashaship_management.page1_edit', $form->id)->with('success', 'All data successfully given');
    }

    /**
     * Updating pag1
     * @param Request $request
     */
    public function page1_update(Request $request, MilashashipManagement $form)
    {
        $form->update($this->dataValidationForPage1($request));

        return redirect()->route('milashaship_management.page2_create', $form->id)->with('success', 'Successfully saved!');
    }

    /**
     * Editing  page1
     * @param Request $request
     */
    public function page1_edit(MilashashipManagement $form)
    {
        return view('milasha_ship_management.confidential_form.edit.page1_edit', compact('form'));
    }

    /**
     * Deleting form
     * @param Request $request
     */
    public function delete(MilashashipManagement $form)
    {
        $form->delete();
        $form->documents()->delete();
        return redirect()->back()->with('success', 'Deleted successfully');
    }

    /**
     * Downloading form
     * @param Request $request
     */
    public function download(Request $request, MilashashipManagement $form)
    {
        $pdf = PDF::chunkLoadView('<html-separator/>', 'milasha_ship_management.confidential_form.download', ['form' => $form]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
