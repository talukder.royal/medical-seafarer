<?php

namespace App\Http\Controllers;

use App\Models\Pfrmeofseafarers;
use App\Traits\PfrmeofseafarersFormDataValidation;
use Illuminate\Http\Request;
use PDF;

class PfrmeofseafarersController extends Controller
{
    use PfrmeofseafarersFormDataValidation;

    /**
     * Showing all forms
     */
    public function index()
    {
        $forms = Pfrmeofseafarers::filter()->latest()->paginate(15);

        return view('pfrmeofseafarers.index', compact('forms'));
    }

    /**
     * creating first page
     * of the form
     */
    public function page1_create()
    {
        return view('pfrmeofseafarers.pages.page1');
    }


    /**
     * creating 2nd page
     * of the form
     * @param Pfrmeofseafarers $form
     */
    public function page2_create(Pfrmeofseafarers $form)
    {
        return view('pfrmeofseafarers.pages.page2', compact('form'));
    }

    /**
     * creating 3rd page
     * of the form
     * @param Pfrmeofseafarers $form
     */
    public function page3_create(Pfrmeofseafarers $form)
    {
        return view('pfrmeofseafarers.pages.page3', compact('form'));
    }

    /**
     * creating first page
     * of the form
     * @param Pfrmeofseafarers $form
     */
    public function page4_create(Pfrmeofseafarers $form)
    {
        return view('pfrmeofseafarers.pages.page4', compact('form'));
    }

    /**
     * storing data for first page
     * @param Request $request
     * @param Pfrmeofseafarers $form
     */
    public function page1_store(Request $request)
    {
        $form = Pfrmeofseafarers::create($this->validatedDataForPage1($request));
        return redirect()->route('pfrmeofseafarer.page2_create', $form->id)->with('success', 'Successfully Saved!');
    }

    /**
     * storing data for 2nd page
     * @param Request $request
     * @param Pfrmeofseafarers $form
     */
    public function page2_store(Request $request, Pfrmeofseafarers $form)
    {
        $form->update($this->validatedDataForPage2($request));

        return redirect()->route('pfrmeofseafarer.page3_create', $form->id)->with('success', 'Successfully saved');
    }


    /**
     * storing data for third page
     * @param Request $request
     * @param Pfrmeofseafarers $form
     */
    public function page3_store(Request $request, Pfrmeofseafarers  $form)
    {
        $form->update($this->validatedDataForPage3($request));

        return redirect()->route('pfrmeofseafarer.page4_create', $form->id)->with('success', 'Successfully Saved!');
    }

    /**
     * storing data for fourth page
     * @param Request $request
     * @param Pfrmeofseafarers $form
     */
    public function page4_store(Request $request, Pfrmeofseafarers $form)
    {
        $form->update($this->validatedDataForPage4($request));
        return view('pfrmeofseafarers.edit.page1_edit', compact('form'))->with('success', 'All data successfully given');
    }


    /**
     * editing data for  first page
     * @param Pfrmeofseafarers $form
     */
    public function page1_edit(Pfrmeofseafarers $form)
    {
        return view('pfrmeofseafarers.edit.page1_edit', compact('form'));
    }

    /**
     * updating data for first page
     * @param Request $request
     * @param Pfrmeofseafarers $form
     */
    public function page1_update(Request $request, Pfrmeofseafarers $form)
    {
        $form->update($this->validatedDataForPage1($request));
        return redirect()->route('pfrmeofseafarer.page2_create', $form->id)->with('success', 'Successfully saved!');
    }

    /**
     * Deleting form
     * @param Pfrmeofseafarers $form
     */
    public function delete(Pfrmeofseafarers $form)
    {
        $form->delete();
        return redirect()->back()->with('success', 'Successfully deleted!');
    }

    /**
     * Downloding form
     * @param Pfrmeofseafarers $form
     */
    public function download(Pfrmeofseafarers $form)
    {
        $pdf = PDF::chunkLoadView('<html-separator/>', 'pfrmeofseafarers.download', ['form' => $form]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
