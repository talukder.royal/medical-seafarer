<?php

namespace App\Http\Controllers;

use App\Models\Mcfpsboard;
use App\Traits\McfpsboardFormDataValidation;
use Illuminate\Http\Request;
use PDF;

class McfpsboardController extends Controller
{
    use McfpsboardFormDataValidation;

    /**
     *
     * show all forms
    */
    public function index()
    {
        $forms = Mcfpsboard::filter()->latest()->paginate(15);
        return view('mcfps_board.index', compact('forms'));
    }

    /**
     *
     * Create form
     */
    public function create()
    {
        return view('mcfps_board.create');
    }

    /**
     * Store data
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $data = $this->validatedData($request);
        $data['name'] =  $request->given_name. ' ' .$request->surname;

        Mcfpsboard::create($data);

        return redirect()->back()->with('success', 'Data has been saved successfully!');
    }

    /**
     * Edit form
     * @param Mcfpsboard $form
     * @return mixed
     */
    public function edit(Mcfpsboard $form)
    {
        return view('mcfps_board.edit', compact('form'));
    }

    /**
     * Update form
     * @param Mcfpsboard $form
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request, Mcfpsboard $form)
    {
        $data = $this->validatedData($request);
        $data['name'] =  $request->given_name. ' ' .$request->surname;

        $form->update($data);

        return redirect()->back()->with('success', 'Data has been updated successfully!');
    }

    /**
     * Delete form
     * @param Mcfpsboard $form
     * @return mixed
     */
    public function delete(Mcfpsboard $form)
    {
        $form->delete();
        return redirect()->back()->with('success', 'The form has been deleted successfully!');
    }

    /**
     * Download form
     * @param Mcfpsboard $form
     * @return void
     */
    public function download(Mcfpsboard $form)
    {
        $pdf = PDF::chunkLoadView(
            '<html-separator/>',
            'mcfps_board.download',
            ['form' => $form],
            [],
            [
                'title'      => 'Service On Board',
                'margin_top' => 10,
                'margin_right' => 5,
                'margin_left' => 5,
                'margin_bottom' => 10,
            ]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
