<?php

namespace App\Http\Controllers;

use App\Models\RmesForm;
use App\Models\Singapore;
use App\Traits\SingaporeFormDataValidation;
use Illuminate\Http\Request;

class SingaporeController extends Controller
{
    use SingaporeFormDataValidation;

    /**
     * SingaPore is related to
     * RmesForm basically singopore form is the
     * second part of RmesForm This is attached with
     * RmesForm
    */

    public function create(RmesForm $form)
    {
        return view('singapore_form.singapore.create', compact('form'));
    }

    /**
     * storing data of singapore form
     * @param Request $request
     * @param RmesForm $form
    */
    public function store(Request  $request, RmesForm $form)
    {
        $form->certificate()->create($this->validatedData($request));
        return redirect()->back()->with('success', 'The form has been saved successfully!');
    }

    /**
     *Editing data of Singapore form
     * @param RmesForm $form
    */
    public function edit(RmesForm $form)
    {
        return view('singapore_form.singapore.edit', compact('form'));
    }

    /**
     *Updating data of Singapore form
     * @param Request $request
     * @param RmesForm $form
     */
    public function update(Request  $request, RmesForm $form)
    {
        $form->certificate()->update($this->validatedData($request));
        return redirect()->back()->with('success', 'The form has been updated successfully!');
    }

}
