<?php

namespace App\Http\Controllers;

use App\Models\Mcfsatsea;
use App\Traits\McfsatseaFormDataValidation;
use Illuminate\Http\Request;
use PDF;

class McfsatseaController extends Controller
{
    use McfsatseaFormDataValidation;

    /**
     * Show all form
     */
    public function index()
    {
        $forms = Mcfsatsea::filter()->latest()->paginate(15);
        return view('mcfsatsea.index', compact('forms'));
    }

    /**
     *
     *Create Form
     */
   public function create()
   {
       return view('mcfsatsea.create');
   }

   /**
    *Storing data for Mcfsatsea Form
    *@param Request $request
    */
    public function store(Request $request)
    {
        $validatedData = $this->validatedData($request);

        $imageName = time().$request->surname. '.' .$request->avatar->extension();
        $request->avatar->move(public_path('avatars'), $imageName);

        $validatedData['name'] = $request->given_name. ' ' .$request->surname;
        $validatedData['avatar'] = $imageName;

        Mcfsatsea::create($validatedData);

        return redirect()->back()->with('success', 'Data has been stored successfully!');
    }

    /**
     *Editing data for Mcfsatsea Form
     *@param Mcfsatsea $form
     */
    public function edit(Mcfsatsea $form)
    {
        return view('mcfsatsea.edit', compact('form'));
    }

    /**
     *updating data for Mcfsatsea Form
     *@param Request $request
     * @param Mcfsatsea $form
     */
    public function update(Request $request, Mcfsatsea $form)
    {
        $validatedData = $this->validatedData($request);

        if (!empty($request->avatar)) {
           $imageName = time().$request->surname. '.' .$request->avatar->extension();
           $request->avatar->move(public_path('avatars'), $imageName);
        } else {
           $imageName = $form->avatar;
        }

        $validatedData['name'] = $request->given_name. ' ' .$request->surname;
        $validatedData['avatar'] = $imageName;

        $form->update($validatedData);

        return redirect()->back()->with('success', 'Data has been updated successfully!');
    }

    /**
     *deleting  Mcfsatsea Form
     *@param Mcfsatsea $form
     */
    public function delete(Mcfsatsea $form)
    {
        $form->delete();
        return redirect()->back()->with('success', 'Form has been deleted successfully!');
    }

    /**
     *Download Mcfsatsea Form
     *@param Mcfsatsea $form
     */
    public function download(Mcfsatsea $form)
    {
        $pdf = PDF::chunkLoadView('<html-separator/>', 'mcfsatsea.download', ['form' => $form]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
