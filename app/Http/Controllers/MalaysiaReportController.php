<?php

namespace App\Http\Controllers;

use App\Models\Malaysia;
use App\Traits\MalaysiaReportDataValidation;
use Illuminate\Http\Request;

class MalaysiaReportController extends Controller
{
    use MalaysiaReportDataValidation;

    /**
     * Create Malaysia form
     */
    public function create(Malaysia $form)
    {
        return view('malaysias.malaysia_reports.create', compact('form'));
    }

    /**
     * store data of malysiaReport
     * @param Request $request
     * @param Malaysia $form
    */
    public function store(Request $request, Malaysia $form)
    {
        $form->report()->create($this->validatedData($request));

        return redirect()->back()->with('success', 'The form has been created successfully!');
    }

    /**
     * Edit Malaysia form
     */
    public function edit(Malaysia $form)
    {
        return view('malaysias.malaysia_reports.edit', compact('form'));
    }

    /**
     * update data of malysiaReport
     * @param Request $request
     * @param Malaysia $form
     */
    public function update(Request $request, Malaysia $form)
    {
        $form->report()->update($this->validatedData($request));

        return redirect()->back()->with('success', 'The form has been updated successfully!');
    }
}
