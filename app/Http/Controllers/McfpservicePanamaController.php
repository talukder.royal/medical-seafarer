<?php

namespace App\Http\Controllers;

use App\Models\Pfrmeofseafarers;
use App\Traits\McfpservicePanamaDataValidation;
use Illuminate\Http\Request;

class McfpservicePanamaController extends Controller
{
    use McfpservicePanamaDataValidation;

    public function create(Pfrmeofseafarers $form)
    {
        return view('pfrmeofseafarers.mcfpservice_panama.certificate', compact('form'));
    }

    /**
     * Store data for Panama form
     * @param Request $request
     * @param Pfrmeofseafarers $form
    */
    public function store(Request $request, Pfrmeofseafarers $form)
    {
          $form->certificate()->create($this->validatedData($request));
          return redirect()->back()->with('success', 'Data has been saved successfully!');
    }

    /**
     * Edit data for Panama form
     *
     * @param Pfrmeofseafarers $form
     */
    public function edit(Pfrmeofseafarers $form)
    {
        return view('pfrmeofseafarers.mcfpservice_panama.edit', compact('form'));
    }

    /**
     * Store data for Panama form
     * @param Request $request
     * @param Pfrmeofseafarers $form
     */
    public function update(Request $request, Pfrmeofseafarers $form)
    {
        $form->certificate()->update($this->validatedData($request));
        return redirect()->back()->with('success', 'Data has been updated successfully!');
    }

}
