<?php

namespace App\Http\Controllers;

use App\Traits\RmesFormDataValidation;
use App\Models\RmesForm;
use Illuminate\Http\Request;
use PDF;


class RmesFormController extends Controller
{
    use RmesFormDataValidation;

    /**
     * only authenticated user can access methods
     * except allForms
     * Protect actions
     */
    public function __construct( )
    {
        $this->middleware('auth')->except('allForms');
    }

    public function create()
    {
        return view('singapore_form.form_1.page_1');
    }

    public function page2_create(RmesForm $form)
    {
        return view('singapore_form.form_1.page_2', compact('form'));
    }

    public function page3_create(RmesForm $form)
    {
        return view('singapore_form.form_1.page_3', compact('form'));
    }

    public function page4_create(RmesForm $form)
    {
        return view('singapore_form.form_1.page_4', compact('form'));
    }

    /**
     * storing data for first page
     * of RmesForm
     *
     * @param Request $request
     *
     */

    public function store(Request $request)
    {
        $form = RmesForm::create($this->validatedData($request));
        return redirect()->route('form_page2.create', compact('form'))->with('success', 'Successfully saved!');
    }


    /**
     * updating data for first page
     * of RmesForm
     *
     * @param Request $request
     *
     */

    public function page1_update(Request $request, RmesForm $form)
    {
        $form ->update($this->validatedDataForPage1($request));
        return redirect()->route('form_page2.create', $form->id)->with('success', 'Successfully updated!');
    }


    /**
     * inserting data for second page
     * of RmesForm
     *
     * @param Request $request
     *
     */

    public function page2_store(Request $request, RmesForm $form)
    {
        $form->update($this->validatedDataForPage2($request));
        return redirect()->route('form_page3.create', $form->id);
    }


    /**
     * inserting data for third page
     * of RmesForm
     *
     * @param Request $request
     *
     */

    public function page3_store(Request $request, RmesForm  $form)
    {
        $form->update($this->validatedDataForPage3($request));
        return redirect()->route('form_page4.create', $form->id);
    }

    /**
     * inserting data for fourth page
     * of RmesForm
     *
     * @param Request $request
     *
     */

    public function page4_store(Request $request, RmesForm $form)
    {
        $form->update($this->validatedDataForPage4($request));
        return view('singapore_form.form_1.edit.page_1_edit', compact('form'))->with('success', 'All data successfully saved');
    }

    /**
     *
     * Showing all forms
     */

    public function allForms()
    {
        $forms = RmesForm::filter()->latest()->paginate(15);
       return view('singapore_form.form_1.all_forms', compact('forms'));
    }

    public function showPage1(RmesForm $form)
    {
        return view('singapore_form.form_1.pages.page1', compact('form'));
    }

    public function showPage2(RmesForm $form)
    {
        return view('singapore_form.form_1.pages.page2', compact('form'));
    }

    public function showPage3(RmesForm $form)
    {
        return view('singapore_form.form_1.pages.page3', compact('form'));
    }

    public function showPage4(RmesForm $form)
    {
        return view('singapore_form.form_1.pages.page4', compact('form'));
    }

    public function showPage5(RmesForm $form)
    {
        return view('singapore_form.form_1.pages.page5', compact('form'));
    }

    /**
     * Editing page1
     * @param RmesForm $form
     */

    public function editPage1(RmesForm $form)
    {
        return view('singapore_form.form_1.edit.page_1_edit', compact('form'));
    }

    /**
     * Deleting form RmesForm
     * @param  RmesForm $form
    */

    public function delete(RmesForm $form)
    {
        $form->delete();
        $form->certificate()->delete();
        return redirect()->back()->with('success', 'The from is deleted successfully!');
    }

    /**
     * Downloading form RmesForm
     * @param  RmesForm $form
     */

    public function download(RmesForm $form)
    {
        $pdf = PDF::chunkLoadView('<html-separator/>', 'singapore_form.form_1.pages.pageOne', ['form' => $form]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
