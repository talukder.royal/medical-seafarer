<?php

namespace App\Http\Controllers;

use App\Models\Belgium;
use App\Traits\BelgiumFormDataValidation;
use Illuminate\Http\Request;
use PDF;

class BelgiumController extends Controller
{
    use BelgiumFormDataValidation;

    /**
     * show all BelgiumForms
     * @param Request $request
     */
    public function index()
    {
        $forms = Belgium::filter()->latest()->paginate(15);
        return view('belgium.index', compact('forms'));
    }

    /**
     * create Belgium form
     *
     */
    public function create()
    {
        return view('belgium.create');
    }
    /**
     * store data of BelgiumForm
     * @param Request $request
     */
    public function store(Request $request)
    {
        $data = $this->validatedData($request);
        $data['name'] =  $request->first_name. ' ' . $request->first_name;

        Belgium::create($data);
        return redirect()->back()->with('success', 'Data has been saved successfully!');
    }

    /**
     * edit of BelgiumForm
     * @param Belgium $form
     */
    public function edit(Belgium $form)
    {
        return view('belgium.edit', compact('form'));
    }

    /**
     * update data of BelgiumForm
     * @param Request $request
     */
    public function update(Request $request, Belgium $form)
    {
        $data = $this->validatedData($request);
        $data['name'] =  $request->first_name. ' ' . $request->first_name;

        $form->update($data);
        return redirect()->back()->with('success', 'Data has been updated successfully!');
    }

    /**
     * delete BelgiumForm
     * @param Belgium $form
     */
    public function delete(Belgium $form)
    {
        $form->delete();
        return redirect()->back()->with('success', 'The form has been deleted successfully!');
    }

    /**
     * Download BelgiumForm
     * @param Belgium $form
     */
    public function download(Belgium $form)
    {
        $pdf = PDF::chunkLoadView('<html-separator/>', 'belgium.download', ['form' => $form]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
