<?php

namespace App\Http\Controllers;

use App\Models\Liberiaper;
use App\Traits\LiberiaperFormDataValidation;
use Illuminate\Http\Request;
use PDF;

class LiberiaperController extends Controller
{
    use LiberiaperFormDataValidation;

    /**
     * Show  all forms
     */
    public function index()
    {
        $forms = Liberiaper::filter()->latest()->paginate(15);
        return view('liberiapers.index', compact('forms'));
    }

    /**
     * Create form
     */
    public function create()
    {
        return view('liberiapers.create');
    }


    /**
     * Store data
     * @param Request $request
    */
    public function store(Request $request)
    {
        Liberiaper::create($this->validatedData($request));
        return redirect()->back()->with('success', 'Data has been saved successfully!');
    }

    /**
     * Edit form
     * @param Liberiaper $form
     */
    public function edit(Liberiaper $form)
    {
        return view('liberiapers.edit', compact('form'));
    }

    /**
     * update data
     * @param Request $request
     * @param Liberiaper $form
     */
    public function update(Request $request, Liberiaper $form)
    {
        $form->update($this->validatedData($request));
        return redirect()->back()->with('success', 'Data has been updated successfully!');
    }

    /**
     * Delete form
     * @param Liberiaper $form
     */
    public function delete(Liberiaper $form)
    {
        $form->delete();
        return redirect()->back()->with('success', 'The form has been deleted successfully!');
    }

    /**
     * Store data
     * @param Liberiaper $form
     */
    public function download(Liberiaper $form)
    {
        $pdf = PDF::chunkLoadView('<html-separator/>', 'liberiapers.download', ['form' => $form]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
