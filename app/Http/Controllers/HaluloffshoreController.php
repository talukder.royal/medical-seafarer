<?php

namespace App\Http\Controllers;

use App\Models\Haluloffshore;
use App\Traits\HaluloffshoreFormDataValidation;
use Illuminate\Http\Request;
use PDF;

class HaluloffshoreController extends Controller
{
    use HaluloffshoreFormDataValidation;

    /**
     *Show all forms
     */
    public function index()
    {
        $forms = Haluloffshore::filter()->latest()->paginate(15);
        return view('halul_offshore.index', compact('forms'));
    }

    /**
     *Create form
     */
    public function create()
    {
        return view('halul_offshore.create');
    }

    /**
     *Store data of the form
     * @param Request $request
     */
    public function store(Request $request)
    {
        $data = $this->validatedData($request);
        $data['name'] = $request->given_name . ' ' . $request->surname;

        Haluloffshore::create($data);

        return redirect()->back()->with('success', 'Data has been saved successfully');
    }

    /**
     *Edit data of the form
     * @param Request $request
     */
    public function edit(Haluloffshore $form)
    {
        return view('halul_offshore.edit', compact('form'));
    }

    /**
     *Update data of the form
     * @param Request $request
     * @param Haluloffshore $form
     * @return mixed
     */
    public function update(Request $request, Haluloffshore $form)
    {
        $data = $this->validatedData($request);
        $data['name'] = $request->given_name . ' ' . $request->surname;

        $form->update($data);

        return redirect()->back()->with('success', 'Data has been updated successfully');
    }

    /**
     *Delete the form
     * @param Request $request
     * @return mixed
     */
    public function delete(Haluloffshore $form)
    {
        $form->delete();
        return redirect()->back()->with('success', 'The form has been deleted successfully!');
    }

    /**
     *Download the form
     * @param Haluloffshore $form
     */
    public function download(Haluloffshore $form)
    {
        $pdf = PDF::chunkLoadView(
            '<html-separator/>',
            'halul_offshore.download',
            ['form' => $form],
            [],
            [
                'title'      => 'Marshall Island',
                'format' => 'A4',
                'margin_top' => 5,
                'margin_right' =>10,
                'margin_left' => 10,
                'margin_bottom' => 10,
            ]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
