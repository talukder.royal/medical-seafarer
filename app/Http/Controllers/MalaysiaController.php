<?php

namespace App\Http\Controllers;

use App\Models\Malaysia;
use App\Traits\MalaysiaFormDataValidation;
use Illuminate\Http\Request;
use PDF;

class MalaysiaController extends Controller
{
    use MalaysiaFormDataValidation;

    /**
     * show all forms
    */
    public function index()
    {
        $forms = Malaysia::filter()->latest()->paginate(15);
        return view('malaysias.index', compact('forms'));
    }

    /**
     * Create form
     */
   public function create()
   {
       return view('malaysias.create');
   }

    /**
     * store data
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $data = $this->validatedData($request);
        $data['name'] = $request->given_name. ' ' .$request->family_name;
        Malaysia::create($data);

        return redirect()->back()->with('success', 'Data has been stored successfully!');
    }

    /**
     * @param Malaysia $form
     * @return mixed
    */
    public function edit(Malaysia $form)
    {
        return view('malaysias.edit', compact('form'));
    }

    /**
     * @param Request $request
     * @param Malaysia $form
     * @return mixed
     */
    public function update(Request $request, Malaysia $form)
    {
        $data = $this->validatedData($request);
        $data['name'] = $request->given_name. ' ' .$request->family_name;
        $form->update($data);

        return redirect()->back()->with('success', 'Data has been stored successfully!');
    }

    /**
     * delete form
     * @param Malaysia $form
     * @return mixed
     */
    public function delete(Malaysia $form)
    {
       $form->delete();
        if (!empty($form->report)) {
            $form->roport->delete();
        }
       return redirect()->back()->with('success', 'This form has been deleted successfully!');
    }

    /**
     * Download Form
     * @param Malaysia $form
     * @return mixed
     */
    public function download(Malaysia $form)
    {
        $pdf = PDF::chunkLoadView(
            '<html-separator/>',
            'malaysias.download',
            ['form' => $form],
            [],
            [
                'title'      => 'Malaysia',
                'margin_top' => 10,
                'margin_right' => 5,
                'margin_left' => 5,
                'margin_bottom' => 10,
            ]);
        $pdf->stream($form->id.'document.pdf');
        exit();
    }
}
