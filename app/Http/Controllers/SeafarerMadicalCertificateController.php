<?php

namespace App\Http\Controllers;

use App\Models\SeafarerMadicalCertificate;
use App\Traits\SeafarerMadicalCertificateDataValidation;
use Illuminate\Http\Request;
use PDF;

class SeafarerMadicalCertificateController extends Controller
{
    use SeafarerMadicalCertificateDataValidation;

    /**
     * show forms for
     * SeafarerMedicalCertificate
     *
     * */
    public function index()
    {
        $smCertificates = SeafarerMadicalCertificate::filter()->latest()->paginate(15);
        return view('sm_certificate.index', compact('smCertificates'));
    }

    /**
     * create form for
     * SeafarerMedicalCertificate
     *
     * */
    public function create()
    {
        return view('sm_certificate.create');
    }

    /**
     * Storing data for SefarerMadicalCertificate
     *
     * @param Request $request
     *
     * */
    public function store(Request $request)
    {
        $data  = $this->validatedData($request);
        $data['name'] = $request->firstName. ' ' .$request->middleName. ' ' .$request->lastName;
        SeafarerMadicalCertificate::create($data);

        return redirect()->back()->with('success', 'Saved successfully');
    }

    /**
     * Edit form for SefarerMedicalCertificate
     * @param Request $request
     *
     * */
    public function edit(SeafarerMadicalCertificate $smCertificate)
    {
        return view('sm_certificate.edit', compact('smCertificate'));
    }

    /**
     * Update form for SefarerMedicalCertificate
     * @param Request $request
     *
     * */

    public function update(Request $request, SeafarerMadicalCertificate $smCertificate)
    {
        $smCertificate->update($this->validatedData($request));
        return redirect()->back()->with('success', 'Successfully edited!');
    }

    /**
     * Delete form for SefarerMedicalCertificate
     * @param Request $request
     *
     * */
    public function delete(SeafarerMadicalCertificate $smCertificate)
    {
        $smCertificate->delete();
        return redirect()->back()->with('success', 'Successfully deleted!');
    }

    /**
     * Download SefarerMedicalCertificate
     * @param Request $request
     *
     * */
    public function download(SeafarerMadicalCertificate $smCertificate)
    {
        $pdf = PDF::chunkLoadView('<html-separator/>', 'sm_certificate.download', ['smCertificate' => $smCertificate]);
        $pdf->stream($smCertificate->id.'document.pdf');
        exit();
    }
}
