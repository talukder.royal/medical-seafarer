<?php

namespace App\Http\Controllers;

use App\Models\MilashashipManagement;
use App\Models\MilashashipManagementDocument;
use App\Traits\MilashashipManagementDocumentDataValidation;
use Illuminate\Http\Request;

class MilashashipManagementDocumentController extends Controller
{
    use MilashashipManagementDocumentDataValidation;

    /**
     * Create MilashaShipManagement
    * @param  MilashashipManagement $form
    *
    */

   public function page1_create(MilashashipManagement $form)
   {
       return view('milasha_ship_management.confidential_document.pages.page1', compact('form'));
   }

    /**
     * Editing MilashaShipManagementDocument
     * @param  MilashashipManagement $form
     *
     */
    public function edit(MilashashipManagement $form)
    {
        return view('milasha_ship_management.confidential_document.edit', compact('form'));
    }

    /**
     * Create MilashaShipManagementDocument
     * @param  MilashashipManagement $form
     * @param Request $request
     */
    public function store(Request $request, MilashashipManagement $form)
    {
        $validatedData = $this->validatedData($request);
        $validatedData['mila_management_id'] = $form->id;

        MilashashipManagementDocument::create($validatedData);

        return redirect()->back()->with('success', 'Data has been saved successfully');
    }

    /**
     * Update MilashaShipManagementDocument
     * @param  MilashashipManagement $form
     * @param Request $request
     */
    public function update(Request $request, MilashashipManagement $form)
    {
        $form->documents()->update($this->validatedData($request));

        return redirect()->back()->with('success', 'Data has been saved successfully');
    }
}
