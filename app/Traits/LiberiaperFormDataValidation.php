<?php
namespace App\Traits;

trait LiberiaperFormDataValidation
{

    public function validatedData($request)
    {
        return $request->validate([
            'last_name' => 'required|string',
            'first_name' => 'required|string',
            'midle_initial' => 'required|string',
            'gender' => 'required|string',
            'national_id' => 'required|string',
            'document_no' => 'required|string',
            'dob' => 'required|date',
            'city' => 'required|string',
            'country' => 'required|string',
            'examination_as_duty' => 'required|numeric',
            'mailing_address' => 'required|string',
            'identi_documents' => 'required|boolean',
            'height' => 'required|string',
            'weight' => 'required|string',
            'blood_pressure' => 'required|string',
            'pulse' => 'required|string',
            'respiration' => 'required|string',
            'general_appearance' => 'required|string',
            'right_eye' => 'required|string',
            'left_eye' => 'required|string',
            'with_g_right_eye' => 'required|string',
            'hearing_right_ear' => 'required|string',
            'with_g_left_eye' => 'required|string',
            'hearing_left_ear' => 'required|string',
            'color_vision' => 'nullable|boolean',
            'date_of_vision_test' => 'nullable|date',
            'color_test_type' => 'nullable|boolean',
            'test_normal' => 'nullable|numeric',
            'head_neck' => 'nullable|string',
            'lungs' => 'nullable|string',
            'extremities_upper' => 'nullable|string',
            'extremities_lower' => 'nullable|string',
            'heart' => 'nullable|string',
            'speech' =>'nullable|string',
            'is_applicant_suffering' => 'nullable|boolean',
            'fit' => 'nullable|boolean',
            'with' => 'nullable|boolean',
            'line1' => 'nullable|string',
            'line2' => 'nullable|string',
            'date_of_exam' => 'nullable|date',
            'date_of_expiration' => 'nullable|date',
        ]);
    }
}
