<?php
namespace App\Traits;

trait SingaporeFormDataValidation {

    public function validatedData($request)
    {
        return $request->validate([
            'confirmation' => 'nullable|boolean',
            'hearing_standard' => 'nullable|boolean',
            'unaided_statisfactory' => 'nullable|boolean',
            'visual_acutity' => 'nullable|boolean',
            'color_vision_standard' => 'nullable|boolean',
            'color_vision_test' => 'nullable|date',
            'fit_lookout' => 'nullable|boolean',
            'unfit_for_service' => 'nullable|boolean',
            'any_restrictions' => 'nullable|boolean',
            'restrinctions_des' => 'nullable|boolean',
            'doe' => 'nullable|date',
            'doexpiry' => 'nullable|date',
        ]);
    }
}
