<?php
namespace App\Traits;

trait McfsatseaFormDataValidation {

    public function validatedData($request)
    {
        return $request->validate([
            'surname' => 'required|string',
            'given_name' => 'required|string',
            'dob' => 'required|date',
            'passport_no' => 'required|string',
            'po_applied_for' => 'required|numeric',
            'gender' => 'required|string',
            'cdc_no' => 'required|string',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'hearing_sight_color' => 'required|boolean',
            'seafarer_is_suffering' => 'required|boolean',
            'visual_aid' => 'required|boolean',
            'regular_medication' => 'required|boolean',
            'fit_for_duty' => 'required|boolean',
            'fit_for_duty_restrictions' => 'required|boolean',
            'performed_duty' => 'required|numeric',
            'issued_date' => 'required|date',
            'valid_till' => 'required|date',
        ]);
    }
}
