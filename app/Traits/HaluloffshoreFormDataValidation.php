<?php
namespace App\Traits;

trait HaluloffshoreFormDataValidation {

    public function validatedData($request)
    {
        return $request->validate([
            'surname' => 'required|string',
            'given_name' => 'required|string',
            'nationality' => 'required|string',
            'gender' => 'required|string',
            'dob' => 'required|date',
            'document_no' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'examination_as_duty' => 'required|numeric',
            'mailing_address' => 'required|string',
            'identi_documents' => 'required|boolean',
            'height' => 'required|string',
            'weight' => 'required|string',
            'blood_pressure' => 'required|string',
            'pulse' => 'required|string',
            'respiration' => 'required|string',
            'general_appearance' => 'required|string',
            'right_eye' => 'required|string',
            'left_eye' => 'required|string',
            'with_g_right_eye' => 'nullable|string',
            'with_g_left_eye' => 'nullable|string',
            'hering_right_ear' => 'nullable|string',
            'hering_left_ear' => 'nullable|string',
            'color_test_type' => 'nullable|boolean',
            'test_normal' => 'nullable|numeric',
            'date_of_vision_test' => 'nullable|date',
            'vision_standard' => 'nullable|boolean',
            'head_neck' => 'nullable|string',
            'lungs' => 'nullable|string',
            'extremities_upper' => 'nullable|string',
            'heart' => 'nullable|string',
            'speech' => 'nullable|string',
            'extremities_lower' => 'nullable|string',
            'vaccinated_requirement' => 'nullable|string',
            'is_applicant_suffering' => 'nullable|boolean',
            'is_prescirption_medication' => 'nullable|boolean',
            'communicable_disease' => 'nullable|boolean',
            'fit' => 'nullable|boolean',
            'with' => 'nullable|boolean',
            'line1' => 'nullable|min:3|max:1000',
            'line2' => 'nullable|min:3|max:1000',
            'doe' => 'nullable|date',
            'doexpiry' => 'nullable|date',
        ]);
    }
}
