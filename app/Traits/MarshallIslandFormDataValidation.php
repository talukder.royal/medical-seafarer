<?php

namespace App\Traits;

trait MarshallIslandFormDataValidation
{
    public function validatedData($request)
    {
        return $request->validate([
            'surname' => 'required|string',
            'given_name' => 'required|string',
            'dob' => 'required|date',
            'city' => 'required|string',
            'country' => 'required|string',
            'gender' => 'required|string',
            'examination_as_duty' => 'required|numeric',
            'mailing_address' => 'required|min:3|max:1000',
            'identi_documents' => 'nullable|boolean',
            'height' => 'nullable|string',
            'weight' => 'nullable|string',
            'blood_pressure' => 'nullable|string',
            'pulse' => 'nullable|string',
            'respiration' => 'nullable|string',
            'general_appearance' => 'nullable|string',
            'right_eye' => 'nullable|string',
            'left_eye' => 'nullable|string',
            'with_g_right_eye' => 'nullable|string',
            'with_g_left_eye' => 'nullable|string',
            'hering_right_ear' => 'nullable|string',
            'hering_left_ear' => 'nullable|string',
            'color_test_type' => 'nullable|boolean',
            'test_normal' => 'nullable|boolean',
            'vision_standard' => 'nullable|boolean',
            'head_neck' => 'nullable|string',
            'lungs' => 'nullable|string',
            'extremities_upper' => 'nullable|string',
            'heart' => 'nullable|string',
            'speech' => 'nullable|string',
            'extremities_lower' => 'nullable|string',
            'vaccinated_requirement' => 'nullable|boolean',
            'is_applicant_suffering' => 'nullable|boolean',
            'is_prescirption_medication' => 'nullable|boolean',
            'communicable_disease' => 'nullable|boolean',
            'cook' => 'nullable|string',
            'with' => 'nullable|boolean',
            'fit' => 'nullable|boolean',
            'restriction' => 'nullable|boolean',
            'doe' => 'required|date',
            'doex' => 'required|date',
        ]);
    }
}
