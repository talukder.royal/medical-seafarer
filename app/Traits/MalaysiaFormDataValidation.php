<?php
namespace App\Traits;

trait MalaysiaFormDataValidation
{
    /**
     * @param $request
     * @return array
    */
    public function validatedData($request)
    {
        return $request->validate([
            'family_name' => 'required|string',
            'given_name' => 'required|string',
            'dob' => 'required|date',
            'gender' => 'required|string',
            'nationality' => 'required|string',
            'passport_no' => 'nullable|string',
            'seaman_id' => 'nullable|string',
            'occupation' => 'required|string',
            'confirmation' => 'required|boolean',
            'hearing_standard' => 'required|boolean',
            'unaided_statisfactory' => 'required|boolean',
            'visual_acutity' => 'nullable|boolean',
            'color_vision_standard' => 'nullable|boolean',
            'color_vision_test' => 'nullable|date',
            'fit_lookout' => 'nullable|boolean',
            'unfit_for_service' => 'nullable|boolean',
            'any_restrictions' => 'nullable|boolean',
            'limit_restriction' => 'nullable|min:5|max:1000',
            'medical_fitness' => 'nullable|boolean',
            'doe' => 'required|date',
            'doexpiry' => 'required|date',
        ]);
    }
}
