<?php
namespace App\Traits;

trait BelgiumFormDataValidation {

    public function validatedData($request)
    {
        return $request->validate([
            'last_name' => 'required|string',
            'first_name' => 'required|string',
            'dob' => 'required|date',
            'gender' => 'required|string',
            'function_on_board' => 'required|string',
            'place_of_birth' => 'required|string',
            'nationality' => 'required|string',
            'identification_number' => 'nullable|string',
            'passport_number' => 'nullable|string',
            'number_of_seaman_book' => 'nullable|string',
            'documents_checked' => 'nullable|boolean',
            'service_watch_b' => 'nullable|string',
            'service_watch_m' => 'nullable|string',
            'seafarer_w_watch_services' => 'nullable|string',
            'remainig_seafarers' => 'nullable|string',
            'sight_s_watch' => 'nullable|string',
            'sight_s_watch_mach' => 'nullable|string',
            'sight_remaing_sea' => 'nullable|string',
            'required_namely' => 'nullable|boolean',
            'color_blindness' => 'nullable|boolean',
            'color_v_test' => 'nullable|date',
            'validity_18_plus' => 'nullable|string',
            'validity_18_under' => 'nullable|string',
            'validity_other' => 'nullable|string',
            'restrictive_condition' => 'nullable|boolean',
            'description' => 'nullable|min:3|max:1000',
            'person_suffering' => 'nullable|boolean',
            'maritime_inspection' => 'nullable|boolean',
            'date_of_exam' => 'nullable|date',
            'date_of_validity' => 'nullable|date',
        ]);
    }
}
