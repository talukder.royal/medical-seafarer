<?php
namespace App\Traits;

trait SeafarerMadicalCertificateDataValidation {

    /**
     *
     *Data validation for MedicalCertificate
     * @param $request
     * @return array
     */
    public function validatedData($request)
    {
        return $request->validate([
            'firstName' => 'required|string',
            'middleName' => 'required|string',
            'lastName' => 'required|string',
            'dob' => 'required|date',
            'gender' => 'required|string',
            'nationality' => 'required|string',
            'passport_no' => 'required|numeric',
            'cdc_no' => 'required|numeric',
            'seaman_id' => 'required|numeric',
            'occupation' => 'required|string',
            'father_husband_name' => 'required|string',
            'mother_name' => 'required|string',
            'mailing_address' => 'required|string',
            'house_no' => 'required|string',
            'street_no' => 'required|string',
            'village' => 'required|string',
            'p_o' => 'required|string',
            'p_s' => 'required|string',
            'district' => 'required|string',
            'confirmation' => 'nullable|boolean',
            'hearing_standard' => 'nullable|boolean',
            'unaided_statisfactory' => 'required|boolean',
            'visual_acutity' => 'required|boolean',
            'color_vision_standard' => 'nullable|boolean',
            'color_vision_test' => 'nullable|date',
            'fit_lookout' => 'nullable|boolean',
            'unfit_for_service' => 'nullable|boolean',
            'any_restrictions' => 'nullable|boolean',
            'duties' => 'nullable|string',
            'location' => 'nullable|string',
            'medical_other' => 'nullable|string',
            'fit_restrictions' => 'nullable|numeric',
            'doe' => 'required|date',
            'doexpiry' => 'required|date',
        ]);
    }
}
