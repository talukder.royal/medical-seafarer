<?php
namespace App\Traits;

trait McfpsboardFormDataValidation
{
    public function validatedData($request)
    {
        return $request->validate([
            'surname' => 'required|string',
            'given_name' => 'required|string',
            'gender' => 'required|string',
            'dob' => 'required|date',
            'examination_as_duty' => 'required|numeric',
            'city' => 'required|string',
            'country' => 'required|string',
            'mailing_address' => 'required|string',
            'right_eye' => 'required|string',
            'left_eye' => 'required|string',
            'with_g_right_eye' => 'required|string',
            'with_g_left_eye' => 'required|string',
            'hering_right_ear' => 'required|string',
            'hering_left_ear' => 'required|string',
            'color_test_type' => 'required|boolean',
            'test_yellow' => 'nullable|string',
            'test_red' => 'nullable|string',
            'test_green' => 'nullable|string',
            'test_blue' => 'nullable|string',
            'confirmation' => 'nullable|boolean',
            'hearing_meets' => 'nullable|boolean',
            'not_applicable' => 'nullable|boolean',
            'hearing_sat' => 'nullable|boolean',
            'visual_meets' => 'nullable|boolean',
            'color_vision' => 'nullable|boolean',
            'date_of_vision_test' => 'nullable|date',
            'watch_keep' => 'nullable|boolean',
            'vision_standard' => 'nullable|boolean',
            'is_applicant_suffering' => 'nullable|boolean',
            'medications' => 'nullable|boolean',
            'fit' => 'nullable|boolean',
            'with' => 'nullable|boolean',
            'line1' => 'nullable|string',
            'line2' => 'nullable|string',
            'doe' => 'nullable|date',
            'doexpiry' => 'nullable|date',
        ]);
    }
}
