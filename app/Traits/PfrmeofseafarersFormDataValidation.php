<?php
namespace App\Traits;

trait PfrmeofseafarersFormDataValidation {

    /**
     *@param $request
     * @return Array
     */
    public function validatedDataForPage1($request)
    {
        return $request->validate([
            'seafarers_name' => 'required|string',
            'gender' => 'required|string',
            'date_of_birth' => 'required|date',
            'passport_no' => 'required|string',
            'dept' => 'required|string',
            'type_of_ship' => 'required|string',
            'home_address' => 'required|string',
            'routine_duties' => 'required|string',
            'trading_area' => 'required|string',
            'high_blood_pressure' => 'nullable|boolean',
            'eye_problem' => 'nullable|boolean',
            'heart_disease' => 'nullable|boolean',
            'heart_surgery' => 'nullable|boolean',
            'varicose_veins' => 'nullable|boolean',
            'asthma_bronchitis' => 'nullable|boolean',
            'blood_disorder' => 'nullable|boolean',
            'diabetes' => 'nullable|boolean',
            'thyroid_problem' => 'nullable|boolean',
            'digestive_disorder' => 'nullable|boolean',
            'kidney_problem' => 'nullable|boolean',
            'skin_problem' => 'nullable|boolean',
            'allergies' => 'nullable|boolean',
            'infectious_diseases' => 'nullable|boolean',
            'hernia' => 'nullable|boolean',
            'genital_disorder' => 'nullable|boolean',
            'pregnancy' => 'nullable|boolean',
            'sleep_problem' => 'nullable|boolean',
            'smoke_alcohol_drug' => 'nullable|boolean',
            'operation' => 'nullable|boolean',
            'epilesy_seizures' => 'nullable|boolean',
            'dizziness' => 'nullable|boolean',
            'loss_of_consciousness' => 'nullable|boolean',
            'psychiatric_problem' => 'nullable|boolean',
            'depression' => 'nullable|boolean',
            'attempted_suicide' => 'nullable|boolean',
            'loss_of_memory' => 'nullable|boolean',
            'balance_problem' => 'nullable|boolean',
            'severe_headache' => 'nullable|boolean',
            'hearing_throat_problem' => 'nullable|boolean',
            'restricted_mobility' => 'nullable|boolean',
            'joint_problem' => 'nullable|boolean',
            'amputation' => 'nullable|boolean',
            'fracture' => 'nullable|boolean',
            'provide_details_1' => 'nullable|min:3|max:1000',
        ]);
    }

    /**
     *@param  $request
     *@return Array
     */
    public function validatedDataForPage2( $request)
    {
        return $request->validate([
            'signed_off_sick' => 'nullable|boolean',
            'hospitalized' => 'nullable|boolean',
            'declared_for_sea_duty' => 'nullable|boolean',
            'medical_certificate_revoked' => 'nullable|boolean',
            'medical_problems' => 'nullable|boolean',
            'feel_healthy' => 'nullable|boolean',
            'allergic_medication' => 'nullable|boolean',
            'prescription_medication' => 'nullable|boolean',
            'provide_details' => 'nullable|min:3|max:1000',
            'medical_condition' => 'nullable|boolean',
            'comments' => 'nullable|min:3|max:1000',
        ]);
    }


    /**
     *@param  $request
     *@return Array
     */
    public function validatedDataForPage3( $request)
    {
        return $request->validate([
            'contact_lenses' => 'nullable|boolean',
            'lense_type' => 'nullable|string',
            'purpose' => 'nullable|string',
            'right_eye_distant' => 'nullable|string',
            'left_eye_distant' => 'nullable|string',
            'binocular_eye_distant' => 'nullable|string',
            'right_eye_near' => 'nullable|string',
            'left_eye_near' => 'nullable|string',
            'binocular_eye_near' => 'nullable|string',
            'aided_right_eye_distant' => 'nullable|string',
            'aided_left_eye_distant' => 'nullable|string',
            'aided_binocular_eye_distant' => 'nullable|string',
            'aided_right_eye_near' => 'nullable|string',
            'aided_left_eye_near' => 'nullable|string',
            'aided_binocular_eye_near' => 'nullable|string',
            'visual_normal' => 'nullable|boolean',
            'visual_defective' => 'nullable|boolean',
            'color_vision' => 'nullable|numeric',
            'right_ear_500_hz' => 'nullable|string',
            'right_ear_1000_hz' => 'nullable|string',
            'right_ear_2000_hz' => 'nullable|string',
            'right_ear_3000_hz' => 'nullable|string',
            'left_ear_500_hz' => 'nullable|string',
            'left_ear_1000_hz' => 'nullable|string',
            'left_ear_2000_hz' => 'nullable|string',
            'left_ear_3000_hz' => 'nullable|string',
            'right_ear_normal' => 'nullable|string',
            'right_ear_whisper' => 'nullable|string',
            'left_ear_normal' => 'nullable|string',
            'left_ear_whisper' => 'nullable|string',
            'clinical_height' => 'nullable|string',
            'clinical_weight' => 'nullable|string',
            'clinical_pulse_rate' => 'nullable|string',
            'clinical_rhythm' => 'nullable|string',
            'clinical_b_pressure' => 'nullable|string',
            'clinical_diastolic' => 'nullable|string',
            'urinalysis_glucose' => 'nullable|string',
            'urinalysis_protin' => 'nullable|string',
            'blood' => 'nullable|string',
        ]);
    }


    /**
     *@param  $request
     *@return Array
     */
    public function validatedDataForPage4( $request)
    {
        return $request->validate([
            'head' => 'nullable|boolean',
            'sinus_nose_throat' => 'nullable|boolean',
            'mouth_teeth' => 'nullable|boolean',
            'ears' => 'nullable|boolean',
            'tympanic_member' => 'nullable|boolean',
            'eyes' => 'nullable|boolean',
            'ophthalmoscopy' => 'nullable|boolean',
            'pupils' => 'nullable|boolean',
            'eye_environment' => 'nullable|boolean',
            'lungs_chest' => 'nullable|boolean',
            'breast_examination' => 'nullable|boolean',
            'heart' => 'nullable|boolean',
            'skin' => 'nullable|boolean',
            'varicose_vein' => 'nullable|boolean',
            'vascular_pulse' => 'nullable|boolean',
            'abdomen_viscera' => 'nullable|boolean',
            'anus' => 'nullable|boolean',
            'hernia_2' => 'nullable|boolean',
            'g_u_system' => 'nullable|boolean',
            'upper_lower_ext' => 'nullable|boolean',
            'spine' => 'nullable|boolean',
            'neurologic' => 'nullable|boolean',
            'psychiatric' => 'nullable|boolean',
            'general_appearance' => 'nullable|boolean',
            'chest_xray' => 'nullable|boolean',
            'date_of_performed_on' => 'nullable|date',
            'date_of_results_on' => 'nullable|date',
            'd_test' => 'nullable|date',
            'd_result' => 'nullable|date',
            'm_examiners_comment' => 'nullable|string',
            'v_s_recorded' => 'nullable|boolean',
            'fit_for_lookout_duty' => 'nullable|boolean',
            'visual_aid' => 'nullable|boolean',
            'fit_desk' => 'nullable|boolean',
            'fit_engine' => 'nullable|boolean',
            'fit_catering' => 'nullable|boolean',
            'fit_other' => 'nullable|boolean',
            'with_restrictions' => 'nullable|boolean',
            'visual_aid_required' => 'nullable|boolean',
            'a_m_examiner' => 'nullable|string',
            'dofex' => 'nullable|date',
            'dofi' => 'nullable|date',
            'nofmc' => 'nullable|string',
            'description_restrictions' => 'nullable|min:3|max:1000'
        ]);
    }
}
