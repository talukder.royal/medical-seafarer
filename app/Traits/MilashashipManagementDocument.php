<?php
namespace App\Traits;

trait MilashashipManagementDocumentDataValidation {

    public function validatedData($request)
    {
        return $request->validate([
            'surname' => 'required|string',
            'given_name' => 'required|string',
            'nationality' =>  'required|string',
            'document_no' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'examination_as_duty' => 'required|string',
            'mailing_address' => 'required|string',
            'identi_documents' => 'nullable|string',
            'height' => 'nullable|numeric',
            'weight' => 'nullable|numeric',
            'blood_pressure' => 'nullable|string',
            'pulse' => 'nullable|numeric',
            'respiration' => 'nullable|string',
            'general_appearance' => 'nullable|string',
            'right_eye' => 'nullable|string',
            'left_eye' => 'nullable|string',
            'with_g_right_eye' => 'nullable|string',
            'with_g_left_eye' => 'nullable|string',
            'hering_right_ear' => 'nullable|string',
            'hering_left_ear' => 'nullable|string',
            'color_test_type' => 'nullable|boolean',
            'test_normal' => 'nullable|numeric',
            'date_of_vision_test' => 'nullable|date',
            'vision_standard' => 'nullable|string',
            'head_neck' => 'nullable|string',
            'lungs' => 'nullable|string',
            'extremities_upper' => 'nullable|string',
            'heart' => 'nullable|string',
            'speech' => 'nullable|string',
            'extremities_lower' => 'nullable|string',
            'vaccinated_requirement' => 'nullable|boolean',
            'is_applicant_suffering' => 'nullable|boolean',
            'is_prescirption_medication' => 'nullable|boolean',
            'communicable_disease' => 'nullable|boolean',
            'fit' => 'nullable|boolean',
            'with' => 'nullable|boolean',
            'restriction' => 'nullable|string',
        ]);
    }
}
