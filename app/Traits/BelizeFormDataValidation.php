<?php
namespace App\Traits;

trait BelizeFormDataValidation {

    public function validatedData($request)
    {
        return $request->validate([
            'last_name' => 'required|string',
            'given_name' => 'required|string',
            'dob' => 'required|date',
            'place_of_birth' => 'required|string',
            'mailing_add' =>'required|min:3|max:1000',
            'gender' => 'required|string',
            'position_board' => 'required|string',
            'confirmation' => 'required|boolean',
            'hearing_standard' => 'required|boolean',
            'unaided_statisfactory' => 'required|boolean',
            'visual_acutity' => 'required|boolean',
            'color_vision_standard' => 'required|boolean',
            'color_vision_test' => 'nullable|date',
            'vision_standard' => 'nullable|boolean',
            'able_watch' => 'nullable|boolean',
            'prescript_medication' => 'nullable|boolean',
            'unfit_for_service' => 'nullable|boolean',
            'fit' => 'nullable|boolean',
            'with' => 'nullable|boolean',
            'line1' => 'nullable|string',
            'line2' => 'nullable|string',
            'doe' => 'nullable|date',
            'doexpiry' => 'nullable|date',
        ]);
    }
}
