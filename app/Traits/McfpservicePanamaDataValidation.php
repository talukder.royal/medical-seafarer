<?php
namespace App\Traits;

trait McfpservicePanamaDataValidation {

    public function validatedData($request)
    {
        return $request->validate([
            'surname' => 'required|string',
            'given_name' => 'required|string',
            'examination_as_duty' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'mailing_address' => 'required|min:3|max:1000',
            'right_eye' => 'nullable|string',
            'left_eye' => 'nullable|string',
            'with_g_right_eye' => 'nullable|string',
            'with_g_left_eye' => 'nullable|string',
            'hering_right_ear' => 'nullable|string',
            'hering_left_ear' => 'nullable|string',
            'color_test_type' => 'nullable|string',
            'test_yellow' => 'nullable|string',
            'test_red' => 'nullable|string',
            'test_green' => 'nullable|string',
            'test_blue' => 'nullable|string',
            'confirmation' => 'nullable|boolean',
            'hearing_meets' => 'nullable|boolean',
            'not_applicable' => 'nullable|boolean',
            'hearing_sat' => 'nullable|boolean',
            'visual_meets' => 'nullable|boolean',
            'color_vision' => 'nullable|boolean',
            'date_of_vision_test' => 'nullable|boolean',
            'watch_keep' => 'nullable|boolean',
            'vision_standard' => 'nullable|boolean',
            'is_applicant_suffering' => 'nullable|boolean',
            'medications' => 'nullable|boolean',
            'fit' => 'nullable|boolean',
            'with' => 'nullable|boolean',
            'line1' => 'nullable|string',
            'line2' => 'nullable|string',
        ]);
    }
}
