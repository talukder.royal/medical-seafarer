<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MalaysiaReport extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function certificate()
    {
        return $this->belongsTo(Malaysia::class);
    }
}
