<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Belgium extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function scopeFilter(Builder $query)
    {
        $search = request('search');

        return $query->where('id', $search)
            ->orWhere('passport_number', '=', $search)
            ->orWhere('identification_number', '=', $search)
            ->orWhere('number_of_seaman_book', '=', $search)
            ->orWhere('name', 'like', "%{$search}%");
    }
}
