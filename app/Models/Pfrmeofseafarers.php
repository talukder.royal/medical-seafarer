<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pfrmeofseafarers extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function certificate()
    {
        return $this->hasOne(McfpservicePanama::class);
    }

    public function scopeFilter(Builder $query)
    {
        $search = request('search');

        return $query->where('id','=', $search)
            ->orWhere('passport_no', '=', $search)
            ->orWhere('seafarers_name', 'like', "%{$search}%");
    }
}
