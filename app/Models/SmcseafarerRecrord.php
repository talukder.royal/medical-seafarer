<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class SmcseafarerRecrord extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function scopeFilter(Builder $query)
    {
        $search = request('search');

        return $query->where('id','=', $search)
            ->orWhere('passport_no', '=', $search)
            ->orWhere('seafarers_name', 'like', "%{$search}%");
    }
}
