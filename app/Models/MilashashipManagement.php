<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PhpParser\Builder;

class MilashashipManagement extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function documents()
    {
        return $this->hasOne(MilashashipManagementDocument::class, 'mila_management_id' );
    }

    public function scopeFilter(\Illuminate\Database\Eloquent\Builder $query)
    {
        $search = request('search');

        return $query->where('id', '=', $search)
            ->orWhere('passport_no', '=', $search)
            ->orWhere('seafarers_name', 'like', "%{$search}%");
    }

}
