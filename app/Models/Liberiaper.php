<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Liberiaper extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function scopeFilter(Builder $query)
    {
        $search = request('search');

        return $query->where('id','=',  $search)
            ->orWhere('national_id', '=', $search)
            ->orWhere('document_no', '=', $search)
            ->orWhere('name', 'like', "%{$search}%");
    }
}
