<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MarshallIsland extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function scopeFilter(Builder $query)
    {
        $search = request('search');

        return $query->where('id', '=' , $search)
            ->orWhere('name', 'like', "%{$search}%");
    }
}
